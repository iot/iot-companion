# IoT Companion Framework

This repository contains experimental software to allow communication with IoT devices on local communication interfaces (Bluetooth LE, USB, NFC) using a RPC framework.
The RPC framework is based on protocol buffers, and a Python code-generator helps generating client and server-side code.

##  Licenses

### Outbound

The content of this repository is made available under the [Apache 2 License](LICENSE.txt) (SPDX: Apache-2.0).

### Inbound

This project is provided for reference and is therefore not accepting contributions at this stage.

### Third-party code

The following third-party software is used under their respective licenses:
* [Boost](https://www.boost.org/): [Boost Software License](https://www.boost.org/users/license.html)
* [Djinni](https://github.com/dropbox/djinni): [Apache 2 License](https://github.com/dropbox/djinni/blob/master/LICENSE)
* [Google Protocol Buffers](https://developers.google.com/protocol-buffers): [BSD 3-Clause](https://github.com/protocolbuffers/protobuf/blob/master/LICENSE)
* [Google Test](https://github.com/google/googletest): [BSD 3-Clause](https://github.com/google/googletest/blob/master/LICENSE)
* [NanoPb](https://github.com/nanopb/nanopb): [Zlib License](https://github.com/nanopb/nanopb/blob/master/LICENSE.txt)
* [Mbed OS](https://github.com/ARMmbed/mbed-os): [Apache 2 License](https://github.com/ARMmbed/mbed-os/blob/master/LICENSE-apache-2.0.txt)

##  Supported platforms

Devices:
* Mbed OS

Host:
* macOS
* iOS
* Android (WIP)
* Linux (WIP)

Transports:
* BLE
* USB (WIP)
* NFC (WIP)

##  Setting up

* Retrieve and build all dependencies from the `dependencies/` folder using the provided scripts:
    * `get_boost.sh`, `clone_gtest.sh` and `get_protobuf.sh`
    * `build_all_dependencies.sh` (or invoke individual scripts)
* Install the code generator from the `tools/generator` directory: `python3 setup.py install`
* Generate Djinni bindings: `generate-djinni-bindings.sh`
* Generate Protobuf files: `generate-protobuf-cpp.sh`

## Running the example

macOS is the easiest platform to get the example running. 
* Build the Mbed OS example in `examples/simple`
* Build and run the `simple-example-macos` target in the XCode project in `platform/darwin/Song2.xcodeproj`

The example shows how to execute a RPC command on a Bluetooth device:
* The device-specific code is in `examples/simple/device`
* The client-specific code is in `examples/simple/client`
* Generated code is in `examples/simple/generated`
* The Protocol Buffer/RPC IDL file is in `examples/simple/dummy_service.proto`

### Client example - call a method on the device 

The Client C++ code uses asynchronous promises to execute operations. Command parameters and responses are serialized using JSON.

```cpp
// Say hello!
arpc->call_method_json("arm.song2.test.Dummy", "SayHello", json_t("{a_string: \"everyone\"}"))
    ->then([&](std::future<json_t>& fut)
    {
        auto response = fut.get();
        // Do something with response
    }, queue);
```

## Generator

Install the generator using: `python3 setup.py install`

RPC interfaces can be written using the [protocol buffers IDL](https://developers.google.com/protocol-buffers/docs/proto#services).

The proto2 syntax needs to be used as the proto3 syntax is no supported at this moment.

For instance:

```
syntax = "proto2";

package arm.song2.test;

message IntegerMessage {
    required int32 an_int = 1;
}

message StringMessage {
    required string a_string = 1;
}

message EmtpyMessage {}

service Dummy {
  rpc SetInteger(IntegerMessage) returns (EmtpyMessage) {}
  rpc GetInteger(EmtpyMessage) returns (IntegerMessage) {}

  rpc GetString(EmtpyMessage) returns (StringMessage) {}
  rpc SetString(StringMessage) returns (EmtpyMessage) {}

  rpc SayHello(StringMessage) returns (EmtpyMessage) {}
}
```

[nanopb](https://github.com/nanopb/nanopb) is used under the hood so a matching `.options` file can be used side by side.

You can then use the generator to create the device-side interfaces:
```bash
arpc-codegen dummy_service.proto
```

This will output C and C++ code thant contains virtual classes that can be implemented to handle RPC methods. An intermediary `blob.pb` file is also created - this can be consumed by the client code to convert JSON data to protocol buffer data and vice-versa.

## Open items

* Implementation of Protocol Buffers' RpcChannel and RpcController classes on supported platforms to leverage protoc generator on the client side
* Easier-to-use E2E examples and documentation
* USB support