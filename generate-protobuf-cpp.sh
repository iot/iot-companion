#!/bin/bash

#
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


TARGET_DIR=`pwd`/gen/proto/cpp
PROTO_DIRS="`pwd`/proto `pwd`/tools/generator/arpc_codegen/services"

if [ -z "$PROTOC" ]; then
PROTOC=`which protoc`
fi

echo "Using compiler $PROTOC"

mkdir -p $TARGET_DIR || exit 1
for CURRENT_PROTO_DIR in $PROTO_DIRS
do
    pushd $CURRENT_PROTO_DIR
    PROTO_FILES=`ls -1 *.proto`
    $PROTOC -I. --cpp_out=$TARGET_DIR $PROTO_FILES
    popd
done
