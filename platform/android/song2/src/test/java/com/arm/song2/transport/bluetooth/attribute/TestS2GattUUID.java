/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.attribute;

import org.junit.Test;

import com.arm.song2.utils.HexArray;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestS2GattUUID {

    @Test
    public void testUUIDFromBytes() {
        // see https://stackoverflow.com/questions/36212020/how-can-i-convert-a-bluetooth-16-bit-service-uuid-into-a-128-bit-uuid/36212021
        String currentTimeService = "1805";
        UUID uuid = S2GattUUID.fromBytes(HexArray.hexStringToBytes(currentTimeService));
        assertEquals("00001805-0000-1000-8000-00805f9b34fb", uuid.toString());
        String some32BitService = "11221805";
        uuid = S2GattUUID.fromBytes(HexArray.hexStringToBytes(some32BitService));
        assertEquals("11221805-0000-1000-8000-00805f9b34fb", uuid.toString());

    }

    @Test
    public void containsBaseUUID() {
        String aUUID = "00000000-0000-1000-8000-00805F9B34FB";
        assertTrue(S2GattUUID.containsBluetoothBaseUUID(UUID.fromString(aUUID)));
        aUUID = "00000000-4598-1000-8000-00805F9B34FB";
        assertTrue(S2GattUUID.containsBluetoothBaseUUID(UUID.fromString(aUUID)));
        aUUID = "00004542-0000-1000-8000-00805F9B34FB";
        assertTrue(S2GattUUID.containsBluetoothBaseUUID(UUID.fromString(aUUID)));
        aUUID = "00004542-0000-1000-8000-00805F9B34FF";
        assertFalse(S2GattUUID.containsBluetoothBaseUUID(UUID.fromString(aUUID)));
        aUUID = "00004542-0000-1010-8000-00805F9B34FB";
        assertFalse(S2GattUUID.containsBluetoothBaseUUID(UUID.fromString(aUUID)));
        aUUID = "00000000-0000-1001-8000-00805F9B34FB";
        assertFalse(S2GattUUID.containsBluetoothBaseUUID(UUID.fromString(aUUID)));
    }

    @Test
    public void testUUIDToBytes() {
        byte[] bytes16 = S2GattUUID.toBytes(UUID.fromString("00001805-0000-1000-8000-00805f9b34fb"));
        assertNotNull(bytes16);
        assertEquals(2, bytes16.length);
        assertEquals("1805", HexArray.bytesToHex(bytes16));
        byte[] bytes32 = S2GattUUID.toBytes(UUID.fromString("11221805-0000-1000-8000-00805f9b34fb"));
        assertNotNull(bytes32);
        assertEquals(4, bytes32.length);
        assertEquals("11221805", HexArray.bytesToHex(bytes32));
    }

}
