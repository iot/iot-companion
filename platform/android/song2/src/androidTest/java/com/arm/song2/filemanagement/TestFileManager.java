/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.filemanagement;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.arm.song2.utils.CheckNative;
import com.arm.song2.utils.ContextHolder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class TestFileManager {
    public static final String LOGGER_TAG = "Song2 Test file management";
    public static final String TEST_RESOURCE_NAME = "test_resource.txt";
    public static final String TEST_RESOURCE_CONTENT = "This is a test file for resource extraction!";

    @Before
    public void setup() {
        ContextHolder.setApplicationContext(InstrumentationRegistry.getContext());
    }

    @Test
    public void testResourceExtractionAndReadingFromNative() {
        try (FileManager manager = new FileManager()) {
            final File file = manager.downloadFile(TEST_RESOURCE_NAME);
            assertEquals(TEST_RESOURCE_CONTENT, CheckNative.checkResourceAccessFromNative(file.getAbsolutePath()));
            assertTrue(manager.isFileDownloaded(TEST_RESOURCE_NAME));
        } catch (S2FileManagementException | IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void testFileDownloadFromMultipleThreads() {
        ExecutorService service = Executors.newFixedThreadPool(2);
        try (FileManager manager = new FileManager()) {

            ExecutorMock executorMock1 = new ExecutorMock();
            ExecutorMock executorMock2 = new ExecutorMock();
            service.submit(executorMock1.generateTask(manager));
            service.submit(executorMock2.generateTask(manager));

            while (!executorMock1.finished || !executorMock2.finished) {
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            assertTrue(executorMock1.succeeded);
            assertTrue(executorMock2.succeeded);
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
        service.shutdownNow();
    }

    private static class ExecutorMock {
        public volatile boolean finished = false;
        public volatile boolean succeeded = false;

        public Runnable generateTask(FileManager manager) {
            return new Runnable() {
                @Override
                public void run() {
                    try {
                        final File file = manager.downloadFile(TEST_RESOURCE_NAME);
                        if (TEST_RESOURCE_CONTENT.equals(CheckNative.checkResourceAccessFromNative(file.getAbsolutePath())) && manager.isFileDownloaded(TEST_RESOURCE_NAME)) {
                            succeeded = true;
                        }
                    } catch (S2FileManagementException e) {
                        e.printStackTrace();
                        succeeded = false;
                    }
                    finished = true;
                }
            };
        }
    }
}
