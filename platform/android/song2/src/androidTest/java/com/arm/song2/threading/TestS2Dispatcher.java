/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.threading;

import android.support.test.runner.AndroidJUnit4;

import com.arm.song2.api.DispatcherStatus;
import com.arm.song2.api.Queue;
import com.arm.song2.utils.AndroidUtils;
import com.arm.song2.utils.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiFunction;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class TestS2Dispatcher {

    //TODO add some test to verify that all tasks are performed sequentially
    @Test
    public void testStateMachine() {
        try {
            S2Dispatcher dispatcher = new S2Dispatcher(true);
            assertEquals(DispatcherStatus.PENDING, dispatcher.status());
            assertTrue(dispatcher.start());
            assertEquals(DispatcherStatus.RUNNING, dispatcher.status());
            assertTrue(dispatcher.terminate(false));
            assertEquals(DispatcherStatus.TERMINATED, dispatcher.status());
        } catch (Exception exception) {
            exception.printStackTrace();
            fail(exception.getMessage());
        }
    }

    @Test
    public void testDispatcher() {
        try {
            ProcessingRecorder.reset();
            List<MockQueue> pendingQueues = Arrays.asList(new MockQueue(1), new MockQueue(2), new MockQueue(3), new MockQueue(4), new MockQueue(5));
            List<MockQueue> queues = Arrays.asList(new MockQueue(6), new MockQueue(7), new MockQueue(8), new MockQueue(9), new MockQueue(10));
            S2Dispatcher dispatcher = new S2Dispatcher(true);
            for (final Queue q : pendingQueues) {
                dispatcher.notify(q, 10);
            }
            assertTrue(dispatcher.start());
            for (final Queue q : queues) {
                dispatcher.notify(q, (long) (Math.random() * 10.0));
            }
            Thread.sleep(1000);
            assertTrue(dispatcher.terminate(false));
            ProcessingRecorder.dumpRecords();
            // Checks that all pending queues have been processed
            assertEquals(0, countNonProcessedQueues(pendingQueues));
            // Checks that all notified queues have not been processed
            assertEquals(0, countNonProcessedQueues(queues));
        } catch (Exception exception) {
            exception.printStackTrace();
            fail(exception.getMessage());
        }
    }

    private int countNonProcessedQueues(List<MockQueue> queues) {
        int counter = 0;
        for (MockQueue q : queues) {
            if (ProcessingRecorder.getNumberOfProcessing(q.getId()) < 1) {
                counter++;
            }
        }
        return counter;
    }

    private static class ProcessingRecorder {
        private static final ConcurrentMap<Integer, Integer> recorder = new ConcurrentHashMap<>();

        public static void record(int queueId) {
            if (AndroidUtils.isApiAvailable(23)) {
                recorder.compute(Integer.valueOf(queueId), new BiFunction<Integer, Integer, Integer>() {
                    @Override
                    public Integer apply(Integer k, Integer v) {
                        return (v == null) ? 1 : v + 1;
                    }
                });
            } else {
                synchronized (TestS2Dispatcher.class) {
                    final Integer k = Integer.valueOf(queueId);
                    final Integer oldValue = recorder.get(k);
                    if (oldValue == null) {
                        recorder.putIfAbsent(k, 1);
                    } else {
                        recorder.replace(k, oldValue + 1);
                    }
                }

            }
        }

        public static int getNumberOfProcessing(int queueId) {
            Integer count = recorder.get(queueId);
            return count == null ? 0 : count.intValue();
        }

        public static void reset() {
            recorder.clear();
        }

        public static void dumpRecords() {
            Logger.i("Hits", recorder.toString());
        }
    }

    private static class MockQueue implements Queue {
        private final int id;

        public MockQueue(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        @Override
        public long process() {
            ProcessingRecorder.record(id);
            return Math.random() > 0.2 ? (long) (Math.random() * 1000.0) : -1;
        }
    }
}
