/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.utils;

import org.junit.Test;
import org.junit.runner.RunWith;

import android.support.test.runner.AndroidJUnit4;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertArrayEquals;

@RunWith(AndroidJUnit4.class)
public class TestBase64 {

    @Test
    public void testEncoding() {
        // Result generated from https://www.base64encode.org/
        assertArrayEquals("VGhpcyBpcyBhIHRlc3Qh".getBytes(StandardCharsets.UTF_8), Base64.encode("This is a test!".getBytes(StandardCharsets.UTF_8)));
    }

    @Test
    public void testDecoding() {
        // Result generated from https://www.base64encode.org/
        assertArrayEquals("This is a test!".getBytes(StandardCharsets.UTF_8), Base64.decode("VGhpcyBpcyBhIHRlc3Qh".getBytes(StandardCharsets.UTF_8)));
    }

    @Test
    public void testDecodingEncoding() {
        byte[] test = "This is a test!".getBytes(StandardCharsets.UTF_8);
        assertArrayEquals(test, Base64.decode(Base64.encode(test)));
    }
}
