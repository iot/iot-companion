/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.arm.song2.api.BleAdapterStatus;
import com.arm.song2.api.BleError;
import com.arm.song2.api.GattcCharacteristic;
import com.arm.song2.api.GattcService;
import com.arm.song2.transport.bluetooth.utils.BleUtils;
import com.arm.song2.utils.ContextHolder;
import com.arm.song2.utils.Logger;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Iterator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class TestBle {
    public static final String LOGGER_TAG = "Song2 Test BLE";

    @Before
    public void setup() {
        ContextHolder.setApplicationContext(InstrumentationRegistry.getContext());
    }

    //"Test" performing some BLE actions
    @Test
    public void testScan() {
        S2BleAdapter adapter = new S2BleAdapter(null);
        assertTrue(BleUtils.isBLESupported());
        assertTrue(disableBluetooth());
        assertFalse(BleUtils.isBluetoothEnabled());
        assertEquals(BleAdapterStatus.PERMISSION_REQUIRED, adapter.getStatus());
        Assert.assertTrue(enableBluetooth());
        assertTrue(BleUtils.isBluetoothEnabled());
        assertTrue(BleUtils.isLocationPermissionGranted());
        assertEquals(BleAdapterStatus.ENABLED, adapter.getStatus());
        assertEquals(BleError.OK, adapter.startScanning(null, null));
        assertEquals(BleAdapterStatus.SCANNING_BACKGROUND, adapter.getStatus());
        Logger.i(LOGGER_TAG, "Start scanning");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            //Nothing to do
        }
        Logger.i(LOGGER_TAG, "Stop scanning");
        assertEquals(BleError.OK, adapter.stopScanning());
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            //Nothing to do
        }
        assertNotEquals(BleAdapterStatus.SCANNING_BACKGROUND, adapter.getStatus());
        Logger.i(LOGGER_TAG, "Scanning stopped");
        if (adapter.getDeviceManager().foundDevices()) {
            Logger.i(LOGGER_TAG, "Found " + adapter.getDeviceManager().foundDeviceIds().size() + " BLE devices");
            Logger.i(LOGGER_TAG, "Found BLE devices: " + adapter.getDeviceManager().foundDeviceIds());
            Iterator<String> deviceIt = adapter.getDeviceManager().foundDeviceIds().iterator();
            while (deviceIt.hasNext()) {
                String aDeviceId = deviceIt.next();
                assertNotNull(aDeviceId);

                S2BleDevice aDevice = adapter.getDeviceManager().fetchDevice(aDeviceId);
                assertNotNull(aDevice);
                Logger.i(LOGGER_TAG, "Connecting to device: " + aDevice);
                adapter.connectDevice(aDevice, null);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    //Nothing to do
                }
                Logger.i(LOGGER_TAG, "Discovering services");
                adapter.gattcDiscoverServices(aDevice, null);
                Logger.i(LOGGER_TAG, "Requesting PHY");
                adapter.gattcGetPhy(aDevice);
                Logger.i(LOGGER_TAG, "Status " + adapter.getStatus());
                Logger.i(LOGGER_TAG, "Requesting RSSI");
                adapter.getDeviceRssi(aDevice);
                Logger.i(LOGGER_TAG, "Requesting MTU");
                adapter.gattcRequestMtu(aDevice, 15);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    //Nothing to do
                }
                ArrayList<GattcService> services = aDevice.gattcServices();
                Logger.i(LOGGER_TAG, "Found services: " + services + " on device " + aDeviceId);
                assertNotNull(services);
                Iterator<GattcService> serviceIt = services.iterator();
                while (serviceIt.hasNext()) {
                    adapter.gattcDiscoverCharacteristics(serviceIt.next(), null);
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    //Nothing to do
                }
                Iterator<GattcService> serviceIt2 = services.iterator();
                while(serviceIt2.hasNext()){
                    Iterator<GattcCharacteristic>characteristicIt=serviceIt2.next().characteristics().iterator();
                    Logger.d(LOGGER_TAG,"Setting notifications for characteristics");
                    while (characteristicIt.hasNext()){
                        adapter.gattcCharacteristicSetNotifications(characteristicIt.next(),true);
                    }
                }
                Logger.d(LOGGER_TAG,"Waiting for notifications to come back");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    //Nothing to do
                }
                Logger.d(LOGGER_TAG,"Disconnecting from the device");

                //do something
                adapter.disconnectDevice(aDevice);

            }
        } else {
            Logger.i(LOGGER_TAG, "No BLE devices found");
        }

        adapter.getDeviceManager().reset();
        assertFalse(adapter.getDeviceManager().foundDevices());
        assertTrue(disableBluetooth());
    }


    private boolean enableBluetooth() {
        BluetoothAdapter adapter = BleUtils.fetchBleAdapter();
        if (adapter != null) {
            if (!adapter.enable()) {
                return false;
            }
            while (!BleUtils.isBluetoothEnabled()) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    //Nothing to do
                }
            }
            return true;
        }
        return false;
    }

    private boolean disableBluetooth() {
        BluetoothAdapter adapter = BleUtils.fetchBleAdapter();
        if (adapter != null) {
            if (!adapter.disable()) {
                return false;
            }
            while (BleUtils.isBluetoothEnabled()) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    //Nothing to do
                }
            }
            return true;
        }
        return false;
    }
}
