/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.utils;

/**
 * Utility to check the JNI link works correctly
 */
public class CheckNative {
    static {
        NativeCodeLoader.load();
    }

    private CheckNative() {
    }

    /**
     * Calls a native testing method.
     *
     * @return testing string defined in the Cpp layer.
     */
    public String fetchTestingNativeString() {
        return fetchTestStringFromJNI();
    }


    /**
     * Calls a native testing method for checking exception handling.
     *
     * @return a test string
     */
    public String handleNativeExceptions() {
        return handleCppException();
    }

    /**
     * Calls a native function to read a file present in the internal store.
     *
     * @param filePath path of the file to read
     * @return file content.
     */
    public String readTestingFileContent(String filePath) {
        return filePath == null ? null : readFile(filePath);
    }

    private static class CheckHolder {
        public static final CheckNative INSTANCE = new CheckNative();
    }

    /**
     * Testing method to verify that the native code was correctly loaded.
     *
     * @return a string defined in the Cpp layer.
     */
    public static String checkNativeLoad() {
        return CheckHolder.INSTANCE.fetchTestingNativeString();
    }

    public static String checkExceptionHandling() {
        return CheckHolder.INSTANCE.handleNativeExceptions();
    }

    public static String checkResourceAccessFromNative(String resourceName) {
        return CheckHolder.INSTANCE.readTestingFileContent(resourceName);
    }

    /**
     * A native method for testing purposes that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String fetchTestStringFromJNI();

    /**
     * A native method for testing that CPP can read files in internal store.
     */
    public native String readFile(String path);

    /**
     * A native method for testing that CPP can handle exceptions.
     */
    public native String handleCppException();
}
