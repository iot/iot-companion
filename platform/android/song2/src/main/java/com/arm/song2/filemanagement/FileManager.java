/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.filemanagement;

import com.arm.song2.filemanagement.utils.FileManagementUtils;
import com.arm.song2.utils.Logger;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class FileManager implements Closeable {
    private final ConcurrentMap<String, File> mDownloadedFiles;
    private final Object lock;

    public FileManager() {
        mDownloadedFiles = new ConcurrentHashMap<>();
        lock = new Object();
        initialLoad();
    }

    private void initialLoad() {
        try {
            Logger.d(FileManagementUtils.LOGGER_TAG, "Loading initial files");
            downloadFile("global_init.js");
            downloadFile("Rx.min.js");
        } catch (S2FileManagementException exception) {
            Logger.e(FileManagementUtils.LOGGER_TAG, "Downloading initial files failed. Reason: " + exception.getMessage());
        }
    }

    public void reset() {
        mDownloadedFiles.clear();
    }

    public boolean isFileDownloaded(String fileName) {
        if (fileName == null || fileName.isEmpty()) {
            return false;
        }
        return mDownloadedFiles.get(fileName) != null;
    }

    public File downloadFile(String fileName) throws S2FileManagementException {
        if (fileName == null || fileName.isEmpty()) {
            FileManagementUtils.throwException("Failed downloading the file", new IllegalArgumentException("Missing file name parameter"));
        }
        final File file = mDownloadedFiles.get(fileName);
        if (file != null) {
            return file;
        }
        synchronized (lock) {
            //Test number two
            final File file2 = mDownloadedFiles.get(fileName);
            if (file2 != null) {
                return file2;
            }
            final File file3 = FileManagementUtils.installResourceFileOnDevice(fileName);
            mDownloadedFiles.put(fileName, file3);
            return file3;
        }
    }

    @Override
    public void close() throws IOException {
        reset();
    }
}
