/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.utils;

import android.bluetooth.BluetoothDevice;

import com.arm.song2.api.BleDeviceConnectionFlags;
import com.arm.song2.api.BleDevicePhy;
import com.arm.song2.utils.AndroidUtils;

import java.util.EnumSet;

public class BlePhyUtils {
    public static int phy(EnumSet<BleDeviceConnectionFlags> flags) {
        if (flags == null || flags.isEmpty()) {
            return BluetoothDevice.PHY_LE_1M_MASK;
        }
        int phy = 0;
        if (flags.contains(BleDeviceConnectionFlags.PHY_1M)) {
            phy |= BluetoothDevice.PHY_LE_1M_MASK;
        }
        if (flags.contains(BleDeviceConnectionFlags.PHY_2M)) {
            phy |= BluetoothDevice.PHY_LE_2M_MASK;
        }
        if (flags.contains(BleDeviceConnectionFlags.PHY_CODED)) {
            phy |= BluetoothDevice.PHY_LE_CODED_MASK;
        }
        return phy;
    }

    public static EnumSet<BleDevicePhy> phy(int flag) {

        if (AndroidUtils.isApiAvailable(26)) {
            switch (flag) {
                case BluetoothDevice.PHY_LE_1M:
                    return EnumSet.of(BleDevicePhy.PHY_1M);
                case BluetoothDevice.PHY_LE_2M:
                    return EnumSet.of(BleDevicePhy.PHY_2M);
                case BluetoothDevice.PHY_LE_CODED:
                    return EnumSet.of(BleDevicePhy.PHY_CODED);
            }
        } else {
            switch (flag) {
                case 1:
                    return EnumSet.of(BleDevicePhy.PHY_1M);
                case 2:
                    return EnumSet.of(BleDevicePhy.PHY_2M);
                case 3:
                    return EnumSet.of(BleDevicePhy.PHY_CODED);
            }
        }
        return EnumSet.noneOf(BleDevicePhy.class);
    }

    public static EnumSet<BleDevicePhy> mergePhy(EnumSet<BleDevicePhy> txPhy, EnumSet<BleDevicePhy> rxPhy) {
        if (txPhy == null) {
            return rxPhy;
        }
        EnumSet<BleDevicePhy> merge = txPhy.clone();
        if (rxPhy != null) {
            txPhy.addAll(rxPhy);
        }
        return merge;
    }

}
