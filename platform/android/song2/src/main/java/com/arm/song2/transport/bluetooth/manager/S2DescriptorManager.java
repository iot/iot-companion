/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.manager;

import com.arm.song2.api.BleDevice;
import com.arm.song2.api.BleError;
import com.arm.song2.api.GattcCharacteristic;
import com.arm.song2.api.GattcDescriptor;
import com.arm.song2.api.GattcService;
import com.arm.song2.transport.bluetooth.attribute.S2GattAttributeId;
import com.arm.song2.transport.bluetooth.attribute.S2GattCharacteristic;
import com.arm.song2.transport.bluetooth.attribute.S2GattDescriptor;
import com.arm.song2.transport.bluetooth.attribute.S2GattService;
import com.arm.song2.transport.bluetooth.attribute.S2GattUUID;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class S2DescriptorManager extends AbstractManager<S2GattDescriptor> implements IS2Manager {
    private final ConcurrentMap<S2GattDescriptor, S2GattCharacteristic> mMapping;

    public S2DescriptorManager() {
        super();
        mMapping = new ConcurrentHashMap<>();
    }

    @Override
    protected S2GattAttributeId determineAttributeId(Object attribute) {
        if (attribute == null || !(attribute instanceof GattcDescriptor)) {
            return null;
        }
        final GattcDescriptor theAttribute = (GattcDescriptor) attribute;
        return new S2GattAttributeId(theAttribute.uuid(), theAttribute.identifier());
    }

    public S2GattCharacteristic getGattcCharacteristicForDescriptor(GattcDescriptor descriptor) {
        final S2GattDescriptor correspondingDescriptor = fetchAttribute(descriptor);
        return correspondingDescriptor == null ? null : mMapping.get(correspondingDescriptor);
    }


    public BleError registerDescriptor(S2GattDescriptor descriptor, S2GattCharacteristic characteristic) {
        if (characteristic == null || !characteristic.isValid()) {
            return BleError.UNKNOWN_GATTC_CHARACTERISTIC;
        }
        if (descriptor == null || !descriptor.isValid()) {
            return BleError.UNKNOWN_ERROR;
        }
        synchronized (this) {
            registerNewAttribute(descriptor);
            mMapping.put(descriptor, characteristic);
        }
        return BleError.OK;
    }

    public void deregisterDescriptor(GattcDescriptor descriptor) {
        final S2GattDescriptor correspondingDescriptor = fetchAttribute(descriptor);
        if (correspondingDescriptor != null) {
            synchronized (this) {
                deregisterAttribute(correspondingDescriptor);
                mMapping.remove(correspondingDescriptor);
            }
        }
    }

    public BleError registerDescriptors(List<S2GattDescriptor> descriptors, S2GattCharacteristic characteristic) {
        if (descriptors == null) {
            return BleError.UNKNOWN_ERROR;
        }
        for (final S2GattDescriptor descriptor : descriptors) {
            final BleError status = registerDescriptor(descriptor, characteristic);
            if (status != BleError.OK) {
                return status;
            }
        }
        return BleError.OK;
    }

    @Override
    public void reset() {
        super.reset();
        mMapping.clear();
    }
}
