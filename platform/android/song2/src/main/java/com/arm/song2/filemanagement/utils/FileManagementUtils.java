/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.filemanagement.utils;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;

import com.arm.song2.Song2;
import com.arm.song2.filemanagement.S2FileManagementException;
import com.arm.song2.utils.AndroidUtils;
import com.arm.song2.utils.ContextHolder;
import com.arm.song2.utils.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Locale;

public class FileManagementUtils {
    public static final String LOGGER_TAG = Song2.LOGGER_TAG + " file management";
    public static final int BUFFER_SIZE = 1024;


    // File should be put in res/raw folder and its name should contain the extension
    public static InputStream openPackagedFile(Context ctx, String fileName) throws S2FileManagementException {
        if (ctx == null) {
            throwException("Failed finding the file in resources", new NullPointerException("Null context"));
        }
        if (fileName == null || fileName.isEmpty()) {
            throwException("Failed finding the file in resources", new IllegalArgumentException("Invalid filename: " + fileName));
        }

        final Resources resources = ctx.getResources();
        final String packageName = ctx.getPackageName();
        if (resources == null || packageName == null) {
            throwException("Failed finding resources", new NullPointerException((resources == null ? "ctx.getResources()" : "ctx.getPackageName()") + " returns null"));
        }
        try {
            return resources.openRawResource(getFileResourceId(resources, fileName.trim(), packageName));
        } catch (Resources.NotFoundException exception) {
            throwException("Failed finding the file in resources", exception);
        }
        return null;
    }

    public static InputStream openPackagedFile(String fileName) throws S2FileManagementException {
        return openPackagedFile(getContext(), fileName);
    }

    public static File createFileInInternalStorage(Context ctx, String fileName) throws S2FileManagementException {
        if (ctx == null) {
            throwException("Failed finding the file in resources", new NullPointerException("Null context"));
        }
        if ( fileName == null || fileName.isEmpty()) {
            throwException("Failed creating a file in the internal storage", new IllegalArgumentException("Invalid filename: " + fileName));
        }
        final File internalStorageDir = ctx.getFilesDir();
        if (internalStorageDir == null) {
            throwException("Failed creating a file in the internal storage", new IOException("Internal storage could not be accessed"));
        }
        return new File(internalStorageDir, fileName);
    }

    public static File createFileInInternalStorage(String fileName) throws S2FileManagementException {
        return createFileInInternalStorage(getContext(), fileName);
    }

    public static void downloadFile(InputStream in, File destination) throws S2FileManagementException {
        if (in == null || destination == null) {
            throwException("Failed downloading a file to [" + destination + "]", new IllegalArgumentException("Invalid parameter"));
        }
        try {
            if (AndroidUtils.isApiAvailable(26)) {
                Files.copy(in, destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } else {
                legacyFileCopy(in, destination);
            }
        } catch (IOException exception) {
            throwException("Failed downloading a file to [" + destination + "]", exception);
        }
    }

    public static File installResourceFileOnDevice(String fileName) throws S2FileManagementException {
        return installResourceFileOnDevice(getContext(), fileName);
    }

    public static File installResourceFileOnDevice(Context ctx, String fileName) throws S2FileManagementException {
        try (InputStream in = openPackagedFile(ctx, fileName)) {
            final File destination = createFileInInternalStorage(ctx, fileName);
            downloadFile(in, destination);
            return destination;
        } catch (IOException exception) {
            throwException("Failed installing a file on the system [" + fileName + "]", exception);
        }
        return null;
    }

    public static Context getContext() {
        return ContextHolder.getApplicationContext();
    }

    public static void throwException(String message, Throwable cause) throws S2FileManagementException {
        final S2FileManagementException exception = new S2FileManagementException(message, cause);
        Logger.e(LOGGER_TAG, message + ". Reason: " + cause.getMessage());
        throw exception;
    }

    private static void legacyFileCopy(InputStream in, File destination) throws IOException {
        try (OutputStream out = new FileOutputStream(destination)) {
            final byte[] buf = new byte[BUFFER_SIZE];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        }
    }

    private static int getFileResourceId(Resources resources, String fileName, String packageName) throws S2FileManagementException {
        try {
            // See https://developer.android.com/guide/topics/resources/providing-resources
            Logger.d(LOGGER_TAG, "Getting resource " + fileName + " from package " + packageName);
            return resources.getIdentifier(getCorrespondingResourceName(fileName), "raw", packageName);
        } catch (Exception e) {
            throwException("Failed finding the file resource Id", e);
        }
        return 0;
    }

    @NonNull
    public static String getCorrespondingResourceName(String fileName) {
        // Files in resources should be lower case and '.' should be replaced by '_'
        return removeFileNameInvalidCharacters(removeFileNameInvalidCharacters(convertToLowercase(fileName)));
    }


    private static String convertToLowercase(String stringToConvert) {
        if (stringToConvert == null || stringToConvert.isEmpty()) {
            return stringToConvert;
        }
        return stringToConvert.toLowerCase();
    }

    private static String removeFileNameInvalidCharacters(String filename) {
        return filename.replace('.', '_');
    }
}
