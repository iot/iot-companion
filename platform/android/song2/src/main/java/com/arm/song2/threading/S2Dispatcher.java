/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.threading;

import android.os.Handler;
import android.os.HandlerThread;

import com.arm.song2.Song2;
import com.arm.song2.api.Dispatcher;
import com.arm.song2.api.DispatcherStatus;
import com.arm.song2.api.Queue;
import com.arm.song2.utils.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class S2Dispatcher implements Dispatcher, Closeable {
    public static final String LOGGER_TAG = Song2.LOGGER_TAG + " Dispatcher";
    public static final String UNKNOWN_NAME = "Unknown";
    private final boolean mIsMain;
    protected final String mName;
    private final HandlerThread mHandlerThread;
    protected volatile Handler mHandler;
    private volatile DispatcherStatus mStatus;
    private final BlockingQueue<com.arm.song2.api.Queue> mPendingQueues;

    public S2Dispatcher(boolean isMainDispatcher) {
        this(isMainDispatcher, UNKNOWN_NAME);
    }

    public S2Dispatcher(boolean isMainDispatcher, String name) {
        mStatus = DispatcherStatus.PENDING;
        mPendingQueues = new LinkedBlockingQueue<>();
        mIsMain = isMainDispatcher;
        mName = name;
        mHandlerThread = new HandlerThread(mName);
        mHandler = null;
    }

    @Override
    public DispatcherStatus status() {
        return mStatus;
    }

    @Override
    public boolean start() {
        Logger.d(LOGGER_TAG, "Starting dispatcher [" + mName + "]");
        synchronized (this) {
            if (mStatus != DispatcherStatus.PENDING) {
                Logger.d(LOGGER_TAG, "Dispatcher [" + mName + "] is already in state [" + mStatus + "]");
                return false;
            }
            mHandlerThread.start();
            mHandler = new Handler(mHandlerThread.getLooper());
            processPendingQueues(true);
            return true;
        }
    }

    @Override
    public boolean terminate(boolean kill) {
        Logger.d(LOGGER_TAG, "Dispatcher [" + mName + "] is about to be terminated. kill: " + kill);
        if (mStatus == DispatcherStatus.TERMINATED) {
            Logger.d(LOGGER_TAG, "Dispatcher [" + mName + "] was already terminated");
            return true;
        }
        if (mStatus == DispatcherStatus.PENDING) {
            processPendingQueues(!kill);
        }
        mStatus = DispatcherStatus.TERMINATING;
        Logger.d(LOGGER_TAG, "Dispatcher [" + mName + "] is shutting down");
        if (kill) {
            mHandlerThread.quit();
        } else {
            mHandlerThread.quitSafely();
        }
        mStatus = DispatcherStatus.TERMINATED;
        Logger.d(LOGGER_TAG, "Dispatcher [" + mName + "] has been terminated");
        return true;
    }

    @Override
    public boolean notify(Queue queue, long delayMs) {
        if (queue == null) {
            return false;
        }
        synchronized (this) {
            switch (mStatus) {
                case PENDING:
                    try {
                        mPendingQueues.offer(queue, 1, TimeUnit.SECONDS);
                    } catch (InterruptedException e) {
                        Logger.e(LOGGER_TAG, "[Dispatcher " + mName + "]: Failed storing pending queue. Reason: " + e.getMessage());
                    }
                    return true;
                case TERMINATED:
                case TERMINATING:
                    return false;
                case RUNNING:
                    if (mHandler == null) {
                        Logger.e(LOGGER_TAG, "[Dispatcher " + mName + "]: Considered running but not started properly");
                        return false;
                    }
            }
            return postJob(queue, delayMs);
        }
    }


    @Override
    public void close() throws IOException {
        Logger.d(LOGGER_TAG, "Closing dispatcher [" + mName + "]");
        terminate(false);
    }

    protected boolean postJob(Queue queue, long delayMs) {
        return mHandler.postDelayed(newJobProcessor(queue, this), delayMs);
    }

    private void processPendingQueues(boolean process) {
        try {
            mStatus = DispatcherStatus.RUNNING;
            if (process) {
                Logger.d(LOGGER_TAG, "Dispatcher [" + mName + "] is processing pending queues");
                while (notify(mPendingQueues.poll(), 0)) ;
            }
            mPendingQueues.clear();
        } catch (Exception e) {
            Logger.e(LOGGER_TAG, "Problem encountered by dispatcher [" + mName + "] while processing pending queues. Reason" + e.getMessage());
        }
    }


    protected static Runnable newJobProcessor(Queue queue, Dispatcher dispatcher) {
        return queue == null ? null : new Runnable() {

            @Override
            public void run() {
                try {
                    final long delayMs = queue.process();
                    if (delayMs >= 0) {
                        if (dispatcher == null) {
                            return;
                        }
                        dispatcher.notify(queue, delayMs);
                    }
                } catch (Throwable e) {
                    Logger.e(LOGGER_TAG, "Problem encountered by dispatcher [" + dispatcher + "] while processing queue [" + queue + "]. Reason" + e.getMessage());
                }
            }
        };
    }

    @Override
    public boolean isEqual(Dispatcher other) {
        final boolean equal = other == this;
        if (!equal) {
            Logger.w(LOGGER_TAG, "dispatchers don't match");
        }
        return equal;
    }

    @Override
    public String toString() {
        return "S2Dispatcher {" +
                "mName='" + mName + '\'' +
                '}';
    }
}
