/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.os.ParcelUuid;
import android.util.SparseArray;

import com.arm.song2.api.BleDevice;
import com.arm.song2.api.BleDevicePhy;
import com.arm.song2.api.BleDeviceStatus;
import com.arm.song2.api.BleManufacturerSpecificDataType;
import com.arm.song2.api.BleServicesDataType;
import com.arm.song2.api.GattcService;
import com.arm.song2.transport.bluetooth.attribute.S2GattUUID;
import com.arm.song2.utils.HexArray;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class S2BleDevice implements BleDevice {
    private final BluetoothDevice mNativeDevice;
    private final String id;
    private final String mName;


    private volatile BleScanRecord mScanRecord;
    private volatile BleDeviceStatus mStatus;
    private volatile int mMtu;
    private volatile int mRssi;
    private volatile EnumSet<BleDevicePhy> mPhy;
    private volatile ArrayList<GattcService> mServices;

    public S2BleDevice(BluetoothDevice nativeDevice, int rssi, BleScanRecord scanRecord) {
        this.mNativeDevice = nativeDevice;
        id = mNativeDevice == null ? null : mNativeDevice.getAddress();
        mName = mNativeDevice == null ? null : mNativeDevice.getName();
        mScanRecord = scanRecord == null ? new BleScanRecord() : scanRecord;
        setServices(new ArrayList<>());
        setStatus(BleDeviceStatus.DISCONNECTED);
        setPhy(S2Gatt.DEFAULT_PHY);
        setMtu(S2Gatt.DEFAULT_MTU);
        setRssi(rssi);
    }

    public List<BleManufacturerSpecificDataType> getScanResultManufacturerSpecificData() {
        return mScanRecord.manufacturerSpecificData();
    }

    public BleManufacturerSpecificDataType getFirstScanResultManufacturerSpecificData() {
        final List<BleManufacturerSpecificDataType> data = getScanResultManufacturerSpecificData();
        return data == null || data.isEmpty() ? null : data.get(0);
    }

    public ArrayList<BleServicesDataType> getScanResultServicesData() {
        return mScanRecord.getServicesData();
    }

    public ArrayList<byte[]> getScanResultServicesUuids() {
        return S2GattUUID.uuidsAsByteList(mScanRecord.getServicesUuids());
    }


    @Override
    public BleDeviceStatus status() {
        return mStatus;
    }

    @Override
    public EnumSet<BleDevicePhy> phy() {
        return mPhy;
    }

    @Override
    public int rssi() {
        return mRssi;
    }

    @Override
    public int gattcMtu() {
        return mMtu;
    }

    public void setMtu(int mtu) {
        mMtu = mtu;
    }

    public void setRssi(int rssi) {
        mRssi = rssi;
    }

    public void setStatus(BleDeviceStatus status) {
        mStatus = status;
    }

    public void setPhy(EnumSet<BleDevicePhy> phy) {
        mPhy = phy;
    }


    @Override
    public ArrayList<GattcService> gattcServices() {
        return mServices;
    }

    public void setServices(ArrayList<GattcService> services) {
        mServices = services;
    }

    public BluetoothDevice getNative() {
        return mNativeDevice;
    }

    @Override
    public byte[] identifier() {
        return hasId() ? getId().getBytes(StandardCharsets.UTF_8) : null;
    }

    public String getId() {
        return id;
    }

    public boolean hasId() {
        return id != null && !id.isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof S2BleDevice)) return false;
        final S2BleDevice that = (S2BleDevice) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public boolean isValid() {
        return mNativeDevice != null && hasId();
    }

    public void updateDevice(S2BleDevice updatedDevice) {
        if (updatedDevice == null || !updatedDevice.equals(this)) {
            return;
        }
        synchronized (this) {
            this.mScanRecord = updatedDevice.mScanRecord;
            this.mMtu = updatedDevice.mMtu;
            this.mPhy = updatedDevice.mPhy;
            this.mRssi = updatedDevice.mRssi;
            this.mStatus = updatedDevice.mStatus;
            this.mServices = updatedDevice.mServices;
        }
    }

    public static BleDeviceStatus determineConnectionStatus(int state) {
        switch (state) {
            case BluetoothProfile.STATE_CONNECTED:
                return BleDeviceStatus.CONNECTED;
            case BluetoothProfile.STATE_CONNECTING:
                return BleDeviceStatus.CONNECTING;
            case BluetoothProfile.STATE_DISCONNECTED:
            case BluetoothProfile.STATE_DISCONNECTING:
                return BleDeviceStatus.DISCONNECTED;
        }
        return BleDeviceStatus.DISCONNECTED;
    }

    public static S2BleDevice determineFromScanResult(ScanResult result) {
        return new S2BleDevice(result.getDevice(), result.getRssi(), new BleScanRecord(result.getScanRecord()));
    }

    @Override
    public String toString() {
        return "S2BleDevice{" +
                "id='" + id + '\'' +
                (mName != null ? ", Name=" + mName : "") +
                ", ScanRecord=" + HexArray.bytesToHex(mScanRecord.getRawRecord()) +
                ", Status=" + mStatus +
                ", MTU=" + mMtu +
                ", RSSI=" + mRssi +
                ", PHY=" + mPhy +
                '}';
    }

    private static class BleScanRecord {
        private final List<BleManufacturerSpecificDataType> mManufacturerSpecificData;
        private final ArrayList<BleServicesDataType> mServicesData;
        private final List<S2GattUUID> mServicesUuids;
        private final byte[] mRawRecord;

        public BleScanRecord() {
            this(null);
        }

        public BleScanRecord(ScanRecord scanResult) {
            mServicesUuids = scanResult == null ? null : retrieveServiceUuids(scanResult.getServiceUuids());
            mServicesData = scanResult == null ? null : retrieveServicesData(scanResult.getServiceData());
            mManufacturerSpecificData = scanResult == null ? null : retrieveManufacturerData(scanResult.getManufacturerSpecificData());
            mRawRecord = scanResult == null ? null : scanResult.getBytes();
        }

        private List<BleManufacturerSpecificDataType> retrieveManufacturerData(SparseArray<byte[]> manufacturerSpecificData) {
            if (manufacturerSpecificData == null || manufacturerSpecificData.size() == 0) {
                return new ArrayList<>();
            }
            final List<BleManufacturerSpecificDataType> data = new LinkedList<>();
            for (int i = 0; i < manufacturerSpecificData.size(); i++) {
                if (manufacturerSpecificData.valueAt(i) != null) {
                    final BleManufacturerSpecificDataType manufacturer = new BleManufacturerSpecificDataType(manufacturerSpecificData.keyAt(i), manufacturerSpecificData.valueAt(i));
                    data.add(manufacturer);
                }
            }
            return data;
        }

        private ArrayList<BleServicesDataType> retrieveServicesData(Map<ParcelUuid, byte[]> serviceData) {
            if (serviceData == null || serviceData.isEmpty()) {
                return new ArrayList<>();
            }
            final ArrayList<BleServicesDataType> data = new ArrayList<>(serviceData.size());
            for (final Map.Entry<ParcelUuid, byte[]> element : serviceData.entrySet()) {
                final BleServicesDataType dataType = new BleServicesDataType(new S2GattUUID(element.getKey().getUuid()).toBytes(), element.getValue());
                data.add(dataType);
            }
            return data;
        }

        private List<S2GattUUID> retrieveServiceUuids(List<ParcelUuid> serviceUuids) {
            if (serviceUuids == null || serviceUuids.isEmpty()) {
                return new ArrayList<>();
            }
            final List<S2GattUUID> uuids = new ArrayList<>(serviceUuids.size());
            for (final ParcelUuid parcel : serviceUuids) {
                final S2GattUUID uuid = new S2GattUUID(parcel.getUuid());
                if (uuid.isValid()) {
                    uuids.add(uuid);
                }
            }
            return uuids;
        }

        public List<BleManufacturerSpecificDataType> manufacturerSpecificData() {
            return mManufacturerSpecificData;
        }

        public ArrayList<BleServicesDataType> getServicesData() {
            return mServicesData;
        }

        public List<S2GattUUID> getServicesUuids() {
            return mServicesUuids;
        }

        public byte[] getRawRecord() {
            return mRawRecord;
        }
    }
}
