/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.utils;

import com.arm.song2.Song2;

public final class NativeCodeLoader {

    private boolean hasLoaded;

    private NativeCodeLoader() {
        hasLoaded = false;
    }

    /**
     * Loads native code once.
     */
    public synchronized void loadNative() {
        if (!hasLoaded) {
            Logger.d(Song2.LOGGER_TAG, "Loading native library");
            System.loadLibrary(Song2.NATIVE_LIBRARY_NAME);
            Logger.d(Song2.LOGGER_TAG, "Native libraries are loaded");
            hasLoaded = true;
        }
    }


    private static class LoaderHolder {
        public static final NativeCodeLoader INSTANCE = new NativeCodeLoader();
    }

    /**
     * Loads native code.
     */
    public static void load() {
        LoaderHolder.INSTANCE.loadNative();
    }

}
