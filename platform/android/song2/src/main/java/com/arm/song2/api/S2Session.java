/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.api;

import com.arm.song2.Song2;
import com.arm.song2.utils.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public class S2Session implements Closeable {
    private final JsApp mJsApp;
    private final S2Factory mFactory;
    private static final AtomicBoolean mLoadedFactory = new AtomicBoolean(false);

    private S2Session() {
        Logger.d(Song2.LOGGER_TAG, "Starting session");
        mFactory = new S2Factory();
        mJsApp = JsApp.create(mFactory);
        Logger.d(Song2.LOGGER_TAG, "Session is started");
    }

    public static S2Session get() {
        mLoadedFactory.set(true);
        return S2SessionHolder.INSTANCE;
    }

    public static void terminate() throws IOException {
        if (mLoadedFactory.get()) {
            get().close();
        }
    }

    public JsApp getJsApp() {
        return mJsApp;
    }

    public S2Factory getPlatformFactory() {
        return mFactory;
    }

    @Override
    public void close() throws IOException {
        Logger.d(Song2.LOGGER_TAG, "Closing the session");
        mFactory.close();
    }

    private static class S2SessionHolder {
        public static final S2Session INSTANCE = new S2Session();

    }
}
