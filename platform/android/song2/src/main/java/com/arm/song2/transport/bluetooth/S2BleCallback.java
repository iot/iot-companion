/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;

import com.arm.song2.api.BleAdapterDelegate;
import com.arm.song2.api.BleDeviceStatus;
import com.arm.song2.api.BleError;
import com.arm.song2.transport.bluetooth.attribute.S2GattCharacteristic;
import com.arm.song2.transport.bluetooth.attribute.S2GattDescriptor;
import com.arm.song2.transport.bluetooth.manager.S2BleAttributeManagers;
import com.arm.song2.transport.bluetooth.utils.BleAdapterDelegateHolder;
import com.arm.song2.transport.bluetooth.utils.BlePhyUtils;
import com.arm.song2.transport.bluetooth.utils.BleStatus;
import com.arm.song2.utils.Logger;

import java.util.concurrent.atomic.AtomicReference;

public class S2BleCallback extends BluetoothGattCallback {
    private final BleAdapterDelegateHolder mDelegate;
    private final S2BleDevice mDevice;
    private final S2BleAttributeManagers mAttributeManagers;
    private final AtomicReference<Runnable> mCallBackWhenAttributesAreDiscovered;

    public S2BleCallback(BleAdapterDelegateHolder delegate, S2BleDevice device, S2BleAttributeManagers attributeManagers) {
        mDelegate = delegate;
        mDevice = device;
        mAttributeManagers = attributeManagers;
        mCallBackWhenAttributesAreDiscovered = new AtomicReference<>(new Runnable() {
            @Override
            public void run() {
                Logger.i(S2Gatt.LOGGER_TAG, "Attributes were discovered.");
            }
        });
    }

    public void updateDelegate(BleAdapterDelegate delegate) {
        mDelegate.updateDelegate(delegate);
    }

    public void setCallBackWhenAttributesAreDiscovered(Runnable runnable) {
        if (runnable != null) {
            mCallBackWhenAttributesAreDiscovered.set(runnable);
        }
    }

    @Override
    public void onPhyUpdate(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
        final BleError error = BleStatus.determineStatus(status);
        Logger.d(S2Gatt.LOGGER_TAG, "onPhyUpdate [status= " + error + "]");
        if (!BleStatus.isError(error)) {
            mDevice.setPhy(BlePhyUtils.mergePhy(BlePhyUtils.phy(txPhy), (BlePhyUtils.phy(rxPhy))));
        } else {
            Logger.e(S2Gatt.LOGGER_TAG, "Error during PHY update operation [" + error + "]");
        }
        mDelegate.onGattcPhyUpdated(mDevice, error);
    }

    @Override
    public void onPhyRead(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
        final BleError error = BleStatus.determineStatus(status);
        Logger.d(S2Gatt.LOGGER_TAG, "onPhyRead [status= " + error + "]");
        if (!BleStatus.isError(error)) {
            mDevice.setPhy(BlePhyUtils.mergePhy(BlePhyUtils.phy(txPhy), (BlePhyUtils.phy(rxPhy))));
        } else {
            Logger.e(S2Gatt.LOGGER_TAG, "Error during PHY read operation[" + error + "]");
        }
        mDelegate.onGattcPhyUpdated(mDevice, error);
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        final BleDeviceStatus connectionStatus = S2BleDevice.determineConnectionStatus(newState);
        final BleError error = BleStatus.determineStatus(status);
        Logger.d(S2Gatt.LOGGER_TAG, "onConnectionStateChange [status= " + error + "]");
        switch (connectionStatus) {
            case CONNECTED:
                Logger.i(S2Gatt.LOGGER_TAG, "Connected to GATT server.");
                mDevice.setStatus(BleDeviceStatus.CONNECTED);
                mDelegate.onDeviceConnected(mDevice, error);
                break;
            case DISCONNECTED:
                Logger.i(S2Gatt.LOGGER_TAG, "Disconnected from GATT server.");
                mDevice.setStatus(BleDeviceStatus.DISCONNECTED);
                mDelegate.onDeviceDisconnected(mDevice, error);
                break;
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        final BleError error = BleStatus.determineStatus(status);
        Logger.d(S2Gatt.LOGGER_TAG, "onServicesDiscovered [status= " + error + "]");
        if (!BleStatus.isError(error)) {
            mCallBackWhenAttributesAreDiscovered.get().run();
        } else {
            Logger.e(S2Gatt.LOGGER_TAG, "Error when discovering services [" + error + "]");
        }
        mDelegate.onGattcServicesDiscovered(mDevice, error);
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        final BleError error = BleStatus.determineStatus(status);
        Logger.d(S2Gatt.LOGGER_TAG, "onCharacteristicRead [status= " + error + "]");
        final S2GattCharacteristic characteristic1 = new S2GattCharacteristic(characteristic);
        if (!BleStatus.isError(error)) {
            mAttributeManagers.updateCharacteristic(characteristic1);
        } else {
            Logger.e(S2Gatt.LOGGER_TAG, "Error during characteristic read operation [" + error + "]");
        }
        mDelegate.onGattcCharacteristicRead(characteristic1, error);
    }

    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        final BleError error = BleStatus.determineStatus(status);
        Logger.d(S2Gatt.LOGGER_TAG, "onCharacteristicWrite [status= " + error + "]");
        final S2GattCharacteristic characteristic1 = new S2GattCharacteristic(characteristic);
        if (!BleStatus.isError(error)) {
            mAttributeManagers.updateCharacteristic(characteristic1);
        } else {
            Logger.e(S2Gatt.LOGGER_TAG, "Error during characteristic write operation [" + error + "]");
        }
        mDelegate.onGattcCharacteristicWritten(characteristic1, error);
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        Logger.d(S2Gatt.LOGGER_TAG, "onCharacteristicChanged [status= " + BleError.OK + "]");
        final S2GattCharacteristic characteristic1 = new S2GattCharacteristic(characteristic);
        mAttributeManagers.updateCharacteristic(characteristic1);
        mDelegate.onGattcCharacteristicUpdated(characteristic1, BleError.OK);
    }

    @Override
    public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        final BleError error = BleStatus.determineStatus(status);
        Logger.d(S2Gatt.LOGGER_TAG, "onDescriptorRead [status= " + error + "]");
        final S2GattDescriptor descriptor1 = new S2GattDescriptor(descriptor);
        if (!BleStatus.isError(error)) {
            mAttributeManagers.updateDescriptor(descriptor1);
        } else {
            Logger.e(S2Gatt.LOGGER_TAG, "Error during descriptor read operation [" + error + "]");
        }
        mDelegate.onGattcDescriptorRead(descriptor1, error);
    }

    @Override
    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        final BleError error = BleStatus.determineStatus(status);
        Logger.d(S2Gatt.LOGGER_TAG, "onDescriptorWrite [status= " + error + "]");
        final S2GattDescriptor descriptor1 = new S2GattDescriptor(descriptor);
        if (!BleStatus.isError(error)) {
            mAttributeManagers.updateDescriptor(descriptor1);
        } else {
            Logger.e(S2Gatt.LOGGER_TAG, "Error during descriptor write operation [" + error + "]");
        }
        mDelegate.onGattcDescriptorWritten(descriptor1, error);
    }

    @Override
    public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
        final BleError error = BleStatus.determineStatus(status);
        Logger.d(S2Gatt.LOGGER_TAG, "onReliableWriteCompleted [status= " + error + "]");
        if (BleStatus.isError(error)) {
            Logger.e(S2Gatt.LOGGER_TAG, "Error during reliable write transaction [" + error + "]");
        }
    }

    @Override
    public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
        final BleError error = BleStatus.determineStatus(status);
        Logger.d(S2Gatt.LOGGER_TAG, "onReadRemoteRssi [status= " + error + "]");
        if (!BleStatus.isError(error)) {
            mDevice.setRssi(rssi);
        } else {
            Logger.e(S2Gatt.LOGGER_TAG, "Error during RSSI read [" + error + "]");
        }
        mDelegate.onDeviceRssiUpdated(mDevice, error);
    }

    @Override
    public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
        final BleError error = BleStatus.determineStatus(status);
        Logger.d(S2Gatt.LOGGER_TAG, "onMtuChanged [status= " + error + "]");
        if (!BleStatus.isError(error)) {
            mDevice.setMtu(mtu);
        } else {
            Logger.e(S2Gatt.LOGGER_TAG, "Error during when MTU changed [" + error + "]");
        }
        mDelegate.onGattcMtuUpdated(mDevice, error);
    }
}
