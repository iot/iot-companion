/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.manager;

import com.arm.song2.api.BleDevice;
import com.arm.song2.api.BleError;
import com.arm.song2.api.GattcService;
import com.arm.song2.transport.bluetooth.S2BleDevice;
import com.arm.song2.transport.bluetooth.attribute.S2GattAttributeId;
import com.arm.song2.transport.bluetooth.attribute.S2GattService;
import com.arm.song2.transport.bluetooth.attribute.S2GattUUID;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class S2ServiceManager extends AbstractManager<S2GattService> implements IS2Manager {
    private final ConcurrentMap<S2GattService, S2BleDevice> mMapping;


    public S2ServiceManager() {
        super();
        mMapping = new ConcurrentHashMap<>();
    }

    @Override
    protected S2GattAttributeId determineAttributeId(Object attribute) {
        if (attribute == null || !(attribute instanceof GattcService)) {
            return null;
        }
        final GattcService theAttribute = (GattcService) attribute;
        return new S2GattAttributeId(theAttribute.uuid(), theAttribute.identifier());
    }

    public S2BleDevice getDeviceForGattcService(GattcService service) {
        final S2GattService correspondingService = fetchAttribute(service);
        return correspondingService == null ? null : mMapping.get(correspondingService);
    }

    public BleError registerService(S2GattService service, S2BleDevice device) {
        if (device == null || !device.isValid()) {
            return BleError.UNKNOWN_DEVICE;
        }
        if (service == null || !service.isValid()) {
            return BleError.UNKNOWN_GATTC_SERVICE;
        }
        synchronized (this) {
            registerNewAttribute(service);
            mMapping.put(service, device);
        }
        return BleError.OK;
    }

    public BleError registerServices(List<S2GattService> services, S2BleDevice device) {
        if (services == null) {
            return BleError.UNKNOWN_GATTC_SERVICE;
        }
        for (final S2GattService service : services) {
            final BleError status = registerService(service, device);
            if (status != BleError.OK) {
                return status;
            }
        }
        return BleError.OK;
    }

    public void deregisterService(GattcService service) {
        final S2GattService correspondingService = fetchAttribute(service);
        if (correspondingService != null) {
            synchronized (this) {
                deregisterAttribute(correspondingService);
                mMapping.remove(correspondingService);
            }
        }
    }

    @Override
    public void reset() {
        super.reset();
        mMapping.clear();
    }
}
