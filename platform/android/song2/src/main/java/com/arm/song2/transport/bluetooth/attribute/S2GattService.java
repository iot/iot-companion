/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.attribute;

import android.bluetooth.BluetoothGattService;

import com.arm.song2.api.GattcCharacteristic;
import com.arm.song2.api.GattcService;

import java.util.ArrayList;
import java.util.List;

public class S2GattService extends S2GattAttribute implements GattcService {

    private final BluetoothGattService mNativeService;

    public S2GattService(BluetoothGattService service) {
        super(service == null ? null : service.getUuid(), service == null ? 0 : service.getInstanceId());
        mNativeService = service;
    }

    @Override
    public ArrayList<GattcCharacteristic> characteristics() {
        return S2GattCharacteristic.translateCharacteristics(isValid() ? mNativeService.getCharacteristics() : new ArrayList<>());
    }

    @Override
    public int identifier() {
        return mId.getInstanceId();
    }

    @Override
    public boolean isValid() {
        return mNativeService != null && isUuidValid();
    }

    public static ArrayList<GattcService> translateServices(List<BluetoothGattService> services) {
        if (services == null || services.isEmpty()) {
            return new ArrayList<>();
        }
        final ArrayList<GattcService> translatedServices = new ArrayList<>(services.size());
        for (final BluetoothGattService service : services) {
            translatedServices.add(new S2GattService(service));
        }
        return translatedServices;
    }

    @Override
    public String toString() {
        return "S2GattService{" + mId.toString() +
                '}';
    }
}
