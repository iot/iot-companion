/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.manager;

import com.arm.song2.api.GattcService;
import com.arm.song2.transport.bluetooth.attribute.S2GattAttribute;
import com.arm.song2.transport.bluetooth.attribute.S2GattAttributeId;
import com.arm.song2.transport.bluetooth.attribute.S2GattService;
import com.arm.song2.transport.bluetooth.attribute.S2GattUUID;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public abstract class AbstractManager<T extends S2GattAttribute> implements IS2Manager {

    private final ConcurrentMap<S2GattAttributeId, T> mAttributes;

    public AbstractManager() {
        mAttributes = new ConcurrentHashMap<>();
    }

    public T fetchAttribute(Object attribute) {
        if (attribute == null) {
            return null;
        }
        if (attribute instanceof S2GattAttribute) {
            final T theAttribute = (T) attribute;
            if (theAttribute.isValid()) {
                return theAttribute;
            }
            return fetch(theAttribute.getId());
        }
        return fetch(determineAttributeId(attribute));
    }

    protected T fetch(S2GattAttributeId id) {
        return id == null || !id.isValid() ? null : mAttributes.get(id);
    }

    protected void registerNewAttribute(T attribute) {
        if (attribute == null || !attribute.isValid()) {
            return;
        }
        mAttributes.putIfAbsent(attribute.getId(), attribute);
    }

    protected void updateAttribute(T attribute) {
        if (attribute == null || !attribute.isValid()) {
            return;
        }
        mAttributes.replace(attribute.getId(), attribute);
    }

    protected void deregisterAttribute(T attribute) {
        if (attribute == null || !attribute.isValid()) {
            return;
        }
        mAttributes.remove(attribute.getId());
    }

    protected abstract S2GattAttributeId determineAttributeId(Object attribute);

    @Override
    public void reset() {
        mAttributes.clear();
    }
}
