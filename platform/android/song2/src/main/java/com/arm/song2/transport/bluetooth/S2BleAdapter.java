/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth;

import com.arm.song2.Song2;
import com.arm.song2.api.BleAdapter;
import com.arm.song2.api.BleAdapterDelegate;
import com.arm.song2.api.BleAdapterStatus;
import com.arm.song2.api.BleDevice;
import com.arm.song2.api.BleDeviceConnectionFlags;
import com.arm.song2.api.BleError;
import com.arm.song2.api.BleScanFlags;
import com.arm.song2.api.Dispatcher;
import com.arm.song2.api.GattcCharacteristic;
import com.arm.song2.api.GattcDescriptor;
import com.arm.song2.api.GattcService;
import com.arm.song2.transport.bluetooth.attribute.S2GattUUID;
import com.arm.song2.transport.bluetooth.manager.S2BleDeviceManager;
import com.arm.song2.transport.bluetooth.utils.BleUtils;
import com.arm.song2.utils.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;

public class S2BleAdapter implements BleAdapter, Closeable {
    private final Dispatcher mDispatcher;
    private final S2BleDeviceManager mDeviceManager;


    public S2BleAdapter(Dispatcher mDispatcher) {
        this.mDispatcher = mDispatcher;
        if (mDispatcher != null) {
            this.mDispatcher.start();
        }
        mDeviceManager = new S2BleDeviceManager();
    }

    public S2BleDeviceManager getDeviceManager() {
        return mDeviceManager;
    }

    @Override
    public Dispatcher getDispatcher() {
        return mDispatcher;
    }

    @Override
    public void setDelegate(BleAdapterDelegate delegate) {
        mDeviceManager.setAdapterDelegate(delegate);
    }

    @Override
    public BleAdapterStatus getStatus() {
        if (!BleUtils.isBLESupported()) {
            return BleAdapterStatus.NO_CONTROLLER;
        }
        if (!BleUtils.isBluetoothEnabled() || !BleUtils.isLocationPermissionGranted()) {
            return BleAdapterStatus.PERMISSION_REQUIRED;
        }
        return mDeviceManager.getStatus();
    }

    @Override
    public BleError startScanning(ArrayList<byte[]> servicesUuids, EnumSet<BleScanFlags> flags) {
        return mDeviceManager.startScanning(S2GattUUID.uuidsFromList(servicesUuids), flags);
    }

    @Override
    public BleError stopScanning() {
        return mDeviceManager.stopScanning();
    }

    @Override
    public void connectDevice(BleDevice device, EnumSet<BleDeviceConnectionFlags> flags) {
        mDeviceManager.connectDevice(device, flags);
    }

    @Override
    public void disconnectDevice(BleDevice device) {
        mDeviceManager.disconnectDevice(device);
    }

    @Override
    public void getDeviceRssi(BleDevice device) {
        mDeviceManager.getDeviceRssi(device);
    }

    @Override
    public BleDevice getDeviceForGattcService(GattcService service) {
        return mDeviceManager.getDeviceForGattcService(service);
    }

    @Override
    public GattcService getGattcServiceForCharacteristic(GattcCharacteristic characteristic) {
        return mDeviceManager.getGattcServiceForCharacteristic(characteristic);
    }

    @Override
    public GattcCharacteristic getGattcCharacteristicForDescriptor(GattcDescriptor descriptor) {
        return mDeviceManager.getGattcCharacteristicForDescriptor(descriptor);
    }

    @Override
    public void gattcDiscoverServices(BleDevice device, ArrayList<byte[]> serviceUuids) {
        mDeviceManager.gattcDiscoverServices(device, S2GattUUID.uuidsFromList(serviceUuids));
    }

    @Override
    public void gattcDiscoverCharacteristics(GattcService service, ArrayList<byte[]> characteristicsUuids) {
        mDeviceManager.gattcDiscoverCharacteristics(service, S2GattUUID.uuidsFromList(characteristicsUuids));
    }

    @Override
    public void gattcDiscoverDescriptors(GattcCharacteristic characteristic) {
        mDeviceManager.gattcDiscoverDescriptors(characteristic);
    }

    @Override
    public void gattcCharacteristicWrite(GattcCharacteristic characteristic, byte[] value, boolean withResponse) {
        mDeviceManager.gattcCharacteristicWrite(characteristic, value, withResponse);
    }

    @Override
    public void gattcCharacteristicRead(GattcCharacteristic characteristic) {
        mDeviceManager.gattcCharacteristicRead(characteristic);
    }

    @Override
    public void gattcCharacteristicSetNotifications(GattcCharacteristic characteristic, boolean enable) {
        mDeviceManager.gattcCharacteristicSetNotifications(characteristic, enable);
    }

    @Override
    public void gattcDescriptorWrite(GattcDescriptor descriptor, byte[] value) {
        mDeviceManager.gattcDescriptorWrite(descriptor, value);
    }

    @Override
    public void gattcDescriptorRead(GattcDescriptor descriptor) {
        mDeviceManager.gattcDescriptorRead(descriptor);
    }

    @Override
    public void gattcRequestMtu(BleDevice device, int mtu) {
        mDeviceManager.gattcRequestMtu(device, mtu);
    }

    @Override
    public void gattcGetPhy(BleDevice device) {
        mDeviceManager.gattcGetPhy(device);
    }

    @Override
    public void close() throws IOException {
        Logger.d(Song2.LOGGER_TAG, "Closing BLE Adapter");
        mDeviceManager.reset();
        if (mDispatcher != null) {
            mDispatcher.terminate(false);
        }
    }
}
