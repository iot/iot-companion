/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.utils;

import com.arm.song2.api.BleAdapterDelegate;
import com.arm.song2.api.BleDevice;
import com.arm.song2.api.BleError;
import com.arm.song2.api.BleManufacturerSpecificDataType;
import com.arm.song2.api.BleServicesDataType;
import com.arm.song2.api.GattcCharacteristic;
import com.arm.song2.api.GattcDescriptor;
import com.arm.song2.api.GattcService;

import java.util.ArrayList;

/**
 * Holder of a Ble Adapter delegate. If not defined, it defaults to a No Op delegate which does not performed anything other than printing debug information.
 */
public class BleAdapterDelegateHolder implements BleAdapterDelegate {

    private volatile BleAdapterDelegate mDelegate;

    public BleAdapterDelegateHolder() {
        mDelegate = new NoOpBleAdapterDelegate();
    }

    public synchronized void setImplementation(BleAdapterDelegate delegate) {
        if (delegate != null) {
            mDelegate = delegate;
        }
    }

    public boolean isSameImplementation(BleAdapterDelegate delegate) {
        return BleAdapterDelegateUtils.areSame(mDelegate, delegate);
    }

    public void updateDelegate(BleAdapterDelegate delegate) {
        if (!isSameImplementation(delegate)) {
            setImplementation(delegate);
        }
    }

    @Override
    public void onStatusUpdated() {
        mDelegate.onStatusUpdated();
    }

    @Override
    public void onDeviceDiscovered(BleDevice device, int rssi, ArrayList<byte[]> services, ArrayList<BleServicesDataType> servicesData, BleManufacturerSpecificDataType manufacturerSpecificData) {
        mDelegate.onDeviceDiscovered(device, rssi, services, servicesData, manufacturerSpecificData);
    }

    @Override
    public void onDeviceConnected(BleDevice device, BleError error) {
        mDelegate.onDeviceConnected(device, error);
    }

    @Override
    public void onDeviceDisconnected(BleDevice device, BleError error) {
        mDelegate.onDeviceDisconnected(device, error);
    }

    @Override
    public void onDeviceRssiUpdated(BleDevice device, BleError error) {
        mDelegate.onDeviceRssiUpdated(device, error);
    }

    @Override
    public void onGattcServicesDiscovered(BleDevice device, BleError error) {
        mDelegate.onGattcServicesDiscovered(device, error);
    }

    @Override
    public void onGattcCharacteristicsDiscovered(GattcService service, BleError error) {
        mDelegate.onGattcCharacteristicsDiscovered(service, error);
    }

    @Override
    public void onGattcDescriptorsDiscovered(GattcCharacteristic characteristic, BleError error) {
        mDelegate.onGattcDescriptorsDiscovered(characteristic, error);
    }

    @Override
    public void onGattcCharacteristicWritten(GattcCharacteristic characteristic, BleError error) {
        mDelegate.onGattcCharacteristicWritten(characteristic, error);
    }

    @Override
    public void onGattcCharacteristicRead(GattcCharacteristic characteristic, BleError error) {
        mDelegate.onGattcCharacteristicRead(characteristic, error);
    }

    @Override
    public void onGattcCharacteristicUpdated(GattcCharacteristic characteristic, BleError error) {
        mDelegate.onGattcCharacteristicUpdated(characteristic, error);
    }

    @Override
    public void onGattcCharacteristicNotificationsSet(GattcCharacteristic characteristic, BleError error) {
        mDelegate.onGattcCharacteristicNotificationsSet(characteristic, error);
    }

    @Override
    public void onGattcDescriptorWritten(GattcDescriptor descriptor, BleError error) {
        mDelegate.onGattcDescriptorWritten(descriptor, error);
    }

    @Override
    public void onGattcDescriptorRead(GattcDescriptor descriptor, BleError error) {
        mDelegate.onGattcDescriptorRead(descriptor, error);
    }

    @Override
    public void onGattcMtuUpdated(BleDevice device, BleError error) {
        mDelegate.onGattcMtuUpdated(device, error);
    }

    @Override
    public void onGattcPhyUpdated(BleDevice device, BleError error) {
        mDelegate.onGattcPhyUpdated(device, error);
    }
}
