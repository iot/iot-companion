/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.attribute;

import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;

import com.arm.song2.api.GattcDescriptor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class S2GattDescriptor extends S2GattAttribute implements GattcDescriptor {
    public static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
    private final BluetoothGattDescriptor mNativeDescriptor;

    public S2GattDescriptor(BluetoothGattDescriptor descriptor) {
        super(descriptor == null ? null : descriptor.getUuid(), 0);
        mNativeDescriptor = descriptor;

    }

    @Override
    public int identifier() {
        return mId.getInstanceId();
    }

    @Override
    public byte[] value() {
        return mNativeDescriptor == null ? null : mNativeDescriptor.getValue();
    }

    public void setValue(byte[] value) {
        if (mNativeDescriptor != null) {
            mNativeDescriptor.setValue(value);
        }
    }

    public boolean isClientCharacteristicConfiguration() {
        return isClientCharacteristicConfiguration(mNativeDescriptor);
    }

    @Override
    public boolean isValid() {
        return mNativeDescriptor != null && isUuidValid();
    }

    public BluetoothGattDescriptor getNativeDescriptor() {
        return mNativeDescriptor;
    }

    public static ArrayList<GattcDescriptor> translateDescriptors(List<BluetoothGattDescriptor> descriptors) {
        if (descriptors == null || descriptors.isEmpty()) {
            return new ArrayList<>();
        }
        final ArrayList<GattcDescriptor> translatedDescriptors = new ArrayList<>(descriptors.size());
        for (final BluetoothGattDescriptor descriptor : descriptors) {
            translatedDescriptors.add(translateDescriptor(descriptor));
        }
        return translatedDescriptors;
    }

    public static GattcDescriptor translateDescriptor(BluetoothGattDescriptor descriptor) {
        if (descriptor == null) {
            return null;
        }
        return new S2GattDescriptor(descriptor);
    }

    public static boolean isClientCharacteristicConfiguration(BluetoothGattDescriptor descriptor) {
        if (descriptor == null) {
            return false;
        }
        return UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG).equals(descriptor.getUuid());
    }


    @Override
    public String toString() {
        return "S2GattDescriptor{" + mId.toString() +
                '}';
    }
}
