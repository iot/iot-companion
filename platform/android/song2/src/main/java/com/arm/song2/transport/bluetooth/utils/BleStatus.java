/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.utils;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.le.ScanCallback;

import com.arm.song2.api.BleAdapter;
import com.arm.song2.api.BleAdapterStatus;
import com.arm.song2.api.BleError;

public class BleStatus {

    public static BleError determineStatus(int status) {
        switch (status) {
            /** A GATT operation completed successfully */
            case BluetoothGatt.GATT_SUCCESS:
                return BleError.OK;

            /** GATT read operation is not permitted */
            case BluetoothGatt.GATT_READ_NOT_PERMITTED:
                return BleError.ATT_READ_NOT_PERMITTED;
            /** GATT write operation is not permitted */
            case BluetoothGatt.GATT_WRITE_NOT_PERMITTED:
                return BleError.ATT_WRITE_NOT_PERMITTED;
            /** Insufficient authentication for a given operation */
            case BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION:
                return BleError.ATT_INSUFFICIENT_AUTHENTICATION;
            /** The given request is not supported */
            case BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED:
                return BleError.ATT_NOT_SUPPORTED;
            /** Insufficient encryption for a given operation */
            case BluetoothGatt.GATT_INSUFFICIENT_ENCRYPTION:
                return BleError.ATT_INSUFFICIENT_ENCRYPTION;
            /** A read or write operation was requested with an invalid offset */
            case BluetoothGatt.GATT_INVALID_OFFSET:
                return BleError.ATT_INVALID_OFFSET;
            /** A write operation exceeds the maximum length of the attribute */
            case BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH:
                return BleError.ATT_INVALID_LENGTH;
            /** A remote device connection is congested. */
            case BluetoothGatt.GATT_CONNECTION_CONGESTED:
                return BleError.BUSY;
            /** A GATT operation failed, errors other than the above */
            case BluetoothGatt.GATT_FAILURE:
                return BleError.UNKNOWN_ERROR;
        }
        return BleError.UNKNOWN_ERROR;
    }

    public static boolean isError(BleError error) {
        return error == null || error != BleError.OK;
    }

    public static BleAdapterStatus determineScanStatus(int status) {
        switch (status) {
            case ScanCallback.SCAN_FAILED_ALREADY_STARTED:
            case ScanCallback.SCAN_FAILED_APPLICATION_REGISTRATION_FAILED:
            case ScanCallback.SCAN_FAILED_INTERNAL_ERROR:
                return BleAdapterStatus.ERROR;
            case ScanCallback.SCAN_FAILED_FEATURE_UNSUPPORTED:
                return BleAdapterStatus.NO_CONTROLLER;
        }
        return BleAdapterStatus.DISABLED;

    }

    public static boolean isAdapterWorkingFine(BleAdapterStatus status) {
        switch (status) {
            case SCANNING_FOREGROUND:
            case SCANNING_BACKGROUND:
            case ENABLED:
                return true;
            case ERROR:
            case DISABLED:
            case NO_CONTROLLER:
            case PERMISSION_REQUIRED:
                return false;
        }
        return false;
    }
}


//
//    OK,
//    UNKNOWN_DEVICE,
//    UNKNOWN_GATTC_SERVICE,
//    UNKNOWN_GATTC_CHARACTERISTIC,
//    FAILED_TO_CONNECT,
//    NOT_IMPLEMENTED,
//    INVALID_PARAM,
//    INVALID_STATE,
//    TIMEOUT,
//    PEER_DISCONNECTED,
//    ADAPTER_STATE_CHANGED,
//    BUSY,
//    UNKNOWN_ERROR,
//    NOT_PERMITTED,
//    RESOURCE_EXHAUSTED,
//    CANCELLED,
//    ATT_INSUFFICIENT_AUTHENTICATION,
//    ATT_INSUFFICIENT_AUTHORIZATION,
//    ATT_INSUFFICIENT_ENCRYPTION,
//    ATT_READ_NOT_PERMITTED,
//    ATT_WRITE_NOT_PERMITTED,
//    ATT_INVALID_LENGTH,
//    ATT_INVALID_OFFSET,
//    ATT_NOT_SUPPORTED,
//    ;


