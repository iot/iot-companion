/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.utils;


import com.arm.song2.api.BleAdapterDelegate;
import com.arm.song2.api.BleDevice;
import com.arm.song2.api.BleError;
import com.arm.song2.api.BleManufacturerSpecificDataType;
import com.arm.song2.api.BleServicesDataType;
import com.arm.song2.api.GattcCharacteristic;
import com.arm.song2.api.GattcDescriptor;
import com.arm.song2.api.GattcService;
import com.arm.song2.transport.bluetooth.S2Gatt;
import com.arm.song2.utils.Logger;

import java.util.ArrayList;

// Implementation of a delegate performing no operation.
public class NoOpBleAdapterDelegate implements BleAdapterDelegate {

    public static final String LOGGER_TAG = S2Gatt.LOGGER_TAG + " - No Op BLE adapter delegate";

    private void performNoOp(String event, BleError error) {
        Logger.i(LOGGER_TAG, "Received an event: " + event + " status: " + error);
    }

    @Override
    public void onStatusUpdated() {
        performNoOp("onStatusUpdated", BleError.OK);
    }

    @Override
    public void onDeviceDiscovered(BleDevice device, int rssi, ArrayList<byte[]> services, ArrayList<BleServicesDataType> servicesData, BleManufacturerSpecificDataType manufacturerSpecificData) {
        performNoOp("onDeviceDiscovered", BleError.OK);
    }

    @Override
    public void onDeviceConnected(BleDevice device, BleError error) {
        performNoOp("onDeviceConnected", error);
    }

    @Override
    public void onDeviceDisconnected(BleDevice device, BleError error) {
        performNoOp("onDeviceDisconnected", error);
    }

    @Override
    public void onDeviceRssiUpdated(BleDevice device, BleError error) {
        performNoOp("onDeviceRssiUpdated", error);
    }

    @Override
    public void onGattcServicesDiscovered(BleDevice device, BleError error) {
        performNoOp("onGattcServicesDiscovered", error);
    }

    @Override
    public void onGattcCharacteristicsDiscovered(GattcService service, BleError error) {
        performNoOp("onGattcCharacteristicsDiscovered", error);
    }

    @Override
    public void onGattcDescriptorsDiscovered(GattcCharacteristic characteristic, BleError error) {
        performNoOp("onGattcDescriptorsDiscovered", error);
    }

    @Override
    public void onGattcCharacteristicWritten(GattcCharacteristic characteristic, BleError error) {
        performNoOp("onGattcCharacteristicWritten", error);
    }

    @Override
    public void onGattcCharacteristicRead(GattcCharacteristic characteristic, BleError error) {
        performNoOp("onGattcCharacteristicRead", error);
    }

    @Override
    public void onGattcCharacteristicUpdated(GattcCharacteristic characteristic, BleError error) {
        performNoOp("onGattcCharacteristicUpdated", error);
    }

    @Override
    public void onGattcCharacteristicNotificationsSet(GattcCharacteristic characteristic, BleError error) {
        performNoOp("onGattcCharacteristicNotificationsSet", error);
    }

    @Override
    public void onGattcDescriptorWritten(GattcDescriptor descriptor, BleError error) {
        performNoOp("onGattcDescriptorWritten", error);
    }

    @Override
    public void onGattcDescriptorRead(GattcDescriptor descriptor, BleError error) {
        performNoOp("onGattcDescriptorRead", error);
    }

    @Override
    public void onGattcMtuUpdated(BleDevice device, BleError error) {
        performNoOp("onGattcMtuUpdated", error);
    }

    @Override
    public void onGattcPhyUpdated(BleDevice device, BleError error) {
        performNoOp("onGattcPhyUpdated", error);
    }
}
