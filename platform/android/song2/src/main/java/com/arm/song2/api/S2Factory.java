/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.api;

import com.arm.song2.Song2;
import com.arm.song2.api.PlatformFactory;
import com.arm.song2.api.Dispatcher;
import com.arm.song2.api.PlatformHelper;
import com.arm.song2.api.BleAdapter;
import com.arm.song2.api.NfcAdapter;
import com.arm.song2.api.PlatformBundle;

import com.arm.song2.filemanagement.FileManager;
import com.arm.song2.filemanagement.S2FileManagementException;
import com.arm.song2.threading.S2Dispatcher;
import com.arm.song2.transport.bluetooth.S2BleAdapter;
import com.arm.song2.utils.ContextHolder;
import com.arm.song2.utils.Logger;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class S2Factory implements PlatformFactory, Closeable {
    public static final String MAIN_DISPATCHER_NAME = "main";
    public static final String UNKNOWN_DISPATCHER_NAME = "unknown";
    private final ConcurrentMap<String, Dispatcher> mDispatchers;
    private final S2BleAdapter mBleAdapter;
    private final FileManager mFileManager;

    public S2Factory() {
        mDispatchers = new ConcurrentHashMap<>(4);
        createDispatcher(true, MAIN_DISPATCHER_NAME);
        mBleAdapter = new S2BleAdapter(createDispatcher("BLE"));
        mFileManager = new FileManager();
    }

    @Override
    public Dispatcher getMainDispatcher() {
        Logger.d(Song2.LOGGER_TAG, "Getting main dispatcher");
        return getDispatcher(MAIN_DISPATCHER_NAME);
    }

    public Dispatcher getDispatcher(String name) {
        Logger.d(Song2.LOGGER_TAG, "Getting dispatcher [" + name + "]");
        final String dispatcherName = (name == null) ? UNKNOWN_DISPATCHER_NAME : name;
        return mDispatchers.get(dispatcherName);
    }

    @Override
    public Dispatcher createDispatcher(String name) {
        Logger.d(Song2.LOGGER_TAG, "Creating dispatcher [" + name + "]");
        return createDispatcher(false, name);
    }

    @Override
    public void runMainDispatcher() {
        Logger.d(Song2.LOGGER_TAG, "Starting main dispatcher");
        getMainDispatcher().start();
    }

    @Override
    public PlatformHelper getPlatformHelper() {
        Logger.e(Song2.LOGGER_TAG, "platform helper is not implemented");
        return null;
    }

    @Override
    public BleAdapter getBleAdapter() {
        Logger.d(Song2.LOGGER_TAG, "Getting the BLE adapter");
        return mBleAdapter;
    }

    @Override
    public NfcAdapter getNfcAdapter() {
        Logger.e(Song2.LOGGER_TAG, "NFC adapter is not implemented");
        return null;
    }

    @Override
    public String getResourcePath(PlatformBundle bundleRef, String resourceName) {
        Logger.d(Song2.LOGGER_TAG, "Retrieving [" + resourceName + "].");
        try {
            final File file = mFileManager.downloadFile(resourceName);
            return file.getAbsolutePath();
        } catch (S2FileManagementException e) {
            Logger.e(Song2.LOGGER_TAG, "File [" + resourceName + "] could not be retrieved. Reason: " + e.getMessage());
            return null;
        }
    }

    @Override
    public String getStoragePath() {
        return ContextHolder.getApplicationContext().getFilesDir().getAbsolutePath();
    }

    private Dispatcher createDispatcher(boolean isMain, String name) {
        final String dispatcherName = (name == null) ? UNKNOWN_DISPATCHER_NAME : name;
        final Dispatcher dispatcher = new S2Dispatcher(isMain, dispatcherName);
        mDispatchers.putIfAbsent(dispatcherName, dispatcher);
        return mDispatchers.get(dispatcherName);
    }


    @Override
    public void close() throws IOException {
        Logger.d(Song2.LOGGER_TAG, "Closing Song2 factory");
        mBleAdapter.close();
        mFileManager.close();
        closeDispatchers();
    }

    private synchronized void closeDispatchers() {
        Iterator<Dispatcher> dispatcherIterator = mDispatchers.values().iterator();
        while (dispatcherIterator.hasNext()) {
            dispatcherIterator.next().terminate(true);
        }
        mDispatchers.clear();
    }

}
