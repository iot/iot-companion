/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import com.arm.song2.BuildConfig;

public class AndroidUtils {

    public static boolean isApiAvailable(int minimumLevel) {
        return minimumLevel <= Build.VERSION.SDK_INT;
    }

    public static boolean isOnAndroid() {
        try {
            return !(Build.BRAND.startsWith(Build.UNKNOWN) && Build.DEVICE.startsWith(Build.UNKNOWN) && Build.PRODUCT.startsWith(Build.UNKNOWN));
        } catch (Exception exception) {
            return false;
        }
    }

    public static boolean isDebug() {
        try {
            return BuildConfig.DEBUG;
        } catch (Exception exception) {
            return false;
        }
    }

    public static boolean isPermissionGranted(Context ctx, String permissionType) {
        if (ctx == null || permissionType == null) {
            return false;
        }
        if (isApiAvailable(23)) {
            return ctx.checkSelfPermission(permissionType) == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

}
