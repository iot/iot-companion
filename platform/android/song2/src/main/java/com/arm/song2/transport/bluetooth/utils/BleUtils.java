/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.utils;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.ParcelUuid;

import com.arm.song2.transport.bluetooth.attribute.S2GattUUID;
import com.arm.song2.utils.AndroidUtils;
import com.arm.song2.utils.ContextHolder;

import java.util.ArrayList;
import java.util.List;

public class BleUtils {

    public static boolean isBLESupported(Context ctx) {
        return ctx == null ? false : ctx.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    public static boolean isBLESupported() {
        return isBLESupported(getContext());
    }

    public static Context getContext() {
        return ContextHolder.getApplicationContext();
    }

    public static BluetoothManager fetchManager(Context ctx) {
        return ctx == null ? null : (BluetoothManager) ctx.getSystemService(Context.BLUETOOTH_SERVICE);
    }

    public static BluetoothManager fetchManager() {
        return fetchManager(getContext());
    }

    public static boolean isBluetoothEnabled(BluetoothManager manager) {
        final BluetoothAdapter adapter = fetchAdapter(manager);
        return adapter == null ? false : adapter.isEnabled();
    }

    private static BluetoothAdapter fetchAdapter(BluetoothManager manager) {
        return manager == null ? null : manager.getAdapter();
    }

    public static BluetoothAdapter fetchBleAdapter() {
        return fetchAdapter(fetchManager());
    }

    public static boolean isLocationPermissionGranted(Context ctx) {
        return AndroidUtils.isPermissionGranted(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) || AndroidUtils.isPermissionGranted(ctx, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public static boolean isLocationPermissionGranted() {
        return isLocationPermissionGranted(getContext());
    }

    public static boolean isBluetoothEnabled() {
        return isBluetoothEnabled(fetchManager());
    }

    public static boolean scanBLEDevice(BluetoothManager manager, boolean stop, List<S2GattUUID> uuids, ScanCallback callback) {
        if (manager == null || callback == null) {
            return false;
        }

        final BluetoothAdapter adapter = fetchAdapter(manager);
        if (adapter == null) {
            return false;
        }
        final BluetoothLeScanner scanner = adapter.getBluetoothLeScanner();
        if (scanner == null) {
            return false;
        }

        if (stop) {
            scanner.stopScan(callback);
            return true;
        } else {
            final ScanSettings.Builder scanSettingsBuilder = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
            if (AndroidUtils.isApiAvailable(23)) {
                scanSettingsBuilder.setCallbackType(
                        ScanSettings.CALLBACK_TYPE_ALL_MATCHES);
                //scanSettingsBuilder.setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE);
            }
            if (AndroidUtils.isApiAvailable(26)) {
                scanSettingsBuilder.setLegacy(true);
            }
            final List<ScanFilter> scanFilters = uuids == null || uuids.isEmpty() ? new ArrayList<>() : new ArrayList<>(uuids.size());
            if (uuids != null && !uuids.isEmpty()) {
                for (S2GattUUID uuid : uuids) {
                    if (uuid.isValid()) {
                        scanFilters.add(new ScanFilter.Builder().setServiceUuid(new ParcelUuid(uuid.toUUID())).build());
                    }
                }
            }
            scanner.startScan(scanFilters, scanSettingsBuilder.build(), callback);
            return true;
        }

    }

    public static boolean scanBLEDevice(boolean stop, List<S2GattUUID> uuids, ScanCallback callback) {
        return scanBLEDevice(fetchManager(), stop, uuids, callback);
    }
}
