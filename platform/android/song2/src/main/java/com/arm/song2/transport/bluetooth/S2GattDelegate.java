/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattDescriptor;

import com.arm.song2.api.BleAdapterDelegate;
import com.arm.song2.api.BleDeviceConnectionFlags;
import com.arm.song2.api.BleDeviceStatus;
import com.arm.song2.api.BleError;
import com.arm.song2.api.GattcCharacteristic;
import com.arm.song2.api.GattcService;
import com.arm.song2.transport.bluetooth.attribute.S2GattCharacteristic;
import com.arm.song2.transport.bluetooth.attribute.S2GattDescriptor;
import com.arm.song2.transport.bluetooth.attribute.S2GattService;
import com.arm.song2.transport.bluetooth.attribute.S2GattUUID;
import com.arm.song2.transport.bluetooth.manager.S2BleAttributeManagers;
import com.arm.song2.transport.bluetooth.utils.BleAdapterDelegateHolder;
import com.arm.song2.transport.bluetooth.utils.BlePhyUtils;
import com.arm.song2.transport.bluetooth.utils.BleUtils;
import com.arm.song2.utils.AndroidUtils;
import com.arm.song2.utils.Logger;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class S2GattDelegate implements Closeable {
    private final BleAdapterDelegateHolder mDelegate;
    private final S2BleCallback mCallback;
    private final S2BleDevice mDevice;
    private final S2BleAttributeManagers mAttributeManagers;
    private final Object mlock;

    private volatile BluetoothGatt mNativeBluetoothGatt;


    public S2GattDelegate(BleAdapterDelegateHolder delegate, S2BleDevice device, S2BleAttributeManagers attributeManagers) {
        mDelegate = delegate;
        mDevice = device;
        mAttributeManagers = attributeManagers;
        mCallback = new S2BleCallback(mDelegate, mDevice, mAttributeManagers);
        mlock = new Object();
    }

    public void connect(boolean autoConnect, EnumSet<BleDeviceConnectionFlags> flags) {
        Logger.i(S2Gatt.LOGGER_TAG, "Connecting to " + mDevice);
        try {
            synchronized (mlock) {
                close();
                if (mDevice.isValid()) {
                    mDevice.setStatus(BleDeviceStatus.CONNECTING);
                    mNativeBluetoothGatt = AndroidUtils.isApiAvailable(26) ? mDevice.getNative().connectGatt(BleUtils.getContext(), autoConnect, mCallback, BluetoothDevice.TRANSPORT_AUTO, BlePhyUtils.phy(flags)) :
                            mDevice.getNative().connectGatt(BleUtils.getContext(), autoConnect, mCallback);
                    mCallback.setCallBackWhenAttributesAreDiscovered(defineRegisterAttributesCallback());
                } else {
                    mDevice.setStatus(BleDeviceStatus.DISCONNECTED);
                    mNativeBluetoothGatt = null;
                }
                if (isClosed()) {
                    throw new Exception("Invalid device");
                }
            }
        } catch (Exception e) {
            Logger.e(S2Gatt.LOGGER_TAG, "Failed GATT connection. Reason: " + e.getMessage());
            mDelegate.onDeviceConnected(mDevice, BleError.FAILED_TO_CONNECT);
        }
    }


    public void disconnect() {
        Logger.d(S2Gatt.LOGGER_TAG, "Disconnecting from " + mDevice);
        try {
            synchronized (mlock) {
                if (!isClosed()) {
                    mNativeBluetoothGatt.disconnect();
                }
                close();
            }
        } catch (Exception e) {
            Logger.e(S2Gatt.LOGGER_TAG, "Failed GATT disconnection. Reason: " + e.getMessage());
            mDelegate.onDeviceDisconnected(mDevice, BleError.UNKNOWN_ERROR);
        }
    }

    public ArrayList<GattcService> listOfferedServices() {
        Logger.d(S2Gatt.LOGGER_TAG, "Listing services offered by " + mDevice);
        synchronized (mlock) {
            if (checkInvalidState("Failed fetching offered services")) {
                mDelegate.onGattcServicesDiscovered(mDevice, BleError.INVALID_STATE);
                return new ArrayList<>();
            }
            final ArrayList<GattcService> list = S2GattService.translateServices(mNativeBluetoothGatt.getServices());
            return list;
        }

    }

    //Callback to apply after service discovery is carried out
    private Runnable defineRegisterAttributesCallback() {
        return new Runnable() {
            @Override
            public void run() {
                updateBleDeviceServices();
            }
        };
    }

    public void updateBleDeviceServices() {
        Logger.d(S2Gatt.LOGGER_TAG, "Updating the list of services offered by " + mDevice);
        mAttributeManagers.registerAttributes(mDevice, listOfferedServices());
    }

    public void discoverCharacteristics(GattcService service, List<S2GattUUID> characteristicsUuids) {
        Logger.d(S2Gatt.LOGGER_TAG, "Discovering characteristics of " + mDevice);
        updateBleDeviceServices();
        // characteristics should have been discovered on service discovery
        mDelegate.onGattcCharacteristicsDiscovered(service, BleError.OK);
    }

    public void discoverDescriptors(GattcCharacteristic characteristic) {
        Logger.d(S2Gatt.LOGGER_TAG, "Discovering descriptors of " + mDevice);
        updateBleDeviceServices();
        // descriptors should have been discovered on service discovery
        mDelegate.onGattcDescriptorsDiscovered(characteristic, BleError.OK);
    }


    public void gattcDiscoverServices(List<S2GattUUID> serviceUuids) {
        Logger.d(S2Gatt.LOGGER_TAG, "Discovering services of" + mDevice);
        if (checkInvalidState("Failed discovering services")) {
            mDelegate.onGattcServicesDiscovered(mDevice, BleError.INVALID_STATE);
            return;
        }
        synchronized (mlock) {
            if (checkInvalidState("Failed discovering services")) {
                mDelegate.onGattcServicesDiscovered(mDevice, BleError.INVALID_STATE);
                return;
            }
            //Cannot filter by service UUIDs
            final boolean success = mNativeBluetoothGatt.discoverServices();
            if (!success) {
                Logger.e(S2Gatt.LOGGER_TAG, "Failed discovering services. Reason: Failed starting the remote service discovery");
                mDelegate.onGattcServicesDiscovered(mDevice, BleError.UNKNOWN_ERROR);
            }
        }
    }


    public void gattcCharacteristicWrite(S2GattCharacteristic characteristic, byte[] value, boolean withResponse) {
        Logger.d(S2Gatt.LOGGER_TAG, "Writing to characteristic " + characteristic + " [" + mDevice + "]");
        if (characteristic == null || !characteristic.isValid() || checkInvalidState("Failed writing characteristic [" + characteristic + "]")) {
            mDelegate.onGattcCharacteristicWritten(characteristic, BleError.INVALID_STATE);
            return;
        }
        synchronized (mlock) {
            if (checkInvalidState("Failed writing to characteristic [" + characteristic + "]")) {
                mDelegate.onGattcCharacteristicWritten(characteristic, BleError.INVALID_STATE);
                return;
            }
            characteristic.setValue(value);
            final boolean success = mNativeBluetoothGatt.writeCharacteristic(characteristic.getNativeCharacteristic());
            if (!success) {
                Logger.e(S2Gatt.LOGGER_TAG, "Failed writing to characteristic [" + characteristic + "]. Reason: the write operation was not initiated successfully");
                mDelegate.onGattcCharacteristicWritten(characteristic, BleError.UNKNOWN_ERROR);
                return;
            }
            mAttributeManagers.updateCharacteristic(characteristic);
        }
    }


    public void gattcCharacteristicRead(S2GattCharacteristic characteristic) {
        Logger.d(S2Gatt.LOGGER_TAG, "Reading from characteristic " + characteristic + " [" + mDevice + "]");
        if (characteristic == null || !characteristic.isValid() || checkInvalidState("Failed reading characteristic [" + characteristic + "]")) {
            mDelegate.onGattcCharacteristicRead(characteristic, BleError.INVALID_STATE);
            return;
        }
        synchronized (mlock) {
            if (checkInvalidState("Failed reading characteristic [" + characteristic + "]")) {
                mDelegate.onGattcCharacteristicRead(characteristic, BleError.INVALID_STATE);
                return;
            }
            final boolean success = mNativeBluetoothGatt.readCharacteristic(characteristic.getNativeCharacteristic());
            if (!success) {
                Logger.e(S2Gatt.LOGGER_TAG, "Failed reading characteristic [" + characteristic + "]. Reason: the read operation was not initiated successfully");
                mDelegate.onGattcCharacteristicRead(characteristic, BleError.UNKNOWN_ERROR);
            }
        }
    }


    public void gattcCharacteristicSetNotifications(S2GattCharacteristic characteristic, boolean enable) {
        // See https://stackoverflow.com/questions/32184536/enabling-bluetooth-characteristic-notification-in-android-bluetooth-low-energy
        Logger.d(S2Gatt.LOGGER_TAG, (enable ? "Enabling" : "Disabling") + " notification from characteristic " + characteristic + " [" + mDevice + "]");
        if (characteristic == null || !characteristic.isValid() || checkInvalidState("Failed setting notification status for the characteristic [" + characteristic + "]")) {
            mDelegate.onGattcCharacteristicNotificationsSet(characteristic, BleError.INVALID_STATE);
            return;
        }
        synchronized (mlock) {
            if (checkInvalidState("Failed setting notification status for the characteristic[" + characteristic + "]")) {
                mDelegate.onGattcCharacteristicNotificationsSet(characteristic, BleError.INVALID_STATE);
                return;
            }
            final boolean success = mNativeBluetoothGatt.setCharacteristicNotification(characteristic.getNativeCharacteristic(), enable);
            if (!success) {
                Logger.e(S2Gatt.LOGGER_TAG, "Failed setting notification status for the characteristic[" + characteristic + "]. Reason: the requested notification status was not set successfully ");
                mDelegate.onGattcCharacteristicNotificationsSet(characteristic, BleError.UNKNOWN_ERROR);
                return;
            }
            characteristic.setNotificationStatus(enable);
            mAttributeManagers.updateCharacteristic(characteristic);
        }

        // This particular operation happens locally so is executed synchronously
        mDelegate.onGattcCharacteristicNotificationsSet(characteristic, BleError.OK);
    }


    public boolean gattcDescriptorWrite(S2GattDescriptor descriptor, byte[] value) {
        Logger.d(S2Gatt.LOGGER_TAG, "Writing to descriptor " + descriptor + " [" + mDevice + "]");
        if (descriptor == null || !descriptor.isValid() || checkInvalidState("Failed writing descriptor [" + descriptor + "]")) {
            mDelegate.onGattcDescriptorWritten(descriptor, BleError.INVALID_STATE);
            return false;
        }
        synchronized (mlock) {
            if (checkInvalidState("Failed writing descriptor [" + descriptor + "]")) {
                mDelegate.onGattcDescriptorWritten(descriptor, BleError.INVALID_STATE);
                return false;
            }
            descriptor.setValue(value);
            final boolean success = mNativeBluetoothGatt.writeDescriptor(descriptor.getNativeDescriptor());
            if (!success) {
                Logger.e(S2Gatt.LOGGER_TAG, "Failed writing descriptor [" + descriptor + "]. Reason: the write operation was not initiated successfully");
                mDelegate.onGattcDescriptorWritten(descriptor, BleError.UNKNOWN_ERROR);
            } else {
                mAttributeManagers.updateDescriptor(descriptor);
            }
            return success;
        }
    }


    public void gattcDescriptorRead(S2GattDescriptor descriptor) {
        Logger.d(S2Gatt.LOGGER_TAG, "Reading from descriptor " + descriptor + " [" + mDevice + "]");
        if (descriptor == null || !descriptor.isValid() || checkInvalidState("Failed reading descriptor [" + descriptor + "]")) {
            mDelegate.onGattcDescriptorRead(descriptor, BleError.INVALID_STATE);
            return;
        }
        synchronized (mlock) {
            if (checkInvalidState("Failed reading descriptor [" + descriptor + "]")) {
                mDelegate.onGattcDescriptorRead(descriptor, BleError.INVALID_STATE);
                return;
            }
            final boolean success = mNativeBluetoothGatt.readDescriptor(descriptor.getNativeDescriptor());
            if (!success) {
                Logger.e(S2Gatt.LOGGER_TAG, "Failed reading descriptor [" + descriptor + "]. Reason: the read operation was not initiated successfully");
                mDelegate.onGattcDescriptorRead(descriptor, BleError.UNKNOWN_ERROR);
            }
        }
    }


    public void gattcRequestMtu(int mtu) {
        Logger.d(S2Gatt.LOGGER_TAG, "Requesting MTU [" + mDevice + "]");
        if (checkInvalidState("Failed requesting MTU")) {
            mDelegate.onGattcMtuUpdated(mDevice, BleError.INVALID_STATE);
            return;
        }
        synchronized (mlock) {
            if (checkInvalidState("Failed requesting MTU")) {
                mDelegate.onGattcMtuUpdated(mDevice, BleError.INVALID_STATE);
                return;
            }
            final boolean success = mNativeBluetoothGatt.requestMtu(mtu);
            if (!success) {
                Logger.e(S2Gatt.LOGGER_TAG, "Failed requesting MTU. Reason: new MTU value could not be requested");
                mDelegate.onGattcMtuUpdated(mDevice, BleError.UNKNOWN_ERROR);
            }
        }
    }

    public void getDeviceRssi() {
        Logger.d(S2Gatt.LOGGER_TAG, "Requesting RSSI [" + mDevice + "]");
        if (checkInvalidState("Failed requesting getting RSSI")) {
            mDelegate.onDeviceRssiUpdated(mDevice, BleError.INVALID_STATE);
            return;
        }
        synchronized (mlock) {
            if (checkInvalidState("Failed requesting getting RSSI")) {
                mDelegate.onDeviceRssiUpdated(mDevice, BleError.INVALID_STATE);
                return;
            }
            final boolean success = mNativeBluetoothGatt.readRemoteRssi();
            if (!success) {
                Logger.e(S2Gatt.LOGGER_TAG, "Failed requesting RSSI. Reason: new RSSI value could not be requested");
                mDelegate.onDeviceRssiUpdated(mDevice, BleError.UNKNOWN_ERROR);
            }
        }
    }

    public void gattcGetPhy() {
        Logger.d(S2Gatt.LOGGER_TAG, "Requesting PHY [" + mDevice + "]");
        if (checkInvalidState("Failed requesting getting PHY")) {
            mDelegate.onGattcPhyUpdated(mDevice, BleError.INVALID_STATE);
            return;
        }
        synchronized (mlock) {
            if (checkInvalidState("Failed requesting getting PHY")) {
                return;
            }
            if (AndroidUtils.isApiAvailable(26)) {
                mNativeBluetoothGatt.readPhy();
            } else {
                mDelegate.onGattcPhyUpdated(mDevice, BleError.NOT_IMPLEMENTED);
            }
        }
    }

    @Override
    public void close() throws IOException {
        Logger.d(S2Gatt.LOGGER_TAG, "Closing GATT manager for device " + mDevice);
        synchronized (this) {
            if (isClosed()) {
                return;
            }
            mNativeBluetoothGatt.close();
            mNativeBluetoothGatt = null;
        }
        Logger.d(S2Gatt.LOGGER_TAG, "Manager for device " + mDevice + " is closed");
    }

    public boolean isClosed() {
        return mNativeBluetoothGatt == null;
    }


    public void updateCallback(BleAdapterDelegate callback) {
        if (callback == null) {
            return;
        }
        mDelegate.updateDelegate(callback);
        mCallback.updateDelegate(callback);
    }

    private boolean checkInvalidState(String errorMessage) {
        if (isClosed()) {
            Logger.e(S2Gatt.LOGGER_TAG, errorMessage + ". Reason: The device is not connected");
            return true;
        }
        return false;
    }

    //FIXME remove the following!!
    // Hack to make action wait callbacks to arrive. This should be done in the Song2 state machine rather than here, with appropriated actions performed on Characteristic written or descriptor written callbacks
    private static class ActionBlocker implements Runnable {
        private final AtomicBoolean mShouldContinue;

        public ActionBlocker() {
            mShouldContinue = new AtomicBoolean(false);
        }

        public synchronized void block() {
            Logger.d(S2Gatt.LOGGER_TAG, "Blocking until told otherwise");
            try {
                while (!mShouldContinue.get()) {
                    wait(100);
                }
            } catch (InterruptedException e) {
                Logger.d(S2Gatt.LOGGER_TAG, "Unblocking due to timeout " + e.getMessage());
            }
        }

        public void reset() {
            mShouldContinue.set(false);
        }

        @Override
        public synchronized void run() {
            Logger.d(S2Gatt.LOGGER_TAG, "Unblocking");
            mShouldContinue.set(true);
            notifyAll();
        }
    }

}
