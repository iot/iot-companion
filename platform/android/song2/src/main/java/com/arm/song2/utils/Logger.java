/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.utils;

import android.util.Log;

import com.arm.song2.BuildConfig;


public class Logger {
    private static final Song2Logger logger = LoggerFactory.get();

    public static void e(String tag, String message) {
        logger.e(tag, message);
    }


    public static void w(String tag, String message) {
        logger.w(tag, message);
    }


    public static void i(String tag, String message) {
        logger.i(tag, message);
    }


    public static void v(String tag, String message) {
        logger.v(tag, message);
    }

    public static void d(String tag, String message) {
        logger.d(tag, message);
    }

    public interface Song2Logger {
        void e(String tag, String message);

        void w(String tag, String message);

        void i(String tag, String message);

        void v(String tag, String message);

        void d(String tag, String message);
    }

    private static class UnitTestLogger implements Song2Logger {
        public enum Level {
            ERROR, WARNING, INFO, DEBUG, VERBOSE;
        }

        private void print(Level type, String tag, String message) {
            StringBuilder builder = new StringBuilder();
            builder.append("[").append(type == null ? "Log" : type).append("]: (").append(tag == null ? "Song2" : tag).append(") ").append(message);
            System.out.println(builder.toString());
        }

        @Override
        public void e(String tag, String message) {
            print(Level.ERROR, tag, message);
        }

        @Override
        public void w(String tag, String message) {
            print(Level.WARNING, tag, message);
        }

        @Override
        public void i(String tag, String message) {
            print(Level.INFO, tag, message);
        }

        @Override
        public void v(String tag, String message) {
            print(Level.VERBOSE, tag, message);
        }

        @Override
        public void d(String tag, String message) {
            print(Level.DEBUG, tag, message);
        }
    }

    private static class NoOpLogger implements Song2Logger {

        @Override
        public void e(String tag, String message) {

        }

        @Override
        public void w(String tag, String message) {

        }

        @Override
        public void i(String tag, String message) {

        }

        @Override
        public void v(String tag, String message) {

        }

        @Override
        public void d(String tag, String message) {

        }
    }

    private static class AndroidLogger implements Song2Logger {
        @Override
        public void e(String tag, String message) {
            Log.e(tag, message);
        }

        @Override
        public void w(String tag, String message) {
            Log.w(tag, message);
        }

        @Override
        public void v(String tag, String message) {
            Log.v(tag, message);
        }

        @Override
        public void i(String tag, String message) {
            Log.i(tag, message);
        }

        @Override
        public void d(String tag, String message) {
            Log.d(tag, message);
        }
    }

    private static class LoggerFactory {
        private LoggerFactory() {
        }

        public static Song2Logger get() {
            try {
                if (AndroidUtils.isOnAndroid()) {
                    if (AndroidUtils.isDebug()) {
                        Log.i("Song2", "Initialising the logger");
                        return new AndroidLogger();
                    }
                    return new NoOpLogger();
                }
                return new UnitTestLogger();
            } catch (Exception exception) {
                return new NoOpLogger();
            }
        }

    }
}
