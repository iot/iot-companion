/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.attribute;

import java.util.Objects;
import java.util.UUID;

public abstract class S2GattAttribute {
    protected final S2GattAttributeId mId;

    public S2GattAttribute(UUID uuid, int instanceId) {
        mId = new S2GattAttributeId(uuid, instanceId);
    }

    public byte[] uuid() {
        return mId.uuid();
    }

    public S2GattUUID getUuid() {
        return mId.getUuid();
    }

    public S2GattAttributeId getId() {
        return mId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof S2GattAttribute)) return false;
        S2GattAttribute that = (S2GattAttribute) o;
        return Objects.equals(mId, that.mId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mId);
    }

    public boolean isUuidValid() {
        return mId.isUuidValid();
    }

    public abstract boolean isValid();
}
