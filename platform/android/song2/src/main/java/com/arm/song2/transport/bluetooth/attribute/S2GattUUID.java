/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.attribute;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class S2GattUUID {

    public static final UUID BLUETOOTH_BASE_UUID = UUID.fromString("00000000-0000-1000-8000-00805F9B34FB");

    private final byte[] mBytes;

    public S2GattUUID(byte[] bytes) {
        mBytes = bytes;
    }

    public S2GattUUID(UUID uuid) {
        this(toBytes(uuid));
    }

    public UUID toUUID() {
        return fromBytes(mBytes);
    }

    public byte[] toBytes() {
        return mBytes;
    }

    public boolean isValid() {
        return mBytes != null && mBytes.length > 0;
    }

    public static List<UUID> uuidsAsList(List<S2GattUUID> ids) {
        if (ids == null) {
            return null;
        }
        final List<UUID> uuids = new ArrayList<>(ids.size());
        for (final S2GattUUID id : ids) {
            final UUID uuid = id.toUUID();
            if (uuid != null) {
                uuids.add(uuid);
            }
        }
        return uuids;
    }

    public static boolean containsBluetoothBaseUUID(UUID uuid) {
        if (uuid == null) {
            return false;
        }
        if (BLUETOOTH_BASE_UUID.getLeastSignificantBits() != uuid.getLeastSignificantBits()) {
            return false;
        }
        final ByteBuffer buffer = ByteBuffer.wrap(new byte[8]);
        buffer.putLong(uuid.getMostSignificantBits());
        buffer.rewind();
        buffer.getInt();
        buffer.getShort();
        return buffer.getShort() == (short) 4096;
    }

    public static ArrayList<byte[]> uuidsAsByteList(List<S2GattUUID> ids) {
        if (ids == null) {
            return null;
        }
        final ArrayList<byte[]> uuids = new ArrayList<>(ids.size());
        for (final S2GattUUID id : ids) {
            final byte[] uuid = id.toBytes();
            if (uuid != null) {
                uuids.add(uuid);
            }
        }
        return uuids;
    }

    public static List<S2GattUUID> uuidsFromList(List<byte[]> ids) {
        if (ids == null) {
            return null;
        }
        final List<S2GattUUID> uuids = new ArrayList<>(ids.size());
        for (final byte[] id : ids) {
            final S2GattUUID uuid = new S2GattUUID(id);
            if (uuid.isValid()) {
                uuids.add(uuid);
            }
        }
        return uuids;
    }


    public static UUID fromBytes(byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        ByteBuffer bb = null;
        //The initial order of a byte buffer is always BIG_ENDIAN. See https://docs.oracle.com/javase/7/docs/api/java/nio/ByteBuffer.html
        if (bytes.length < 16) {
            // https://stackoverflow.com/questions/36212020/how-can-i-convert-a-bluetooth-16-bit-service-uuid-into-a-128-bit-uuid/36212021
            bb = ByteBuffer.wrap(toBytes(BLUETOOTH_BASE_UUID, false));
            bb.rewind();
            if (bytes.length == 2) {
                bb.putShort((short) 0);
            }
            bb.put(bytes);
            bb.rewind();
        } else {
            bb = ByteBuffer.wrap(bytes);
        }
        long mostSignificant = bb.getLong();
        long leastSignificant = bb.getLong();
        return new UUID(mostSignificant, leastSignificant);
    }

    public static byte[] toBytes(UUID uuid) {
        return toBytes(uuid, true);
    }

    public static byte[] toBytes(UUID uuid, boolean reduce) {
        if (uuid == null) {
            return null;
        }
        if (containsBluetoothBaseUUID(uuid) && reduce) {
            final ByteBuffer bb = ByteBuffer.wrap(new byte[8]);
            bb.putLong(uuid.getMostSignificantBits());
            bb.rewind();
            if (bb.getShort() == (short) 0) {
                final ByteBuffer bb16 = ByteBuffer.allocate(2);
                bb16.putShort(bb.getShort());
                return bb16.array();
            } else {
                bb.rewind();
                final ByteBuffer bb32 = ByteBuffer.allocate(4);
                bb32.putInt(bb.getInt());
                return bb32.array();
            }
        }

        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        return bb.array();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof S2GattUUID)) return false;
        final S2GattUUID that = (S2GattUUID) o;
        return Arrays.equals(mBytes, that.mBytes);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(mBytes);
    }

    @Override
    public String toString() {
        return "UUID=" + fromBytes(mBytes).toString();
    }
}
