/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.attribute;

import java.util.Objects;
import java.util.UUID;

public class S2GattAttributeId {
    protected final S2GattUUID mUuid;
    protected final int mIdentifier;

    public S2GattAttributeId(UUID uuid, int instanceId) {
        this.mUuid = new S2GattUUID(uuid);
        this.mIdentifier = instanceId;
    }

    public S2GattAttributeId(byte[] uuid, int instanceId) {
        this(S2GattUUID.fromBytes(uuid), instanceId);
    }

    public byte[] uuid() {
        return mUuid.toBytes();
    }

    public S2GattUUID getUuid() {
        return mUuid;
    }

    public int getInstanceId() {
        return mIdentifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof S2GattAttributeId)) return false;
        S2GattAttributeId that = (S2GattAttributeId) o;
        return Objects.equals(mUuid, that.mUuid) && mIdentifier == that.mIdentifier;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mUuid, mIdentifier);
    }

    public boolean isUuidValid() {
        return mUuid.isValid();
    }

    public boolean isValid() {
        return isUuidValid();
    }

    @Override
    public String toString() {
        return "S2GattAttributeId{" +
                "MUUID=" + mUuid +
                ", ID=" + mIdentifier +
                '}';
    }
}
