/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.manager;

import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;

import com.arm.song2.api.BleAdapterDelegate;
import com.arm.song2.api.BleAdapterStatus;
import com.arm.song2.api.BleDevice;
import com.arm.song2.api.BleDeviceConnectionFlags;
import com.arm.song2.api.BleError;
import com.arm.song2.api.BleScanFlags;
import com.arm.song2.api.GattcCharacteristic;
import com.arm.song2.api.GattcDescriptor;
import com.arm.song2.api.GattcService;
import com.arm.song2.transport.bluetooth.S2BleDevice;
import com.arm.song2.transport.bluetooth.S2GattDelegate;
import com.arm.song2.transport.bluetooth.attribute.S2GattCharacteristic;
import com.arm.song2.transport.bluetooth.attribute.S2GattDescriptor;
import com.arm.song2.transport.bluetooth.attribute.S2GattUUID;
import com.arm.song2.transport.bluetooth.utils.BleAdapterDelegateHolder;
import com.arm.song2.transport.bluetooth.utils.BleStatus;
import com.arm.song2.transport.bluetooth.utils.BleUtils;
import com.arm.song2.utils.AndroidUtils;
import com.arm.song2.utils.Logger;

import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class S2BleDeviceManager implements IS2Manager {
    public static final String LOGGER_TAG = "Song2 Ble Device Manager";
    private final ConcurrentMap<S2BleDevice, S2GattDelegate> mMapping;
    private final ConcurrentMap<String, S2BleDevice> mDevices;

    private final S2BleAttributeManagers mAttributeManagers;
    private final AtomicReference<ScanCallback> mScanCallBack;
    private final BleAdapterDelegateHolder mDelegate;
    private final AtomicBoolean mIsScanning;
    private volatile BleAdapterStatus mStatus;

    public S2BleDeviceManager() {
        mDelegate = new BleAdapterDelegateHolder();
        mMapping = new ConcurrentHashMap<>();
        mDevices = new ConcurrentHashMap<>();
        mScanCallBack = new AtomicReference();
        mAttributeManagers = new S2BleAttributeManagers();
        mIsScanning = new AtomicBoolean(false);
        updateStatus(BleAdapterStatus.ENABLED);
    }

    @Override
    public void reset() {
        Logger.d(LOGGER_TAG, "Resetting device manager ");
        stopScanning();
        synchronized (this) {
            Iterator<S2GattDelegate> delegateIt = mMapping.values().iterator();
            while (delegateIt.hasNext()) {
                delegateIt.next().disconnect();
            }
        }
        mMapping.clear();
        mDevices.clear();
        mAttributeManagers.reset();
    }


    public void setAdapterDelegate(BleAdapterDelegate callback) {
        Logger.d(LOGGER_TAG, "Setting the BLE delegate");
        if (callback == null) {
            return;
        }
        synchronized (this) {
            if (!mDelegate.isSameImplementation(callback)) {
                mDelegate.updateDelegate(callback);
                Iterator<S2GattDelegate> delegateIt = mMapping.values().iterator();
                while (delegateIt.hasNext()) {
                    delegateIt.next().updateCallback(mDelegate);
                }
            }
        }
    }


    public BleAdapterStatus getStatus() {
        //TODO understand the difference between background and foreground scanning for Android
//        ERROR,
//        DISABLED,
//        ENABLED,
//        SCANNING_BACKGROUND,
//        SCANNING_FOREGROUND,
        return mIsScanning.get() ? BleAdapterStatus.SCANNING_BACKGROUND : mStatus;
    }


    public BleError startScanning(List<S2GattUUID> servicesUuids, EnumSet<BleScanFlags> flags) {
        Logger.d(LOGGER_TAG, "Starting scanning for BLE devices");
        mScanCallBack.compareAndSet(null, defineCallBackOnNewDevice());
        if (BleUtils.scanBLEDevice(false, servicesUuids, mScanCallBack.get())) {
            mIsScanning.set(true);
            mDelegate.onStatusUpdated();
            return BleError.OK;
        }
        return BleError.UNKNOWN_ERROR;
    }


    public BleError stopScanning() {
        Logger.d(LOGGER_TAG, "Stopping BLE device scan");
        mScanCallBack.compareAndSet(null, defineCallBackOnNewDevice());
        if (BleUtils.scanBLEDevice(true, null, mScanCallBack.get())) {
            mIsScanning.set(false);
            mDelegate.onStatusUpdated();
            return BleError.OK;
        }
        return BleError.UNKNOWN_ERROR;
    }


    public void connectDevice(BleDevice device, EnumSet<BleDeviceConnectionFlags> flags) {
        final S2GattDelegate gattDelegate = fetchGattDelegate(device);
        if (gattDelegate == null) {
            mDelegate.onDeviceConnected(device, BleError.UNKNOWN_DEVICE);
            return;
        }
        //TODO: Change if the autoconnection is desired by defaults
        gattDelegate.connect(false, flags);
    }


    public void disconnectDevice(BleDevice device) {
        final S2GattDelegate gattDelegate = fetchGattDelegate(device);
        if (gattDelegate == null) {
            mDelegate.onDeviceDisconnected(device, BleError.UNKNOWN_DEVICE);
            return;
        }
        gattDelegate.disconnect();
    }


    public void getDeviceRssi(BleDevice device) {
        final S2GattDelegate gattDelegate = fetchGattDelegate(device);
        if (gattDelegate == null) {
            mDelegate.onDeviceRssiUpdated(device, BleError.UNKNOWN_DEVICE);
            return;
        }
        gattDelegate.getDeviceRssi();
    }


    public BleDevice getDeviceForGattcService(GattcService service) {
        return mAttributeManagers.getDeviceForGattcService(service);
    }


    public GattcService getGattcServiceForCharacteristic(GattcCharacteristic characteristic) {
        return mAttributeManagers.getGattcServiceForCharacteristic(characteristic);
    }


    public GattcCharacteristic getGattcCharacteristicForDescriptor(GattcDescriptor descriptor) {
        return mAttributeManagers.getGattcCharacteristicForDescriptor(descriptor);
    }


    public void gattcDiscoverServices(BleDevice device, List<S2GattUUID> serviceUuids) {
        final S2GattDelegate gattDelegate = fetchGattDelegate(device);
        if (gattDelegate == null) {
            mDelegate.onGattcServicesDiscovered(device, BleError.UNKNOWN_DEVICE);
            return;
        }
        gattDelegate.gattcDiscoverServices(serviceUuids);
    }


    public void gattcDiscoverCharacteristics(GattcService service, List<S2GattUUID> characteristicsUuids) {
        final GattcService registeredService = mAttributeManagers.getServices().fetchAttribute(service);
        final S2GattDelegate gattDelegate = fetchGattDelegate(registeredService);
        if (registeredService == null || gattDelegate == null) {
            mDelegate.onGattcCharacteristicsDiscovered(service, BleError.UNKNOWN_GATTC_SERVICE);
            return;
        }
        gattDelegate.discoverCharacteristics(service, characteristicsUuids);
    }


    public void gattcDiscoverDescriptors(GattcCharacteristic characteristic) {
        final GattcCharacteristic registeredCharacteristic = mAttributeManagers.getCharacteristics().fetchAttribute(characteristic);
        final S2GattDelegate gattDelegate = fetchGattDelegate(registeredCharacteristic);
        if (registeredCharacteristic == null || gattDelegate == null) {
            mDelegate.onGattcDescriptorsDiscovered(characteristic, BleError.UNKNOWN_GATTC_CHARACTERISTIC);
            return;
        }
        gattDelegate.discoverDescriptors(characteristic);
    }


    public void gattcCharacteristicWrite(GattcCharacteristic characteristic, byte[] value, boolean withResponse) {
        final S2GattCharacteristic registeredCharacteristic = mAttributeManagers.getCharacteristics().fetchAttribute(characteristic);
        final S2GattDelegate delegate = fetchGattDelegate(registeredCharacteristic);
        if (registeredCharacteristic == null || delegate == null) {
            mDelegate.onGattcCharacteristicWritten(characteristic, BleError.UNKNOWN_GATTC_CHARACTERISTIC);
            return;
        }
        delegate.gattcCharacteristicWrite(registeredCharacteristic, value, withResponse);
    }


    public void gattcCharacteristicRead(GattcCharacteristic characteristic) {
        final S2GattCharacteristic registeredCharacteristic = mAttributeManagers.getCharacteristics().fetchAttribute(characteristic);
        final S2GattDelegate delegate = fetchGattDelegate(registeredCharacteristic);
        if (registeredCharacteristic == null || delegate == null) {
            mDelegate.onGattcCharacteristicRead(characteristic, BleError.UNKNOWN_GATTC_CHARACTERISTIC);
            return;
        }
        delegate.gattcCharacteristicRead(registeredCharacteristic);
    }


    public void gattcCharacteristicSetNotifications(GattcCharacteristic characteristic, boolean enable) {
        final S2GattCharacteristic registeredCharacteristic = mAttributeManagers.getCharacteristics().fetchAttribute(characteristic);
        final S2GattDelegate delegate = fetchGattDelegate(registeredCharacteristic);
        if (registeredCharacteristic == null || delegate == null) {
            mDelegate.onGattcCharacteristicNotificationsSet(characteristic, BleError.UNKNOWN_GATTC_CHARACTERISTIC);
            return;
        }
        delegate.gattcCharacteristicSetNotifications(registeredCharacteristic, enable);
    }


    public void gattcDescriptorWrite(GattcDescriptor descriptor, byte[] value) {
        final S2GattDescriptor registeredDescriptor = mAttributeManagers.getDescriptors().fetchAttribute(descriptor);
        final S2GattDelegate delegate = fetchGattDelegate(registeredDescriptor);
        if (registeredDescriptor == null || delegate == null) {
            mDelegate.onGattcDescriptorWritten(descriptor, BleError.INVALID_PARAM);
            return;
        }
        delegate.gattcDescriptorWrite(registeredDescriptor, value);
    }


    public void gattcDescriptorRead(GattcDescriptor descriptor) {
        final S2GattDescriptor registeredDescriptor = mAttributeManagers.getDescriptors().fetchAttribute(descriptor);
        final S2GattDelegate delegate = fetchGattDelegate(registeredDescriptor);
        if (registeredDescriptor == null || delegate == null) {
            mDelegate.onGattcDescriptorRead(descriptor, BleError.INVALID_PARAM);
            return;
        }
        delegate.gattcDescriptorRead(registeredDescriptor);
    }


    public void gattcRequestMtu(BleDevice device, int mtu) {
        final S2GattDelegate gattDelegate = fetchGattDelegate(device);
        if (gattDelegate == null) {
            mDelegate.onGattcMtuUpdated(device, BleError.UNKNOWN_DEVICE);
            return;
        }
        gattDelegate.gattcRequestMtu(mtu);
    }


    public void gattcGetPhy(BleDevice device) {
        final S2GattDelegate gattDelegate = fetchGattDelegate(device);
        if (gattDelegate == null) {
            mDelegate.onGattcPhyUpdated(device, BleError.UNKNOWN_DEVICE);
            return;
        }
        gattDelegate.gattcGetPhy();
    }

    private ScanCallback defineCallBackOnNewDevice() {
        return new ScanCallback() {

            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                super.onBatchScanResults(results);
                if (results != null && !results.isEmpty()) {
                    for (final ScanResult result : results) {
                        acknowledgeScanResult(false, result);
                    }
                }
            }

            @Override
            public void onScanFailed(int errorCode) {
                super.onScanFailed(errorCode);
                updateStatus(BleStatus.determineScanStatus(errorCode));
            }

            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                super.onScanResult(callbackType, result);
                boolean isLost = false;
                if (AndroidUtils.isApiAvailable(23)) {
                    isLost = callbackType == ScanSettings.CALLBACK_TYPE_MATCH_LOST;
                }
                acknowledgeScanResult(isLost, result);
            }
        };
    }

    private void updateStatus(BleAdapterStatus bleAdapterStatus) {
        mStatus = bleAdapterStatus;
        if (!BleStatus.isAdapterWorkingFine(mStatus)) {
            reset();
        }
        mDelegate.onStatusUpdated();
    }

    private void acknowledgeScanResult(boolean lost, ScanResult result) {
        if (result == null) {
            return;
        }
        if (lost) {
            deregisterDevice(S2BleDevice.determineFromScanResult(result));
        } else {
            registerNewDevice(S2BleDevice.determineFromScanResult(result));
        }
    }

    private void deregisterDevice(S2BleDevice lostDevice) {
        if (lostDevice == null || !lostDevice.hasId()) {
            return;
        }
        final S2GattDelegate delegate = fetchGattDelegate(lostDevice);
        if (delegate != null) {
            delegate.disconnect();
        }
        synchronized (this) {
            mMapping.remove(lostDevice);
            mDevices.remove(lostDevice.getId());
        }

    }

    public boolean foundDevices() {
        return !mDevices.isEmpty();
    }


    public Set<String> foundDeviceIds() {
        return mDevices.keySet();
    }


    private void registerNewDevice(S2BleDevice foundDevice) {
        if (foundDevice == null || !foundDevice.isValid()) {
            return;
        }
        mMapping.putIfAbsent(foundDevice, new S2GattDelegate(mDelegate, foundDevice, mAttributeManagers));
        mDevices.putIfAbsent(foundDevice.getId(), foundDevice);
        updateRegisteredDevice(foundDevice);
        mDelegate.onDeviceDiscovered(foundDevice, foundDevice.rssi(), foundDevice.getScanResultServicesUuids(), foundDevice.getScanResultServicesData(), foundDevice.getFirstScanResultManufacturerSpecificData());
    }

    private void updateRegisteredDevice(S2BleDevice foundDevice) {
        final S2BleDevice registeredDevice = fetchDevice(foundDevice.getId());
        if (registeredDevice != null) {
            registeredDevice.updateDevice(foundDevice);
        }
    }

    public S2BleDevice fetchDevice(String id) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        return mDevices.get(id);
    }

    private S2GattDelegate fetchGattDelegate(BleDevice device) {
        if (device == null) {
            return null;
        }
        if (device instanceof S2BleDevice) {
            return mMapping.get(device);
        }
        return null;
    }

    private S2GattDelegate fetchGattDelegate(GattcService service) {
        if (service == null) {
            return null;
        }
        final BleDevice device = getDeviceForGattcService(service);
        if (device == null) {
            return null;
        }
        final S2GattDelegate delegate = fetchGattDelegate(device);
        if (delegate == null) {
            //The device has been deregistered and hence, the service should as well
            mAttributeManagers.getServices().deregisterService(service);
        }
        return delegate;
    }

    private S2GattDelegate fetchGattDelegate(GattcCharacteristic characteristic) {
        if (characteristic == null) {
            return null;
        }
        final GattcService service = getGattcServiceForCharacteristic(characteristic);
        if (service == null) {
            return null;
        }
        final S2GattDelegate delegate = fetchGattDelegate(service);
        if (delegate == null) {
            //The device or the service have been deregistered and hence, the characteristic should as well
            mAttributeManagers.getCharacteristics().deregisterCharacteristic(characteristic);
        }
        return delegate;
    }

    private S2GattDelegate fetchGattDelegate(GattcDescriptor descriptor) {
        if (descriptor == null) {
            return null;
        }
        final GattcCharacteristic characteristic = getGattcCharacteristicForDescriptor(descriptor);
        if (characteristic == null) {
            return null;
        }
        final S2GattDelegate delegate = fetchGattDelegate(characteristic);
        if (delegate == null) {
            //The device or the service or the characteristic have been deregistered and hence, the descriptor should as well
            mAttributeManagers.getDescriptors().deregisterDescriptor(descriptor);
        }
        return delegate;
    }
}
