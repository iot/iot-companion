/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.manager;

import com.arm.song2.api.BleDevice;
import com.arm.song2.api.GattcCharacteristic;
import com.arm.song2.api.GattcDescriptor;
import com.arm.song2.api.GattcService;
import com.arm.song2.transport.bluetooth.S2BleDevice;
import com.arm.song2.transport.bluetooth.attribute.S2GattCharacteristic;
import com.arm.song2.transport.bluetooth.attribute.S2GattDescriptor;
import com.arm.song2.transport.bluetooth.attribute.S2GattService;

import java.util.ArrayList;
import java.util.List;

public class S2BleAttributeManagers implements IS2Manager {

    private final S2CharacteristicManager mCharacteristics;
    private final S2ServiceManager mServices;
    private final S2DescriptorManager mDescriptors;

    public S2BleAttributeManagers() {
        super();
        mCharacteristics = new S2CharacteristicManager();
        mServices = new S2ServiceManager();
        mDescriptors = new S2DescriptorManager();
    }

    @Override
    public void reset() {
        mCharacteristics.reset();
        mServices.reset();
        mDescriptors.reset();
    }

    public void registerAttributes(S2BleDevice device, ArrayList<GattcService> services) {
        if (services == null || device == null) {
            return;
        }
        device.setServices(services);
        for (final GattcService service : services) {
            mServices.registerService((S2GattService) service, device);
            for (final GattcCharacteristic characteristic : service.characteristics()) {
                mCharacteristics.registerCharacteristic((S2GattCharacteristic) characteristic, (S2GattService) service);
                for (final GattcDescriptor descriptor : characteristic.descriptors()) {
                    mDescriptors.registerDescriptor((S2GattDescriptor) descriptor, (S2GattCharacteristic) characteristic);
                }
            }
        }
    }

    public S2BleDevice getDeviceForGattcService(GattcService service) {
        return mServices.getDeviceForGattcService(service);
    }

    public S2GattService getGattcServiceForCharacteristic(GattcCharacteristic characteristic) {
        return mCharacteristics.getGattcServiceForCharacteristic(characteristic);
    }

    public S2GattCharacteristic getGattcCharacteristicForDescriptor(GattcDescriptor descriptor) {
        return mDescriptors.getGattcCharacteristicForDescriptor(descriptor);
    }

    public void updateCharacteristic(S2GattCharacteristic characteristic) {
        mCharacteristics.updateAttribute(characteristic);
    }

    public void updateDescriptor(S2GattDescriptor descriptor) {
        mDescriptors.updateAttribute(descriptor);
    }

    public S2CharacteristicManager getCharacteristics() {
        return mCharacteristics;
    }

    public S2ServiceManager getServices() {
        return mServices;
    }

    public S2DescriptorManager getDescriptors() {
        return mDescriptors;
    }
}
