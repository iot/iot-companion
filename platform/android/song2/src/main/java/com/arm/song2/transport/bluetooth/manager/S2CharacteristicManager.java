/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.manager;

import com.arm.song2.api.BleError;
import com.arm.song2.api.GattcCharacteristic;
import com.arm.song2.api.GattcService;
import com.arm.song2.transport.bluetooth.attribute.S2GattAttributeId;
import com.arm.song2.transport.bluetooth.attribute.S2GattCharacteristic;
import com.arm.song2.transport.bluetooth.attribute.S2GattDescriptor;
import com.arm.song2.transport.bluetooth.attribute.S2GattService;
import com.arm.song2.transport.bluetooth.attribute.S2GattUUID;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class S2CharacteristicManager extends AbstractManager<S2GattCharacteristic> implements IS2Manager {
    private final ConcurrentMap<S2GattCharacteristic, S2GattService> mMapping;

    public S2CharacteristicManager() {
        mMapping = new ConcurrentHashMap<>();
    }

    @Override
    protected S2GattAttributeId determineAttributeId(Object attribute) {
        if (attribute == null || !(attribute instanceof GattcCharacteristic)) {
            return null;
        }
        final GattcCharacteristic theAttribute = (GattcCharacteristic) attribute;
        return new S2GattAttributeId(theAttribute.uuid(), theAttribute.identifier());
    }

    public S2GattService getGattcServiceForCharacteristic(GattcCharacteristic characteristic) {
        final S2GattCharacteristic correspondingCharacteristic = fetchAttribute(characteristic);
        return correspondingCharacteristic == null ? null : mMapping.get(correspondingCharacteristic);
    }


    public BleError registerCharacteristic(S2GattCharacteristic characteristic, S2GattService service) {
        if (characteristic == null || !characteristic.isValid()) {
            return BleError.UNKNOWN_GATTC_CHARACTERISTIC;
        }
        if (service == null || !service.isValid()) {
            return BleError.UNKNOWN_GATTC_SERVICE;
        }
        synchronized (this) {
            registerNewAttribute(characteristic);
            mMapping.put(characteristic, service);
        }
        return BleError.OK;
    }

    public BleError registerCharacteristics(List<S2GattCharacteristic> characteristics, S2GattService service) {
        if (characteristics == null) {
            return BleError.UNKNOWN_GATTC_CHARACTERISTIC;
        }
        for (final S2GattCharacteristic characteristic : characteristics) {
            final BleError status = registerCharacteristic(characteristic, service);
            if (status != BleError.OK) {
                return status;
            }
        }
        return BleError.OK;
    }

    public void deregisterCharacteristic(GattcCharacteristic characteristic) {
        final S2GattCharacteristic correspondingCharacteristic = fetchAttribute(characteristic);
        if (correspondingCharacteristic != null) {
            synchronized (this) {
                deregisterAttribute(correspondingCharacteristic);
                mMapping.remove(correspondingCharacteristic);
            }
        }
    }


    @Override
    public void reset() {
        super.reset();
        mMapping.clear();
    }
}
