/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.utils;

public class Base64 {

    public static byte[] encode(byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        if (AndroidUtils.isOnAndroid() && !AndroidUtils.isApiAvailable(26)) {
            return android.util.Base64.encode(bytes, android.util.Base64.NO_WRAP);
        } else {
            return java.util.Base64.getEncoder().encode(bytes);
        }
    }

    public static byte[] decode(byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        if (AndroidUtils.isOnAndroid() && !AndroidUtils.isApiAvailable(26)) {
            return android.util.Base64.decode(bytes, android.util.Base64.NO_WRAP);
        } else {
            return java.util.Base64.getDecoder().decode(bytes);
        }
    }
}
