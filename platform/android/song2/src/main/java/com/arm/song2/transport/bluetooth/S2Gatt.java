/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth;

import com.arm.song2.api.BleDevicePhy;

import java.util.EnumSet;

public interface S2Gatt {
    String LOGGER_TAG = "Song2 GATT service";
    int DEFAULT_MTU = 20;
    EnumSet<BleDevicePhy> DEFAULT_PHY = EnumSet.of(BleDevicePhy.PHY_1M);

}
