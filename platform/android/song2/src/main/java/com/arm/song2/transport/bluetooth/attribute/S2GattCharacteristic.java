/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2.transport.bluetooth.attribute;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;

import com.arm.song2.api.GattcCharacteristic;
import com.arm.song2.api.GattcDescriptor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

public class S2GattCharacteristic extends S2GattAttribute implements GattcCharacteristic {
    private final BluetoothGattCharacteristic mNativeCharacteristic;
    private final AtomicBoolean mNotificationStatus;


    public S2GattCharacteristic(BluetoothGattCharacteristic characteristic) {
        super(characteristic == null ? null : characteristic.getUuid(), characteristic == null ? 0 : characteristic.getInstanceId());
        mNotificationStatus = new AtomicBoolean(false);
        mNativeCharacteristic = characteristic;
    }

    @Override
    public int identifier() {
        return mId.getInstanceId();
    }

    @Override
    public byte[] value() {
        return mNativeCharacteristic == null ? null : mNativeCharacteristic.getValue();
    }

    public void setValue(byte[] value) {
        if (mNativeCharacteristic != null) {
            mNativeCharacteristic.setValue(value);
        }
    }

    public BluetoothGattCharacteristic getNativeCharacteristic() {
        return mNativeCharacteristic;
    }


    @Override
    public ArrayList<GattcDescriptor> descriptors() {
        return S2GattDescriptor.translateDescriptors(isValid() ? mNativeCharacteristic.getDescriptors() : new ArrayList<>());
    }

    @Override
    public boolean notifications() {
        return mNotificationStatus.get();
    }

    @Override
    public boolean isValid() {
        return mNativeCharacteristic != null && isUuidValid();
    }

    public void setNotificationStatus(boolean enabled) {
        mNotificationStatus.set(enabled);
    }

    public GattcDescriptor getClientCharacteristicConfigurationDescriptor() {
        return mNativeCharacteristic == null ? null : S2GattDescriptor.translateDescriptor(mNativeCharacteristic.getDescriptor(UUID.fromString(S2GattDescriptor.CLIENT_CHARACTERISTIC_CONFIG)));
    }


    public static ArrayList<GattcCharacteristic> translateCharacteristics(List<BluetoothGattCharacteristic> characteristics) {
        if (characteristics == null || characteristics.isEmpty()) {
            return new ArrayList<>();
        }
        final ArrayList<GattcCharacteristic> translatedCharacteristics = new ArrayList<>(characteristics.size());
        for (final BluetoothGattCharacteristic characteristic : characteristics) {
            translatedCharacteristics.add(new S2GattCharacteristic(characteristic));
        }
        return translatedCharacteristics;
    }

    @Override
    public String toString() {
        return "S2GattCharacteristic{" + mId.toString() +
                ", notifications=" + mNotificationStatus.get() +
                '}';
    }
}
