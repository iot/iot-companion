//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <jni.h>
#include <string>
#include <fstream>
#include <streambuf>
#include <android/log.h>
#include <exception>      // std::exception_ptr, std::current_exception, std::rethrow_exception
#include <stdexcept>      // std::logic_error

#define  LOG_TAG    "Song2 native"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)

std::string jstring2string(JNIEnv *env, jstring jStr);

extern "C" JNIEXPORT jstring JNICALL
Java_com_arm_song2_utils_CheckNative_fetchTestStringFromJNI(JNIEnv *env, jobject instance) {

    LOGI("Hello from native code!");
    std::string testString = "The JNI link works properly.";
    return env->NewStringUTF(testString.c_str());

}

extern "C" JNIEXPORT jstring JNICALL
Java_com_arm_song2_utils_CheckNative_handleCppException(JNIEnv *env, jobject instance) {
    // Running example http://www.cplusplus.com/reference/exception/rethrow_exception/

    LOGI("Checking that exceptions are correctly handled");
    std::string testString = "No exception caught";

    std::exception_ptr p;
    try {
        throw std::logic_error("some logic_error exception");   // throws
    } catch (const std::exception &e) {
        p = std::current_exception();
        LOGI("Exception caught properly");
    }
    try {
        LOGI("Testing exception rethrow");
        std::rethrow_exception(p);
    } catch (const std::exception &e) {
        LOGI("Rethrown exception caught properly: %s", e.what());
        testString = e.what();
    }
    return env->NewStringUTF(testString.c_str());

}

extern "C" JNIEXPORT jstring JNICALL
Java_com_arm_song2_utils_CheckNative_readFile(JNIEnv *env, jobject instance, jstring path) {

    LOGI("Hello from native code!");
    LOGI("About to read a file from the internal storage");
    std::string filePath = jstring2string(env, path);
    LOGI("Requested file is: %s", filePath.c_str());

    std::ifstream filestream(filePath);
    std::string fileContent;

    filestream.seekg(0, std::ios::end);
    fileContent.reserve(filestream.tellg());
    filestream.seekg(0, std::ios::beg);

    fileContent.assign((std::istreambuf_iterator<char>(filestream)),
                       std::istreambuf_iterator<char>());

    if (filestream.is_open()) {
        filestream.close();
    }
    LOGI("Read the file from the internal storage");
    LOGI("Its content is: %s", fileContent.c_str());
    return env->NewStringUTF(fileContent.c_str());
}

std::string jstring2string(JNIEnv *env, jstring jStr) {
    if (!jStr)
        return "";

    const jclass stringClass = env->GetObjectClass(jStr);
    const jmethodID getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    const jbyteArray stringJbytes = (jbyteArray) env->CallObjectMethod(jStr, getBytes,
                                                                       env->NewStringUTF("UTF-8"));

    size_t length = (size_t) env->GetArrayLength(stringJbytes);
    jbyte *pBytes = env->GetByteArrayElements(stringJbytes, NULL);

    std::string ret = std::string((char *) pBytes, length);
    env->ReleaseByteArrayElements(stringJbytes, pBytes, JNI_ABORT);

    env->DeleteLocalRef(stringJbytes);
    env->DeleteLocalRef(stringClass);
    return ret;
}


