/*
 * Mbed - IoT Companion
 *
 * Copyright 2021 Arm Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arm.song2_tests;

import android.app.Activity;
import android.os.Handler;
import android.os.Bundle;

import com.arm.song2.Song2Lib;
import com.arm.song2.api.Daemon;
import com.arm.song2.api.S2Factory;

import java.util.ArrayList;

public class MainActivity extends Activity {
    Thread mThread;
    Handler mHandler = new Handler();
    S2Factory mFactory;

    void close() {
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        Song2Lib.init(getApplicationContext());
        System.loadLibrary("song2-test-lib");
        mFactory = new S2Factory();

        mThread = new Thread(new Runnable() {
            @Override
            public void run() {

                ArrayList<String> params = new ArrayList<>();
                params.add("song2-test");
                params.add("--gtest_filter=-RealDeviceTest.*");
                Daemon.run(params, mFactory);
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        close();
                    }
                });
            }
        });
        mThread.start();
    }
}
