# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

cmake_minimum_required(VERSION 3.4.1)

set(TOP_PROJECT "${CMAKE_CURRENT_LIST_DIR}/../../..")
set(GENERATED_CODE "${TOP_PROJECT}/gen")
set(CORE_CODE "${TOP_PROJECT}/core")
set(CPP_CORE_CODE "${TOP_PROJECT}/cpp")
set(CPP_TEST_CODE "${TOP_PROJECT}/test")
set(RESOURCE_DIR "${CMAKE_CURRENT_LIST_DIR}/src/main/res/raw")
set(BOOST_INCLUDE "${TOP_PROJECT}/dependencies/boost/boost_1_66_0")

if("${ANDROID_ABI}" MATCHES "armeabi-v7a")
  set(ANDROID_ARCH "arm")
elseif("${ANDROID_ABI}" MATCHES "arm64-v8a")
  set(ANDROID_ARCH "arm64")
elseif("${ANDROID_ABI}" MATCHES "x86_64")
  set(ANDROID_ARCH "x86_64")
elseif("${ANDROID_ABI}" MATCHES "x86")
  set(ANDROID_ARCH "x86")
else()
  message( FATAL_ERROR "Unknown ABI ${ANDROID_ABI}" )
endif()

set(DEPENDENCIES_PLATFORM_DIR "${TOP_PROJECT}/dependencies/platform/android-${ANDROID_ARCH}")

file(GLOB SRC
  "${CPP_TEST_CODE}/*.cpp"
  "${CPP_TEST_CODE}/cpp/dummy_device.cpp"
  "${CPP_TEST_CODE}/cpp/pipe.cpp"
  "${CPP_TEST_CODE}/cpp/threading.cpp"
  "${CPP_TEST_CODE}/cpp/workflows.cpp"
  "${CPP_TEST_CODE}/cpp/real_device.cpp"
  "${CPP_TEST_CODE}/cpp/dummy_device/mbed-song2-mbed-os/song2/arpc/source/*.cpp"
  "${CPP_TEST_CODE}/cpp/dummy_device/mbed-song2-mbed-os/song2/srtp/source/*.cpp"
  "${CPP_TEST_CODE}/cpp/dummy_device/mbed-song2-mbed-os/song2/arpc/*.h"
  "${CPP_TEST_CODE}/cpp/dummy_device/mbed-song2-mbed-os/song2/srtp/*.h"
  "${CPP_TEST_CODE}/cpp/dummy_device/mbed-song2-mbed-os/third_party/pb/*.c"
  "${CPP_TEST_CODE}/cpp/dummy_device/*.cpp"
  "${CPP_TEST_CODE}/cpp/dummy_device/*.c"

  "${GENERATED_CODE}/djinni/lib/jni/djinni_main.cpp"
  "${GENERATED_CODE}/djinni/lib/jni/djinni_support.cpp"
  "${GENERATED_CODE}/djinni/jni/native_daemon.cpp"
)

include_directories(
  ${CORE_CODE}/acore
  ${CORE_CODE}/srtp
  ${CORE_CODE}/arpc
  ${CPP_CORE_CODE}/3rdparty
  ${TOP_PROJECT}
  ${GENERATED_CODE}/djinni/lib/jni
  ${GENERATED_CODE}/djinni/cpp
  ${BOOST_INCLUDE}
  ${CPP_TEST_CODE}
  ${CPP_TEST_CODE}/cpp/dummy_device
  ${CPP_TEST_CODE}/cpp/dummy_device/mbed-os-stubs
  ${CPP_TEST_CODE}/cpp/dummy_device/mbed-os-stubs/platform
  ${CPP_TEST_CODE}/cpp/dummy_device/mbed-song2-mbed-os
  ${CPP_TEST_CODE}/cpp/dummy_device/mbed-song2-mbed-os/song2/arpc
  ${CPP_TEST_CODE}/cpp/dummy_device/mbed-song2-mbed-os/song2/srtp
  ${CPP_TEST_CODE}/cpp/dummy_device/mbed-song2-mbed-os/song2/ble
  ${CPP_TEST_CODE}/cpp/dummy_device/mbed-song2-mbed-os/third_party
)

add_definitions(-D__ANDROID__)

configure_file("${TOP_PROJECT}/test/cpp/dummy_device/blob.pb" "${RESOURCE_DIR}/blob_pb" COPYONLY)

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.

add_library( # Sets the name of the library.
             song2-test-lib

             # Sets the library as a shared library.
             SHARED

             # Provides a relative path to your source file(s).
             ${SRC} )

# Searches for a specified prebuilt library and stores the path as a
# variable. Because CMake includes system libraries in the search path by
# default, you only need to specify the name of the public NDK library
# you want to add. CMake verifies that the library exists before

find_library( # Sets the name of the path variable.
              log-lib

              # Specifies the name of the NDK library that
              # you want CMake to locate.
              log )

find_library( # Sets the name of the path variable.
              android-lib

              # Specifies the name of the NDK library that
              # you want CMake to locate.
              android )

find_library( # Sets the name of the path variable.
              libgcc-lib

              # Specifies the name of the NDK library that
              # you want CMake to locate.
              gcc )

add_library(gtest STATIC IMPORTED)
set_target_properties(gtest PROPERTIES IMPORTED_LOCATION
                      ${DEPENDENCIES_PLATFORM_DIR}/lib/libgtest.a
                      INTERFACE_INCLUDE_DIRECTORIES ${DEPENDENCIES_PLATFORM_DIR}/include
)

# Import Song2.
add_library(song2 SHARED IMPORTED)
set_target_properties(song2 PROPERTIES IMPORTED_LOCATION
                      ${SONG2_LIBPATH}/${ANDROID_ABI}/libsong2-native-lib.so
)

# Specifies libraries CMake should link to your target library. You
# can link multiple libraries, such as libraries you define in this
# build script, prebuilt third-party libraries, or system libraries.

target_link_libraries( # Specifies the target library.
                       song2-test-lib
                       PRIVATE
                       # Static dependencies
                       gtest
                       song2
                    )

target_link_libraries( # Specifies the target library.
                       song2-test-lib
                       PUBLIC
                       #${android-lib}
                       ${log-lib}
                    )
