# Song2 Android library

The current project builds an Android library (i.e. .aar) containing all Song2 native code (i.e. cpp code & JNI/Java links).

## Prerequisites
- [CMake (v3.12.1)](https://cmake.org/download/): ensure to put CMake in the Path
- [Ninja](https://github.com/ninja-build/ninja/releases): ensure that ninja is also in the Path
- Android SDK & NDK: Install [`sdkmanager`](https://developer.android.com/studio/#command-tools) and see below instructions
- Java JDK (1.8)
- Gradle: use `gradlew` present in /platform/android

## Install Android SDK & NDK
Ensure to set the `sdk-root-path` option and install the following packages:
* ndk-bundle: `sdkmanager --sdk-root-path="path" install "ndk-bundle"`
* platform: `sdkmanager --sdk-root-path="path" install "platforms;android-18"`
Accept licenses using the manager; `sdkmanager --licenses`
Note: It is also possible to use Android Studio to install the necessary libraries (`sudo snap install --classic android-studio` on Ubuntu 18.04)

## Project setup to work in the environment
In /platform/android, create a file called `local.properties` and set the different variables so that Android is aware of the environment.
* SDK: `sdk.dir=(path to Android SDK)`
* NDK: `ndk.dir=(path to NDK bundle)`
* Cmake: `cmake.dir=(path to cmake)`


## Build library
In /platform/android, run gradle `./gradlew build`
