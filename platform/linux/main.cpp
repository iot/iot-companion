//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "LinuxFactory.hpp"
#include "gen/djinni/cpp/daemon.hpp"

#include <vector>
#include <string>
#include <memory>

using namespace arm::song2;

/**
 * Entry point for all Linux targets
 */
int main(int argc, const char * argv[]) {
    std::vector<std::string> args;
        
    for(size_t argn = 0; argn < argc; argn++)        
    {
        args.push_back(argv[argn]);
    }

    auto factory = std::make_shared<platform::linux::LinuxFactory>();
    
    return platform::Daemon::run(args, factory);
}


