//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "gen/djinni/cpp/platform_factory.hpp"

#include "cpp/threading/BlockingDispatcher.hpp"

namespace arm { 
    namespace song2 { 
        namespace platform {
            class BleAdapter;
            class Dispatcher;
            class NfcAdapter;
            class PlatformHelper;
            enum class PlatformBundle;
            namespace linux {
                class LinuxFactory : public platform::PlatformFactory {
                    public:
                        LinuxFactory();

                        virtual ~LinuxFactory();

                        virtual std::shared_ptr<Dispatcher> get_main_dispatcher() override;

                        virtual std::shared_ptr<Dispatcher> create_dispatcher(const std::string & name) override;

                        virtual void run_main_dispatcher() override;

                        virtual std::shared_ptr<PlatformHelper> get_platform_helper() override;

                        virtual std::shared_ptr<BleAdapter> get_ble_adapter() override;

                        virtual std::shared_ptr<NfcAdapter> get_nfc_adapter() override;

                        virtual std::string get_resource_path(PlatformBundle bundle_ref, const std::string & resource_name) override;

                        virtual std::shared_ptr<CryptoAdapter> get_crypto_adapter() override;

                        virtual std::string get_storage_path() override;

                    private:
                        typename threading::BlockingDispatcher::ptr _main_dispatcher;
                };
            }
        }
    }
}

