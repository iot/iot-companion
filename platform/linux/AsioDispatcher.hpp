//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "gen/djinni/cpp/dispatcher.hpp"

#include <boost/asio.hpp>
#include <thread>
#include <mutex>
#include <memory>
#include <string>

namespace arm { 
    namespace song2 { 
        namespace platform {
            namespace linux {
                class AsioDispatcher : public platform::Dispatcher {
                    public:
                        AsioDispatcher(const std::string& name);

                        virtual ~AsioDispatcher();

                        virtual bool is_equal(const std::shared_ptr<Dispatcher> & other) override;

                        virtual DispatcherStatus status() override;

                        virtual bool start() override;

                        virtual bool terminate(bool kill) override;

                        virtual bool notify(const std::shared_ptr<Queue> & queue, int64_t delay_ms) override;

                    private:
                        bool start_unsafe();

                        std::mutex _mtx;
                        std::string _name;
                        boost::asio::io_service _io_service;
                        std::unique_ptr<boost::asio::io_service::work> _work;
                        DispatcherStatus _status;
                        std::thread _thread;
                };
            }
        }
    }
}