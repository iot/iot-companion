//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "LinuxFactory.hpp"
#include "AsioDispatcher.hpp"

#include "gen/djinni/cpp/dispatcher.hpp"
#include "gen/djinni/cpp/dispatcher_status.hpp"
#include "gen/djinni/cpp/platform_bundle.hpp"

#include <stdexcept>
#include <boost/filesystem.hpp>

using namespace arm::song2::platform;
using namespace arm::song2::platform::linux;

LinuxFactory::LinuxFactory() : _main_dispatcher(threading::make_blocking_dispatcher())
{

}

LinuxFactory::~LinuxFactory()
{

}

std::shared_ptr<Dispatcher> LinuxFactory::get_main_dispatcher()
{
    return _main_dispatcher;
}

std::shared_ptr<Dispatcher> LinuxFactory::create_dispatcher(const std::string & name)
{
    return std::make_shared<AsioDispatcher>(name);
}

void LinuxFactory::run_main_dispatcher()
{
    if(!_main_dispatcher->start())
    {
        return;
    }

    // TODO do we need to add signal handling?

    while(_main_dispatcher->status() != DispatcherStatus::TERMINATED)
    {
        // Execute work
        _main_dispatcher->process();

        // Wait for more work
        _main_dispatcher->wait();
    }
}

std::shared_ptr<PlatformHelper> LinuxFactory::get_platform_helper()
{
    throw std::logic_error("LinuxFactory::get_platform_helper() is not implemented");
}

std::shared_ptr<BleAdapter> LinuxFactory::get_ble_adapter()
{
    // TODO Cordio/BlueZ integration?
    throw std::logic_error("LinuxFactory::get_ble_adapter() is not implemented");
}

std::shared_ptr<NfcAdapter> LinuxFactory::get_nfc_adapter()
{
    throw std::logic_error("LinuxFactory::get_nfc_adapter() is not implemented");
}

std::shared_ptr<CryptoAdapter> LinuxFactory::get_crypto_adapter() 
{
    throw std::logic_error("LinuxFactory::get_crypto_adapter() is not implemented");
}

std::string LinuxFactory::get_storage_path()
{
    throw std::logic_error("LinuxFactory::get_storage_path() is not implemented");
}

std::string LinuxFactory::get_resource_path(PlatformBundle bundle_ref, const std::string& resource_name)
{
    // If it's the app, use current working directory
    boost::filesystem::path path;
    if(bundle_ref == PlatformBundle::APP)
    {
        path = boost::filesystem::current_path();
    }
    else // (bundle_ref == PlatformBundle::SDK)
    {
        // FIXME: Resources should be embedded by CMake (and extracted to a temp directory at startup? Or API should be updated to allow this)
        path = boost::filesystem::current_path();
    }

    path += "/";
    path += resource_name;

    return path.string();
}