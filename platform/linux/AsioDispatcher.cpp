//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "AsioDispatcher.hpp"

#include "gen/djinni/cpp/dispatcher_status.hpp"
#include "gen/djinni/cpp/queue.hpp"

#include <pthread.h>
#include <stdexcept>

using namespace arm::song2::platform;
using namespace arm::song2::platform::linux;

AsioDispatcher::AsioDispatcher(const std::string& name) : _name(name), _status(DispatcherStatus::PENDING), _work(std::make_unique<boost::asio::io_service::work>(_io_service))
{
    if(_name.length() > 15)
    {
        throw std::logic_error("pthread names are limited to 15 characters");
    }
}

AsioDispatcher::~AsioDispatcher() 
{
    _thread.join();
}

bool AsioDispatcher::is_equal(const std::shared_ptr<Dispatcher> & other) { 
    return this == other.get();
}

DispatcherStatus AsioDispatcher::status()
{
    std::unique_lock<std::mutex> lock(_mtx);
    return _status;
}

bool AsioDispatcher::start()
{
    std::unique_lock<std::mutex> lock(_mtx);
    return start_unsafe();
}

bool AsioDispatcher::terminate(bool kill)
{
    std::unique_lock<std::mutex> lock(_mtx);
    if(_status == DispatcherStatus::PENDING) // Shortcut from pending to terminated
    {
        if(!kill)
        {
            start_unsafe();
        }
        else
        {
            // Set it as terminated
            _status = DispatcherStatus::TERMINATED;

            // Clear IO service
            _io_service.reset();
            return true;
        }
    }

    if(_status >= DispatcherStatus::TERMINATING)
    {
        return false;
    }

    _status = DispatcherStatus::TERMINATING;
    
    // Reset work to allow run to exit
    _work = nullptr;

    if(kill)
    {
        _io_service.stop(); // Make all invocations of run() return asap
    }

    return true;
}

bool AsioDispatcher::notify(const std::shared_ptr<Queue> & queue, int64_t delay_ms)
{
    std::unique_lock<std::mutex> lock(_mtx);

    if(_status > DispatcherStatus::RUNNING)
    {
        // Cannot accept new work
        return false;
    }

    if(delay_ms > 0)
    {
        auto timer = std::make_shared<boost::asio::deadline_timer>(_io_service, boost::posix_time::milliseconds(delay_ms));
        timer->async_wait([this, timer, queue](const boost::system::error_code& error){
            if(error)
            {
                return;
            }

            int64_t new_delay_ms = queue->process();
            if(new_delay_ms > 0)
            {
                notify(queue, new_delay_ms);
            }
        });
    }
    else
    {
        _io_service.post([this, queue](){
            int64_t new_delay_ms = queue->process();
            if(new_delay_ms > 0)
            {
                notify(queue, new_delay_ms);
            }
        });
    } 

    return true;
}

bool AsioDispatcher::start_unsafe()
{
    // Check that the dispatcher has not been started already
    if(_status != DispatcherStatus::PENDING)
    {
        return false;
    }

    _status = DispatcherStatus::RUNNING;
    
    // Create processing thread
    _thread = std::thread([this](){        
        bool terminating = true;
        while(!terminating)
        {
            {
                std::unique_lock<std::mutex> lock(_mtx);
                if(_status == DispatcherStatus::TERMINATING)
                {
                    // Give one last chance for work to run
                    terminating = true;
                }
            }

            // This will not return until _work is reset
            _io_service.run();
        }

        std::unique_lock<std::mutex> lock(_mtx);
        _status = DispatcherStatus::TERMINATED;
    });

    // Name the thread
    pthread_setname_np(_thread.native_handle(), _name.c_str());
}