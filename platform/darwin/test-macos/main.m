//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  main.m
//  Song2 Tests
//
//  Created by Donatien Garnier on 03/01/2018.

//

#import <Foundation/Foundation.h>
#import "platform/darwin/S2DarwinFactory.h"
#import "gen/djinni/objc/S2Daemon.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSMutableArray* args = [NSMutableArray array];
        for( int i = 0; i < argc; i++ )
        {
            [args addObject:[NSString stringWithUTF8String:argv[i]]];
        }
        
        S2DarwinFactory* factory = [[S2DarwinFactory alloc] init];
        
        return [S2Daemon run:args platformFactory:factory];
    }
    return 0;
}


