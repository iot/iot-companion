//
//  main.m
//  simple-example-ios
//
//  Created by Donatien Garnier on 11/10/2021.
//  Copyright © 2021 ARM Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "AppDelegate.h"
//
//int main(int argc, char * argv[]) {
//    NSString * appDelegateClassName;
//    @autoreleasepool {
//        // Setup code that might create autoreleased objects goes here.
//        appDelegateClassName = NSStringFromClass([AppDelegate class]);
//    }
//    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
//}

#import "platform/darwin/S2DarwinFactory.h"
#import "gen/djinni/objc/S2Daemon.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSMutableArray* args = [NSMutableArray array];
        for( int i = 0; i < argc; i++ )
        {
            [args addObject:[NSString stringWithUTF8String:argv[i]]];
        }
        
        S2DarwinFactory* factory = [[S2DarwinFactory alloc] init];
        
        return [S2Daemon run:args platformFactory:factory];
    }
    return 0;
}
