//
//  AppDelegate.h
//  simple-example-ios
//
//  Created by Donatien Garnier on 11/10/2021.
//  Copyright © 2021 ARM Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

