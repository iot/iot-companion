//
//  main.m
//  Song2 Tests
//
//  Created by Donatien Garnier on 03/04/2018.

//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
