//
//  com_arm_song2_sdk_macos.h
//  com.arm.song2-sdk-macos
//
//  Created by Donatien Garnier on 15/01/2019.
//  Copyright © 2019 ARM Ltd. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for com_arm_song2_sdk_macos.
FOUNDATION_EXPORT double com_arm_song2_sdk_macosVersionNumber;

//! Project version string for com_arm_song2_sdk_macos.
FOUNDATION_EXPORT const unsigned char com_arm_song2_sdk_macosVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <com_arm_song2_sdk_macos/PublicHeader.h>


