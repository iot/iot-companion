//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2DarwinBleAdapter.h
//  song2
//
//  Created by Donatien Garnier on 25/02/2018.

//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#include "gen/djinni/objc/S2BleAdapter.h"
#include "gen/djinni/objc/S2BleAdapterDelegate.h"

@class S2DarwinBleDevice;
@class S2DarwinGattcService;
@class S2DarwinGattcCharacteristic;
@class S2DarwinGattcDescriptor;
@class S2FoundationDispatcher;

@interface S2DarwinBleAdapter : NSObject<S2BleAdapter, CBCentralManagerDelegate>

- (nonnull instancetype)initWithQueue:(nonnull dispatch_queue_t)queue;

- (nullable S2DarwinBleDevice*)findS2Device:(nullable id<S2BleDevice>)device
                             checkConnected:(BOOL) connected
                            error:(nullable S2BleError*) ble_error;
- (nullable S2DarwinGattcService*)findS2GattcService:(nullable id<S2GattcService>)service
                                      checkConnected:(BOOL) connected
                                      error:(nullable S2BleError*) ble_error;
- (nullable S2DarwinGattcCharacteristic*)findS2GattcCharacteristic:(nullable id<S2GattcCharacteristic>)characteristic
                                                    checkConnected:(BOOL) connected
                                                 error:(nullable S2BleError*) ble_error;
- (nullable S2DarwinGattcDescriptor*)findS2GattcDescriptor:(nullable id<S2GattcDescriptor>)descriptor
                                                    checkConnected:(BOOL) connected
                                                             error:(nullable S2BleError*) ble_error;

// Callbacks from S2DarwinBleDevice
- (void)onDeviceRssiUpdated:(nullable id<S2BleDevice>)device
                      error:(S2BleError)error;

- (void)onGattcServicesDiscovered:(nullable id<S2BleDevice>)device
                          error:(S2BleError)error;

- (void)onGattcCharacteristicsDiscovered:(nullable id<S2GattcService>)service
                                 error:(S2BleError)error;

- (void)onGattcDescriptorsDiscovered:(nullable id<S2GattcCharacteristic>)characteristic
                             error:(S2BleError)error;

- (void)onGattcCharacteristicWritten:(nullable id<S2GattcCharacteristic>)characteristic
                             error:(S2BleError)error;

- (void)onGattcCharacteristicRead:(nullable id<S2GattcCharacteristic>)characteristic
                          error:(S2BleError)error;

- (void)onGattcCharacteristicUpdated:(nullable id<S2GattcCharacteristic>)characteristic
                             error:(S2BleError)error;

- (void)onGattcCharacteristicNotificationsSet:(nullable id<S2GattcCharacteristic>)characteristic
                                   error:(S2BleError)error;

- (void)onGattcDescriptorWritten:(nullable id<S2GattcDescriptor>)descriptor
                         error:(S2BleError)error;

- (void)onGattcDescriptorRead:(nullable id<S2GattcDescriptor>)descriptor
                      error:(S2BleError)error;

- (void)onGattcMtuUpdated:(nullable id<S2BleDevice>)device
                  error:(S2BleError)error;

- (void)onGattcPhyUpdated:(nullable id<S2BleDevice>)device
                  error:(S2BleError)error;

// Helper
+ (S2BleError)S2BleErrorFromNSError:(nullable NSError*)ns_error;

@property (nonnull, nonatomic, strong) dispatch_queue_t ns_queue;
@property (nullable, nonatomic, strong) S2FoundationDispatcher* dispatcher;
@property (nullable, nonatomic, strong) CBCentralManager* cb_central_manager;
@property (nullable, nonatomic, strong) S2BleAdapterDelegate* s2_delegate;

@property (nonnull, nonatomic, strong) NSMutableArray<S2DarwinBleDevice *>* s2_devices;
@end
