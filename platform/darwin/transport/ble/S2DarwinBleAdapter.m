//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2DarwinBleAdapter.m
//  song2
//
//  Created by Donatien Garnier on 25/02/2018.

//

#import "S2DarwinBleAdapter.h"
#import "S2DarwinBleDevice.h"
#import "S2DarwinGattcService.h"
#import "S2DarwinGattcCharacteristic.h"
#import "S2DarwinGattcDescriptor.h"

#import "S2UUIDUtils.h"
#import "platform/darwin/threading/S2FoundationDispatcher.h"

#define LEGACY_API (defined(__MAC_OS_X_VERSION_MAX_ALLOWED) && (__MAC_OS_X_VERSION_MAX_ALLOWED < 1013))

@interface S2DarwinBleAdapter()
#if LEGACY_API
// CBManagerState appears from High Sierra in macOS
+ (S2BleAdapterStatus) S2BleAdapterStatusWithCBManagerState:(CBCentralManagerState) cbstate;
#else
+ (S2BleAdapterStatus) S2BleAdapterStatusWithCBManagerState:(CBManagerState) cbstate;
#endif

- (nullable S2DarwinBleDevice*)findOrCreateS2DeviceForCBPeripheral:(nonnull CBPeripheral *)cb_peripheral;

@property BOOL is_scanning;
@property BOOL is_in_foreground;
@end

@implementation S2DarwinBleAdapter

- (instancetype)initWithQueue:(dispatch_queue_t)queue {
    // NOTE this queue must be the same queue as the one being used to call us back for thread-safety
    self = [super init];
    self.ns_queue = queue;
    self.dispatcher = [[S2FoundationDispatcher alloc] initWithName:@"BLE" dispatchQueue:self.ns_queue];
    [self.dispatcher start];
    self.is_scanning = NO;
    self.is_in_foreground = YES;
    self.s2_devices = [[NSMutableArray alloc] init];
    return self;
}

// Only init CBCentralManager when delegate is ready
- (void)setDelegate:(nullable S2BleAdapterDelegate *)delegate {
    if(delegate != NULL)
    {
        self.s2_delegate = delegate;
    
        // This will ask the user to enable Bluetooth if disabled
        self.cb_central_manager = [[CBCentralManager alloc] initWithDelegate:self queue:self.ns_queue options:@{CBCentralManagerOptionShowPowerAlertKey:[NSNumber numberWithBool:YES]}];
    }
    else
    {
        self.cb_central_manager = NULL;
        self.s2_delegate = NULL;
    }
}

- (S2DarwinBleDevice *)findS2Device:(id<S2BleDevice>)device
                     checkConnected:(BOOL) connected
                            error:(nullable S2BleError*) ble_error;
{
    if(device == NULL)
    {
        if(ble_error != NULL)
        {
            *ble_error = S2BleErrorUnknownDevice;
        }
        
        return NULL;
    }
    
    //if([_s2_devices containsObject:(S2DarwinBleDevice*)device])
    S2DarwinBleDevice* s2_device = (S2DarwinBleDevice*)device;
    
    if([s2_device lost])
    {
        if(ble_error != NULL)
        {
            *ble_error = S2BleErrorUnknownDevice;
        }
        
        return NULL;
    }
    
    if(connected && ([s2_device status] != S2BleDeviceStatusConnected))
    {
        if(ble_error != NULL)
        {
            *ble_error = S2BleErrorInvalidState;
        }
        
        return NULL;
    }
    
    if(ble_error != NULL)
    {
        *ble_error = S2BleErrorOk;
    }
    
    return s2_device;
}

- (S2DarwinGattcService *)findS2GattcService:(id<S2GattcService>)service
                              checkConnected:(BOOL) connected
                                       error:(S2BleError *)ble_error
{
    if(service == NULL)
    {
        if(ble_error != NULL)
        {
            *ble_error = S2BleErrorUnknownGattcService;
        }
        
        return NULL;
    }
    
    S2DarwinGattcService * s2_service = (S2DarwinGattcService *) service;
    if([self findS2Device:[s2_service s2_device] checkConnected:connected error:ble_error] == NULL)
    {
        return NULL;
    }
    
    if([s2_service lost])
    {
        if(ble_error != NULL)
        {
            *ble_error = S2BleErrorUnknownGattcService;
        }
        
        return NULL;
    }
    
    return s2_service;
}

-(S2DarwinGattcCharacteristic *)findS2GattcCharacteristic:(id<S2GattcCharacteristic>)characteristic
                                           checkConnected:(BOOL) connected
                                                    error:(S2BleError *)ble_error
{
    if(characteristic == NULL)
    {
        if(ble_error != NULL)
        {
            *ble_error = S2BleErrorUnknownGattcCharacteristic;
        }
        
        return NULL;
    }
    
    S2DarwinGattcCharacteristic * s2_characteristic = (S2DarwinGattcCharacteristic *) characteristic;
    if([self findS2GattcService:[s2_characteristic s2_service] checkConnected:connected error:ble_error] == NULL)
    {
        return NULL;
    }
    
    if([s2_characteristic lost])
    {
        if(ble_error != NULL)
        {
            *ble_error = S2BleErrorUnknownGattcCharacteristic;
        }
        
        return NULL;
    }
    
    return s2_characteristic;
}

-(S2DarwinGattcDescriptor *)findS2GattcDescriptor:(id<S2GattcDescriptor>)descriptor checkConnected:(BOOL)connected error:(S2BleError *)ble_error
{
    if(descriptor == NULL)
    {
        if(ble_error != NULL)
        {
            *ble_error = S2BleErrorUnknownGattcCharacteristic; // Descriptor is tightly coupled with characteristic
        }
        
        return NULL;
    }
    
    S2DarwinGattcDescriptor * s2_descriptor = (S2DarwinGattcDescriptor *) descriptor;
    if([self findS2GattcCharacteristic:[s2_descriptor s2_characteristic] checkConnected:connected error:ble_error] == NULL)
    {
        return NULL;
    }
    
    if([s2_descriptor lost])
    {
        if(ble_error != NULL)
        {
            *ble_error = S2BleErrorUnknownGattcCharacteristic;
        }
        
        return NULL;
    }
    
    return s2_descriptor;
}

- (id<S2Dispatcher>)getDispatcher
{
    return _dispatcher;
}

- (S2BleAdapterStatus)getStatus {
    S2BleAdapterStatus status = [S2DarwinBleAdapter S2BleAdapterStatusWithCBManagerState:[_cb_central_manager state]];
    if( status == S2BleAdapterStatusEnabled )
    {
        if( _is_scanning )
        {
            if( _is_in_foreground )
            {
                status = S2BleAdapterStatusScanningForeground;
            }
            else
            {
                status =  S2BleAdapterStatusScanningBackground;
            }
        }
    }
    
    return status;
}

- (S2BleError)startScanning:(nonnull NSArray<NSData *> *)servicesUuids flags:(S2BleScanFlags)flags {
    S2BleAdapterStatus status = [S2DarwinBleAdapter S2BleAdapterStatusWithCBManagerState:[_cb_central_manager state]];
    if(status < S2BleAdapterStatusEnabled)
    {
        return S2BleErrorInvalidState;
    }
    
    [_cb_central_manager scanForPeripheralsWithServices:[S2UUIDUtils CBUUIDArrayWithS2UUIDArray:servicesUuids]
                                                options:@{CBCentralManagerScanOptionAllowDuplicatesKey:[NSNumber numberWithBool:YES]}];
    
    self.is_scanning = YES;
    
    if(_s2_delegate)
    {
        [_s2_delegate onStatusUpdated];
    }
    
    return S2BleErrorOk;
}

- (S2BleError)stopScanning {
    S2BleAdapterStatus status = [S2DarwinBleAdapter S2BleAdapterStatusWithCBManagerState:[_cb_central_manager state]];
    if(status < S2BleAdapterStatusEnabled)
    {
        return S2BleErrorInvalidState;
    }
    
    [_cb_central_manager stopScan];
    
    self.is_scanning = NO;
    
    if(_s2_delegate)
    {
        [_s2_delegate onStatusUpdated];
    }
    
    return S2BleErrorOk;
}

- (void)connectDevice:(nullable id<S2BleDevice>)device flags:(S2BleDeviceConnectionFlags)flags {
    S2BleError s2_error = S2BleErrorOk;
    S2DarwinBleDevice* s2_device = [self findS2Device:device checkConnected:NO error:&s2_error];
    if(s2_error != S2BleErrorOk)
    {
        // This could happen if a user tries to connect to a device that has been erased on this side
        [_s2_delegate onDeviceConnected:device error:s2_error];
        return;
    }
    
    // Fail if already connected or connecting
    if([s2_device status] != S2BleDeviceStatusDisconnected)
    {
        [_s2_delegate onDeviceConnected:device error:S2BleErrorInvalidState];
        return;
    }
    
    // TODO PHY support when CoreBluetooth supports it
    [_cb_central_manager connectPeripheral:[s2_device cb_peripheral] options:nil];
}

- (void)disconnectDevice:(nullable id<S2BleDevice>)device {
    S2BleError s2_error = S2BleErrorOk;
    S2DarwinBleDevice* s2_device = [self findS2Device:device checkConnected:YES error:&s2_error];
    if(s2_error != S2BleErrorOk)
    {
        [_s2_delegate onDeviceDisconnected:device error:s2_error];
        return;
    }
    
    [_cb_central_manager cancelPeripheralConnection:[s2_device cb_peripheral]];
}

- (void)gattcCharacteristicSetNotifications:(nullable id<S2GattcCharacteristic>)characteristic enable:(BOOL)enable {
    S2BleError s2_error = S2BleErrorOk;
    S2DarwinGattcCharacteristic* s2_characteristic = [self findS2GattcCharacteristic:characteristic checkConnected:YES error:&s2_error];
    if(s2_error != S2BleErrorOk)
    {
        [_s2_delegate onGattcCharacteristicNotificationsSet:characteristic error:s2_error];
        return;
    }
    
    [[[[s2_characteristic s2_service] s2_device] cb_peripheral] setNotifyValue:enable forCharacteristic:[s2_characteristic cb_characteristic]];
}

- (void)gattcCharacteristicRead:(nullable id<S2GattcCharacteristic>)characteristic {
    S2BleError s2_error = S2BleErrorOk;
    S2DarwinGattcCharacteristic* s2_characteristic = [self findS2GattcCharacteristic:characteristic checkConnected:YES error:&s2_error];
    if(s2_error != S2BleErrorOk)
    {
        [_s2_delegate onGattcCharacteristicRead:characteristic error:s2_error];
        return;
    }

    [[[[s2_characteristic s2_service] s2_device] cb_peripheral] readValueForCharacteristic:[s2_characteristic cb_characteristic]];
}

- (void)gattcCharacteristicWrite:(nullable id<S2GattcCharacteristic>)characteristic value:(nonnull NSData *)value withResponse:(BOOL)withResponse {
    S2BleError s2_error = S2BleErrorOk;
    S2DarwinGattcCharacteristic* s2_characteristic = [self findS2GattcCharacteristic:characteristic checkConnected:YES error:&s2_error];
    if(s2_error != S2BleErrorOk)
    {
        [_s2_delegate onGattcCharacteristicWritten:characteristic error:s2_error];
        return;
    }
    
    CBCharacteristicWriteType write_type = withResponse ? CBCharacteristicWriteWithResponse : CBCharacteristicWriteWithoutResponse;
    
    [[[[s2_characteristic s2_service] s2_device] cb_peripheral] writeValue:value forCharacteristic:[s2_characteristic cb_characteristic] type:write_type];
    
    if(!withResponse) {
        // CoreBluetooth will only call the peripheral:didWriteValueForCharacteristic callback for writes with responses - so call synchronously
        [_s2_delegate onGattcCharacteristicWritten:s2_characteristic error:S2BleErrorOk];
    }
}

- (void)gattcDescriptorRead:(nullable id<S2GattcDescriptor>)descriptor {
    S2BleError s2_error = S2BleErrorOk;
    S2DarwinGattcDescriptor* s2_descriptor = [self findS2GattcDescriptor:descriptor checkConnected:YES error:&s2_error];
    if(s2_error != S2BleErrorOk)
    {
        [_s2_delegate onGattcDescriptorRead:descriptor error:s2_error];
        return;
    }
    
    [[[[[s2_descriptor s2_characteristic] s2_service] s2_device] cb_peripheral] readValueForDescriptor:[s2_descriptor cb_descriptor]];
}

- (void)gattcDescriptorWrite:(nullable id<S2GattcDescriptor>)descriptor value:(nonnull NSData *)value {
    S2BleError s2_error = S2BleErrorOk;
    S2DarwinGattcDescriptor* s2_descriptor = [self findS2GattcDescriptor:descriptor checkConnected:YES error:&s2_error];
    
    // If this is CCCD, do a no-op as the iOS/macOS stack won't let us do it like this
    if([[[s2_descriptor cb_descriptor] UUID] isEqual:([CBUUID UUIDWithString:CBUUIDClientCharacteristicConfigurationString])])
    {
        [_s2_delegate onGattcDescriptorWritten:descriptor error:S2BleErrorOk];
        return;
    }
    
    if(s2_error != S2BleErrorOk)
    {
        [_s2_delegate onGattcDescriptorWritten:descriptor error:s2_error];
        return;
    }
    
    [[[[[s2_descriptor s2_characteristic] s2_service] s2_device] cb_peripheral] writeValue:value forDescriptor:[s2_descriptor cb_descriptor]];
}

- (void)gattcDiscoverCharacteristics:(nullable id<S2GattcService>)service characteristicsUuids:(nonnull NSArray<NSData *> *)characteristicsUuids {
    S2BleError s2_error = S2BleErrorOk;
    S2DarwinGattcService* s2_service = [self findS2GattcService:service checkConnected:YES error:&s2_error];
    if(s2_error != S2BleErrorOk)
    {
        [_s2_delegate onGattcCharacteristicsDiscovered:service error:s2_error];
        return;
    }

    [[[s2_service s2_device] cb_peripheral] discoverCharacteristics:[S2UUIDUtils CBUUIDArrayWithS2UUIDArray:characteristicsUuids] forService:[s2_service cb_service]];
}

- (void)gattcDiscoverDescriptors:(nullable id<S2GattcCharacteristic>)characteristic {
    S2BleError s2_error = S2BleErrorOk;
    S2DarwinGattcCharacteristic* s2_characteristic = [self findS2GattcCharacteristic:characteristic checkConnected:YES error:&s2_error];
    if(s2_error != S2BleErrorOk)
    {
        [_s2_delegate onGattcDescriptorsDiscovered:characteristic error:s2_error];
        return;
    }
    
    [[[[s2_characteristic s2_service] s2_device] cb_peripheral] discoverDescriptorsForCharacteristic:[s2_characteristic cb_characteristic]];
}

- (void)gattcDiscoverServices:(nullable id<S2BleDevice>)device serviceUuids:(nonnull NSArray<NSData *> *)serviceUuids {
    S2BleError s2_error = S2BleErrorOk;
    S2DarwinBleDevice* s2_device = [self findS2Device:device checkConnected:YES error:&s2_error];
    if(s2_error != S2BleErrorOk)
    {
        // This could happen if a user tries to connect to a device that has been erased on this side
        [_s2_delegate onGattcServicesDiscovered:device error:s2_error];
        return;
    }
    
    [[s2_device cb_peripheral] discoverServices:[S2UUIDUtils CBUUIDArrayWithS2UUIDArray:serviceUuids]];
}

- (void)gattcGetPhy:(nullable id<S2BleDevice>)device {
    // For now, unavailable for CoreBluetooth - although 2M PHY can be negotiated by peripheral
    [_s2_delegate onGattcPhyUpdated:device error:S2BleErrorNotImplemented];
}

- (void)gattcRequestMtu:(nullable id<S2BleDevice>)device mtu:(int32_t)mtu {
    // For now, unavailable for CoreBluetooth - although will be negotatied in the background
    [_s2_delegate onGattcMtuUpdated:device error:S2BleErrorNotImplemented];
}

- (nullable id<S2BleDevice>)getDeviceForGattcService:(nullable id<S2GattcService>)service {
    S2DarwinGattcService* s2_service = [self findS2GattcService:service checkConnected:NO error:NULL];
    if(s2_service == NULL)
    {
        return NULL;
    }
    
    return [s2_service s2_device];
}

- (void)getDeviceRssi:(nullable id<S2BleDevice>)device {
    S2BleError s2_error = S2BleErrorOk;
    S2DarwinBleDevice* s2_device = [self findS2Device:device checkConnected:YES error:&s2_error];
    if(s2_error != S2BleErrorOk)
    {
        // This could happen if a user tries to connect to a device that has been erased on this side
        [_s2_delegate onDeviceRssiUpdated:device error:s2_error];
        return;
    }
    
    [[s2_device cb_peripheral] readRSSI];
}

- (nullable id<S2GattcCharacteristic>)getGattcCharacteristicForDescriptor:(nullable id<S2GattcDescriptor>)descriptor {
    S2DarwinGattcDescriptor* s2_descriptor = [self findS2GattcDescriptor:descriptor checkConnected:NO error:NULL];
    if(s2_descriptor == NULL)
    {
        return NULL;
    }
    
    return [s2_descriptor s2_characteristic];
}

- (nullable id<S2GattcService>)getGattcServiceForCharacteristic:(nullable id<S2GattcCharacteristic>)characteristic {
    S2DarwinGattcCharacteristic* s2_characteristic = [self findS2GattcCharacteristic:characteristic checkConnected:NO error:NULL];
    if(s2_characteristic == NULL)
    {
        return NULL;
    }
    
    return [s2_characteristic s2_service];
}

// Callbacks from Central Manager

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    S2BleAdapterStatus state = [self getStatus];
    
    if( state <= S2BleAdapterStatusDisabled )
    {
        // Cannot be scanning
        self.is_scanning = NO;
    }
    
    if( state <= S2BleAdapterStatusDisabled )
    {
        // Clear devices cache as spec'd in
        // https://developer.apple.com/documentation/corebluetooth/cbcentralmanagerdelegate/1518888-centralmanagerdidupdatestate?language=objc
        for (S2DarwinBleDevice* device in _s2_devices) {
            [device makeLost];
        }
        [_s2_devices removeAllObjects];
    }
    
    if(_s2_delegate)
    {
        [_s2_delegate onStatusUpdated];
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(nonnull CBPeripheral *)peripheral advertisementData:(nonnull NSDictionary<NSString *,id> *)advertisementData RSSI:(nonnull NSNumber *)RSSI
{
    S2DarwinBleDevice* s2_device = [self findOrCreateS2DeviceForCBPeripheral:peripheral];
    [s2_device setPriv_rssi:[RSSI intValue]];
    if(_s2_delegate)
    {
        // Extract services
        NSArray<CBUUID *> * cb_services_uuids = advertisementData[CBAdvertisementDataServiceUUIDsKey];
        NSArray<NSData *> * s2_services_uuids = [S2UUIDUtils S2UUIDArrayWithCBUUIDArray:cb_services_uuids];
        
        // Extract services data
        NSDictionary<CBUUID *, NSData*>* cb_services_data_dict = advertisementData[CBAdvertisementDataServiceDataKey];
        
        NSMutableArray<S2BleServicesDataType *> * s2_services_data = [NSMutableArray arrayWithCapacity:cb_services_data_dict.count];
        
        NSArray<CBUUID* > * cb_services_keys = [cb_services_data_dict allKeys];
        for(size_t i = 0; i < [cb_services_data_dict count]; i++)
        {
            s2_services_data[i] = [[S2BleServicesDataType alloc] initWithServiceUuid:[S2UUIDUtils S2UUIDWithCBUUID:cb_services_keys[i]] data:cb_services_data_dict[cb_services_keys[i]]];
        }
        
        // Extract manufacturer data
        NSData* man_data_raw = advertisementData[CBAdvertisementDataManufacturerDataKey];
        S2BleManufacturerSpecificDataType* s2_man_data = NULL;
        if( (man_data_raw != NULL) && (man_data_raw.length >= 2) )
        {
            uint16_t man_id = ((((uint8_t*)man_data_raw.bytes)[0]) << 8) | ((uint8_t*)man_data_raw.bytes)[1];
            
            NSData* payload = [man_data_raw subdataWithRange:NSMakeRange(2, man_data_raw.length - 2)];
            
            s2_man_data = [S2BleManufacturerSpecificDataType bleManufacturerSpecificDataTypeWithCompanyIdentifier:man_id data:payload];
        }
        
        [_s2_delegate onDeviceDiscovered:s2_device rssi:[RSSI intValue] services:s2_services_uuids servicesData:s2_services_data manufacturerSpecificData:s2_man_data];
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    S2DarwinBleDevice* s2_device = [self findOrCreateS2DeviceForCBPeripheral:peripheral];
    
    [s2_device setPriv_status:S2BleDeviceStatusConnected];
    
    if(_s2_delegate)
    {
        [_s2_delegate onDeviceConnected:s2_device error:S2BleErrorOk];
    }
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    S2DarwinBleDevice* s2_device = [self findOrCreateS2DeviceForCBPeripheral:peripheral];
    
    [s2_device setPriv_status:S2BleDeviceStatusDisconnected];
    
    if(_s2_delegate)
    {
        [_s2_delegate onDeviceConnected:s2_device error:S2BleErrorFailedToConnect];
    }
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    S2DarwinBleDevice* s2_device = [self findOrCreateS2DeviceForCBPeripheral:peripheral];
    
    [s2_device setPriv_status:S2BleDeviceStatusDisconnected];
    
    // Clear cache
    [s2_device clearCache];
    
    if(_s2_delegate)
    {
        S2BleError s2_error = [S2DarwinBleAdapter S2BleErrorFromNSError:error];
        [_s2_delegate onDeviceDisconnected:s2_device error:s2_error];
    }
}

// Callbacks from S2DarwinBleDevice - forward to delegate
- (void)onDeviceRssiUpdated:(nullable id<S2BleDevice>)device
                      error:(S2BleError)error
{
    [_s2_delegate onDeviceRssiUpdated:device error:error];
}

- (void)onGattcServicesDiscovered:(id<S2BleDevice>)device error:(S2BleError)error
{
    [_s2_delegate onGattcServicesDiscovered:device error:error];
}

- (void)onGattcCharacteristicsDiscovered:(nullable id<S2GattcService>)service
                                 error:(S2BleError)error
{
    [_s2_delegate onGattcCharacteristicsDiscovered:service error:error];
}

- (void)onGattcDescriptorsDiscovered:(nullable id<S2GattcCharacteristic>)characteristic
                             error:(S2BleError)error
{
    [_s2_delegate onGattcDescriptorsDiscovered:characteristic error:error];
}

- (void)onGattcCharacteristicWritten:(nullable id<S2GattcCharacteristic>)characteristic
                             error:(S2BleError)error
{
    [_s2_delegate onGattcCharacteristicWritten:characteristic error:error];
}

- (void)onGattcCharacteristicRead:(nullable id<S2GattcCharacteristic>)characteristic
                          error:(S2BleError)error
{
    [_s2_delegate onGattcCharacteristicRead:characteristic error:error];
}

- (void)onGattcCharacteristicUpdated:(nullable id<S2GattcCharacteristic>)characteristic
                             error:(S2BleError)error
{
    [_s2_delegate onGattcCharacteristicUpdated:characteristic error:error];
}

- (void)onGattcCharacteristicNotificationsSet:(id<S2GattcCharacteristic>)characteristic error:(S2BleError)error
{
    [_s2_delegate onGattcCharacteristicNotificationsSet:characteristic error:error];
}

- (void)onGattcDescriptorWritten:(nullable id<S2GattcDescriptor>)descriptor
                         error:(S2BleError)error
{
    [_s2_delegate onGattcDescriptorWritten:descriptor error:error];
}

- (void)onGattcDescriptorRead:(nullable id<S2GattcDescriptor>)descriptor
                      error:(S2BleError)error
{
    [_s2_delegate onGattcDescriptorRead:descriptor error:error];
}

- (void)onGattcMtuUpdated:(nullable id<S2BleDevice>)device
                  error:(S2BleError)error
{
    [_s2_delegate onGattcMtuUpdated:device error:error];
}

- (void)onGattcPhyUpdated:(nullable id<S2BleDevice>)device
                  error:(S2BleError)error
{
    [_s2_delegate onGattcPhyUpdated:device error:error];
}

#if LEGACY_API
// CBManagerState appears from High Sierra in macOS
+ (S2BleAdapterStatus) S2BleAdapterStatusWithCBManagerState:(CBCentralManagerState) cbstate
{
    switch(cbstate)
    {
        case CBCentralManagerStateUnknown:
        case CBCentralManagerStateUnsupported:
        default:
            return S2BleAdapterStatusNoController;
            
        case CBCentralManagerStateUnauthorized:
            return S2BleAdapterStatusPermissionRequired;
            
        case CBCentralManagerStateResetting: // Special case - see whether we see this in pratice
        case CBCentralManagerStatePoweredOff:
            return S2BleAdapterStatusDisabled;
            
        case CBCentralManagerStatePoweredOn:
            return S2BleAdapterStatusEnabled;
    }
}
#else
+ (S2BleAdapterStatus) S2BleAdapterStatusWithCBManagerState:(CBManagerState) cbstate
{
    switch(cbstate)
    {
        case CBManagerStateUnknown:
        case CBManagerStateUnsupported:
        default:
            return S2BleAdapterStatusNoController;
            
        case CBManagerStateUnauthorized:
            return S2BleAdapterStatusPermissionRequired;
            
        case CBManagerStateResetting: // Special case - see whether we see this in pratice
        case CBManagerStatePoweredOff:
            return S2BleAdapterStatusDisabled;
            
        case CBManagerStatePoweredOn:
            return S2BleAdapterStatusEnabled;
    }
}
#endif


- (S2DarwinBleDevice *)findOrCreateS2DeviceForCBPeripheral:(CBPeripheral *)cb_peripheral
{
    NSUInteger idx = [_s2_devices indexOfObjectPassingTest:^BOOL(S2DarwinBleDevice * _Nonnull s2_device, NSUInteger idx, BOOL * _Nonnull stop) {
        return ([s2_device cb_peripheral] == cb_peripheral);
    }];
    
    if( idx == NSNotFound )
    {
        // Create a new object and append to array
        [_s2_devices addObject:[[S2DarwinBleDevice alloc] initWithAdapter:self peripheral:cb_peripheral]];
        
        // Update idx
        idx = [_s2_devices count] - 1;
    }
    
    return _s2_devices[idx];
}

+ (S2BleError)S2BleErrorFromNSError:(NSError *)ns_error
{
    S2BleError s2_error = S2BleErrorOk;
    if(ns_error)
    {
        if([ns_error domain] == CBErrorDomain)
        {
            switch([ns_error code])
            {
                case CBErrorConnectionTimeout:
                    s2_error = S2BleErrorTimeout;
                    break;
                    
                case CBErrorPeripheralDisconnected:
                    s2_error = S2BleErrorPeerDisconnected;
                    break;
                    
#if !LEGACY_API
                case CBErrorConnectionFailed:
                    s2_error = S2BleErrorFailedToConnect;
                    break;
#endif
#if !LEGACY_API
                case CBErrorConnectionLimitReached:
#endif
                case CBErrorOutOfSpace:
                    s2_error = S2BleErrorResourceExhausted;
                    break;
                    
                case CBErrorAlreadyAdvertising:
                case CBErrorNotConnected:
                    s2_error = S2BleErrorInvalidState;
                    break;
                    
                case CBErrorInvalidParameters:
                case CBErrorInvalidHandle:
                case CBErrorUUIDNotAllowed:
                    s2_error = S2BleErrorInvalidParam;
                    break;
                    
                case CBErrorOperationCancelled:
                    s2_error = S2BleErrorCancelled;
                    break;
                    
                case CBErrorUnknown:
                default:
                    s2_error = S2BleErrorUnknownError;
                    break;
            }
        }
        else if ([ns_error domain] == CBATTErrorDomain)
        {
            switch([ns_error code])
            {
                case CBATTErrorSuccess:
                    s2_error = S2BleErrorOk;
                    break;
                    
                case CBATTErrorInvalidHandle:
                    s2_error = S2BleErrorInvalidParam;
                    break;
                    
                case CBATTErrorReadNotPermitted:
                    s2_error = S2BleErrorAttReadNotPermitted;
                    break;
                    
                case CBATTErrorWriteNotPermitted:
                    s2_error = S2BleErrorAttWriteNotPermitted;
                    break;
                    
                case CBATTErrorInvalidPdu:
                    s2_error = S2BleErrorInvalidParam;
                    break;
                    
                case CBATTErrorInsufficientAuthentication:
                    s2_error = S2BleErrorAttInsufficientAuthentication;
                    break;
                    
                case CBATTErrorRequestNotSupported:
                    s2_error = S2BleErrorAttNotSupported;
                    break;
                    
                case CBATTErrorInvalidOffset:
                    s2_error = S2BleErrorAttInvalidOffset;
                    break;
                    
                case CBATTErrorInsufficientAuthorization:
                    s2_error = S2BleErrorAttInsufficientAuthorization;
                    break;
                    
                case CBATTErrorPrepareQueueFull:
                    s2_error = S2BleErrorResourceExhausted;
                    break;
                    
                case CBATTErrorAttributeNotFound:
                    s2_error = S2BleErrorInvalidParam;
                    break;
                    
                case CBATTErrorAttributeNotLong:
                    s2_error = S2BleErrorInvalidParam;
                    break;
                    
                case CBATTErrorInsufficientEncryption:
                case CBATTErrorInsufficientEncryptionKeySize: // do we need a separate error code?
                    s2_error = S2BleErrorAttInsufficientEncryption;
                    break;
                    
                case CBATTErrorInvalidAttributeValueLength:
                    s2_error = S2BleErrorAttInvalidLength;
                    break;
                    
                case CBATTErrorUnlikelyError: // W**!?
                    s2_error = S2BleErrorUnknownError;
                    break;
                    
                case CBATTErrorUnsupportedGroupType:
                    s2_error = S2BleErrorAttNotSupported;
                    break;
                    
                case CBATTErrorInsufficientResources:
                    s2_error = S2BleErrorResourceExhausted;
                    break;
                    
                default:
                    s2_error = S2BleErrorUnknownError;
                    break;
            }
        }
        else
        {
            s2_error = S2BleErrorUnknownError;
        }
    }
    return s2_error;
}

@end

