//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2DarwinGattcDescriptor.m
//  song2
//
//  Created by Donatien Garnier on 09/03/2018.

//

#import "S2DarwinGattcDescriptor.h"
#import "S2UUIDUtils.h"

@implementation S2DarwinGattcDescriptor
- (instancetype)initWithCharacteristic:(S2DarwinGattcCharacteristic *)characteristic descriptor:(CBDescriptor *)descriptor
{
    self = [super init];
    self.s2_characteristic = characteristic;
    self.cb_descriptor = descriptor;
    self.lost = FALSE;
    return self;
}

- (nonnull NSData *)uuid {
    return [S2UUIDUtils S2UUIDWithCBUUID:[_cb_descriptor UUID]];
}

- (nullable NSData *)value {
    return [_cb_descriptor value];
}

- (int32_t)identifier {
    // FIXME: add an equality function ...
    return (int32_t) self;
}


@end
