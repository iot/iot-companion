//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2DarwinGattcService.m
//  song2
//
//  Created by Donatien Garnier on 25/02/2018.

//

#import "S2DarwinGattcService.h"
#import "S2UUIDUtils.h"
#import "S2DarwinGattcCharacteristic.h"

@implementation S2DarwinGattcService

- (instancetype)initWithDevice:(S2DarwinBleDevice *)device service:(CBService *)service {
    self = [super init];
    self.s2_device = device;
    self.cb_service = service;
    self.s2_characteristics = [[NSMutableArray alloc] init];
    self.lost = FALSE;
    return self;
}

- (nonnull NSArray<id<S2GattcCharacteristic>> *)characteristics {
    return _s2_characteristics;
}

- (nonnull NSData *)uuid {
    return [S2UUIDUtils S2UUIDWithCBUUID:[_cb_service UUID]];
}

- (int32_t)identifier {
    // FIXME: add an equality function ...
    return (int32_t) self;
}


@end
