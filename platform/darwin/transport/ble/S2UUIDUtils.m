//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2UUIDUtils.m
//  song2
//
//  Created by Donatien Garnier on 25/02/2018.

//

#import "S2UUIDUtils.h"

@implementation S2UUIDUtils

+ (CBUUID *)CBUUIDWithS2UUID:(NSData *)nsdata
{
    return [CBUUID UUIDWithData:nsdata];
}

+ (NSData *)S2UUIDWithCBUUID:(CBUUID *)cbuuid
{
    // Always 128-bit long
    NSData* nsdata = [cbuuid data];
    
    return nsdata;
}

+ (NSArray<CBUUID *> *)CBUUIDArrayWithS2UUIDArray:(NSArray<NSData *> *)nsdata_arr
{
    NSMutableArray<CBUUID *> * cbuuids = [[NSMutableArray alloc] initWithCapacity:nsdata_arr.count];
    
    for(size_t i = 0; i < nsdata_arr.count; i++)
    {
        cbuuids[i] = [S2UUIDUtils CBUUIDWithS2UUID:nsdata_arr[i]];
    }
    
    return cbuuids;
}

+ (NSArray<NSData *> *)S2UUIDArrayWithCBUUIDArray:(NSArray<CBUUID *> *)cbuuid_arr
{
    NSMutableArray<NSData *> * bleuuids = [[NSMutableArray alloc] initWithCapacity:cbuuid_arr.count];
    
    for(size_t i = 0; i < cbuuid_arr.count; i++)
    {
        bleuuids[i] = [S2UUIDUtils S2UUIDWithCBUUID:cbuuid_arr[i]];
    }
    
    return bleuuids;
}

+ (NSDictionary<NSData *,NSData *> *)S2UUIDDictionaryWithCBUUIDDictionary:(NSDictionary<CBUUID *,NSData *> *)cbuuid_dict
{
    NSMutableDictionary<NSData *, NSData *> * bleuuids_dict = [[NSMutableDictionary alloc] initWithCapacity:cbuuid_dict.count];
    NSArray<CBUUID* > * keys = [cbuuid_dict allKeys];
    
    for(size_t i = 0; i < cbuuid_dict.count; i++)
    {
        CBUUID* cbuuid = keys[i];
        bleuuids_dict[[S2UUIDUtils S2UUIDWithCBUUID:cbuuid]] = cbuuid_dict[cbuuid];
    }
    
    return bleuuids_dict;
}
@end
