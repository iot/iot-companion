//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2DarwinBleDevice.m
//  song2
//
//  Created by Donatien Garnier on 25/02/2018.

//

#import "S2DarwinBleDevice.h"
#import "S2UUIDUtils.h"
#import "S2DarwinGattcService.h"
#import "S2DarwinGattcCharacteristic.h"
#import "S2DarwinGattcDescriptor.h"
#import "S2DarwinBleAdapter.h"

@interface S2DarwinBleDevice()
- (nullable S2DarwinGattcService*)findOrCreateS2GattcServiceForCBService:(nonnull CBService *)cb_service;
- (nullable S2DarwinGattcCharacteristic*)findOrCreateS2GattcCharacteristicForCBCharacteristic:(nonnull CBCharacteristic *)cb_characteristic;
- (nullable S2DarwinGattcDescriptor*)findOrCreateS2GattcDescriptorForCBDescriptor:(nonnull CBDescriptor *)cb_descriptor;
@end

@implementation S2DarwinBleDevice
-(instancetype)initWithAdapter:(S2DarwinBleAdapter *)adapter peripheral:(CBPeripheral *)peripheral
{
    self = [super init];
    self.s2_adapter = adapter;
    self.cb_peripheral = peripheral;
    self.s2_services = [[NSMutableArray alloc] init];
    self.lost = NO;
    
    // Defaults
    self.priv_status = S2BleDeviceStatusDisconnected;
    self.priv_rssi = INT32_MIN;
    self.priv_phy = S2BleDevicePhyPhy1m;
    self.priv_gattc_mtu = 20;
    
    [self.cb_peripheral setDelegate:self]; // Do it last
    return self;
}

- (int32_t)gattcMtu {
    return _priv_gattc_mtu;
}

- (nonnull NSArray<id<S2GattcService>> *)gattcServices {
    return _s2_services;
}

- (S2BleDevicePhy)phy {
    return _priv_phy;
}

- (int32_t)rssi {
    return _priv_rssi;
}

- (S2BleDeviceStatus)status {
    return _priv_status;
}

- (nonnull NSData *)identifier {
    NSUUID* dev_uuid = self.cb_peripheral.identifier;
    //NSString* [dev_uuid]
    return [dev_uuid.UUIDString dataUsingEncoding:NSUTF8StringEncoding];
}

-(void)makeLost {
    _lost = YES;
    [self clearCache];
}

- (void)clearCache {
    for (S2DarwinGattcService* service in _s2_services) {
        [service setLost:YES];
        for (S2DarwinGattcCharacteristic* characteristic in [service s2_characteristics]) {
            [characteristic setLost:YES];
            for (S2DarwinGattcDescriptor* descriptor in [characteristic s2_descriptors]) {
                [descriptor setLost:YES];
            }
            [[characteristic s2_descriptors] removeAllObjects];
        }
        [[service s2_characteristics] removeAllObjects];
    }
    [_s2_services removeAllObjects];
}

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray<CBService *> *)invalidatedServices
{
    // TODO - not supported by our API
}

- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error
{
    _priv_rssi = [RSSI intValue];
    [_s2_adapter onDeviceRssiUpdated:self error:S2BleErrorOk];
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if(!error) {
        // TODO update
        for(CBService* cb_service in [peripheral services])
        {
            [self findOrCreateS2GattcServiceForCBService:cb_service];
        }
    }
    [_s2_adapter onGattcServicesDiscovered:self error:[S2DarwinBleAdapter S2BleErrorFromNSError:error]];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error
{
    // TODO - not supported by our API
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    S2DarwinGattcService* s2_service = [self findOrCreateS2GattcServiceForCBService:service];
    if(!error) {
        // TODO update
        for(CBCharacteristic* cb_characteristic in [service characteristics])
        {
            [self findOrCreateS2GattcCharacteristicForCBCharacteristic:cb_characteristic];
        }
    }
    [_s2_adapter onGattcCharacteristicsDiscovered:s2_service error:[S2DarwinBleAdapter S2BleErrorFromNSError:error]];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    // Note: the Darwin API does not let us distinguish between the result of a read or a notification
    // https://github.com/WebBluetoothCG/web-bluetooth/issues/274
    // So, ... ugly fix
    // Maybe let's queue read requests instead?
    S2DarwinGattcCharacteristic* s2_characteristic = [self findOrCreateS2GattcCharacteristicForCBCharacteristic:characteristic];
    if([[s2_characteristic cb_characteristic] isNotifying])
    {
        [_s2_adapter onGattcCharacteristicUpdated:s2_characteristic error:[S2DarwinBleAdapter S2BleErrorFromNSError:error]];
    }
    else
    {
        [_s2_adapter onGattcCharacteristicRead:s2_characteristic error:[S2DarwinBleAdapter S2BleErrorFromNSError:error]];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    S2DarwinGattcCharacteristic* s2_characteristic = [self findOrCreateS2GattcCharacteristicForCBCharacteristic:characteristic];
    [_s2_adapter onGattcCharacteristicWritten:s2_characteristic error:[S2DarwinBleAdapter S2BleErrorFromNSError:error]];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    S2DarwinGattcCharacteristic* s2_characteristic = [self findOrCreateS2GattcCharacteristicForCBCharacteristic:characteristic];
    [_s2_adapter onGattcCharacteristicNotificationsSet:s2_characteristic error:[S2DarwinBleAdapter S2BleErrorFromNSError:error]];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    S2DarwinGattcCharacteristic* s2_characteristic = [self findOrCreateS2GattcCharacteristicForCBCharacteristic:characteristic];
    if(!error) {
        // TODO update
        for(CBDescriptor* cb_decriptor in [characteristic descriptors])
        {
            [self findOrCreateS2GattcDescriptorForCBDescriptor:cb_decriptor];
        }
    }
    [_s2_adapter onGattcDescriptorsDiscovered:s2_characteristic error:[S2DarwinBleAdapter S2BleErrorFromNSError:error]];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error
{
    S2DarwinGattcDescriptor* s2_descriptor = [self findOrCreateS2GattcDescriptorForCBDescriptor:descriptor];
    [_s2_adapter onGattcDescriptorRead:s2_descriptor error:[S2DarwinBleAdapter S2BleErrorFromNSError:error]];
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error
{
    S2DarwinGattcDescriptor* s2_descriptor = [self findOrCreateS2GattcDescriptorForCBDescriptor:descriptor];
    [_s2_adapter onGattcDescriptorWritten:s2_descriptor error:[S2DarwinBleAdapter S2BleErrorFromNSError:error]];
}

- (void)peripheralIsReadyToSendWriteWithoutResponse:(CBPeripheral *)peripheral
{
    // Not really mappable
}

- (S2DarwinGattcService *)findOrCreateS2GattcServiceForCBService:(CBService *)cb_service
{
    NSUInteger idx = [_s2_services indexOfObjectPassingTest:^BOOL(S2DarwinGattcService * _Nonnull s2_service, NSUInteger idx, BOOL * _Nonnull stop) {
        return ([s2_service cb_service] == cb_service);
    }];
    
    if( idx == NSNotFound )
    {
        // Create a new object and append to array
        [_s2_services addObject:[[S2DarwinGattcService alloc] initWithDevice:self service:cb_service]];
        
        // Update idx
        idx = [_s2_services count] - 1;
    }
    
    return _s2_services[idx];
}

- (S2DarwinGattcCharacteristic *)findOrCreateS2GattcCharacteristicForCBCharacteristic:(CBCharacteristic *)cb_characteristic
{
    S2DarwinGattcService* s2_service = [self findOrCreateS2GattcServiceForCBService:[cb_characteristic service]];
    
    NSUInteger idx = [[s2_service s2_characteristics] indexOfObjectPassingTest:^BOOL(S2DarwinGattcCharacteristic * _Nonnull s2_characteristic, NSUInteger idx, BOOL * _Nonnull stop) {
        return ([s2_characteristic cb_characteristic] == cb_characteristic);
    }];
    
    if( idx == NSNotFound )
    {
        // Create a new object and append to array
        [[s2_service s2_characteristics] addObject:[[S2DarwinGattcCharacteristic alloc] initWithService:s2_service characteristic:cb_characteristic]];
        
        // Update idx
        idx = [[s2_service s2_characteristics] count] - 1;
    }
    
    return [s2_service s2_characteristics][idx];
}

- (S2DarwinGattcDescriptor *)findOrCreateS2GattcDescriptorForCBDescriptor:(CBDescriptor *)cb_descriptor
{
    S2DarwinGattcCharacteristic* s2_characteristic = [self findOrCreateS2GattcCharacteristicForCBCharacteristic:[cb_descriptor characteristic]];
    
    NSUInteger idx = [[s2_characteristic s2_descriptors] indexOfObjectPassingTest:^BOOL(S2DarwinGattcDescriptor * _Nonnull s2_descriptor, NSUInteger idx, BOOL * _Nonnull stop) {
        return ([s2_descriptor cb_descriptor] == cb_descriptor);
    }];
    
    if( idx == NSNotFound )
    {
        // Create a new object and append to array
        [[s2_characteristic s2_descriptors] addObject:[[S2DarwinGattcDescriptor alloc] initWithCharacteristic:s2_characteristic descriptor:cb_descriptor]];
        
        // Update idx
        idx = [[s2_characteristic s2_descriptors] count] - 1;
    }
    
    return [s2_characteristic s2_descriptors][idx];
}

@end


