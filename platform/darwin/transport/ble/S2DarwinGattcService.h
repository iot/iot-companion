//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2DarwinGattcService.h
//  song2
//
//  Created by Donatien Garnier on 25/02/2018.

//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#include "gen/djinni/objc/S2GattcService.h"

@class S2DarwinBleDevice;
@class S2DarwinGattcCharacteristic;

@interface S2DarwinGattcService : NSObject<S2GattcService>
- (nonnull instancetype)initWithDevice:(nonnull S2DarwinBleDevice*) device
                             service:(nonnull CBService*)service;

@property (nullable, nonatomic, weak) S2DarwinBleDevice* s2_device;
@property (nonnull, nonatomic, strong) CBService* cb_service;
@property (nonnull, nonatomic, strong) NSMutableArray<S2DarwinGattcCharacteristic *>* s2_characteristics;
@property BOOL lost;
@end
