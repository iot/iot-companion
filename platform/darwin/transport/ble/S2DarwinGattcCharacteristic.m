//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2DarwinGattcCharacteristic.m
//  song2
//
//  Created by Donatien Garnier on 26/02/2018.

//

#import "S2DarwinGattcCharacteristic.h"
#import "S2UUIDUtils.h"
#import "S2DarwinGattcDescriptor.h"

@implementation S2DarwinGattcCharacteristic

-(instancetype)initWithService:(S2DarwinGattcService *)service characteristic:(CBCharacteristic *)characteristic
{
    self = [super init];
    self.s2_service = service;
    self.cb_characteristic = characteristic;
    self.s2_descriptors = [[NSMutableArray alloc] init];
    self.lost = FALSE;
    return self;
}

-(NSData *)uuid
{
    return [S2UUIDUtils S2UUIDWithCBUUID:[_cb_characteristic UUID]];
}

-(NSData *)value
{
    return [_cb_characteristic value];
}

-(NSArray<id<S2GattcDescriptor>> *)descriptors
{
    return _s2_descriptors;
}

- (BOOL)notifications
{
    return [_cb_characteristic isNotifying];
}

- (int32_t)identifier {
    // FIXME: add an equality function ...
    return (int32_t) self;
}


@end
