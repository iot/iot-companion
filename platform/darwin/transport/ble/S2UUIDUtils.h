//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2UUIDUtils.h
//  song2
//
//  Created by Donatien Garnier on 25/02/2018.

//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface S2UUIDUtils : NSObject
+ (nonnull CBUUID*) CBUUIDWithS2UUID:(nonnull NSData *)nsdata;
+ (nonnull NSData*) S2UUIDWithCBUUID:(nonnull CBUUID *)cbuuid;

+ (nonnull NSArray<CBUUID*>*) CBUUIDArrayWithS2UUIDArray:(nonnull NSArray<NSData*>*)nsdata_arr;
+ (nonnull NSArray<NSData*>*) S2UUIDArrayWithCBUUIDArray:(nonnull NSArray<CBUUID*>*) cbuuid_arr;
+ (nonnull NSDictionary<NSData*, NSData*>*) S2UUIDDictionaryWithCBUUIDDictionary:(nonnull NSDictionary<CBUUID*, NSData*>*) cbuuid_dict;
@end
