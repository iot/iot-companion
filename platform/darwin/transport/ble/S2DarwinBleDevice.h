//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2DarwinBleDevice.h
//  song2
//
//  Created by Donatien Garnier on 25/02/2018.

//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#include "gen/djinni/objc/S2BleDevice.h"
#include "gen/djinni/objc/S2BleDeviceStatus.h"

@class S2DarwinBleAdapter;
@class S2DarwinGattcService;

@interface S2DarwinBleDevice : NSObject<S2BleDevice, CBPeripheralDelegate>

- (nonnull instancetype)initWithAdapter:(nonnull S2DarwinBleAdapter*) adapter
                            peripheral:(nonnull CBPeripheral*)peripheral;

- (void)makeLost;

- (void)clearCache;

@property S2BleDeviceStatus priv_status;
@property int32_t priv_rssi;
@property S2BleDevicePhy priv_phy;
@property int32_t priv_gattc_mtu;
@property BOOL lost;

@property (nullable, nonatomic, weak) S2DarwinBleAdapter* s2_adapter;
@property (nonnull, nonatomic, strong) CBPeripheral* cb_peripheral;
@property (nonnull, nonatomic, strong) NSMutableArray<S2DarwinGattcService *>* s2_services;

@end
