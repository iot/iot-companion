//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2DarwinGattcCharacteristic.h
//  song2
//
//  Created by Donatien Garnier on 26/02/2018.

//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#include "gen/djinni/objc/S2GattcCharacteristic.h"

@class S2DarwinGattcService;
@class S2DarwinGattcDescriptor;

@interface S2DarwinGattcCharacteristic : NSObject<S2GattcCharacteristic>
- (nonnull instancetype)initWithService:(nonnull S2DarwinGattcService*) service
                               characteristic:(nonnull CBCharacteristic*) characteristic;

@property (nullable, nonatomic, weak) S2DarwinGattcService* s2_service;
@property (nonnull, nonatomic, strong) CBCharacteristic* cb_characteristic;
@property (nonnull, nonatomic, strong) NSMutableArray<S2DarwinGattcDescriptor *>* s2_descriptors;
@property BOOL lost;
@end
