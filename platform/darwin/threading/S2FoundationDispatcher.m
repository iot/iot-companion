//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "S2FoundationDispatcher.h"

@interface S2FoundationDispatcher()
- (BOOL)notifyUnsafe:(nullable S2Queue *)queue delayMs:(int64_t)delayMs;
- (BOOL)startUnsafe;
@end

@implementation S2FoundationDispatcher

static void s2_dispatcher_impl_queue_finalizer(void* ctx);

- (instancetype)initWithName:(NSString *)name
{
    self = [super init];
    self.dispatch_queue = dispatch_queue_create([name UTF8String], NULL);
    self.priv_status = S2DispatcherStatusPending;
    self.pending_queues = [[NSMutableArray alloc] init];
    self.lock = [[NSLock alloc] init];
    
    self.own_dispatch_queue = YES;
    dispatch_set_context(self.dispatch_queue, (__bridge void*)self);
    dispatch_set_finalizer_f(self.dispatch_queue, s2_dispatcher_impl_queue_finalizer);
    
    return self;
}


- (instancetype)initWithName:(NSString *)name dispatchQueue:(dispatch_queue_t)dispatchQueue
{
    self = [super init];
    self.dispatch_queue = dispatchQueue;
    self.priv_status = S2DispatcherStatusPending;
    self.pending_queues = [[NSMutableArray alloc] init];
    self.lock = [[NSLock alloc] init];
    
    // We do not own queue
    self.own_dispatch_queue = NO;
    
    return self;
}

- (dispatch_queue_t)dispatchQueue
{
    return _dispatch_queue;
}

void s2_dispatcher_impl_queue_finalizer(void* ctx)
{
    //S2FoundationDispatcher* self = (__bridge S2FoundationDispatcher*) ctx;
    //self.priv_status = S2DispatcherStatusTerminated;
}

- (BOOL)isEqual:(nullable id<S2Dispatcher>)other;
{
    return self == other;
}

- (BOOL)start
{
    BOOL result = NO;
    
    [_lock lock];
    
    result = [self startUnsafe];
    
exit:
    [_lock unlock];
    return result;
}

- (BOOL)startUnsafe
{
    if(_priv_status != S2DispatcherStatusPending)
    {
        return NO;
    }
    
    _priv_status = S2DispatcherStatusRunning;
    
    for(S2Queue* queue in _pending_queues)
    {
        // Re-notify
        [self notifyUnsafe:queue delayMs:0];
    }
    
    // Clear pending queues
    [_pending_queues removeAllObjects];
    
    return YES;
}

- (BOOL)terminate:(BOOL)kill
{
    BOOL result = NO;
    
    [_lock lock];
    if(_priv_status == S2DispatcherStatusPending) // Shortcut from pending to terminated
    {
        if(!kill)
        {
            [self startUnsafe];
        }
        else
        {
            // Set is as running but does not notify
            _priv_status = S2DispatcherStatusRunning;
            // Clear pending queues
            [_pending_queues removeAllObjects];
        }
    }
    
    if(_priv_status >= S2DispatcherStatusTerminating)
    {
        result = NO;
        goto exit;
    }
    
    result = YES;
    
    _priv_status = S2DispatcherStatusTerminating;
    
    if(!kill || !_own_dispatch_queue)
    {
        // _dispatch_queue == dispatch_get_main_queue()
        
        // This is blocking so release lock first
        [_lock unlock];
        dispatch_queue_t dispatch_queue = _dispatch_queue;
        dispatch_barrier_sync(dispatch_queue, ^{
            // Wait for all pending work to be processed (will block)
        });
        [_lock lock];
    }
    
    if(!_own_dispatch_queue)
    {
        // If this is the main dispatch queue (or a queue we don't own), finalizer will never get called, so set status here - otherwise check it's been set correctly
        _priv_status = S2DispatcherStatusTerminated;
    }
    
    _dispatch_queue = nil; // Kill queue and check status
    NSAssert(_priv_status == S2DispatcherStatusTerminated, @"Dispatcher hasn't reached terminated status");
    
exit:
    [_lock unlock];
    return result;
}


- (BOOL)notify:(nullable S2Queue *)queue delayMs:(int64_t)delayMs
{
    BOOL result = NO;
    [_lock lock];
    result = [self notifyUnsafe:queue delayMs:delayMs];
    [_lock unlock];
    return result;
}

- (BOOL)notifyUnsafe:(nullable S2Queue *)queue delayMs:(int64_t)delayMs;
{
    if(_priv_status == S2DispatcherStatusPending)
    {
        // Add queue to list of pending queues and return
        [_pending_queues addObject:queue];
        return YES;
    }
    
    if(_priv_status != S2DispatcherStatusRunning)
    {
        // Cannot accept new work
        return NO;
    }
    
    if(delayMs > 0)
    {
        // 1 ms is 1E6 ns
        dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, delayMs * 1E6L);
        
        dispatch_after(when, _dispatch_queue, ^{
            int64_t delayMs = [queue process];
            if( delayMs >= 0 )
            {
                [self notify:queue delayMs:delayMs];
            }
        });
    }
    else
    {
        dispatch_async(_dispatch_queue, ^{
            int64_t delayMs = [queue process];
            if( delayMs >= 0 )
            {
                [self notify:queue delayMs:delayMs];
            }
        });
    }
    
    return YES;
}

- (S2DispatcherStatus)status {
    S2DispatcherStatus result;
    [_lock lock];
    result = _priv_status;
    [_lock unlock];
    return result;
}


@end
