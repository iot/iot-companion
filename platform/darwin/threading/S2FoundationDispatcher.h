//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#pragma once

#import <Foundation/Foundation.h>

#include "S2Dispatcher.h"
#include "S2DispatcherStatus.h"
#include "S2Queue.h"

@interface S2FoundationDispatcher : NSObject <S2Dispatcher>

- (nonnull instancetype)initWithName:(NSString* _Nonnull)name;

- (nonnull instancetype)initWithName:(NSString* _Nonnull)name dispatchQueue:(nonnull dispatch_queue_t)dispatchQueue;

- (nullable dispatch_queue_t)dispatchQueue;

@property BOOL own_dispatch_queue;
@property (nonatomic, strong, nullable) dispatch_queue_t dispatch_queue;
@property (nonatomic, strong, nonnull) NSMutableArray<S2Queue*>* pending_queues;
@property (nonatomic, strong, nonnull) NSLock* lock;
@property S2DispatcherStatus priv_status;
@end

