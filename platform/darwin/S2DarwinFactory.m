//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  S2DarwinFactory.m
//  Song2 Tests
//
//  Created by Donatien Garnier on 03/01/2018.

//

#import "S2DarwinFactory.h"
#import "S2FoundationDispatcher.h"
#import "transport/ble/S2DarwinBleAdapter.h"

#import "S2DarwinBleAdapter.h"

@interface S2DarwinFactory()
@property (strong, nonnull, nonatomic) S2DarwinBleAdapter* bleAdapter;
@end

@implementation S2DarwinFactory

-(instancetype)init
{
    self = [super init];
    S2FoundationDispatcher* mainDispatcher = [[S2FoundationDispatcher alloc] initWithName:@"main" dispatchQueue:dispatch_get_main_queue()];

    self.mainDispatcher = mainDispatcher;
    
    self.bleAdapter = [[S2DarwinBleAdapter alloc] initWithQueue:dispatch_queue_create("BLE", DISPATCH_QUEUE_SERIAL)];
    
    return self;
}

-(id<S2Dispatcher>)createDispatcher:(NSString *)name
{
    S2FoundationDispatcher* dispatcher = [[S2FoundationDispatcher alloc] initWithName:name];
    return dispatcher;
}

- (NSString *)getResourcePath:(S2PlatformBundle)bundleRef resourceName:(NSString *)resourceName
{
    NSBundle *bundle;
    if( bundleRef == S2PlatformBundleApp )
    {
        // Get the app's main bundle
        bundle = [NSBundle mainBundle];
    }
    else // bundleRef == S2PlatformBundleSdk
    {
        // Get the SDK's framework bundle - S2DarwinFactory should be part of it
        bundle = [NSBundle bundleForClass:[S2DarwinFactory class]];
    }
    return [NSString stringWithFormat:@"%@/%@", [bundle resourcePath], resourceName];
}

- (NSString *)getStoragePath
{
    NSString *rootAppSupportDirectory = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) lastObject];
    
    // Append bundle id
    NSMutableString *appSupportDirectory = [NSMutableString stringWithString:rootAppSupportDirectory];
    [appSupportDirectory appendString:@"/"];
    [appSupportDirectory appendString:[[NSBundle mainBundle] bundleIdentifier] ];
    
    // Check whether this exists
    if (![[NSFileManager defaultManager] fileExistsAtPath:appSupportDirectory isDirectory:NULL]) {
        NSError *error = nil;
        
        // Create directory
        if (![[NSFileManager defaultManager] createDirectoryAtPath:appSupportDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSLog(@"Could not create application support directory: %@", error.localizedDescription);
            return NULL;
        }
        
        // Exclude for iCloud backups
        NSURL *appSupportDirectoryUrl = [NSURL fileURLWithPath:appSupportDirectory];
        if (![appSupportDirectoryUrl setResourceValue:@YES
                            forKey:NSURLIsExcludedFromBackupKey
                             error:&error])
        {
            NSLog(@"Could not exclude application support directory from backup: %@", error.localizedDescription);
            return NULL;
        }
    }
    
    return appSupportDirectory;
}

-(id<S2Dispatcher>)getMainDispatcher
{
    return _mainDispatcher;
}

-(void)runMainDispatcher
{
    // Run main dispatcher
//    dispatch_main();
    CFRunLoopRun();
}

-(id<S2BleAdapter>)getBleAdapter {
    return _bleAdapter;
}

- (nullable id<S2NfcAdapter>)getNfcAdapter {
    // TODO
    return NULL;
}


- (nullable id<S2PlatformHelper>)getPlatformHelper {
    // TODO
    return NULL;
}



@end

