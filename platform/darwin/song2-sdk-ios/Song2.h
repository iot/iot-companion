//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  song2_sdk.h
//  song2-sdk
//
//  Created by Donatien Garnier on 03/04/2018.

//

#import <Foundation/Foundation.h>

//! Project version number for song2_sdk.
FOUNDATION_EXPORT double song2_sdkVersionNumber;

//! Project version string for song2_sdk.
FOUNDATION_EXPORT const unsigned char song2_sdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <song2_sdk/PublicHeader.h>

#import <song2_ios/S2DarwinFactory.h>
#import <song2_ios/S2Daemon.h>
#import <song2_ios/S2JsApp.h>
#import <song2_ios/S2FoundationDispatcher.h>
#import <song2_ios/S2Dispatcher.h>

