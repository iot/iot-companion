//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//



#include <cstdlib>
#include <mbed.h>
#include <events/mbed_events.h>

#include "ble/BLE.h"

#include "ArpcWifiCommissioningService.h"
#include "song2/arpc/ArpcEndpoint.h"
#include "song2/ArpcOverBleFacade.h"

#if MBED_CONF_APP_WIFI_SHIELD == WIFI_ISM43362
#include "ISM43362Interface.h"
typedef ISM43362Interface WiFiInterfaceImpl;
#else
#error Please configure a WiFi module
#endif

static EventQueue eventQueue(/* event count */ 32 * EVENTS_EVENT_SIZE);

const uint8_t EDDYSTONE_UUID[] = {0xAA, 0xFE};
const uint8_t EDDYSTOME_URL_FRAME_BASE[] = {0xAA, 0xFE, 0x10, (uint8_t)-3, 3 /*https://*/, 'm', 'b', 'e', 'd', 3 /* .net */};

static DigitalOut led(LED1, 1);

void setLed(bool on)
{
    led = on;
}

void blinkCallback(void)
{
    led = !led;
}

using namespace arm::song2::mbed_os;
class WifiService : public arm::song2::mbed_os::generated::ArpcWifiService {
public:
    WifiService() : generated::ArpcWifiService(), _blinker(0)
    {
        memcpy(&_parameters, &arpc::details::Prototype<com_arm_commissioning_WiFiCommissioningParameters>::value(), sizeof(com_arm_commissioning_WiFiCommissioningParameters));
        memcpy(&_info, &arpc::details::Prototype<com_arm_commissioning_WiFiCommissioningInfo>::value(), sizeof(com_arm_commissioning_WiFiCommissioningInfo));
        _wifi_thread.start(callback(this, &WifiService::wifi_queue_thread));
            
        // Blink!
        blink(BLINK_SLOW);
    }
    ~WifiService() {}

    // Parameters
private:
    Thread _wifi_thread;
    EventQueue _wifi_queue;
    rtos::Mutex _mtx;
    int _blinker;

    WiFiInterfaceImpl _wifi;

    com_arm_commissioning_WiFiCommissioningParameters _parameters;
    com_arm_commissioning_WiFiCommissioningInfo _info;

    enum blink_speed_t {
        BLINK_SLOW,
        BLINK_FAST,
        BLINK_SOLID_GREEN
    };

    void blink(blink_speed_t speed)
    {
        if(_blinker) {
            eventQueue.cancel(_blinker);
            _blinker = 0;
        }

        switch(speed) {
            case BLINK_SLOW:
                _blinker = eventQueue.call_every(500, blinkCallback);
                break;
            case BLINK_FAST:
                _blinker = eventQueue.call_every(50, blinkCallback);
                break;
            case BLINK_SOLID_GREEN:
            default:
                setLed(true);
                break;
        }

    }

    void wifi_queue_thread() {
        _wifi_queue.dispatch_forever();
    }

    // On WiFi queue
    void disconnect_async(const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage> >& call)
    {
        _wifi.disconnect();

        eventQueue.call<WifiService, void, const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage> >& >
        (this, &WifiService::disconnect_complete, call);
    }

    void disconnect_complete(const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage> >& call)
    {
        printf("Disonnected\r\n");
        call->success();
    }
    
    void connect_async(const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage> >& call)
    {
        com_arm_commissioning_WiFiCommissioningParameters params;
        _mtx.lock();
        memcpy(&params, &_parameters, sizeof(com_arm_commissioning_WiFiCommissioningParameters));
        memcpy(&_info, &arpc::details::Prototype<com_arm_commissioning_WiFiCommissioningInfo>::value(), sizeof(com_arm_commissioning_WiFiCommissioningInfo));
        _mtx.unlock();

        if(params.has_dhcp) {
            _wifi.set_dhcp(_parameters.dhcp);
        }

        printf("Trying to connect to %s\r\n", params.ssid);

        nsapi_error_t ret = _wifi.connect(params.ssid, params.passphrase, (nsapi_security_t)params.security, params.has_channel?params.channel:0);
        _mtx.lock();

        printf("connect() returned = %d\r\n", ret);

        // Update status
        _info.has_status = true;
        
        // TODO do a ping
        switch(ret) {
            case NSAPI_ERROR_OK:
                _info.status = com_arm_commissioning_WiFiCommissioningStatusError_NoError;
                break;
            case NSAPI_ERROR_CONNECTION_TIMEOUT:
            case NSAPI_ERROR_NO_CONNECTION:
                _info.status = com_arm_commissioning_WiFiCommissioningStatusError_Timeout;
                break;
            case NSAPI_ERROR_NO_SSID:
                _info.status = com_arm_commissioning_WiFiCommissioningStatusError_APNotFound;
                break;
            case NSAPI_ERROR_AUTH_FAILURE:
                _info.status = com_arm_commissioning_WiFiCommissioningStatusError_AuthProblem;
                break;
            case NSAPI_ERROR_CONNECTION_LOST:
                _info.status = com_arm_commissioning_WiFiCommissioningStatusError_WeakSignal;
                break;
            case NSAPI_ERROR_DHCP_FAILURE:
                _info.status = com_arm_commissioning_WiFiCommissioningStatusError_DHCPFailed;
                break;
            default:
                _info.status = com_arm_commissioning_WiFiCommissioningStatusError_DeviceError;
                break;
        }

        if(ret == NSAPI_ERROR_OK) {
            // At the moment, Scout does not display this data - do not send it for now

            // RSSI
            // _info.has_rssi = true;
            // _info.rssi = _wifi.get_rssi();

            // TODO need to do a scan()
            // // BSSID
            // _info.has_bssid = true;
            // strcpy(_info.bssid, _wifi.get_bssid());

            // Same for channel

            // IP Address, netmask, gateway, mac
            // _info.has_ipAddress = _info.has_netmask = _info.has_gateway = _info.has_macAddress = true;
            // strcpy(_info.ipAddress, _wifi.get_ip_address());
            // strcpy(_info.netmask, _wifi.get_netmask());
            // strcpy(_info.gateway, _wifi.get_gateway());
            // strcpy(_info.macAddress, _wifi.get_mac_address());
        }
        _mtx.unlock();

        eventQueue.call<WifiService, void, const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage> >& >
            (this, &WifiService::connect_complete, call);
    }

    void connect_complete(const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage> >& call)
    {
        _mtx.lock();
        bool is_connected = _info.status == com_arm_commissioning_WiFiCommissioningStatusError_NoError;
        _mtx.unlock();

        printf("Connected\r\n");
        if(is_connected)
        {
            blink(BLINK_SOLID_GREEN);
        }
        else 
        {
            blink(BLINK_SLOW);
        }
        call->success();
    }
    
public:
    
    virtual void disconnect(const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage> >& call)
    {
        printf("Disconnecting\r\n");
        blink(BLINK_SLOW);
        _wifi_queue.call<WifiService, void, const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage> >& >
            (this, &WifiService::disconnect_async, call);
    }
    
    virtual void connect(const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage> >& call)
    {
        printf("Connecting\r\n");
        blink(BLINK_FAST);
        _wifi_queue.call<WifiService, void, const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage> >& >
            (this, &WifiService::connect_async, call);
    }
    
    virtual void set_parameters(const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_WiFiCommissioningParameters, com_arm_commissioning_EmptyMessage> >& call)
    {
        printf("Setting parameters\r\n");
        _mtx.lock();
        memcpy(&_parameters, &call->request(), sizeof(com_arm_commissioning_WiFiCommissioningParameters));
        _mtx.unlock();
        call->success();
    }
    
    virtual void get_info(const mbed::util::SharedPointer< arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_WiFiCommissioningInfo> >& call)
    {
        printf("Getting info\r\n");
        _mtx.lock();
        memcpy(&call->response(), &_info, sizeof(com_arm_commissioning_WiFiCommissioningInfo));
        _mtx.unlock();
        call->success();
    }
};

using arm::song2::mbed_os::ArpcOverBleFacade;
using arm::song2::mbed_os::arpc::ArpcEndpoint;

static ArpcOverBleFacade arpc_over_ble;
static ArpcEndpoint arpc_endpoint;
static WifiService wifi_service;


void disconnectionCallback(const Gap::DisconnectionCallbackParams_t *params)
{
    BLE::Instance().gap().startAdvertising();
}

/**
 * This function is called when the ble initialization process has failled
 */
void onBleInitError(BLE &ble, ble_error_t error)
{
    /* Initialization error handling should go here */
}

void printMacAddress()
{
    /* Print out device MAC address to the console*/
    Gap::AddressType_t addr_type;
    Gap::Address_t address;
    BLE::Instance().gap().getAddress(&addr_type, address);
    printf("DEVICE MAC ADDRESS: ");
    for (int i = 5; i >= 1; i--){
        printf("%02x:", address[i]);
    }
    printf("%02x\r\n", address[0]);
}

// BLE init complete callback
void bleInitComplete(BLE::InitializationCompleteCallbackContext *params)
{
    // BLE init
    BLE&        ble   = params->ble;
    ble_error_t error = params->error;
    
    if (error != BLE_ERROR_NONE) {
        /* In case of error, forward the error handling to onBleInitError */
        onBleInitError(ble, error);
        return;
    }

    // setup facade: register service and install callbacks managing the lifecycle
    // of the connection.
    arpc_over_ble.register_facade(ble);

    /* Ensure that it is the default instance of BLE */
    if(ble.getInstanceID() != BLE::DEFAULT_INSTANCE) {
        return;
    }

    ble.gap().onDisconnection(disconnectionCallback);

    /* Setup advertising */
    // Build Eddystone Frame
    static const char* alphanumeric = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static size_t alphanumeric_count = strlen(alphanumeric);

    // Build URL Frame
    static uint8_t eddystone_url_frame[22];
    memcpy(eddystone_url_frame, EDDYSTOME_URL_FRAME_BASE, sizeof(EDDYSTOME_URL_FRAME_BASE));

    // Add random characters
    // use the Bluetooth address as seed
    Gap::AddressType_t addr_type;
    Gap::Address_t addr;
    ble.gap().getAddress(&addr_type, addr);
    uint8_t* random_data = addr;
    srand(*((unsigned int*)random_data));

    for(size_t i = sizeof(EDDYSTOME_URL_FRAME_BASE); i < sizeof(eddystone_url_frame); i++) {
        eddystone_url_frame[i] = alphanumeric[ rand() % alphanumeric_count ];
    }

    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::BREDR_NOT_SUPPORTED | GapAdvertisingData::LE_GENERAL_DISCOVERABLE);
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LIST_16BIT_SERVICE_IDS, EDDYSTONE_UUID, sizeof(EDDYSTONE_UUID));
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::SERVICE_DATA, eddystone_url_frame, sizeof(eddystone_url_frame));

    ble.gap().setAdvertisingType(GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED);
    ble.gap().setAdvertisingInterval(100); /* 100ms */
    ble.gap().startAdvertising();

    printMacAddress();
}

void scheduleBleEventsProcessing(BLE::OnEventsToProcessCallbackContext* context) {
    BLE &ble = BLE::Instance();
    eventQueue.call(Callback<void()>(&ble, &BLE::processEvents));
}



int main()
{
    // ARPC init
    arpc_over_ble.bind_connection(arpc_endpoint.connection());
    arpc_endpoint.register_service(wifi_service);

    // BLE init
    BLE &ble = BLE::Instance();
    ble.onEventsToProcess(scheduleBleEventsProcessing);
    ble.init(bleInitComplete);

    eventQueue.dispatch_forever();
}