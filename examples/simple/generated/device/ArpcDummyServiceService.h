//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef ARPC_ARPC_SERVICE_DUMMY_SERVICE_
#define ARPC_ARPC_SERVICE_DUMMY_SERVICE_

#include "arpc-device/ArpcService.h"

#include "dummy_service.pb.h"

namespace arpc {
    namespace mbed {
        namespace generated {
            class ArpcDummyService : public arpc::mbed::ArpcService {
            public:
                ArpcDummyService();
                virtual ~ArpcDummyService();

            protected:
                virtual void set_integer(const ::mbed::SharedPtr<arpc::mbed::ArpcMethodCall<arm_song2_test_IntegerMessage, arm_song2_test_EmtpyMessage> >& call) = 0;
                virtual void get_integer(const ::mbed::SharedPtr<arpc::mbed::ArpcMethodCall<arm_song2_test_EmtpyMessage, arm_song2_test_IntegerMessage> >& call) = 0;
                virtual void get_string(const ::mbed::SharedPtr<arpc::mbed::ArpcMethodCall<arm_song2_test_EmtpyMessage, arm_song2_test_StringMessage> >& call) = 0;
                virtual void set_string(const ::mbed::SharedPtr<arpc::mbed::ArpcMethodCall<arm_song2_test_StringMessage, arm_song2_test_EmtpyMessage> >& call) = 0;
                virtual void say_hello(const ::mbed::SharedPtr<arpc::mbed::ArpcMethodCall<arm_song2_test_StringMessage, arm_song2_test_EmtpyMessage> >& call) = 0;
            private:
                virtual void get_method_descriptors(const arpc::mbed::ArpcMethodDescriptor** methods, size_t* count) const;
                virtual const char* get_name() const;

                static const char* _s_name;
                static size_t _s_methods_count;
                static arpc::mbed::ArpcMethodDescriptor _s_methods_descriptors[];
            };

        }
    }
}

#endif /* ARPC_ARPC_SERVICE_DUMMY_SERVICE_ */