//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "dummy_service.pb.h"
#include "ArpcDummyServiceService.h"

#include "pb/pb.h"

namespace arpc {
namespace mbed {
namespace generated {

ArpcDummyService::ArpcDummyService() : arpc::mbed::ArpcService()
{

}

ArpcDummyService::~ArpcDummyService()
{

}


size_t ArpcDummyService::_s_methods_count = 5;

arpc::mbed::ArpcMethodDescriptor ArpcDummyService::_s_methods_descriptors[] = {
    {"SetInteger", arm_song2_test_IntegerMessage_fields, arm_song2_test_EmtpyMessage_fields,
        arpc::mbed::ArpcMethodCall<arm_song2_test_IntegerMessage, arm_song2_test_EmtpyMessage>::make_shared,
        (const arpc::mbed::ArpcMethodRequestCallback_t) &ArpcDummyService::set_integer
        },
    {"GetInteger", arm_song2_test_EmtpyMessage_fields, arm_song2_test_IntegerMessage_fields,
        arpc::mbed::ArpcMethodCall<arm_song2_test_EmtpyMessage, arm_song2_test_IntegerMessage>::make_shared,
        (const arpc::mbed::ArpcMethodRequestCallback_t) &ArpcDummyService::get_integer
        },
    {"GetString", arm_song2_test_EmtpyMessage_fields, arm_song2_test_StringMessage_fields,
        arpc::mbed::ArpcMethodCall<arm_song2_test_EmtpyMessage, arm_song2_test_StringMessage>::make_shared,
        (const arpc::mbed::ArpcMethodRequestCallback_t) &ArpcDummyService::get_string
        },
    {"SetString", arm_song2_test_StringMessage_fields, arm_song2_test_EmtpyMessage_fields,
        arpc::mbed::ArpcMethodCall<arm_song2_test_StringMessage, arm_song2_test_EmtpyMessage>::make_shared,
        (const arpc::mbed::ArpcMethodRequestCallback_t) &ArpcDummyService::set_string
        },
    {"SayHello", arm_song2_test_StringMessage_fields, arm_song2_test_EmtpyMessage_fields,
        arpc::mbed::ArpcMethodCall<arm_song2_test_StringMessage, arm_song2_test_EmtpyMessage>::make_shared,
        (const arpc::mbed::ArpcMethodRequestCallback_t) &ArpcDummyService::say_hello
        },
};

const char* ArpcDummyService::_s_name = "arm.song2.test.Dummy";

void ArpcDummyService::get_method_descriptors(const arpc::mbed::ArpcMethodDescriptor** methods, size_t* count) const {
    *methods = _s_methods_descriptors;
    *count = _s_methods_count;
}

const char* ArpcDummyService::get_name() const {
    return _s_name;
}


}

namespace details {
template<> const arm_song2_test_IntegerMessage& Prototype<arm_song2_test_IntegerMessage>::value() {
    static arm_song2_test_IntegerMessage prototype = arm_song2_test_IntegerMessage_init_default;
    return prototype;
}
template<> const arm_song2_test_StringMessage& Prototype<arm_song2_test_StringMessage>::value() {
    static arm_song2_test_StringMessage prototype = arm_song2_test_StringMessage_init_default;
    return prototype;
}
template<> const arm_song2_test_EmtpyMessage& Prototype<arm_song2_test_EmtpyMessage>::value() {
    static arm_song2_test_EmtpyMessage prototype = arm_song2_test_EmtpyMessage_init_default;
    return prototype;
}
}

}
}