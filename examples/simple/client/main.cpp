//
//  main.cpp
//  song2
//
//  Created by Donatien Garnier on 08/10/2021.
//  Copyright © 2021 ARM Ltd. All rights reserved.
//

#include <stdio.h>
#include "cpp/platform/Platform.hpp"
#include "gen/djinni/cpp/daemon.hpp"

#include "cpp/threading/Queue.hpp"
#include "cpp/threading/BlockingDispatcher.hpp"
#include "cpp/threading/Promise.hpp"
#include "cpp/threading/Observable.hpp"

#include "cpp/device/Device.hpp"

#include "cpp/transport/ble/BleScanner.hpp"
#include "cpp/platform/Platform.hpp"

#include "cpp/logging/Logging.hpp"

#include "cpp/srtp/SrtpEndpoint.hpp"
#include "cpp/arpc/ArpcEndpoint.hpp"
#include "cpp/arpc/ArpcException.hpp"
#include "cpp/arpc/ProtoStore.hpp"

using namespace arm::song2::platform;

int32_t arm::song2::platform::Daemon::run(const std::vector<std::string> & args, const std::shared_ptr<PlatformFactory> & platform_factory)
{
    platform::set_platform_factory(platform_factory);
    auto dptch = threading::make_blocking_dispatcher();
    std::unique_ptr<transport::BleScanner> scanner;
    std::shared_ptr<transport::Device> device = nullptr;
    std::unique_ptr<srtp::SrtpEndpoint> srtp = nullptr;
    std::unique_ptr<arpc::ArpcEndpoint> arpc = nullptr;
    
    // Recover blob file
    auto blob_file = platform::platform_factory()->get_resource_path(platform::PlatformBundle::APP, "blob.pb");
    
    // Instantiate proto store
    auto proto_store = std::make_shared<arpc::ProtoStore>();
    
    // Do this on dispatcher
    auto queue = threading::make_queue(dptch);
    queue->post([&](){
        // Create BLE scanner
        scanner = std::make_unique<transport::BleScanner>(platform::platform_factory()->get_ble_adapter());
        
        // Scan for devices
        scanner->visible_devices()->next([&](std::future<std::shared_ptr<transport::Device>>& dev_fut){
            if(device == nullptr) {
                device = dev_fut.get();
            }
            
            device->connect()->then([&](std::future<void>& f){
                f.get();
                scanner->stop_scanning();
                                
                // Register protobuf blob
                proto_store->register_services_from_files({blob_file});
                
                // Instantiate arpc and srtp
                srtp = std::make_unique<srtp::SrtpEndpoint>(device->open_endpoint_for_channel(0));
                arpc = std::make_unique<arpc::ArpcEndpoint>(srtp->open());
                arpc->set_proto_store(proto_store);
                
                // Say hello!
                arpc->call_method_json("arm.song2.test.Dummy", "SayHello", json_t("{a_string: \"everyone\"}"))->then([&](std::future<json_t>& fut)
                                                                                                      {
                                                                                                            fut.get();
                                                                                                            device->disconnect()->then([&](std::future<void>& fut){
                                                                                                            dptch->terminate(false);
                                                                                                        }, queue);
                                                                                                      }, queue);
                
            }, queue);
        }, queue);
        
        scanner->adapter_status()->next([&device, &scanner, &queue](std::future<platform::BleAdapterStatus>& status_fut){
            if(device == nullptr) {
                auto status = status_fut.get();
                if(status == platform::BleAdapterStatus::ENABLED) {
                    scanner->start_scanning();
                }
            }
        }, queue);
        
        scanner->start_scanning();
    });
    
    dptch->start();
        
    while(dptch->status() != platform::DispatcherStatus::TERMINATED) {
        dptch->wait();
        dptch->process();
    }
    
    return 0;
}

