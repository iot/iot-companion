/* mbed Microcontroller Library
 * Copyright (c) 2021 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 */

#include "mbed.h"
#include <cstdlib>

#include "ble/BLE.h"

#include "arpc-device/ArpcEndpoint.h"
#include "arpc-mbed/ArpcOverBleFacade.h"
#include "../generated/device/ArpcDummyServiceService.h"

class DummyService : public arpc::mbed::generated::ArpcDummyService {
private:
    int32_t _integer;
    char _string[128];
    
public:
    DummyService() : arpc::mbed::generated::ArpcDummyService(), _integer(0) {
        _string[0] = '\0';
    }
    virtual ~DummyService() {}

    virtual void set_integer(const ::mbed::SharedPtr<arpc::mbed::ArpcMethodCall<arm_song2_test_IntegerMessage, arm_song2_test_EmtpyMessage> >& call) {
        _integer = call->request().an_int;
        call->success();
    }
    
    virtual void get_integer(const ::mbed::SharedPtr<arpc::mbed::ArpcMethodCall<arm_song2_test_EmtpyMessage, arm_song2_test_IntegerMessage> >& call) {
        call->response().an_int = _integer;
        call->success();
    }
    
    virtual void get_string(const ::mbed::SharedPtr<arpc::mbed::ArpcMethodCall<arm_song2_test_EmtpyMessage, arm_song2_test_StringMessage> >& call) {
        strncpy(call->response().a_string, _string, sizeof(call->response().a_string));
        call->response().a_string[sizeof(call->response().a_string) - 1] = 0;
        call->success();
    }
    
    virtual void set_string(const ::mbed::SharedPtr<arpc::mbed::ArpcMethodCall<arm_song2_test_StringMessage, arm_song2_test_EmtpyMessage> >& call) {
        strncpy(_string, call->request().a_string, sizeof(call->request().a_string));
        _string[sizeof(call->request().a_string) - 1] = 0;
        call->success();
    }
    
    virtual void say_hello(const ::mbed::SharedPtr<arpc::mbed::ArpcMethodCall<arm_song2_test_StringMessage, arm_song2_test_EmtpyMessage> >& call) {
        printf("Hello, ");
        printf(call->request().a_string);
        printf("!\r\n");
        call->success();
    }
};

static EventQueue eventQueue(/* event count */ 32 * EVENTS_EVENT_SIZE);

const UUID EDDYSTONE_UUID = 0xFEAA;
const uint8_t EDDYSTOME_URL_FRAME_BASE[] = {0x10, (uint8_t)-3, 3 /*https://*/, 'm', 'b', 'e', 'd', 3 /* .net */};


class SimpleArpcDemo : ble::Gap::EventHandler {
public:
    SimpleArpcDemo(EventQueue& queue) : _queue(queue), _arpc_endpoint(), _adv_data_builder(_adv_buffer) {

    }

    void start() {
        // ARPC init
        _arpc_over_ble.bind_connection(_arpc_endpoint.connection());
        _arpc_endpoint.register_service(_dummy_service);

        // BLE init
        BLE &ble = BLE::Instance();

        ble.onEventsToProcess(makeFunctionPointer(this, &SimpleArpcDemo::scheduleBleEventsProcessing));
        ble.init(this, &SimpleArpcDemo::bleInitComplete);
    }

private:
    EventQueue& _queue;
    arpc::mbed::ArpcOverBleFacade _arpc_over_ble;
    arpc::mbed::ArpcEndpoint _arpc_endpoint;
    ChainableGapEventHandler _chainable_gap_event_handler;
    ChainableGattServerEventHandler _chainable_gatt_server_event_handler;
    DummyService _dummy_service;
    uint8_t _adv_buffer[ble::LEGACY_ADVERTISING_MAX_SIZE];
    ble::AdvertisingDataBuilder _adv_data_builder;

    void onDisconnectionComplete(const ble::DisconnectionCompleteEvent &event) override
    {
        printf("Client disconnected\r\n");
        BLE::Instance().gap().startAdvertising(ble::LEGACY_ADVERTISING_HANDLE);
    }

    /**
    * This function is called when the ble initialization process has failled
    */
    void onBleInitError(BLE &ble, ble_error_t error)
    {
        /* Initialization error handling should go here */
        printf("Could not initialize BLE (error %u)\r\n", error);
    }

    void printMacAddress()
    {
        /* Print out device MAC address to the console*/
        ble::own_address_type_t addr_type;
        ble::address_t address;
        BLE::Instance().gap().getAddress(addr_type, address);
        printf("DEVICE MAC ADDRESS: ");
        for (int i = 5; i >= 1; i--){
            printf("%02x:", address[i]);
        }
        printf("%02x\r\n", address[0]);
    }

    // BLE init complete callback
    void bleInitComplete(BLE::InitializationCompleteCallbackContext *params)
    {
        // BLE init
        BLE&        ble   = params->ble;
        ble_error_t error = params->error;
        
        if (error != BLE_ERROR_NONE) {
            /* In case of error, forward the error handling to onBleInitError */
            onBleInitError(ble, error);
            return;
        }

        // Add chainable event handlers
        ble.gap().setEventHandler(&_chainable_gap_event_handler);
        ble.gattServer().setEventHandler(&_chainable_gatt_server_event_handler);

        // setup facade: register service and install callbacks managing the lifecycle
        // of the connection.
        _arpc_over_ble.register_facade(ble, _chainable_gap_event_handler, _chainable_gatt_server_event_handler);

        // Handle disconnection events
        _chainable_gap_event_handler.addEventHandler(this);

        /* Setup advertising */
        // Build Eddystone Frame
        static const char* alphanumeric = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        static size_t alphanumeric_count = strlen(alphanumeric);

        // Build URL Frame
        static uint8_t eddystone_url_frame[20];
        memcpy(eddystone_url_frame, EDDYSTOME_URL_FRAME_BASE, sizeof(EDDYSTOME_URL_FRAME_BASE));

        // Add random characters
        // use the Bluetooth address as seed
        ble::own_address_type_t addr_type;
        ble::address_t addr;
        ble.gap().getAddress(addr_type, addr);
        uint8_t* random_data = addr.data();
        srand(*((unsigned int*)random_data));

        for(size_t i = sizeof(EDDYSTOME_URL_FRAME_BASE); i < sizeof(eddystone_url_frame); i++) {
            eddystone_url_frame[i] = alphanumeric[ rand() % alphanumeric_count ];
        }

        _adv_data_builder.clear();
        _adv_data_builder.setFlags(ble::adv_data_flags_t().setBredrNotSupported().setGeneralDiscoverable());
        // _adv_data_builder.setAppearance(ble::adv_data_appearance_t::UNKNOWN);
        _adv_data_builder.setLocalServiceList(mbed::make_Span(&EDDYSTONE_UUID, 1));
        _adv_data_builder.setServiceData(EDDYSTONE_UUID, mbed::make_Span(eddystone_url_frame, sizeof(eddystone_url_frame)));

        ble::AdvertisingParameters adv_parameters(
                    ble::advertising_type_t::CONNECTABLE_UNDIRECTED,
                    ble::adv_interval_t(ble::millisecond_t(10))
            );

        error = ble.gap().setAdvertisingParameters(
                    ble::LEGACY_ADVERTISING_HANDLE,
                    adv_parameters
            );

        if (error) {
            printf("ble.gap().setAdvertisingParameters() failed\r\n");
            return;
        }

        error = ble.gap().setAdvertisingPayload(
                ble::LEGACY_ADVERTISING_HANDLE,
                _adv_data_builder.getAdvertisingData()
        );

        if (error) {
            printf("ble.gap().setAdvertisingPayload() failed\r\n");
            return;
        }

        ble.gap().startAdvertising(ble::LEGACY_ADVERTISING_HANDLE);

        printMacAddress();
    }

    void scheduleBleEventsProcessing(BLE::OnEventsToProcessCallbackContext* context) {
        BLE &ble = BLE::Instance();
        eventQueue.call(Callback<void()>(&ble, &BLE::processEvents));
    }
};

int main()
{
    SimpleArpcDemo demo(eventQueue);
    printf("Startup\r\n");

    demo.start();
    eventQueue.dispatch_forever();
}