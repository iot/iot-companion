#
#  Be sure to run `pod spec lint Song2.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "Song2"
  s.version      = "0.0.2"
  s.summary      = "The mbed commissioning SDK"

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  s.description  = <<-DESC
                  The mbed commissioning SDK
                   DESC

  s.homepage     = "https://github.com/ARMmbed/mbed-song2"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See http://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  s.license      = "Proprietary"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  s.authors            = { "Donatien Garnier" => "donatien.garnier@arm.com" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  #  When using multiple platforms
  s.ios.deployment_target = "10.0"
  s.osx.deployment_target = "10.13"

  s.prepare_command= <<-CMD
     echo Extracting dependencies
    if [ ! -d dependencies/platform ]; then
      # Extract archive
      tar -xzf ~/.song2-cache/song2-dependencies-0.0.1.tar.bz2 -C dependencies || exit 1
    fi

    echo Setup Djinni
    export DJINNI_PATH=`pwd`/dependencies/djinni    

    echo Setup protoc compiler to use
    export PROTOC=`pwd`/dependencies/platform/macos/bin/protoc

    echo Generating Djinni bindings
    ./generate-djinni-bindings.sh

    echo Generating Protobuf code
    ./generate-protobuf-cpp.sh
  CMD

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  s.source       = { :git => "git@github.com:ARMmbed/mbed-song2.git", :tag => "#{s.version}", :submodules => true }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  s.source_files  = "core/**/*{.h,.c}", "cpp/**/*{.hpp,.cpp}", "platform/darwin/threading/*{.h,.m}", "platform/darwin/transport/**/*{.h,.m}", "platform/darwin/S2DarwinFactory{.h,.m}",
    "gen/djinni/cpp/**/*{.hpp,.cpp}", "gen/djinni/objc/**/*{.h,.m,.mm}", "gen/djinni/lib/*.hpp", "gen/djinni/lib/objc/*{.h,.mm}", "gen/proto/cpp/*{.h,.cc}",
    "platform/darwin/song2-sdk/Song2.h", "platform/darwin/S2DarwinFactory.h", "gen/djinni/objc/S2JsApp.h", "gen/djinni/objc/S2Daemon.h",
    "gen/djinni/objc/S2PlatformFactory.h", "gen/djinni/objc/S2PlatformBundle.h", "gen/djinni/objc/S2PlatfromHelper.h", "gen/djinni/objc/S2Queue.h", 
    "gen/djinni/objc/S2Dispatcher.h", "gen/djinni/objc/S2DispatcherStatus.h", "gen/djinni/objc/S2FoundationDispatcher.h",
    "test/cpp/external/**/*{.h,.c}", "test/cpp/dummy_device/**/*{.h,.c,.hpp,.cpp}", "cpp/3rdparty/**/*{.h,.c}"

  # TODO these should probably not be there in the first place
  s.exclude_files = "gen/djinni/objc/S2App+Private.mm", "gen/djinni/objc/S2Daemon+Private.mm", 
    "cpp/api/**", "cpp/daemon/**", "cpp/service/**", "cpp/tools/**", "cpp/jsapi/Ble.cpp"

  s.public_header_files = "platform/darwin/song2-sdk/Song2.h", "platform/darwin/S2DarwinFactory.h", "platform/darwin/threading/S2FoundationDispatcher.h", "gen/djinni/objc/S2JsApp.h", "gen/djinni/objc/S2Dameon.h",
    "gen/djinni/objc/S2PlatformFactory.h", "gen/djinni/objc/S2PlatformBundle.h", "gen/djinni/objc/S2PlatfromHelper.h", "gen/djinni/objc/S2Queue.h", 
    "gen/djinni/objc/S2Dispatcher.h", "gen/djinni/objc/S2DispatcherStatus.h", "gen/djinni/objc/S2FoundationDispatcher.h", "gen/djinni/objc/S2Daemon.h"

  s.pod_target_xcconfig= { 'HEADER_SEARCH_PATHS' => "${PODS_TARGET_SRCROOT} ${PODS_TARGET_SRCROOT}/core/srtp ${PODS_TARGET_SRCROOT}/core/arpc ${PODS_TARGET_SRCROOT}/core/acore ${PODS_TARGET_SRCROOT}/test/cpp/external" }

  # https://stackoverflow.com/questions/19481125/add-static-library-to-podspec/19609714#19609714
  s.subspec 'Protobuf' do |protobuf|
    protobuf.libraries = 'protobuf', 'protobuf-lite', 'protoc'
    
    protobuf.ios.preserve_paths = 'dependencies/platform/ios/include/google/protobuf/**/*.h'
    protobuf.ios.vendored_libraries = 'dependencies/platform/ios/lib/libprotobuf.a', 'dependencies/platform/ios/lib/libprotobuf-lite.a', 'dependencies/platform/ios/lib/libprotoc.a'
    protobuf.ios.pod_target_xcconfig= { 'HEADER_SEARCH_PATHS' => "${PODS_TARGET_SRCROOT}/dependencies/platform/ios/include" }

    protobuf.macos.preserve_paths = 'dependencies/platform/macos/include/google/protobuf/**/*.h'
    protobuf.macos.vendored_libraries = 'dependencies/platform/macos/lib/libprotobuf.a', 'dependencies/platform/macos/lib/libprotobuf-lite.a', 'dependencies/platform/macos/lib/libprotoc.a'
    protobuf.macos.pod_target_xcconfig= { 'HEADER_SEARCH_PATHS' => "${PODS_TARGET_SRCROOT}/dependencies/platform/macos/include" }
  end

  s.subspec 'Boost' do |boost|
    boost.preserve_paths = 'dependencies/boost/boost_1_66_0/boost/**/*{.h,.hpp}'
    boost.pod_target_xcconfig= { 'HEADER_SEARCH_PATHS' => "${PODS_TARGET_SRCROOT}/dependencies/boost/boost_1_66_0" }
  end
  
  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  s.frameworks = "CoreBluetooth", "JavascriptCore", "Foundation"

  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # s.requires_arc = true

  # s.pod_target_xcconfig= { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }


end
