#!/bin/sh

#
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

MACHINE_TYPE=`uname`
if [ ${MACHINE_TYPE} = 'Linux' ]; then
  SHELL="bash"
fi

if [ ${MACHINE_TYPE} != 'Linux' ]; then
  echo Building Android Toolchains
  ./build_android_toolchains.sh || exit 1
fi

echo Retrieving Protobuf
$SHELL ./get_protobuf.sh || exit 1

echo Building Protobuf
$SHELL ./build_protobuf.sh || exit 1

echo Retrieving Boost
if [ ${MACHINE_TYPE} != 'Linux' ]; then
  ./get_boost.sh || exit 1
fi

echo Retrieving GTest
$SHELL ./clone_gtest.sh || exit 1

echo Building GTest
$SHELL ./build_gtest.sh || exit 1

echo Installing Djinni
$SHELL ./build_djinni.sh || exit 1

BOOST_FILES=""

if [ ${MACHINE_TYPE} != 'Linux' ]; then
  echo Building archive
  
  BOOST_LIBS="optional aligned_storage assert blank blank_fwd call_traits checked_delete config cstdint core current_function detail functional iterator integer integer_fwd integer_traits io io_fwd limits math math_fwd move mpl pending predef preprocessor random shared_ptr smart_ptr static_assert throw_exception exception none type_traits type uuid utility variant version"
  
  for boost_lib in $BOOST_LIBS
  do
    BOOST_FILES="${BOOST_FILES} boost/boost_1_*/boost/${boost_lib}*"
  done
fi

tar cvjf song2-dependencies-0.0.6.tar.bz2 platform/ djinni/src/target/scala-2.11/src-assembly-0.1-SNAPSHOT.jar djinni/support-lib $BOOST_FILES || exit 1
