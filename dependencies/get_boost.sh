#!/bin/sh

#
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

echo "Downloading boost 1.66.0"
if [ -f boost_1_66_0.tar.bz2 ]; then
exit 0
fi
curl https://boostorg.jfrog.io/artifactory/main/release/1.66.0/source/boost_1_66_0.tar.bz2 -L -o boost_1_66_0.tar.bz2

# Extract archive
mkdir -p boost
tar -xvjf boost_1_66_0.tar.bz2 -C boost || exit 1
