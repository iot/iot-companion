#!/bin/sh

#
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

if [ -f "platform/gtest-android.done" ]; then
exit 0
fi

for arch in "arm" "arm64" "x86" "x86_64"
do
    ./build_gtest-android-arch.sh $arch || exit 1
done

touch "platform/gtest-android.done"

echo done
