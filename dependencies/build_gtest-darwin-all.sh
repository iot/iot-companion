#!/bin/sh

#
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

if [ -f "platform/gtest.done" ]; then
exit 0
fi

CMAKE_DEFS="-DCMAKE_BUILD_TYPE=Release -DCMAKE_CONFIGURATION_TYPES=Release"
GTEST_DIR=`pwd`/gtest/
BASE_PREFIX=`pwd`/platform/
BASE_BUILD_DIR=`pwd`/build/gtest/

MACHINE_TYPE=`uname`
if [ ${MACHINE_TYPE} = 'Linux' ]; then
  echo "Building for linux"
  BUILD_DIR=$BASE_BUILD_DIR/linux
  PREFIX=$BASE_PREFIX/linux
else
  echo "Building for macOS"
  BUILD_DIR=$BASE_BUILD_DIR/macos
  PREFIX=$BASE_PREFIX/macos
fi

mkdir -p $BUILD_DIR
mkdir -p $PREFIX
pushd $BUILD_DIR
if [ ${MACHINE_TYPE} = 'Linux' ]; then
  cmake -G "Unix Makefiles" $CMAKE_DEFS -DCMAKE_INSTALL_PREFIX=$PREFIX $GTEST_DIR || exit 1
  make install || exit 1
else
  cmake -G Xcode $CMAKE_DEFS -DCMAKE_INSTALL_PREFIX=$PREFIX $GTEST_DIR || exit 1
  cmake --build . -- -target install build || exit 1
fi

popd

if [ ${MACHINE_TYPE} != 'Linux' ]; then
  IOS_TOOLCHAIN_FILE=`pwd`/ios-toolchain.cmake
  export IOS_DEPLOYMENT_TARGET="10.0"

  echo "Building for iOS using $IOS_TOOLCHAIN_FILE"
    for platform in "simulator" "simulator64" "os"
  do
      echo "Building iOS platform $platform"    
      BUILD_DIR=$BASE_BUILD_DIR/ios-$platform
      mkdir -p $BUILD_DIR
      PREFIX=$BASE_PREFIX/ios-$platform
      mkdir -p $PREFIX

      pushd $BUILD_DIR
          PLATFORM_UPPERCASE=`echo $platform | tr '[a-z]' '[A-Z]'`
          cmake -G Xcode $CMAKE_DEFS -DCMAKE_TOOLCHAIN_FILE=${IOS_TOOLCHAIN_FILE} -DIOS_PLATFORM=${PLATFORM_UPPERCASE} -DIOS_DEPLOYMENT_TARGET="10.0" -DCMAKE_INSTALL_PREFIX=$PREFIX $GTEST_DIR  || cmake -G Xcode $CMAKE_DEFS -DCMAKE_TOOLCHAIN_FILE=${IOS_TOOLCHAIN_FILE} -DIOS_PLATFORM=${PLATFORM_UPPERCASE} -DIOS_DEPLOYMENT_TARGET="10.0" -DCMAKE_INSTALL_PREFIX=$PREFIX $GTEST_DIR  || exit 1
          # Sometimes first call fails!?
          cmake --build . -- BITCODE_GENERATION_MODE=bitcode -target install build || exit 1
      popd
  done

  echo Building fat library

  pushd platform/ios-os/lib/
  LIB_LIST=`ls -1 *.a`
  popd

  FAT_LIB_DIR=platform/ios/lib/
  mkdir -p $FAT_LIB_DIR

  for lib in $LIB_LIST
  do
      LIBS_ALL_ARCHS="platform/ios-simulator/lib/$lib platform/ios-simulator64/lib/$lib platform/ios-os/lib/$lib"
      lipo -create -output "${FAT_LIB_DIR}/$lib" $LIBS_ALL_ARCHS
  done

  echo Copying include files
  mkdir -p platform/ios/include
  cp -R "platform/ios-os/include" platform/ios/
fi

touch "platform/gtest.done"

echo done
