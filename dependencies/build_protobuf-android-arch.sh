#!/bin/sh

#
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

export arch=$1

# automake, autoconf, autoge, libtool must be installed

# automake, autoconf, autoge, libtool must be installed

PROTOBUF_ARCHIVE=`ls -1 protobuf-cpp-*.tar.gz`

PROTOBUF_DIR=protobuf-android-$arch

# Extract archive
mkdir -p $PROTOBUF_DIR
tar -xzf $PROTOBUF_ARCHIVE -C $PROTOBUF_DIR || exit 1

# Workaround for libtool bug
export PREFIX=`pwd`/platform/android-$arch
export prefix=${PREFIX}

PROTOC=`which protoc`
mkdir -p "${PREFIX}"

# Add the standalone toolchain to the search path.
TOOLCHAIN_DIR=`pwd`/toolchains/$arch
export PATH=$PATH:$TOOLCHAIN_DIR/bin

# Tell configure what tools to use.
case $arch in
    "arm") TARGET_HOST=arm-linux-androideabi;;
    "arm64") TARGET_HOST=aarch64-linux-android;;
    "x86") TARGET_HOST=i686-linux-android;;
    "x86_64") TARGET_HOST=x86_64-linux-android;;
    *) echo "Unsupported architecture"; exit 1;;
esac

echo "TARGET_HOST = $TARGET_HOST"

export AR=$TARGET_HOST-ar
export AS=$TARGET_HOST-clang
export CC=$TARGET_HOST-clang
export CXX=$TARGET_HOST-clang++
export LD=$TARGET_HOST-ld
export STRIP=$TARGET_HOST-strip

# Tell configure what flags Android requires.
# export CFLAGS="-fPIE -fPIC"
# export CXXFLAGS="-fPIE -fPIC"
# export LDFLAGS="-pie"
export CFLAGS="-fPIC"
export CXXFLAGS="-fPIC"

export HAS_PKG_CONFIG=false
export LIBS="-landroid -llog -lc++_shared"

# export CFLAGS=" -mios-version-min=${MIN_SDK_VERSION} -arch $arch -isysroot ${IPHONEOS_SYSROOT} -Ithird_party/protobuf/src -fembed-bitcode"
# export CXXFLAGS=" -mios-version-min=${MIN_SDK_VERSION} -arch $arch -isysroot ${IPHONEOS_SYSROOT} -Ithird_party/protobuf/src -fembed-bitcode" 
# export GRPC_CROSS_LDOPTS="-arch $arch -mios-version-min=${MIN_SDK_VERSION}"

export PROTOBUF_CONFIG_OPTS="--host=${TARGET_HOST} \
 --exec-prefix=${PREFIX} \
 --prefix=${PREFIX} \
 --with-protoc=${PROTOC} --disable-shared --disable-maintainer-mode
 "

pushd $PROTOBUF_DIR/protobuf-* || exit 1
echo Build for android $arch

echo Build protobuf
./autogen.sh || exit 1
./configure $PROTOBUF_CONFIG_OPTS || exit 1
make -j4 || exit 1
make install || exit 1

popd

# Cleanup 
rm -r $PROTOBUF_DIR