#!/bin/sh

#
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

MACHINE_TYPE=`uname`
if [ ${MACHINE_TYPE} = 'Linux' ]; then
  export platform=linux
else
  export platform=macos
fi

if [ -f "platform/${platform}/protobuf.done" ]; then
exit 0
fi

# automake, autoconf, autogen, libtool must be installed

PROTOBUF_DIR=protobuf-$platform

# Extract archive
mkdir -p $PROTOBUF_DIR

PROTOBUF_ARCHIVE=`ls -1 protobuf-cpp-*.tar.gz`
tar -xzf $PROTOBUF_ARCHIVE -C $PROTOBUF_DIR || exit 1

export PREFIX=`pwd`/platform/$platform
export prefix=${PREFIX}

PROTOC=`which protoc`
mkdir -p "${PREFIX}"

PLATFORM_UPPERCASE=`echo $platform | tr '[a-z]' '[A-Z]'`

export PROTOBUF_CONFIG_OPTS=" --exec-prefix=${PREFIX} \
 --prefix=${PREFIX} \
 --disable-shared --disable-maintainer-mode"

if [ ${MACHINE_TYPE} = 'Linux' ]; then
  echo Build for linux
else
  echo Build for macos
fi

pushd $PROTOBUF_DIR/protobuf-* || exit 1

echo Build protobuf
./autogen.sh || exit 1
./configure $PROTOBUF_CONFIG_OPTS || exit 1
make -j4 || exit 1

echo "This is not a global install"
make install || exit 1

popd

# Cleanup 
rm -r $PROTOBUF_DIR

touch "platform/${platform}/protobuf.done"
