#!/bin/sh

#
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

export platform=$1
export arch=$2

# automake, autoconf, autoge, libtool must be installed

# automake, autoconf, autoge, libtool must be installed

PROTOBUF_ARCHIVE=`ls -1 protobuf-cpp-*.tar.gz`

PROTOBUF_DIR=protobuf-$platform-$arch

# Extract archive
mkdir -p $PROTOBUF_DIR
tar -xzf $PROTOBUF_ARCHIVE -C $PROTOBUF_DIR || exit 1

# Workaround for libtool bug
export MACOSX_DEPLOYMENT_TARGET="10.12"

export PREFIX=`pwd`/platform/ios-$platform-$arch
export prefix=${PREFIX}

PROTOC=`which protoc`
mkdir -p "${PREFIX}"

PLATFORM_UPPERCASE=`echo $platform | tr '[a-z]' '[A-Z]'`

XCODEDIR=`xcode-select --print-path`
MIN_SDK_VERSION=10.0

IPHONEOS_PLATFORM=`xcrun --sdk $platform --show-sdk-platform-path`
IPHONEOS_SYSROOT=`xcrun --sdk $platform --show-sdk-path`

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
export HAS_PKG_CONFIG=false
DARWIN=darwin

export CFLAGS=" -mios-version-min=${MIN_SDK_VERSION} -arch $arch -isysroot ${IPHONEOS_SYSROOT} -Ithird_party/protobuf/src -fembed-bitcode"
export CXXFLAGS=" -mios-version-min=${MIN_SDK_VERSION} -arch $arch -isysroot ${IPHONEOS_SYSROOT} -Ithird_party/protobuf/src -fembed-bitcode" 
export GRPC_CROSS_LDOPTS="-arch $arch -mios-version-min=${MIN_SDK_VERSION}"

export PROTOBUF_CONFIG_OPTS="--host=arm-apple-${DARWIN} \
 --exec-prefix=${PREFIX} \
 --prefix=${PREFIX} \
 --with-protoc=${PROTOC} --disable-shared --disable-maintainer-mode
 "

pushd $PROTOBUF_DIR/protobuf-* || exit 1
echo Build for ios $platform $arch

echo Build protobuf
./autogen.sh || exit 1
./configure $PROTOBUF_CONFIG_OPTS || exit 1
make -j4 || exit 1
make install || exit 1

popd

# Cleanup 
rm -r $PROTOBUF_DIR