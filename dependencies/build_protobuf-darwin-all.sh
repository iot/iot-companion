#!/bin/sh

#
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

if [ -f "platform/protobuf.done" ]; then
exit 0
fi

# macOS

./build_protobuf-posix.sh || exit 1

# Simulator

./build_protobuf-ios-arch.sh iphonesimulator x86_64 || exit 1

./build_protobuf-ios-arch.sh iphonesimulator i386 || exit 1

# OS

for arch in "armv7" "armv7s" "arm64"
do
    ./build_protobuf-ios-arch.sh iphoneos $arch || exit 1
done

echo Building fat library

pushd platform/ios-iphoneos-armv7/lib/
LIB_LIST=`ls -1 *.a`
popd
LA_LIST=`ls -1 platform/ios-iphoneos-armv7/lib/*.la`

FAT_LIB_DIR=platform/ios/lib/
mkdir -p $FAT_LIB_DIR

for lib in $LIB_LIST
do
LIBS_ALL_ARCHS=""
    LIBS_ALL_ARCHS="${LIBS_ALL_ARCHS} platform/ios-iphonesimulator-x86_64/lib/$lib platform/ios-iphonesimulator-i386/lib/$lib"
    for arch in "armv7" "armv7s" "arm64"
    do
        LIBS_ALL_ARCHS="${LIBS_ALL_ARCHS} platform/ios-iphoneos-$arch/lib/$lib"
    done
    lipo -create -output "${FAT_LIB_DIR}/$lib" $LIBS_ALL_ARCHS
done

echo Copying la files
for la in $LA_LIST
do
    cp $la platform/ios/lib
done

echo Copying include files
mkdir -p platform/ios/include
cp -R "platform/ios-iphoneos-armv7/include" platform/ios/

touch "platform/protobuf.done"

# echo Copying binaries
# mkdir -p platform/ios/bin
# cp -R "platform/ios-iphoneos-armv7/bin" platform/ios/bin

echo done
