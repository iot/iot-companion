#!/bin/sh

#
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

export arch=$1

CMAKE_DEFS="-DCMAKE_BUILD_TYPE=Release -DCMAKE_CONFIGURATION_TYPES=Release"
GTEST_DIR=`pwd`/gtest/
BASE_PREFIX=`pwd`/platform/
BASE_BUILD_DIR=`pwd`/build/gtest/

echo "Building for Android $arch"
BUILD_DIR=$BASE_BUILD_DIR/android-$arch
PREFIX=$BASE_PREFIX/android-$arch

# Add the standalone toolchain to the search path.
TOOLCHAIN_DIR=`pwd`/toolchains/$arch
export PATH=$PATH:$TOOLCHAIN_DIR/bin

# Tell configure what tools to use.
case $arch in
    "arm") ANDROID_ABI=armeabi-v7a;;
    "arm64") ANDROID_ABI=arm64-v8a;;
    "x86") ANDROID_ABI=x86;;
    "x86_64") ANDROID_ABI=x86_64;;
    *) echo "Unsupported architecture"; exit 1;;
esac

ANDROID_NATIVE_API_LEVEL=21
ANDROID_NDK=$ANDROID_NDK
CMAKE_TOOLCHAIN_FILE=$ANDROID_NDK/build/cmake/android.toolchain.cmake
ANDROID_CMAKE_FLAGS="-DANDROID_STL=c++_static"

echo "ANDROID_ABI = $ANDROID_ABI"

mkdir -p $BUILD_DIR
mkdir -p $PREFIX
pushd $BUILD_DIR
cmake -GNinja $CMAKE_DEFS -DCMAKE_INSTALL_PREFIX=$PREFIX -DANDROID_ABI=$ANDROID_ABI -DANDROID_NDK=$ANDROID_NDK -DANDROID_NATIVE_API_LEVEL=$ANDROID_NATIVE_API_LEVEL -DCMAKE_TOOLCHAIN_FILE=$CMAKE_TOOLCHAIN_FILE -DANDROID_TOOLCHAIN=clang $ANDROID_CMAKE_FLAGS $GTEST_DIR || exit 1
ninja install || exit 1

popd
