cmake_minimum_required(VERSION 3.5)

project(testapp)

file(GLOB_RECURSE SRC
  "test.cpp"
  "../core/*.c"
  "../cpp/3rdparty/*.c"
  "../cpp/arpc/*.cpp"
  "../cpp/device/*.cpp"


  "../cpp/logging/*.cpp"
  "../cpp/pipe/*.cpp"
  "../cpp/platform/*.cpp"
  "../cpp/srtp/*.cpp"
  "../cpp/threading/*.cpp"
  "../cpp/transport/*.cpp"
  "../cpp/types/*.cpp"
  "../cpp/util/*.cpp"

  "../platform/linux/*.cpp"

  "../gen/djinni/cpp/*.cpp"
  "../gen/proto/cpp/*.cc"
)

include_directories(
  .
  ./..
  ./../core/acore
  ./../core/srtp
  ./../core/arpc
  ./test/cpp/external
  ./test/cpp/dummy_device
  ./cpp/dummy_device
  ./../gen/djinni/lib
  ./../dependencies/platform/linux/include/google/protobuf
  ./../dependencies/platform/linux/include/gtest
  ./../dependencies/platform/linux/include
  ./cpp/external
  ./../cpp/3rdparty/sqlite3
  /usr/include
  /usr/include/webkitgtk-4.0/
)

SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -std=c++14")

link_directories("./../dependencies/platform/linux/lib")

add_executable(testapp ${SRC})

configure_file("../node_modules/rxjs/bundles/Rx.min.js" "Rx.min.js" COPYONLY)
configure_file("../gen/js/global_init.js" "global_init.js" COPYONLY)
configure_file("../ts-commissioning/dist/wifiCommissioning.js" "wifiCommissioning.js" COPYONLY)

target_link_libraries(testapp
  "libgtest.a"
  "libprotobuf.a"
  "protobuf-lite.a"
  "pthread"
  "javascriptcoregtk-4.0"
  "boost_system"
  "boost_filesystem"
)

