//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  threading.cpp
//  Song2 Tests
//
//  Created by Donatien Garnier on 04/01/2018.

//

#include <gtest/gtest.h>

#include "cpp/threading/Queue.hpp"
#include "cpp/threading/BlockingDispatcher.hpp"
#include "cpp/threading/Promise.hpp"
#include "cpp/threading/Observable.hpp"
#include "cpp/threading/ObservableVector.hpp"

using namespace arm::song2;

class ThreadingTest : public ::testing::Test {
public:
    virtual void SetUp()
    {
        _dptch = threading::make_blocking_dispatcher();
        _q = threading::make_queue(_dptch);
        
        _dptch->start();
        _dptch->process();
        
        // Set current queue
        threading::Queue::set_current(_q);
    }
    
    virtual void TearDown()
    {
    }
    
    void run_on_dispatcher(const std::function<void(const threading::Queue::ptr& queue)>& fn)
    {
        auto q = threading::make_queue(_dptch);
        bool has_run = false;
        q->post([&fn, &has_run, &q](){
            fn(q);
            has_run = true;
        });
        _dptch->process();
        ASSERT_TRUE(has_run);
    }
    
    typename threading::BlockingDispatcher::ptr _dptch;
    typename threading::Queue::ptr _q;
};

TEST_F(ThreadingTest, QueueTest) {
    size_t count = 0;
    
    _q->post([&](){
        count++;
    });
    
    ASSERT_EQ(count, 0);
    
    _q->process();
    
    ASSERT_EQ(count, 1);
}

TEST_F(ThreadingTest, BasicPromiseTest) {
    threading::Promise<void> p;
    auto fut = p.get_future();
    size_t count = 0;

    fut->then([&](auto& fut){
        fut.get();
        count++;
    }, _q);
    
    ASSERT_EQ(count, 0);

    p.resolve();
    
    ASSERT_EQ(count, 1);
}

TEST_F(ThreadingTest, ExceptionPromiseTest) {
    threading::Promise<void> p;
    auto fut = p.get_future();
    size_t count = 0;
    
    fut->then([&](auto& fut){
        try {
            fut.get();
        }
        catch (std::runtime_error& e)
        {
            count++;
        }
    }, _q);
    
    ASSERT_EQ(count, 0);
    
    p.reject(std::make_exception_ptr(std::runtime_error("Failed")));
    
    ASSERT_EQ(count, 1);
}

TEST_F(ThreadingTest, PromiseChainTest) {
    threading::Promise<void> p;
    auto fut = p.get_future();
    
    double value = 0;
    
    fut->then([&](auto& fut){
        return 1;
    }, _q)
    ->then([&](auto& fut){
        return fut.get() * 2;
    }, _q)
    ->then([&](auto& fut){
        return static_cast<double>(fut.get());
    }, _q)
    ->then([&](auto& fut){
        value = fut.get();
    }, _q);
    
    ASSERT_EQ(value, 0.0);
    
    p.resolve();
    
    ASSERT_EQ(value, 2.0);
}

TEST_F(ThreadingTest, PromiseNestedChainTest) {
    threading::Promise<void> p;
    auto fut = p.get_future();
    
    double value = 0;
    
    fut->then([&](auto& fut){
        return threading::make_resolved_future(1);
    }, _q)
    ->then([&](auto& fut){
        return threading::make_resolved_future(fut.get() * 2);
    }, _q)
    ->then([&](auto& fut){
        return static_cast<double>(fut.get());
    }, _q)
    ->then([&](auto& fut){
        value = fut.get();
    }, _q);
    
    ASSERT_EQ(value, 0.0);
    
    p.resolve();
    
    ASSERT_EQ(value, 2.0);
}

TEST_F(ThreadingTest, EarlyResolutionPromiseTest) {
    threading::Promise<void> p;
    auto fut = p.get_future();
    size_t count = 0;
    
    p.resolve();
    
    fut->then([&](auto& fut){
        fut.get();
        count++;
    }, _q);
    
    ASSERT_EQ(count, 1);
}

TEST_F(ThreadingTest, AsyncResolutionPromiseTest) {
    threading::Promise<void> p;
    auto fut = p.get_future();
    size_t count = 0;
    
    fut->then_async([&](auto& fut){
        fut.get();
        count++;
    }, _q);
    
    ASSERT_EQ(count, 0);
    
    p.resolve();
    
    ASSERT_EQ(count, 0);
    
    _q->process();
    
    ASSERT_EQ(count, 1);
}

TEST_F(ThreadingTest, AsyncResolutionOnRunningQueuePromiseTest) {
    threading::Promise<void> p;
    
    auto fut = p.get_future();
    size_t count = 0;
    
    _q->post([&](){
        fut->then_async([&](auto& fut){
            fut.get();
            count++;
        });
    });
    
    ASSERT_EQ(count, 0);
    
    p.resolve();
    
    ASSERT_EQ(count, 0);
    
    _q->process();
    
    ASSERT_EQ(count, 1);
}

TEST_F(ThreadingTest, BasicObservableTest) {
    run_on_dispatcher([this](const threading::Queue::ptr& queue){
        threading::Observable<void> o;
        auto s = o.get_subscriber();
        size_t count = 0;
        
        s->next([&](auto& fut){
            fut.get();
            count++;
        }, queue);
        
        ASSERT_EQ(count, 0);
        
        o.next();
        
        ASSERT_EQ(count, 1);
        
        o.next();
        
        ASSERT_EQ(count, 2);
    });
}

TEST_F(ThreadingTest, ExceptionObservableTest) {
    run_on_dispatcher([this](const threading::Queue::ptr& queue){
        threading::Observable<void> o;
        auto s = o.get_subscriber();
        size_t count = 0;
        
        s->next([&](auto& fut){
            ASSERT_THROW(fut.get(), std::runtime_error);
            count++;
        }, queue);
        
        ASSERT_EQ(count, 0);
        
        o.error(std::make_exception_ptr(std::runtime_error("Failed")));
        
        ASSERT_EQ(count, 1);
    });
}

TEST_F(ThreadingTest, CanceledObservableTest) {
    run_on_dispatcher([this](const threading::Queue::ptr& queue){
        threading::Observable<void> o;
        auto s = o.get_subscriber();
        size_t count = 0;
        
        s->next([&](auto& fut){
            fut.get();
            count++;
        }, queue);
        
        ASSERT_EQ(count, 0);
        
        o.next();
        
        ASSERT_EQ(count, 1);
        
        s->cancel();

        o.next();
        
        ASSERT_EQ(count, 1);
    });
}

TEST_F(ThreadingTest, CompletingObservableTest) {
    run_on_dispatcher([this](const threading::Queue::ptr& queue){
        threading::Observable<void> o;
        auto s = o.get_subscriber();
        size_t count = 0;
        
        s->next([&](auto& fut){
            ASSERT_THROW(fut.get(), threading::ObservableCompletedException);
            count++;
        }, queue);
        
        ASSERT_EQ(count, 0);
        
        o.complete();
        
        ASSERT_EQ(count, 1);
    });
}

TEST_F(ThreadingTest, ObservableChainTest) {
    run_on_dispatcher([this](const threading::Queue::ptr& queue){
        threading::Observable<double> o;
        auto s = o.get_subscriber();
        
        std::vector<double> values;
        
        s->next([&](auto& fut){
            return fut.get() * 2;
        }, queue)
        ->next([&](auto& fut){
            return static_cast<double>(fut.get());
        }, queue)
        ->next([&](auto& fut){
            values.push_back(fut.get());
        }, queue);
        
        ASSERT_TRUE(values.empty());
        
        o.next(1);
        
        ASSERT_EQ(values.size(), 1);
        ASSERT_EQ(values[0], 2.0);
        
        o.next(2);
        
        ASSERT_EQ(values.size(), 2);
        ASSERT_EQ(values[1], 4.0);
    });
}


TEST_F(ThreadingTest, EarlyResolutionObservableTest) {
    run_on_dispatcher([this](const threading::Queue::ptr& queue){
        threading::Observable<void> o;
        auto s = o.get_subscriber();
        size_t count = 0;
        
        o.next();
        o.next();
        
        s->next([&](auto& fut){
            fut.get();
            count++;
        }, queue);
        
        ASSERT_EQ(count, 2);
        
        o.next();
        
        ASSERT_EQ(count, 3);
    });
}

TEST_F(ThreadingTest, AsyncResolutionObservableTest) {
    run_on_dispatcher([this](const threading::Queue::ptr& queue){
        threading::Observable<void> o;
        auto s = o.get_subscriber();
        size_t count = 0;
        
        s->next_async([&](auto& fut){
            fut.get();
            count++;
        }, queue);
        
        ASSERT_EQ(count, 0);
        
        o.next();
        o.next();
        
        ASSERT_EQ(count, 0);
        
        queue->process();
        
        ASSERT_EQ(count, 2);
    });
}

TEST_F(ThreadingTest, AsyncResolutionCompletingObservableTest) {
    run_on_dispatcher([this](const threading::Queue::ptr& queue){
        threading::Observable<void> o;
        auto s = o.get_subscriber();
        size_t count = 0;
        
        s->next_async([&](auto& fut){
            ASSERT_THROW(fut.get(), threading::ObservableCompletedException);
            count++;
        }, queue);
        
        ASSERT_EQ(count, 0);
        
        o.complete();
        
        ASSERT_EQ(count, 0);
        
        queue->process();
        
        ASSERT_EQ(count, 1);
    });
}

TEST_F(ThreadingTest, AsyncResolutionOnRunningQueueObservableTest) {
    run_on_dispatcher([this](const threading::Queue::ptr& queue){
        threading::Observable<void> o;
        auto s = o.get_subscriber();
        size_t count = 0;
        
        queue->post([&](){
            s->next_async([&](auto& fut){
                fut.get();
                count++;
            });
        });
        
        ASSERT_EQ(count, 0);
        
        o.next();
        o.next();
        
        ASSERT_EQ(count, 0);
        
        queue->process();
        
        ASSERT_EQ(count, 2);
    });
}

TEST_F(ThreadingTest, BasicObservableVectorTest) {
    run_on_dispatcher([this](const threading::Queue::ptr& queue){
        threading::ObservableVector<void> o;
        auto s1 = o.get_subscriber();
        size_t count1 = 0;
        auto s2 = o.get_subscriber();
        size_t count2 = 0;
        
        s1->next([&](auto& fut){
            fut.get();
            count1++;
        }, queue);
        
        s2->next([&](auto& fut){
            fut.get();
            count2++;
        }, queue);
        
        ASSERT_EQ(count1, 0);
        ASSERT_EQ(count2, 0);
        
        o.next();
        
        ASSERT_EQ(count1, 1);
        ASSERT_EQ(count2, 1);
        
        o.next();
        
        ASSERT_EQ(count1, 2);
        ASSERT_EQ(count2, 2);
    });
}

TEST_F(ThreadingTest, ExceptionObservableVectorTest) {
    run_on_dispatcher([this](const threading::Queue::ptr& queue){
        threading::ObservableVector<void> o;
        auto s1 = o.get_subscriber();
        size_t count1 = 0;
        auto s2 = o.get_subscriber();
        size_t count2 = 0;
        
        s1->next([&](auto& fut){
            ASSERT_THROW(fut.get(), std::runtime_error);
            count1++;
        }, queue);
        
        s2->next([&](auto& fut){
            ASSERT_THROW(fut.get(), std::runtime_error);
            count2++;
        }, queue);
        
        ASSERT_EQ(count1, 0);
        ASSERT_EQ(count2, 0);

        o.error(std::make_exception_ptr(std::runtime_error("Failed")));
        
        ASSERT_EQ(count1, 1);
        ASSERT_EQ(count2, 1);
    });
}

TEST_F(ThreadingTest, QueueDelayTest) {
    // Clear current queue (otherwise the test will deadlock)
    threading::Queue::set_current(nullptr);
    
    size_t count = 0;
    
    auto start = std::chrono::steady_clock::now();

    _q->post_delayed([&](){
        count++;
    }, std::chrono::milliseconds(30));
    
    ASSERT_EQ(count, 0);
    
    _dptch->process();
    
    ASSERT_EQ(count, 0);

    _dptch->wait();
    _dptch->process();

    auto finish = std::chrono::steady_clock::now();
    
    ASSERT_EQ(count, 1);
    
    auto delay = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start);
    
    ASSERT_NEAR(delay.count(), 30, 6);
}

