//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9 at Thu Jun  7 19:14:21 2018. */

#ifndef PB_COM_ARM_COMMISSIONING_WIFI_COMMISSIONING_PB_H_INCLUDED
#define PB_COM_ARM_COMMISSIONING_WIFI_COMMISSIONING_PB_H_INCLUDED
#include <pb/pb.h>
/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Enum definitions */
typedef enum _com_arm_commissioning_WiFiSecurityType {
    com_arm_commissioning_WiFiSecurityType_NoSecurity = 0,
    com_arm_commissioning_WiFiSecurityType_WEP = 1,
    com_arm_commissioning_WiFiSecurityType_WPA = 2,
    com_arm_commissioning_WiFiSecurityType_WPA2 = 3,
    com_arm_commissioning_WiFiSecurityType_WPA_WPA2 = 4,
    com_arm_commissioning_WiFiSecurityType_PAP = 5,
    com_arm_commissioning_WiFiSecurityType_CHAP = 6
} com_arm_commissioning_WiFiSecurityType;
#define _com_arm_commissioning_WiFiSecurityType_MIN com_arm_commissioning_WiFiSecurityType_NoSecurity
#define _com_arm_commissioning_WiFiSecurityType_MAX com_arm_commissioning_WiFiSecurityType_CHAP
#define _com_arm_commissioning_WiFiSecurityType_ARRAYSIZE ((com_arm_commissioning_WiFiSecurityType)(com_arm_commissioning_WiFiSecurityType_CHAP+1))

typedef enum _com_arm_commissioning_WiFiCommissioningStatusError {
    com_arm_commissioning_WiFiCommissioningStatusError_NoError = 0,
    com_arm_commissioning_WiFiCommissioningStatusError_Timeout = 1,
    com_arm_commissioning_WiFiCommissioningStatusError_APNotFound = 2,
    com_arm_commissioning_WiFiCommissioningStatusError_AuthProblem = 3,
    com_arm_commissioning_WiFiCommissioningStatusError_WeakSignal = 4,
    com_arm_commissioning_WiFiCommissioningStatusError_DHCPFailed = 5,
    com_arm_commissioning_WiFiCommissioningStatusError_PingFailed = 6,
    com_arm_commissioning_WiFiCommissioningStatusError_DeviceError = 7
} com_arm_commissioning_WiFiCommissioningStatusError;
#define _com_arm_commissioning_WiFiCommissioningStatusError_MIN com_arm_commissioning_WiFiCommissioningStatusError_NoError
#define _com_arm_commissioning_WiFiCommissioningStatusError_MAX com_arm_commissioning_WiFiCommissioningStatusError_DeviceError
#define _com_arm_commissioning_WiFiCommissioningStatusError_ARRAYSIZE ((com_arm_commissioning_WiFiCommissioningStatusError)(com_arm_commissioning_WiFiCommissioningStatusError_DeviceError+1))

/* Struct definitions */
typedef struct _com_arm_commissioning_EmptyMessage {
    char dummy_field;
/* @@protoc_insertion_point(struct:com_arm_commissioning_EmptyMessage) */
} com_arm_commissioning_EmptyMessage;

typedef struct _com_arm_commissioning_WiFiCommissioningInfo {
    bool has_status;
    com_arm_commissioning_WiFiCommissioningStatusError status;
    bool has_bssid;
    char bssid[64];
    bool has_rssi;
    int32_t rssi;
    bool has_channel;
    int32_t channel;
    bool has_ipAddress;
    char ipAddress[64];
    bool has_netmask;
    char netmask[64];
    bool has_gateway;
    char gateway[64];
    bool has_macAddress;
    char macAddress[64];
/* @@protoc_insertion_point(struct:com_arm_commissioning_WiFiCommissioningInfo) */
} com_arm_commissioning_WiFiCommissioningInfo;

typedef struct _com_arm_commissioning_WiFiCommissioningParameters {
    bool has_ssid;
    char ssid[64];
    bool has_passphrase;
    char passphrase[64];
    bool has_security;
    com_arm_commissioning_WiFiSecurityType security;
    bool has_channel;
    int32_t channel;
    bool has_dhcp;
    bool dhcp;
    bool has_ipAddress;
    char ipAddress[64];
    bool has_netmask;
    char netmask[64];
    bool has_gateway;
    char gateway[64];
/* @@protoc_insertion_point(struct:com_arm_commissioning_WiFiCommissioningParameters) */
} com_arm_commissioning_WiFiCommissioningParameters;

/* Default values for struct fields */

/* Initializer values for message structs */
#define com_arm_commissioning_WiFiCommissioningParameters_init_default {false, "", false, "", false, (com_arm_commissioning_WiFiSecurityType)0, false, 0, false, 0, false, "", false, "", false, ""}
#define com_arm_commissioning_WiFiCommissioningInfo_init_default {false, (com_arm_commissioning_WiFiCommissioningStatusError)0, false, "", false, 0, false, 0, false, "", false, "", false, "", false, ""}
#define com_arm_commissioning_EmptyMessage_init_default {0}
#define com_arm_commissioning_WiFiCommissioningParameters_init_zero {false, "", false, "", false, (com_arm_commissioning_WiFiSecurityType)0, false, 0, false, 0, false, "", false, "", false, ""}
#define com_arm_commissioning_WiFiCommissioningInfo_init_zero {false, (com_arm_commissioning_WiFiCommissioningStatusError)0, false, "", false, 0, false, 0, false, "", false, "", false, "", false, ""}
#define com_arm_commissioning_EmptyMessage_init_zero {0}

/* Field tags (for use in manual encoding/decoding) */
#define com_arm_commissioning_WiFiCommissioningInfo_status_tag 1
#define com_arm_commissioning_WiFiCommissioningInfo_bssid_tag 2
#define com_arm_commissioning_WiFiCommissioningInfo_rssi_tag 3
#define com_arm_commissioning_WiFiCommissioningInfo_channel_tag 4
#define com_arm_commissioning_WiFiCommissioningInfo_ipAddress_tag 5
#define com_arm_commissioning_WiFiCommissioningInfo_netmask_tag 6
#define com_arm_commissioning_WiFiCommissioningInfo_gateway_tag 7
#define com_arm_commissioning_WiFiCommissioningInfo_macAddress_tag 8
#define com_arm_commissioning_WiFiCommissioningParameters_ssid_tag 1
#define com_arm_commissioning_WiFiCommissioningParameters_passphrase_tag 2
#define com_arm_commissioning_WiFiCommissioningParameters_security_tag 3
#define com_arm_commissioning_WiFiCommissioningParameters_channel_tag 4
#define com_arm_commissioning_WiFiCommissioningParameters_dhcp_tag 5
#define com_arm_commissioning_WiFiCommissioningParameters_ipAddress_tag 6
#define com_arm_commissioning_WiFiCommissioningParameters_netmask_tag 7
#define com_arm_commissioning_WiFiCommissioningParameters_gateway_tag 8

/* Struct field encoding specification for nanopb */
extern const pb_field_t com_arm_commissioning_WiFiCommissioningParameters_fields[9];
extern const pb_field_t com_arm_commissioning_WiFiCommissioningInfo_fields[9];
extern const pb_field_t com_arm_commissioning_EmptyMessage_fields[1];

/* Maximum encoded size of messages (where known) */
#define com_arm_commissioning_WiFiCommissioningParameters_size 345
#define com_arm_commissioning_WiFiCommissioningInfo_size 354
#define com_arm_commissioning_EmptyMessage_size  0

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define WIFI_COMMISSIONING_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
