//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  DummyDevice.hpp
//  song2-tests
//
//  Created by Donatien Garnier on 19/01/2018.

//

#pragma once

#include <memory>
#include <map>

#include "cpp/threading/Queue.hpp"
#include "cpp/pipe/Endpoint.hpp"
#include "cpp/srtp/SrtpEndpoint.hpp"

#include "mbed-song2-mbed-os/song2/arpc/ArpcEndpoint.h"
#include "mbed-song2-mbed-os/song2/arpc/ArpcConnection.h"
#include "mbed-song2-mbed-os/song2/arpc/ArpcService.h"
#include "mbed-song2-mbed-os/song2/srtp/SrtpChannel.h"
#include "mbed-song2-mbed-os/song2/arpc/SrtpChannelToArpcTransportAdapter.h"
#include "mbed-song2-mbed-os/song2/srtp/HeapSrtpChannelMsgQueue.h"
#include "mbed-song2-mbed-os/song2/srtp/SrtpBackend.h"

namespace arm {
    namespace song2 {
        namespace test {
            class DummyDevice {
            public:
                DummyDevice();
                
                void new_channel(uint32_t channel_num, pipe::Endpoint::ptr&& endpoint);
                
                struct Channel : mbed_os::srtp::SrtpBackend {
                    Channel(pipe::Endpoint::ptr&& endpoint);
                    pipe::Endpoint::ptr ep;
                    mbed_os::srtp::SrtpChannel srtp_channel;
                    mbed_os::srtp::HeapSrtpChannelMsgQueue srtp_msg_queue;
                    mbed_os::arpc::SrtpChannelToArpcTransportAdapter arpc_srtp_transport;
                    mbed_os::arpc::ArpcEndpoint arpc_ep;
                    std::vector<std::unique_ptr<mbed_os::arpc::ArpcService>> services;
                    
                    /*
                     * SrtpBackend implementation
                     */
                    virtual bool send(mbed_os::srtp::SrtpChannel* channel, ac_buffer_t* pkt);
                    virtual void release(mbed_os::srtp::SrtpChannel* channel);
                };
                
            private:
                std::map<uint32_t, Channel> _channels;
            };
        }
    }
}
