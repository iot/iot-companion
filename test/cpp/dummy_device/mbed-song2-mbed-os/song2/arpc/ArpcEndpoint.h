/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MBED_SONG2_ARPC_ENDPOINT_
#define MBED_SONG2_ARPC_ENDPOINT_

#include "platform/NonCopyable.h"

#include "ArpcConnection.h"
#include "ArpcDiscoveryService.h"

namespace arm {
namespace song2 {
namespace mbed_os {
namespace arpc {

/**
 * The ArpcEndpoint class handles routing ARPC calls to the appropriate service and methods.
 *
 * In order to use this class, you can bind with the provided ArpConnection instance and register the services 
 * the endpoint should respond to.
 * 
 * This class also implements the discovery service (arpc.Discovery) that allows a client to discover whether a service is implemented or not and how to address specific methods.
 * 
 * Services can be easily generated using the ARPC code generator.
 * 
 * @par example
 *
 * @code
    using arm::song2::mbed_os::ArpcOverBleFacade;
    using arm::song2::mbed_os::arpc::ArpcEndpoint;

    static ArpcOverBleFacade arpc_over_ble;
    static ArpcEndpoint arpc_endpoint;
    static WifiService wifi_service;

    // BLE init complete callback
    void bleInitComplete(BLE::InitializationCompleteCallbackContext *params)
    {
        BLE &ble   = params->ble;

        // setup facade: register service and install callbacks managing the lifecycle
        // of the connection.
        arpc_over_ble.register_facade(ble);

        // other post init code
    }

    int main()
    {
        // Bind connection to the facade
        arpc_over_ble.bind_connection(arpc_endpoint.connection());
        arpc_endpoint.register_service(wifi_service);


        // Ble initialization goes here
    }
 * @endcode
 */
class ArpcEndpoint : mbed::NonCopyable<ArpcEndpoint>, ArpcConnection::EventHandler, generated::ArpcDiscoveryService {
public:
    /**
     * Construct an ArpcEndpoint instance.
     */
    ArpcEndpoint();

    /**
     * Destroy an ArpcEndpoint instance.
     */
    virtual ~ArpcEndpoint();

    /** 
     * Get the connection associated with this endpoint.
     * 
     * @return The connection associated with this endpoint.
     */ 
    arpc::ArpcConnection& connection();

    /**
     * Register a service with the endpoint
     * 
     * @param[in] service A reference to the service to register.
     */
    void register_service(ArpcService &service);

private:
    // ARPC Connection's event handler
    virtual void on_connection_opened(ArpcConnection *connection);

    virtual void on_connection_closed(ArpcConnection *connection);

    virtual void on_response_received(
        ArpcConnection *connection,
        arpc_command_t command_id,
        bool last_response,
        arpc_response_code_t response_code,
        const ac_buffer_t* payload
    );

    virtual void on_request_received(
        ArpcConnection *connection,
        arpc_command_t command_id,
        arpc_method_num_t method_num,
        const ac_buffer_t* payload
    );
    
    // Discovery service
    virtual void get_method_number(const mbed::util::SharedPointer< ArpcMethodCall<arpc_GetMethodNumberRequest, arpc_GetMethodNumberResponse> >& call);
    virtual void get_service_number(const mbed::util::SharedPointer< ArpcMethodCall<arpc_GetServiceNumberRequest, arpc_GetServiceNumberResponse> >& call);
    virtual void get_services_count(const mbed::util::SharedPointer< ArpcMethodCall<arpc_GetServicesCountRequest, arpc_GetServicesCountResponse> >& call);
 
    arpc::ArpcConnection _arpc_connection;
    ArpcService* _services_list;
};

} // namespace arpc
} // namespace mbed_os
} // namespace song2
} // namespace arm

#endif /* MBED_SONG2_ARPC_ENDPOINT_ */
