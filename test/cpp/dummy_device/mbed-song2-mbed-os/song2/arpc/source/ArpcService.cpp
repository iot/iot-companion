/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>

#include "ArpcEndpoint.h"
#include "arpc/arpc.h"

#include "mbed_assert.h"

#include "core-util/SharedPointer.h"

#include "acore/ac_buffer.h"
#include "acore/ac_buffer_reader.h"
#include "acore/ac_buffer_builder.h"

#include "third_party/pb/pb.h"
#include "third_party/pb/pb_decode.h"
#include "third_party/pb/pb_encode.h"

namespace arm {
namespace song2 {
namespace mbed_os {
namespace arpc {

static bool proto_read_callback (pb_istream_t* stream, uint8_t* buf, size_t count)
{
  ac_buffer_t* rdr = (ac_buffer_t*)(stream->state);

  if(ac_buffer_reader_readable(rdr) < count)
  {
    return false;
  }

  ac_buffer_read_n_bytes(rdr, buf, count);

  return true;
}

static void proto_read_stream(pb_istream_t* stream, ac_buffer_t* payload)
{
  stream->callback = proto_read_callback;
  stream->state = (void*) payload;
  stream->bytes_left = ac_buffer_reader_readable(payload);
  stream->errmsg = NULL;
}

static bool proto_write_callback (pb_ostream_t* stream, const uint8_t* buf, size_t count)
{
  ac_buffer_builder_t* bldr = (ac_buffer_builder_t*)(stream->state);

  if(ac_buffer_builder_writable(bldr) < count)
  {
    return false;
  }

  ac_buffer_builder_write_n_bytes(bldr, buf, count);

  return true;
}

static void proto_write_stream(pb_ostream_t* stream, ac_buffer_builder_t* output)
{
  stream->callback = proto_write_callback;
  stream->state = (void*) output;
  stream->bytes_written = 0;
  stream->max_size = ac_buffer_builder_space(output);
  stream->errmsg = NULL;
}

BaseArpcMethodCall::BaseArpcMethodCall(const ArpcService* service, const ArpcMethodDescriptor* descriptor, 
    arpc_command_t command_id, const mbed::util::SharedPointer<bool>& cancelation_token) :
    _service(service), _descriptor(descriptor), _command_id(command_id), _responded(false), 
    _cancelation_token(cancelation_token)
{

}

BaseArpcMethodCall::~BaseArpcMethodCall() {
    if(!has_responded() && !is_canceled()) {
        fail(arpc_response_err_internal);
    }
}

void BaseArpcMethodCall::success() {
    MBED_ASSERT( !_responded );

    // Ignore if method has been canceled
    if(is_canceled()) {
        return;
    }

    _responded = true;
    _service->respond(_descriptor, _command_id, arpc_response_ok, response_ptr());
}

void BaseArpcMethodCall::fail(arpc_response_code_t error_code) {
    MBED_ASSERT( !_responded );

    // Ignore if method has been canceled
    if(is_canceled()) {
        return;
    }

    _responded = true;
    _service->respond(_descriptor, _command_id, error_code, NULL);
}

bool BaseArpcMethodCall::has_responded() const {
    return _responded;
}

bool BaseArpcMethodCall::is_canceled() const {
    return *_cancelation_token;
}

ArpcService::ArpcService() : _endpoint(NULL), _next(NULL) {
}

ArpcService::~ArpcService() {

}

size_t ArpcService::methods_count() const {
    // Retrieve method descriptors
    const ArpcMethodDescriptor* method_descriptors;
    size_t methods_count;
    get_method_descriptors(&method_descriptors, &methods_count);

    return methods_count;
}

bool ArpcService::method_number_for_name(const char* name, arpc_method_num_t* method_num) const {
    *method_num = 0;

    // Retrieve method descriptors
    const ArpcMethodDescriptor* method_descriptors;
    size_t methods_count;
    get_method_descriptors(&method_descriptors, &methods_count);

    // Go through descriptors and retrieve method number
    for(arpc_method_num_t num = 0; num < methods_count; num++) {
        if(!strcmp(name, method_descriptors[num].name)) {
            // Found it
            *method_num = num;
            return true;
        }
    }

    // Not there
    return false;
}

void ArpcService::handle_request(arpc_command_t command_id, arpc_method_num_t method_num, const ac_buffer_t* payload) const {
    // We should not receive any requests if a connection is not opened
    // However in the extreme case where we were not able to allocate the cancelation token
    // we should let the user know that we're out of memory
    if(_cancelation_token == NULL) {
        _endpoint->connection().send_response(command_id, true, arpc_response_err_memory_error, NULL);
        return;
    }

    // Retrieve method descriptors
    const ArpcMethodDescriptor* method_descriptors;
    size_t methods_count;
    get_method_descriptors(&method_descriptors, &methods_count);

    // The caller should be aware of the methods count and should never call an invalid method
    MBED_ASSERT(method_num < methods_count);

    // This is a valid method
    const ArpcMethodDescriptor* descriptor = &method_descriptors[method_num];

    // Create method call
    mbed::util::SharedPointer<BaseArpcMethodCall> call = descriptor->method_call_constructor(this, descriptor, command_id, _cancelation_token);

    // Retrieve pointer to request struct
    void* request = call->request_ptr();

    // Try to deserialize payload using nanopb
    ac_buffer_t reader;
    ac_buffer_dup(&reader, payload);

    // Deserialize
    pb_istream_t stream;
    proto_read_stream(&stream, &reader);

    bool r = pb_decode(&stream, descriptor->request_fields, request);
    if(!r)
    {
        // Decoding failed
        _endpoint->connection().send_response(command_id, true, arpc_response_err_parsing_error, NULL);
        return;
    }

    // Call handler
    (const_cast<ArpcService*>(this)->*(descriptor->request_callback))(call);
}

void ArpcService::on_connection_opened() {
    // Create a new cancelation token
    _cancelation_token = mbed::util::SharedPointer<bool>(
            new (std::nothrow) bool(false)
        );
}

void ArpcService::on_connection_closed() {
    // All calls which have not completed are now canceled
    *_cancelation_token = true;
}

void ArpcService::respond(const ArpcMethodDescriptor* descriptor, arpc_command_t command_id, arpc_response_code_t response_code, const void* response) const {
    // The service should have been registered before this method is called
    MBED_ASSERT(_endpoint != NULL);
    
    if(response_code == arpc_response_ok) {
        // Serialize response
        size_t payload_sz = 0;

        if(!pb_get_encoded_size(&payload_sz, descriptor->response_fields, response))
        {
            // Serialization error
            _endpoint->connection().send_response(command_id,
                true, arpc_response_err_internal, NULL);
            return;
        }

        // TODO set a max size?
        // Allocate on stack
        ac_buffer_builder_t builder;
        uint8_t* bytes = new (std::nothrow) uint8_t[payload_sz];
        if(bytes == NULL)
        {
            // Not enough memory
            _endpoint->connection().send_response(command_id,
                true, arpc_response_err_memory_error, NULL);
            return;
        }

        ac_buffer_builder_init(&builder, bytes, payload_sz);
        
        pb_ostream_t stream;
        proto_write_stream(&stream, &builder);

        if(!pb_encode(&stream, descriptor->response_fields, response))
        {
            // Make sure to release bytes
            delete[] bytes;

            // Serialization error
            _endpoint->connection().send_response(command_id,
                true, arpc_response_err_internal, NULL);
            return;
        }

        _endpoint->connection().send_response(command_id,
            true, arpc_response_ok, ac_buffer_builder_buffer(&builder));

        delete[] bytes;
    }
    else {
        // Send error back
        _endpoint->connection().send_response(command_id,
            true, response_code, NULL);
    }
}

void ArpcService::set_endpoint(ArpcEndpoint* endpoint) {
    _endpoint = endpoint;
}

void ArpcService::set_next_service(ArpcService* service) {
    _next = service;
}

ArpcService* ArpcService::get_next_service() const {
    return _next;
}

const char* ArpcService::name() const {
    // Retrieve name
    return get_name();
}

} // namespace arpc
} // namespace mbed_os
} // namespace song2
} // namespace arm
