/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstddef>
#include "ArpcTransport.h"

namespace arm {
namespace song2 {
namespace mbed_os {
namespace arpc {

ArpcTransport::ArpcTransport() : _event_handler(NULL) { }

ArpcTransport::~ArpcTransport()
{
    when_transport_closed();
    // FIXME:  What if it transport has already been closed ?
}

void ArpcTransport::set_event_handler(EventHandler *event_handler)
{
    _event_handler = event_handler;
}

void ArpcTransport::when_message_received(const ac_buffer_t* buffer)
{
    if (!_event_handler) {
        return;
    }

    _event_handler->on_transport_receive(buffer);
}

void ArpcTransport::when_transport_closed()
{
    if (!_event_handler) {
        return;
    }

    _event_handler->on_transport_close();
}

} // namespace arpc
} // namespace mbed_os
} // namespace song2
} // namespace arm

