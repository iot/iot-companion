/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "song2/arpc/ArpcConnection.h"
#include "arpc/arpc.h"

namespace arm {
namespace song2 {
namespace mbed_os {
namespace arpc {

ArpcConnection::ArpcConnection() :
    arpc_connection_t(),
    _transport(NULL),
    _event_handler(NULL)
{
}

ArpcConnection::~ArpcConnection()
{
    // unregister transport event handling
    if (_transport) {
        _transport->set_event_handler(NULL);
    }

    // notify application event handler that the connection has been closed
    arpc_transport_connection_closed(this);
}

void ArpcConnection::set_event_handler(EventHandler *event_handler)
{
    _event_handler = event_handler;
}

bool ArpcConnection::open(ArpcTransport *transport)
{
    if (_transport || (transport == NULL)) {
        return false;
    }

    _transport = transport;
    _transport->set_event_handler(this);

    arpc_transport_connection_open(
        /* connection */ this,
        /* impl */ &_arpc_implementation,
        /* user */ this
    );

    return true;
}

bool ArpcConnection::send_request(
    arpc_command_t command_id,
    arpc_method_num_t method_num,
    const ac_buffer_t* payload
) {
    return arpc_request(
        /* connection */ this,
        command_id,
        method_num,
        payload
    );
}

bool ArpcConnection::send_response(
    arpc_command_t command_id,
    bool last_response,
    arpc_response_code_t response_code,
    const ac_buffer_t* payload
) {
    return arpc_respond(
        /* connection */ this,
        command_id,
        last_response,
        response_code,
        payload
    );
}

void ArpcConnection::on_transport_receive(const ac_buffer_t* buffer)
{
    arpc_transport_received(this, buffer);
}

void ArpcConnection::on_transport_close()
{
    // notify that the connection has been closed
    arpc_transport_connection_closed(this);
}

void ArpcConnection::arpc_impl_connection_opened(
    const arpc_connection_t* connection, void* user
) {
    ArpcConnection* self = as_this(user);
    if (!self->_event_handler) {
        return;
    }

    self->_event_handler->on_connection_opened(self);
}

void ArpcConnection::arpc_impl_connection_closed(
    const arpc_connection_t* connection, void* user
) {
    ArpcConnection* self = as_this(user);

    // unregister this as the transport event handler
    if (self->_transport) {
        self->_transport->set_event_handler(NULL);
    }
    self->_transport = NULL;

    // notify event handler
    if (self->_event_handler) {
        self->_event_handler->on_connection_closed(self);
    }
}

void ArpcConnection::arpc_impl_responded(
    const arpc_connection_t* connection,
    arpc_command_t command_id,
    bool last_response,
    arpc_response_code_t response_code,
    const ac_buffer_t* payload,
    void* user
) {
    ArpcConnection* self = as_this(user);
    if (!self->_event_handler) {
        return;
    }

    self->_event_handler->on_response_received(
        /* connection */ self,
        command_id,
        last_response,
        response_code,
        payload
    );
}

void ArpcConnection::arpc_impl_requested(
    const arpc_connection_t* connection,
    arpc_command_t command_id,
    arpc_method_num_t method_num,
    const ac_buffer_t* payload,
    void* user
) {
    ArpcConnection* self = as_this(user);
    if (!self->_event_handler) {
        return;
    }

    self->_event_handler->on_request_received(
        /* connection */ self,
        command_id,
        method_num,
        payload
    );
}

bool ArpcConnection::arpc_impl_transport_send(
    const arpc_connection_t* connection,
    const ac_buffer_t* buffer,
    void* user
) {
    ArpcConnection* self = as_this(user);
    if (!self->_transport) {
        return false;
    }

    return self->_transport->send(buffer);
}

ArpcConnection* ArpcConnection::as_this(void* user)
{
    return reinterpret_cast<ArpcConnection*>(user);
}

arpc_implementation_t ArpcConnection::_arpc_implementation = {
    ArpcConnection::arpc_impl_connection_opened,
    ArpcConnection::arpc_impl_connection_closed,
    ArpcConnection::arpc_impl_responded,
    ArpcConnection::arpc_impl_requested,
    ArpcConnection::arpc_impl_transport_send
};

} // namespace arpc
} // namespace mbed_os
} // namespace song2
} // namespace arm
