/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MBED_SONG2_ARPC_TRANSPORT_
#define MBED_SONG2_ARPC_TRANSPORT_

#include "platform/NonCopyable.h"
#include "acore/ac_buffer.h"

namespace arm {
namespace song2 {
namespace mbed_os {
namespace arpc {

/**
 * Object used by an ArpcConnection to transport and receive packet.
 *
 * An implementation must implement the virtual function send and call
 * when_message_received when a message has been received and call
 * when_transport_closed when the transport has been closed.
 */
class ArpcTransport : mbed::NonCopyable<ArpcTransport> {

public:
    /**
     * ArpcTransport destructor.
     */
    virtual ~ArpcTransport();

protected:
    /**
     * ArpcTransport constructor.
     */
    ArpcTransport();

    /**
     * Invoked by an implementation when a message has been received.
     *
     * @param[in] buffer The data buffer received.
     */
    void when_message_received(const ac_buffer_t* buffer);

    /**
     * Invoked by an implementation when the transport has been closed.
     */
    void when_transport_closed();

private:
    // An ARPC transport is meant to be used by an ArpcConnection; no reasons to
    // leak details to the outside world.
    friend class ArpcConnection;

    /**
     * Event handler of an ArpcTransport
     */
    struct EventHandler {
        virtual ~EventHandler() { }

        /**
         * Invoked when data has been received.
         *
         * @param[in] buffer The data buffer received.
         */
        virtual void on_transport_receive(const ac_buffer_t* buffer) = 0;

        /**
         * Invoked when the transport has been closed.
         */
        virtual void on_transport_close() = 0;
    };

    /**
     * Install the transport event handler.
     *
     * @param[in] event_handler The event handler to register.
     */
    void set_event_handler(EventHandler *event_handler);

    /**
     * Send a data buffer.
     *
     * @param[in] buffer The data buffer to send.
     *
     * @note This function must be overriden by the implementation.
     */
    virtual bool send(const ac_buffer_t* buffer) = 0;

    EventHandler *_event_handler;
};


} // namespace arpc
} // namespace mbed_os
} // namespace song2
} // namespace arm

#endif /* MBED_SONG2_ARPC_TRANSPORT_ */
