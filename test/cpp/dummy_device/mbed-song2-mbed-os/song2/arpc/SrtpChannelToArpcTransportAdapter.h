/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MBED_SONG2_ARPC_SRTP_TRANSPORT_
#define MBED_SONG2_ARPC_SRTP_TRANSPORT_

#include "song2/arpc/ArpcTransport.h"
#include "SrtpChannel.h"

namespace arm {
namespace song2 {
namespace mbed_os {
namespace arpc {

/**
 * Adapt an SrtpChannel into an ArpcTransport.
 *
 * The transport is usable once it has been opened with an SrtpChannel. It
 * remain usable until the SrtpChannel has been closed. Then it can be opened
 * again with another channel.
 *
 * @note The lifecycle of the embedded SrtpChannel is not managed by the
 * addapter.
 */
class SrtpChannelToArpcTransportAdapter :
    public ArpcTransport,
    private srtp::SrtpChannel::EventHandler {

typedef srtp::SrtpChannel SrtpChannel;

public:
    /**
     * Construct an SrtpChannelToArpcTransportAdapter.
     *
     * It needs to be opened before any operation.
     */
    SrtpChannelToArpcTransportAdapter();

    /**
     * Adapter destructor.
     *
     * @note It doesn't close the embedded srtp channel.
     */
    virtual ~SrtpChannelToArpcTransportAdapter();

    /**
     * Assign a channel to the addapter.
     *
     * This operation is required to make the addapter usable by an
     * ArpcConnection.
     *
     * @param[in] channel The srtp channel used by the addapter.
     *
     * @return true If the instance is not already bound to a channel and false
     * otherwise.
     */
    bool open(srtp::SrtpChannel* channel);

private:

    ////////////////////////////////////////////////////////////////////////////
    // srtp::SrtpChannel::EventHandler

    virtual void on_message_sent(SrtpChannel* channel, bool success);

    virtual void on_message_received(SrtpChannel* channel, ac_buffer_t* msg);

    virtual void on_channel_closed(SrtpChannel* channel);

    ////////////////////////////////////////////////////////////////////////////
    // ArpcTransport

    virtual bool send(const ac_buffer_t* buffer);

    SrtpChannel *_channel;
};


} // namespace arpc
} // namespace mbed_os
} // namespace song2
} // namespace arm

#endif /* MBED_SONG2_ARPC_SRTP_TRANSPORT_ */
