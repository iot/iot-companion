/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MBED_SONG2_SRTP_BLE_BACKEND_
#define MBED_SONG2_SRTP_BLE_BACKEND_

#include "ble/BLETypes.h"
#include "song2/ble/Song2Service.h"
#include "song2/srtp/SrtpBackend.h"
#include "song2/srtp/SrtpChannel.h"
#include "song2/srtp/HeapSrtpChannelMsgQueue.h"

namespace arm {
namespace song2 {
namespace mbed_os {
namespace srtp {
namespace ble {

/**
 * SRTPBackend for a ble Song2Service.
 *
 * @note This backend cannot support more than a single channel at a time.
 */
class BLESrtpBackend : public SrtpBackend, private mbed_os::ble::Song2Service::EventHandler {

    typedef song2::mbed_os::ble::Song2Service Song2Service;
    typedef song2::mbed_os::ble::frame_reception_error_t frame_reception_error_t;
    typedef ::ble::connection_handle_t connection_handle_t;

public:
    BLESrtpBackend();

    virtual ~BLESrtpBackend();

    /**
     * Tie the backend to the Song2Service that will receive SRTP frames.
     *
     * @param[in] service The service used by the backend to send and receive
     * srtp frames.
     */
    void register_service(Song2Service& service);

    /**
     * Get an SrtpChannel for a given connection and channel id.
     *
     * @param[in] connection_handle Handle to the peer that will send and receive
     * frames of this channel.
     * @param[in] channel_id The numerical ID of the channel; it is used during
     * ble exchanges to differentiates concurent channels on a single connection.
     *
     * @return The SrtpChannel ready to be used or NULL in case of error.
     */
    SrtpChannel* bind(::ble::connection_handle_t connection_handle, uint8_t channel_id = 0);

private:
    /*
     * SrtpBackend implementation
     */
    virtual bool send(SrtpChannel* channel, ac_buffer_t* pkt);

    virtual void release(SrtpChannel* channel);

    /*
     * Song2ServiceEventHandler implementation
     */
    virtual void on_packet_received(
        connection_handle_t connection, uint8_t channel, ac_buffer_t* packet
    );

    virtual void on_reception_error(
        connection_handle_t connection, frame_reception_error_t error
    );

    bool is_channel_valid(SrtpChannel* channel);

    Song2Service* _service;
    bool _channel_allocated;
    ::ble::connection_handle_t _connection_handle;
    uint8_t _channel_id;
    SrtpChannel _channel;
    HeapSrtpChannelMsgQueue _msg_queue;
};

} // namespace ble
} // namespace srtp
} // namespace mbed_os
} // namespace song2
} // namespace arm

#endif /* MBED_SONG2_SRTP_BLE_BACKEND_ */
