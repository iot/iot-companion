/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MBED_SONG2_SRTP_BACKEND_
#define MBED_SONG2_SRTP_BACKEND_

#include "platform/NonCopyable.h"

namespace arm {
namespace song2 {
namespace mbed_os {
namespace srtp {

class SrtpChannel;
class SrtpChannelMsgQueue;

/**
 * Abstract class that build and manages SrtpChannel instances for a specific
 * transport.
 *
 * It is expected that the concrete implementation provides a factory function
 * which create SrtpChannel instances for their underlying transport or a function
 * that tie transport specific informations to a channel object suplied by the
 * application code.
 *
 * @code {.cpp}
 * class FactoryBackend : SrtpBackend {
 *    SrtpChannel* create_channel(...);
 * };
 *
 * class RegisterBackend : SrtpBackend {
 *   bool register_channel(SrtpChannel* srtp_channel, ....);
 * };
 * @endcode
 */
class SrtpBackend : mbed::NonCopyable<SrtpBackend> {
public:
    /**
     * Destructor.
     */
    virtual ~SrtpBackend() { }

protected:
    /**
     * Must be invoked by the implementation to finalize registration/creation
     * of the channel.
     *
     * @param[in] channel Channel to open
     * @param[in] msg_queue Message queue that will be used by the channel.
     */
    void open_channel(SrtpChannel* channel, SrtpChannelMsgQueue* msg_queue);

    /**
     * Must be invoked by the implementation when a packet has been received.
     *
     * @param channel[in] The channel which has received the packet.
     * @param pkt[in] The packet received.
     */
    void when_packet_received(SrtpChannel* channel, ac_buffer_t* pkt);

    /**
     * Must be invoked by the implementation when the connection has been
     * unexpectedly closed.
     *
     * This function notify application code and release the channel.
     */
    void when_unexpected_close(SrtpChannel* channel);

private:
    friend SrtpChannel;

    /**
     * Send the packet @p pkt on channel @p channel.
     *
     * @param[in] channel The channel used to send the packet.
     * @param[in] pkt The packet to send.
     */
    virtual bool send(SrtpChannel* channel, ac_buffer_t* pkt) = 0;

    /**
     * Close a channel.
     *
     * The implementation should invalid further uses of the channel. If the
     * implementation allocates the channel it shall delete it.
     *
     * @param channel The channel to release.
     */
    virtual void release(SrtpChannel* channel) = 0;
};

} // namespace srtp
} // namespace mbed_os
} // namespace song2
} // namespace arm

#endif /* MBED_SONG2_SRTP_BACKEND_ */
