/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ARM_SONG2_MBED_SIMPLE_SRTP_CHANNEL_MSG_QUEUE_
#define ARM_SONG2_MBED_SIMPLE_SRTP_CHANNEL_MSG_QUEUE_

#include "SrtpChannelMessageQueue.h"
#include "acore/ac_buffer_reader.h"

namespace arm {
namespace song2 {
namespace mbed_os {
namespace srtp {

/**
 * Implementation of SrtpChannelMsgQueue that uses the heap to store messages.
 */
class HeapSrtpChannelMsgQueue : public SrtpChannelMsgQueue {

    struct packed_buffer_t;

public:
    /**
     * Construct an HeapSrtpChannelMsgQueue
     */
    HeapSrtpChannelMsgQueue();

    /**
     * Destroy an HeapSrtpChannelMsgQueue
     */
    virtual ~HeapSrtpChannelMsgQueue();

    /**
     * @see SrtpChannelMsgQueue::push
     */
    virtual bool push(ac_buffer_t* msg);

    /**
     * @see SrtpChannelMsgQueue::push
     */
    virtual bool get(size_t index, ac_buffer_t* msg) const;

    /**
     * @see SrtpChannelMsgQueue::pop
     */
    virtual bool pop();

private:
    packed_buffer_t* _buffers;
};

} // namespace srtp
} // namespace mbed_os
} // namespace song2
} // namespace arm

#endif /* ARM_SONG2_MBED_SIMPLE_SRTP_CHANNEL_MSG_QUEUE_ */
