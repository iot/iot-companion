/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Song2Service.h"

namespace arm {
namespace song2 {
namespace mbed_os {
namespace ble {

Song2Service::Song2Service() :
    _ble_instance(NULL),
    _event_handler(NULL),
    _tx_characteristic(
        /* uuid */ TX_UUID,
        /* valuePtr */ _tx_buffer,
        /* len */ sizeof(_tx_buffer),
        /* maxLen */ sizeof(_tx_buffer),
        /* properties */ GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE_WITHOUT_RESPONSE | GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE // FIXME does not work if we just use BLE_GATT_CHAR_PROPERTIES_WRITE_WITHOUT_RESPONSE
    ),
    _rx_characteristic(
        /* uuid */ RX_UUID,
        /* valuePtr */ _rx_buffer,
        /* len */ sizeof(_rx_buffer),
        /* maxLen */ sizeof(_rx_buffer),
        /* properties */ GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_NOTIFY
    ),
    _service_revision(0),
    _service_revision_characteristic(
        /* uuid */ VERSION_UUID,
        /* valuePtr */ reinterpret_cast<uint8_t*>(&_service_revision),
        /* len */ sizeof(_service_revision),
        /* maxLen */ sizeof(_service_revision),
        /* properties */ GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ
    ),
    _service(
        /* uuid */ SERVICE_UUID,
        /* characteristics */ _service_characteristics,
        /* numCharacteristics */
            sizeof(_service_characteristics) / sizeof(_service_characteristics[0])
    ),
    _frame_builders(NULL),
    _rx_frame_capacity(DEFAULT_RX_CAPACITY)
{
    // update internal pointers (value, descriptors and characteristics array)
    _service_characteristics[0] = &_tx_characteristic;
    _service_characteristics[1] = &_rx_characteristic;
    _service_characteristics[2] = &_service_revision_characteristic;
}

Song2Service::~Song2Service()
{
    while (_frame_builders) {
        remove_frame_builder(_frame_builders);
    }
    cleanup_handlers();
}

ble_error_t Song2Service::register_service(BLE& ble_instance)
{
    if (_ble_instance) {
        return BLE_ERROR_ALREADY_INITIALIZED;
    }

    ble_error_t err = ble_instance.gattServer().addService(_service);
    if (err) {
        return err;
    }

    _ble_instance = &ble_instance;

    // gatt server management
    _ble_instance->gattServer().onDataWritten(this, &Song2Service::when_tx_written);
    _ble_instance->gattServer().onShutdown(this, &Song2Service::when_server_reset);

    // connection management
    _ble_instance->gap().onDisconnection(this, &Song2Service::when_disconnection);
    return BLE_ERROR_NONE;
}

void Song2Service::register_event_handler(EventHandler& event_handler)
{
    _event_handler = &event_handler;
}

void Song2Service::set_rx_frame_capacity(size_t capacity)
{
    _rx_frame_capacity = capacity;
}

bool Song2Service::send_frame(
    ::ble::connection_handle_t connection,
    uint8_t channel,
    ac_buffer_t* buf
) {
    // FIXME: Sending a packet too large for the stack will always fail;
    // it would be possible to fill the stack with a set of fragments then
    // pause the operation when the stack is full and continue it once the
    // stack has free tx buffers available. Of course this would require to
    // indicate the status of the operation asynchronously as well as keeping
    // a local copy of the data or ask the user to maintain it.
    if (!_ble_instance) {
        return false;
    }

    // ensure that the peer has enabled notifications on the RX characteristic
    bool updates_enabled = false;
    ble_error_t err = _ble_instance->gattServer().areUpdatesEnabled(
        connection, _rx_characteristic, &updates_enabled
    );

    if (err || !updates_enabled) {
        return false;
    }

    // Prepend the channel number and use the resulting buffer as a reader
    ac_buffer_t buf_reader;
    ac_buffer_init(&buf_reader, &channel, 1);
    ac_buffer_set_next(&buf_reader, buf);

    // compute the constants required by the algorithm
    const size_t data_len = ac_buffer_reader_readable(&buf_reader);
    const uint16_t payload_capacity = mtu_size(connection) - HEADER_SIZE;
    const uint16_t fragments_count =
        (data_len / payload_capacity) + ((data_len % payload_capacity) ? 1 : 0);
    const uint16_t last_fragment_id = fragments_count - 1;

    // Send all the fragments except the last one. All these fragments are
    // payload_capacity bytes.
    for (uint16_t i = 0; i < last_fragment_id; ++i) {
        bool success = send_fragment(
            connection,
            /* fragment data */ &buf_reader,
            /* fragment data len */payload_capacity,
            /* fragment id */ i,
            /* last fragment ? */ false
        );
        if (!success) {
            return false;
        }
    }

    // send the last fragment
    return send_fragment(
        connection,
        /* fragment data */ &buf_reader,
        /* fragment len */data_len - (last_fragment_id * payload_capacity),
        /* fragment id */ last_fragment_id,
        /* last fragment ? */ true
    );
}

/*
 * Struture that handles frame reassembly
 */
class Song2Service::FrameBuilder {
public:
    // return a new FrameBuilder if applicable
    static FrameBuilder* create(
        ::ble::connection_handle_t connection,
        const uint8_t* fragment_data,
        size_t size,
        size_t capacity
    ) {
        if (size < 2) {
            return NULL;
        }

        if ((size - 2) > capacity) {
            return NULL;
        }

        // Dismiss if it is the only packet; no allocations are needed
        if (fragment_data[0] == LAST_FRAGMENT_FLAG_MASK) {
            return NULL;
        }

        // Dismiss if it is not the first fragment
        if (fragment_data[0] & FRAGMENT_NUMBER_MASK) {
            return NULL;
        }

        void* result = malloc(sizeof(FrameBuilder) + size - 2);
        return new(result) FrameBuilder(connection, fragment_data, size, capacity);
    }

    // destroy this frame builder
    void release()
    {
        this->~FrameBuilder();
        free(this);
    }

    // push a new fragment into the builder
    bool push_fragment(const uint8_t* fragment_data, size_t fragment_size) {
        // reject fragments of invalid size
        if (fragment_size < 1) {
            return false;
        }

        // reject the last fragment; it should be appended on stack by the caller
        if (fragment_data[0] & LAST_FRAGMENT_FLAG_MASK) {
            return false;
        }

        // ensure that it is the right fragment id
        uint8_t fragment_id = fragment_data[0] & FRAGMENT_NUMBER_MASK;
        if (fragment_id != (_current_fragment_id + 1)) {
            return false;
        }

        const uint8_t* payload = fragment_data + 1;
        size_t payload_size = fragment_size - 1;

        // ensure the payload size does not overflow the frame capacity
        if ((_size + payload_size) > _capacity) {
            return false;
        }

        memcpy(_frame_data + _size, payload, payload_size);

        // update the builder data structure
        _size += payload_size;
        _current_fragment_id = fragment_id;
        return true;
    }

    ::ble::connection_handle_t connection() const
    {
        return _connection;
    }

    uint8_t channel() const
    {
        return _channel;
    }

    uint8_t current_fragment_id() const
    {
        return _current_fragment_id;
    }

    ac_buffer_t frame()
    {
        ac_buffer_t buf;
        ac_buffer_init(&buf, _frame_data, _size);
        return buf;
    }

    FrameBuilder* next()
    {
        return _next;
    }

    void set_next(FrameBuilder* next)
    {
        _next = next;
    }

private:
    FrameBuilder(
        ::ble::connection_handle_t connection,
        const uint8_t* fragment_data,
        size_t fragment_size,
        size_t capacity
    ) : _connection(connection),
        _channel(fragment_data[1]),
        _current_fragment_id(0),
        _next(NULL),
        _capacity(capacity),
        _size(0),
        _frame_data() {
        memcpy(_frame_data, fragment_data + 2, fragment_size - 2);
        _size = fragment_size - 2;
    }

    ~FrameBuilder() { }

    const ::ble::connection_handle_t _connection;
    uint8_t _channel;
    uint8_t _current_fragment_id;
    FrameBuilder* _next;
    const size_t _capacity;
    size_t _size;
    // warning: this field must be the last one to allow variable size allocation
    uint8_t _frame_data[1];
};

/*
 * Invoked when the GATT server containing the service is reset.
 *
 * It discards all pending packets.
 */
void Song2Service::when_server_reset(const GattServer *server)
{
    if (server != &_ble_instance->gattServer()) {
        return;
    }

    // cleanup frame builders
    while(_frame_builders) {
        signal_error(
            _frame_builders->connection(),
            frame_reception_error_t::SERVER_RESET
        );
    }

    cleanup_handlers();

    // reset the ble instance as the service is not registered anywhere
    _ble_instance = NULL;
}

void Song2Service::when_disconnection(
    const Gap::DisconnectionCallbackParams_t *disconnection_params
) {
    signal_error(
        disconnection_params->handle,
        frame_reception_error_t::PEER_DISCONNECTED
    );
}

void Song2Service::cleanup_handlers()
{
    // clean up callacks registered into ble
    _ble_instance->gattServer().onDataWritten().detach(
        makeFunctionPointer(this, &Song2Service::when_tx_written)
    );
    _ble_instance->gattServer().onShutdown().detach(
        makeFunctionPointer(this, &Song2Service::when_server_reset)
    );

    // connection management
    _ble_instance->gap().onDisconnection().detach(
        makeFunctionPointer(this, &Song2Service::when_disconnection)
    );
}

/**
 * Send a fragment to the peer
 *
 * @pre _ble_instance shall be valid
 * @pre notifications are enabled on the TX characteristic.
 * @pre payload_size is less than mtu_size() - 1
 *
 * @param connection The handle to the peer that will receive the packet
 * @param buf The fragment to send
 * @param payload_size Size of the payload of the fragment
 * @param fragment_id Id of the fragment to send
 * @param last_fragment True if the fragment to send is the last fragment of
 * a packet.
 *
 * @return true if the fragment has been succesfully queued in the ble stack
 * and false otherwise.
 *
 * @post read position of buf_reader advanced by payload_size bytes.
 */
bool Song2Service::send_fragment(
    ::ble::connection_handle_t connection,
    ac_buffer_t* buf_reader,
    size_t payload_size,
    uint8_t fragment_id,
    bool last_fragment
) {
    uint8_t fragment[MAX_MTU_SIZE];

    // initialize the header
    fragment[0] = fragment_id;
    fragment[0] |= (last_fragment ? LAST_FRAGMENT_FLAG_MASK : 0);

    // copy the payload into the fragment; advance the reader position
    ac_buffer_read_n_bytes(buf_reader, fragment + 1, payload_size);

    // FIXME: use notify once GattServer offers a function that solely handle
    // notifications
    ble_error_t err = _ble_instance->gattServer().write(
        connection,
        _rx_characteristic.getValueHandle(),
        fragment,
        payload_size + HEADER_SIZE,
        /* local only */ false
    );

    return err ? false : true;
}

void Song2Service::when_tx_written(const GattWriteCallbackParams* write_params)
{
    const ::ble::connection_handle_t connection = write_params->connHandle;
    // ignore write not targetting the tx characteristic
    if (write_params->handle != _tx_characteristic.getValueHandle()) {
        return;
    }

    // ignore non write commands
    if (write_params->writeOp != GattWriteCallbackParams::OP_WRITE_CMD) {
        return;
    }

    const uint8_t* fragment_data = write_params->data;
    const size_t fragment_len = write_params->len;

    // Signal error for write of invalid size
    if (fragment_len < 1) {
        signal_error(connection, frame_reception_error_t::INVALID_FRAGMENT_SIZE);
        return;
    }

    // extract the fragment information
    const uint8_t fragment_number = fragment_data[0] & FRAGMENT_NUMBER_MASK;
    const bool is_last_fragment = fragment_data[0] & LAST_FRAGMENT_FLAG_MASK;

    if (fragment_number == 0) {
        when_first_fragment_received(
            connection, fragment_data, fragment_len, is_last_fragment
        );
    } else {
        when_non_first_fragment_received(
            connection,
            fragment_data,
            fragment_len,
            is_last_fragment,
            fragment_number
        );
    }
}

void Song2Service::when_first_fragment_received(
    ::ble::connection_handle_t connection,
    const uint8_t* fragment_data,
    size_t fragment_len,
    bool is_last_fragment
) {
    if (fragment_len < 2) {
        signal_error(connection, frame_reception_error_t::INVALID_FRAGMENT_SIZE);
        return;
    }

    // dismiss incomplete frame and signal the error
    if (get_frame_builder(connection)) {
        signal_error(connection, frame_reception_error_t::INCOMPLETE_FRAME);
    }

    if (is_last_fragment) {
        // create the ac packet locally and forward it to the event
        // handler
        ac_buffer_t buf;
        ac_buffer_init(&buf, fragment_data + 2, fragment_len - 2);
        _event_handler->on_packet_received(
            connection,
            /* channel */ fragment_data[1],
            &buf
        );
    } else {
        // create a new frame builder and add it into the frame_builders list
        FrameBuilder* rx_fb = FrameBuilder::create(
            connection,
            fragment_data,
            fragment_len,
            _rx_frame_capacity
        );

        if (!rx_fb) {
            signal_error(connection, frame_reception_error_t::OUT_OF_MEMORY);
            return;
        }

        add_frame_builder(rx_fb);
    }
}

void Song2Service::when_non_first_fragment_received(
    ::ble::connection_handle_t connection,
    const uint8_t* fragment_data,
    size_t fragment_len,
    bool is_last_fragment,
    uint8_t fragment_number
) {
    FrameBuilder* frame_builder = get_frame_builder(connection);

    if (!frame_builder) {
        signal_error(connection, frame_reception_error_t::INCOMPLETE_FRAME);
        return;
    }

    if (fragment_number != (frame_builder->current_fragment_id() + 1)) {
        signal_error(connection, frame_reception_error_t::INCOMPLETE_FRAME);
        return;
    }

    if (!is_last_fragment) {
        if (!frame_builder->push_fragment(fragment_data, fragment_len)) {
            signal_error(connection, frame_reception_error_t::INSUFFICIENT_CAPACITY);
        }
    } else {
        // get the frame in the frame builder
        ac_buffer_t frame = frame_builder->frame();

        // create the ac packet locally
        ac_buffer_t last_fragment;
        ac_buffer_init(&last_fragment, fragment_data + 1, fragment_len -1);

        // append the last fragment
        ac_buffer_append(&frame, &last_fragment);

        // notify clients of the frame received
        _event_handler->on_packet_received(
            connection,
            frame_builder->channel(),
            &frame
        );

        // remove and free the frame builder
        remove_frame_builder(frame_builder);
    }
}

/**
 * Return the size of the mtu used in the connection in input.
 *
 * @pre _ble_instance shall be valid
 *
 * @return the size of the mtu for @p connection.
 */
uint16_t Song2Service::mtu_size(::ble::connection_handle_t connection) const
{
    // FIXME: update when ble API supports MTU size updates
    // FIXME: For now, only support long writes
    return MAX_MTU_SIZE;
}

void Song2Service::signal_error(
    ::ble::connection_handle_t connection, frame_reception_error_t error
) {

    if (!_event_handler) {
        return;
    }

    _event_handler->on_reception_error(connection, error);

    FrameBuilder* fb = get_frame_builder(connection);
    if (fb) {
        remove_frame_builder(fb);
    }
}

void Song2Service::add_frame_builder(FrameBuilder* fb)
{
    // doesn't really matter if the frame builder is added on the head or the
    // tail
    fb->set_next(_frame_builders);
    _frame_builders = fb;
}

Song2Service::FrameBuilder* Song2Service::get_frame_builder(
    ::ble::connection_handle_t connection
) const {
    FrameBuilder* fb = _frame_builders;
    while(fb) {
        if (fb->connection() == connection) {
            return fb;
        }
        fb = fb->next();
    }

    return NULL;
}

void Song2Service::remove_frame_builder(FrameBuilder* frame_builder)
{
    // handle head of the list case
    if (frame_builder == _frame_builders) {
        _frame_builders = _frame_builders->next();
        frame_builder->release();
        return;
    }

    FrameBuilder* prev = _frame_builders;
    while(prev) {
        if (prev->next() == frame_builder) {
            prev->set_next(frame_builder->next());
            frame_builder->release();
            return;
        }

        prev = prev->next();
    }
}

const char Song2Service::SERVICE_UUID[] = "EC8C0100-EC9B-30AF-1D41-AB00BEB494DF";
const char Song2Service::TX_UUID[] = "EC8C0101-EC9B-30AF-1D41-AB00BEB494DF";
const char Song2Service::RX_UUID[] = "EC8C0102-EC9B-30AF-1D41-AB00BEB494DF";
const char Song2Service::VERSION_UUID[] = "EC8C0103-EC9B-30AF-1D41-AB00BEB494DF";

} // namespace ble
} // namespace mbed_os
} // namespace song2
} // namespace arm
