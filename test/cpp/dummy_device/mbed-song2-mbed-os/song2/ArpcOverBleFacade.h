/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MBED_SONG2_ARPC_OVER_BLE_
#define MBED_SONG2_ARPC_OVER_BLE_

#include "srtp/srtp.h"
#include "platform/NonCopyable.h"

#include "ble/BLE.h"
#include "ble/Gap.h"

#include "song2/ble/Song2Service.h"
#include "song2/srtp/ble/BLESrtpBackend.h"
#include "song2/arpc/ArpcConnection.h"
#include "song2/arpc/SrtpChannelToArpcTransportAdapter.h"

namespace arm {
namespace song2 {
namespace mbed_os {

/**
 * Facade that ease management of an ArpcConnection over a BLE transport.
 *
 * Arpc, srtp and the Song2 GATT service are well separated software components;
 * however in real life use case these components are assembled and used together.
 * This class simplify the assembly and lifecycle management from a user
 * perspective. It also hides unecessary implementation details.
 *
 * @par example
 *
 * @code
    using arm::song2::mbed_os::arpc::ArpcConnection;
    using arm::song2::mbed_os::ArpcOverBleFacade;

    // must be initialized of the application event handler.
    static ArpcConnection::EventHandler* event_handler;
    static ArpcConnection arpc_connection;
    static ArpcOverBleFacade arpc_over_ble;

    // BLE init complete callback
    void bleInitComplete(BLE::InitializationCompleteCallbackContext *params)
    {
        BLE &ble   = params->ble;

        // setup facade: register service and install callbacks managing the lifecycle
        // of the connection.
        arpc_over_ble.register_facade(ble);

        // other post init code
    }

    int main()
    {
        // tie the event handler to the connection and the connection to the facade
        arpc_connection.set_event_handler(&arpc_event_handler);
        arpc_over_ble.bind_connection(arpc_connection);

        // Ble initialization goes here
    }
 * @endcode
 */
class ArpcOverBleFacade : mbed::NonCopyable<ArpcOverBleFacade> {
    typedef ArpcOverBleFacade Self;

public:
    /**
     * Construct a facade that manages an ArpcConnection over a Bluetooth link.
     */
    ArpcOverBleFacade();

    /**
     * Destroy an ArpcOverBleFacade.
     *
     * The existing connection is closed and callbacks attached to the Bluetooth
     * subsystem are cleaned up.
     */
    ~ArpcOverBleFacade();

    /**
     * Bind a single connection to a facade.
     *
     * Once the connection is bound, application code should be driven by the
     * event handler registered in the connection:
     *   - on_connection_opened is called once a connection to a distant peer is
     *     established.
     *   - on_connection_closed is called when the connection with the distant
     *     peer has been dropped.
     *
     * @param[in] connection The connection that will be managed by the facade.
     *
     * @note The facade manages a single connection at a time. This function
     * should not be called if the connection bound to the facade is already
     * opened.
     */
    void bind_connection(arpc::ArpcConnection &connection);

    /**
     * Tie the facade and its components with @p ble_instance.
     *
     * @param[in] ble_instance The Bluetooth instance used by the facade. The
     * instance in parameter must be usable.
     *
     * @note This function must be called again if @p ble_instance has been
     * shutdown: A shutdown of a ble instance close the managed connection and
     * cleanup callbacks attached to the ble_instance.
     */
    void register_facade(BLE &ble_instance);

private:
    // callback invoked when a bluetooth connection happen.
    void on_connection(const Gap::ConnectionCallbackParams_t * p);

    // callback invoked when a bluetooth connection is closed
    void on_disconnection(const Gap::DisconnectionCallbackParams_t *params);

    // callback invoked when gap is shutdown
    void on_gap_shutdown(const Gap*);

    // callback invoked when gatt server is shutdown
    void on_gatt_shutdown(const GattServer*);

    // release the srtp channel
    void release_srtp_channel();

    // release the srtp channel then cleanup the state associated to _ble.
    void cleanup();

    // accessor to gap; _ble must be valid
    Gap& gap();

    // accessor to gattserver; _ble must be valid
    GattServer& gatt_server();

    // Callback builder helper
    template<typename Arg>
    FunctionPointerWithContext<Arg> as_cb(void (Self::*member)(Arg))
    {
        return makeFunctionPointer(this, member);
    }

    BLE* _ble;
    ble::Song2Service _song2_service;
    srtp::ble::BLESrtpBackend _ble_srtp_backend;
    ::ble::connection_handle_t _connection_handle;
    srtp::SrtpChannel* _channel = NULL;
    arpc::SrtpChannelToArpcTransportAdapter _arpc_srtp_transport;
    arpc::ArpcConnection* _arpc_connection;
};

} // namespace mbed_os
} // namespace song2
} // namespace arm

#endif /* MBED_SONG2_ARPC_OVER_BLE_ */
