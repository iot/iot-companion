/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "song2/ArpcOverBleFacade.h"

namespace arm {
namespace song2 {
namespace mbed_os {

ArpcOverBleFacade::ArpcOverBleFacade() :
    _ble(NULL),
    _song2_service(),
    _ble_srtp_backend(),
    _connection_handle(0xFFFF),
    _channel(NULL),
    _arpc_srtp_transport(),
    _arpc_connection(NULL)
{
}

ArpcOverBleFacade::~ArpcOverBleFacade()
{
    cleanup();
}

void ArpcOverBleFacade::bind_connection(arpc::ArpcConnection& connection)
{
    if (_channel) {
        return;
    }

    _arpc_connection = &connection;
}

void ArpcOverBleFacade::register_facade(BLE& ble_instance)
{
    if (_ble) {
        return;
    }

    _ble = &ble_instance;
    _song2_service.register_service(*_ble);
    _ble_srtp_backend.register_service(_song2_service);

    // register callbacks
    gap().onConnection().add(as_cb(&Self::on_connection));
    gap().onDisconnection().add(as_cb(&Self::on_disconnection));
    gap().onShutdown().add(as_cb(&Self::on_gap_shutdown));
    gatt_server().onShutdown().add(as_cb(&Self::on_gatt_shutdown));
}

void ArpcOverBleFacade::on_connection(const Gap::ConnectionCallbackParams_t * p)
{
    if (_channel) {
        return;
    }

    _connection_handle = p->handle;
    _channel = _ble_srtp_backend.bind(_connection_handle);
    _arpc_srtp_transport.open(_channel);
    _arpc_connection->open(&_arpc_srtp_transport);
}

void ArpcOverBleFacade::on_disconnection(const Gap::DisconnectionCallbackParams_t *params)
{
    if ((params->handle != _connection_handle) || (_channel == NULL)) {
        return;
    }

    release_srtp_channel();
}

void ArpcOverBleFacade::on_gap_shutdown(const Gap*)
{
    cleanup();
}

void ArpcOverBleFacade::on_gatt_shutdown(const GattServer*)
{
    cleanup();
}

void ArpcOverBleFacade::release_srtp_channel()
{
    if (_channel) {
        _channel->close();
        _channel = NULL;
        _connection_handle = 0xFFFF;
    }
}

void ArpcOverBleFacade::cleanup()
{
    release_srtp_channel();

    if (_ble) {
        gap().onConnection().detach(as_cb(&Self::on_connection));
        gap().onDisconnection().detach(as_cb(&Self::on_disconnection));
        gap().onShutdown(as_cb(&Self::on_gap_shutdown));
        gatt_server().onShutdown(as_cb(&Self::on_gatt_shutdown));

        _ble = NULL;
    }
}

Gap& ArpcOverBleFacade::gap()
{
    return _ble->gap();
}

GattServer& ArpcOverBleFacade::gatt_server()
{
    return _ble->gattServer();
}

} // namespace mbed_os
} // namespace song2
} // namespace arm
