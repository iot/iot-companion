//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef ARPC_ARPC_SERVICE_WIFI_COMMISSIONING_
#define ARPC_ARPC_SERVICE_WIFI_COMMISSIONING_

#include "song2/arpc/ArpcService.h"

#include "wifi_commissioning.pb.h"

namespace arm {
    namespace song2 {
        namespace mbed_os {
            namespace generated {
                class ArpcWifiService : public arpc::ArpcService {
                public:
                    ArpcWifiService();
                    virtual ~ArpcWifiService();

                protected:
                    virtual void disconnect(const mbed::util::SharedPointer<arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage>>& call) = 0;
                    virtual void connect(const mbed::util::SharedPointer<arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage>>& call) = 0;
                    virtual void set_parameters(const mbed::util::SharedPointer<arpc::ArpcMethodCall<com_arm_commissioning_WiFiCommissioningParameters, com_arm_commissioning_EmptyMessage>>& call) = 0;
                    virtual void get_info(const mbed::util::SharedPointer<arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_WiFiCommissioningInfo>>& call) = 0;
                private:
                    virtual void get_method_descriptors(const arpc::ArpcMethodDescriptor** methods, size_t* count) const;
                    virtual const char* get_name() const;

                    static const char* _s_name;
                    static size_t _s_methods_count;
                    static arpc::ArpcMethodDescriptor _s_methods_descriptors[];
                };

            }
        }
    }
}

#endif /* ARPC_ARPC_SERVICE_WIFI_COMMISSIONING_ */