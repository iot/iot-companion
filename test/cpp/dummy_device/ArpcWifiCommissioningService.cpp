//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "wifi_commissioning.pb.h"
#include "ArpcWifiCommissioningService.h"

#include "pb/pb.h"

namespace arm {
namespace song2 {
namespace mbed_os {
namespace generated {

ArpcWifiService::ArpcWifiService() : arpc::ArpcService()
{

}

ArpcWifiService::~ArpcWifiService()
{

}


size_t ArpcWifiService::_s_methods_count = 4;

arpc::ArpcMethodDescriptor ArpcWifiService::_s_methods_descriptors[] = {
    {"disconnect", com_arm_commissioning_EmptyMessage_fields, com_arm_commissioning_EmptyMessage_fields,
        arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage>::make_shared,
        (const arpc::ArpcMethodRequestCallback_t) &ArpcWifiService::disconnect
        },
    {"connect", com_arm_commissioning_EmptyMessage_fields, com_arm_commissioning_EmptyMessage_fields,
        arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage>::make_shared,
        (const arpc::ArpcMethodRequestCallback_t) &ArpcWifiService::connect
        },
    {"setParameters", com_arm_commissioning_WiFiCommissioningParameters_fields, com_arm_commissioning_EmptyMessage_fields,
        arpc::ArpcMethodCall<com_arm_commissioning_WiFiCommissioningParameters, com_arm_commissioning_EmptyMessage>::make_shared,
        (const arpc::ArpcMethodRequestCallback_t) &ArpcWifiService::set_parameters
        },
    {"getInfo", com_arm_commissioning_EmptyMessage_fields, com_arm_commissioning_WiFiCommissioningInfo_fields,
        arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_WiFiCommissioningInfo>::make_shared,
        (const arpc::ArpcMethodRequestCallback_t) &ArpcWifiService::get_info
        },
};

const char* ArpcWifiService::_s_name = "com.arm.commissioning.WiFi";

void ArpcWifiService::get_method_descriptors(const arpc::ArpcMethodDescriptor** methods, size_t* count) const {
    *methods = _s_methods_descriptors;
    *count = _s_methods_count;
}

const char* ArpcWifiService::get_name() const {
    return _s_name;
}


}

namespace arpc {

namespace details {
template<> const com_arm_commissioning_WiFiCommissioningParameters& Prototype<com_arm_commissioning_WiFiCommissioningParameters>::value() {
    static com_arm_commissioning_WiFiCommissioningParameters prototype = com_arm_commissioning_WiFiCommissioningParameters_init_default;
    return prototype;
}
template<> const com_arm_commissioning_WiFiCommissioningInfo& Prototype<com_arm_commissioning_WiFiCommissioningInfo>::value() {
    static com_arm_commissioning_WiFiCommissioningInfo prototype = com_arm_commissioning_WiFiCommissioningInfo_init_default;
    return prototype;
}
template<> const com_arm_commissioning_EmptyMessage& Prototype<com_arm_commissioning_EmptyMessage>::value() {
    static com_arm_commissioning_EmptyMessage prototype = com_arm_commissioning_EmptyMessage_init_default;
    return prototype;
}
}

}
}
}
}