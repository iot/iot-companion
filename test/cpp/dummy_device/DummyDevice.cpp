//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  DummyDevice.cpp
//  song2-tests
//
//  Created by Donatien Garnier on 19/01/2018.

//

#include "cpp/logging/Logging.hpp"

#include "DummyDevice.hpp"

#include "ArpcDummyServiceService.h"
#include "ArpcWifiCommissioningService.h"

using namespace arm::song2;
using namespace arm::song2::test;

// Implement ARPC Services

namespace arm {
    namespace song2 {
        namespace mbed_os {
            namespace service {
                class DummyService : public generated::ArpcDummyService {
                private:
                    int32_t _integer;
                    std::string _string;
                    
                public:
                    DummyService() : generated::ArpcDummyService(), _integer(0), _string("") {}
                    virtual ~DummyService() {}
                    
                    virtual void set_integer(const mbed::util::SharedPointer<arpc::ArpcMethodCall<arm_song2_test_IntegerMessage, arm_song2_test_EmtpyMessage>>& call) {
                        _integer = call->request().an_int;
                        call->success();
                    }
                    
                    virtual void get_integer(const mbed::util::SharedPointer<arpc::ArpcMethodCall<arm_song2_test_EmtpyMessage, arm_song2_test_IntegerMessage>>& call) {
                        call->response().an_int = _integer;
                        call->success();
                    }
                    
                    virtual void get_string(const mbed::util::SharedPointer<arpc::ArpcMethodCall<arm_song2_test_EmtpyMessage, arm_song2_test_StringMessage>>& call) {
                        strncpy(call->response().a_string, _string.c_str(), sizeof(call->response().a_string));
                        call->response().a_string[sizeof(call->response().a_string) - 1] = 0;
                        call->success();
                    }
                    
                    virtual void set_string(const mbed::util::SharedPointer<arpc::ArpcMethodCall<arm_song2_test_StringMessage, arm_song2_test_EmtpyMessage>>& call) {
                        _string = call->request().a_string;
                        call->success();
                    }
                    
                    virtual void say_hello(const mbed::util::SharedPointer<arpc::ArpcMethodCall<arm_song2_test_StringMessage, arm_song2_test_EmtpyMessage>>& call) {
                        logging::debug() << "Hello, " << call->request().a_string << "!";
                    }
                };
                
                class WifiService : public generated::ArpcWifiService {
                public:
                    WifiService() : generated::ArpcWifiService() {}
                    ~WifiService() {}
                    
                    virtual void disconnect(const mbed::util::SharedPointer<arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage>>& call)
                    {
                        call->success();
                    }
                    
                    virtual void connect(const mbed::util::SharedPointer<arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_EmptyMessage>>& call)
                    {
                        call->success();
                    }
                    
                    virtual void set_parameters(const mbed::util::SharedPointer<arpc::ArpcMethodCall<com_arm_commissioning_WiFiCommissioningParameters, com_arm_commissioning_EmptyMessage>>& call)
                    {
                        call->success();
                    }
                    
                    virtual void get_info(const mbed::util::SharedPointer<arpc::ArpcMethodCall<com_arm_commissioning_EmptyMessage, com_arm_commissioning_WiFiCommissioningInfo>>& call)
                    {
                        call->success();
                    }
                };
                
            }
        }
    }
}



DummyDevice::DummyDevice()
{
    
}

DummyDevice::Channel::Channel(pipe::Endpoint::ptr&& endpoint) : mbed_os::srtp::SrtpBackend(), ep(std::move(endpoint)) {
    
    // Register ARPC Services
    this->services.push_back(std::make_unique<mbed_os::service::DummyService>());
    this->services.push_back(std::make_unique<mbed_os::service::WifiService>());
    
    for(auto& service : this->services)
    {
        this->arpc_ep.register_service(*service);
    }
    
    // Open SRTP
    this->open_channel(&this->srtp_channel, &this->srtp_msg_queue);
    
    // Connect ARPC and SRTP
    this->arpc_srtp_transport.open(&this->srtp_channel);
    
    // Open ARPC
    this->arpc_ep.connection().open(&this->arpc_srtp_transport);
    
    // Pipe received bytes
    this->ep->receive([this](std::future<bytes_t>& fut){
        ac_buffer_t buf;
        auto bytes = fut.get();
        ac_buffer_map_bytes(&buf, bytes);
        when_packet_received(&this->srtp_channel, &buf);
    });
}

bool DummyDevice::Channel::send(mbed_os::srtp::SrtpChannel* channel, ac_buffer_t* pkt) {
    ac_buffer_t buffer_dup;
    ac_buffer_dup(&buffer_dup, pkt);
    auto bytes = ac_buffer_to_bytes(&buffer_dup);
    
    this->ep->send(std::move(bytes));
    return true;
}

void DummyDevice::Channel::release(mbed_os::srtp::SrtpChannel* channel) {
    
}

void DummyDevice::new_channel(uint32_t channel_num, pipe::Endpoint::ptr&& endpoint)
{
    _channels.emplace(std::piecewise_construct, std::forward_as_tuple(channel_num),  std::forward_as_tuple(std::move(endpoint)));
}
