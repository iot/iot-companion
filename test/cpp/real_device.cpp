//
//  workflows.cpp
//  song2
//
//  Created by Donatien Garnier on 19/02/2019.
//  Copyright © 2019 ARM Ltd. All rights reserved.
//

#include <fstream>

#include "gtest/gtest.h"

#include "test.hpp"

#include "cpp/threading/Queue.hpp"
#include "cpp/threading/BlockingDispatcher.hpp"
#include "cpp/threading/Promise.hpp"
#include "cpp/threading/Observable.hpp"

#include "cpp/device/Device.hpp"

#include "cpp/transport/ble/BleScanner.hpp"
#include "cpp/platform/Platform.hpp"

#include "cpp/logging/Logging.hpp"

#include "cpp/srtp/SrtpEndpoint.hpp"
#include "cpp/arpc/ArpcEndpoint.hpp"
#include "cpp/arpc/ArpcException.hpp"
#include "cpp/arpc/ProtoStore.hpp"

using namespace arm::song2;

class RealDeviceTest : public ::testing::Test {
public:
    // Create JS Environment
    virtual void SetUp()
    {
        _dptch = threading::make_blocking_dispatcher();

        // Do this on dispatcher
        auto q = threading::make_queue(_dptch);
        q->post([this](){
            // Create BLE scanner
            _scanner = std::make_unique<transport::BleScanner>(platform::platform_factory()->get_ble_adapter());
        });
        
        _dptch->start();
        _dptch->process();
    }
    
    virtual void TearDown()
    {
        
    }
    
    void run_on_dispatcher(const std::function<void(const threading::Queue::ptr& queue)>& fn)
    {
        auto q = threading::make_queue(_dptch);
        bool has_run = false;
        q->post([&fn, &has_run, &q](){
            fn(q);
            has_run = true;
        });
        _dptch->process();
        ASSERT_TRUE(has_run);
    }
    
    void timeout(const std::function<bool()>& condition, std::chrono::milliseconds delay) {
        auto q = threading::make_queue(_dptch);
        bool has_run = false;
        q->post_delayed([&condition, &has_run, this](){
            EXPECT_TRUE(condition()); // EXPECT to make sure we don't return early
            has_run = true;
        }, delay);
        while(!condition() && !has_run)
        {
            _dptch->wait();
            _dptch->process();
        }
    }
    
protected:
    typename threading::BlockingDispatcher::ptr _dptch;
    std::unique_ptr<transport::BleScanner> _scanner;
};

TEST_F(RealDeviceTest, ArpcDeviceTest) {
    size_t count = 0;
    std::shared_ptr<transport::Device> dev = nullptr;
    std::unique_ptr<srtp::SrtpEndpoint> srtp = nullptr;
    std::unique_ptr<arpc::ArpcEndpoint> arpc = nullptr;

    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        // Scan for devices
        _scanner->visible_devices()->next([&dev, this](std::future<std::shared_ptr<transport::Device>>& dev_fut){
            if(dev == nullptr) {
                dev = dev_fut.get();
            }
        }, queue);
        _scanner->adapter_status()->next([&dev, this](std::future<platform::BleAdapterStatus>& status_fut){
            if(dev == nullptr) {
                auto status = status_fut.get();
                if(status == platform::BleAdapterStatus::ENABLED) {
                    _scanner->start_scanning();
                }
            }
        }, queue);
        _scanner->start_scanning();
    });
    
    timeout([&dev](){ return dev != nullptr; }, std::chrono::milliseconds(15000));
    
    ASSERT_TRUE(dev != nullptr);

    bool connected = false;
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        dev->connect()->then([this, &connected](std::future<void>& f){
            f.get();
            connected = true;
            _scanner->stop_scanning();
        }, queue);
    });
    
    timeout([&connected](){ return connected == true; }, std::chrono::milliseconds(15000));

    ASSERT_TRUE(connected);

    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_NE(dev, nullptr);
        
        // Recover blob file
        auto blob_file = platform::platform_factory()->get_resource_path(platform::PlatformBundle::APP, "blob.pb");
        
        // Instantiate proto store
        auto proto_store = std::make_shared<arpc::ProtoStore>();
        
        // Register blob
        proto_store->register_services_from_files({blob_file});
        
        // Instantiate arpc and srtp
        srtp = std::make_unique<srtp::SrtpEndpoint>(dev->open_endpoint_for_channel(0));
        arpc = std::make_unique<arpc::ArpcEndpoint>(srtp->open());
        arpc->set_proto_store(proto_store);
        
        // Now send a bunch of dummy commands
        arpc->call_method_json("arm.song2.test.Dummy", "SetInteger", json_t("{an_int: 34}"))->then([&count](std::future<json_t>& fut)
                                                                                                  {
                                                                                                      ASSERT_NO_THROW(fut.get());
                                                                                                      count++;
                                                                                                  }, queue);
    });
    
    timeout([&count](){ return count == 1; }, std::chrono::milliseconds(5000));

    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 1);
        
        arpc->call_method_json("arm.song2.test.Dummy", "GetInteger", json_t("{}"))->then([&count](std::future<json_t>& fut)
                                                                                                  {
                                                                                                      ASSERT_NO_THROW(ASSERT_EQ(fut.get(), json_t("{\"anInt\":34}")));
                                                                                                      count++;
                                                                                                  }, queue);
    });
    
    timeout([&count](){ return count == 2; }, std::chrono::milliseconds(5000));
    
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 2);
        
        arpc->call_method_json("arm.song2.test.Dummy", "SetString", json_t("{a_string: \"Hello World\"}"))->then([&count](std::future<json_t>& fut)
                                                                                                  {
                                                                                                      ASSERT_NO_THROW(fut.get());
                                                                                                      count++;
                                                                                                  }, queue);
    });
    
    timeout([&count](){ return count == 3; }, std::chrono::milliseconds(5000));
    
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 3);
        
        arpc->call_method_json("arm.song2.test.Dummy", "GetString", json_t("{}"))->then([&count](std::future<json_t>& fut)
                                                                                        {
                                                                                            ASSERT_NO_THROW(ASSERT_EQ(fut.get(), json_t("{\"aString\":\"Hello World\"}")));
                                                                                            count++;
                                                                                        }, queue);
    });
    
    timeout([&count](){ return count == 4; }, std::chrono::milliseconds(5000));
    
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 4);
        
        // Send a unknown command (unknown service)
        arpc->call_method_json("arm.song2.test.Dummier", "GetInteger", json_t("{}"))->then([&count](std::future<json_t>& fut)
                                                                                       {
                                                                                           ASSERT_THROW(fut.get(), arpc::ArpcException);
                                                                                           count++;
                                                                                       }, queue);
    });
    
    timeout([&count](){ return count == 5; }, std::chrono::milliseconds(5000));
    
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 5);
        
        // Send a unknown command (unknown method)
        arpc->call_method_json("arm.song2.test.Dummy", "GetSomethingElse", json_t("{}"))->then([&count](std::future<json_t>& fut)
                                                                                          {
                                                                                              ASSERT_THROW(fut.get(), arpc::ArpcException);
                                                                                              count++;
                                                                                          }, queue);
    });
    
    timeout([&count](){ return count == 6; }, std::chrono::milliseconds(5000));
    
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 6);
        
        // Send an invalid message
        arpc->call_method_json("arm.song2.test.Dummy", "SetInteger", json_t("{\"aBlah\": 42}"))->then([&count](std::future<json_t>& fut)
                                                                                              {
                                                                                                  ASSERT_THROW(fut.get(), arpc::ArpcClientException);
                                                                                                  count++;
                                                                                              }, queue);
    });
    
    timeout([&count](){ return count == 7; }, std::chrono::milliseconds(5000));
    
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 7);
        
        // Say hello!
        arpc->call_method_json("arm.song2.test.Dummy", "SayHello", json_t("{a_string: \"everyone\"}"))->then([&count](std::future<json_t>& fut)
                                                                                              {
                                                                                                  ASSERT_THROW(fut.get(), arpc::ArpcClientException);
                                                                                                  count++;
                                                                                              }, queue);
    });
    
    timeout([&count](){ return count == 8; }, std::chrono::milliseconds(5000));
    ASSERT_EQ(count, 8);
}
