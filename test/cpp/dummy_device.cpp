//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  dummy_device.cpp
//  song2-tests
//
//  Created by Donatien Garnier on 23/01/2018.

//

#include <gtest/gtest.h>

#include "cpp/threading/Queue.hpp"
#include "cpp/threading/BlockingDispatcher.hpp"
#include "cpp/transport/in_memory/InMemoryScanner.hpp"
#include "dummy_device/DummyDevice.hpp"
#include "cpp/platform/Platform.hpp"
#include "cpp/srtp/SrtpEndpoint.hpp"
#include "cpp/arpc/ArpcEndpoint.hpp"
#include "cpp/arpc/ArpcException.hpp"
#include "cpp/arpc/ProtoStore.hpp"

using namespace arm::song2;

class DummyDeviceTest : public ::testing::Test {
public:
    // Create JS Environment
    virtual void SetUp()
    {
        _dptch = threading::make_blocking_dispatcher();
        
        auto q = threading::make_queue(_dptch);
        q->post([this](){
            _device = std::make_unique<test::DummyDevice>();
            _scanner = std::make_unique<transport::InMemoryScanner>();
        });
        _dptch->start();
        _dptch->process();
    }
    
    void register_dummy_device()
    {
        transport::DeviceIdentifier id {
           
        };
        _scanner->register_device(id, [this](uint32_t channel_num, pipe::Endpoint::ptr&& ep)
                                  {
                                      _device->new_channel(channel_num, std::move(ep));
                                  },
                                  [](){
                                      
                                  });
    }
    
    virtual void TearDown()
    {
    }
    
    void run_on_dispatcher(const std::function<void(const threading::Queue::ptr& queue)>& fn)
    {
        auto q = threading::make_queue(_dptch);
        bool has_run = false;
        q->post([&fn, &has_run, &q](){
            fn(q);
            has_run = true;
        });
        _dptch->process();
        ASSERT_TRUE(has_run);
    }
    
    typename threading::BlockingDispatcher::ptr _dptch;
    
    std::unique_ptr<test::DummyDevice> _device;
    std::unique_ptr<transport::InMemoryScanner> _scanner;
};

TEST_F(DummyDeviceTest, InstantiationTest) {

}

TEST_F(DummyDeviceTest, SimpleScanningTest) {
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        size_t count = 0;
        _scanner->visible_devices()->next([&count](std::future<std::shared_ptr<transport::Device>>& dev){
            if(count == 0)
            {
                ASSERT_NO_THROW(dev.get());
            }
            else
            {
                ASSERT_THROW(dev.get(), threading::ObservableCompletedException);
            }
            count++;
        }, queue);
        register_dummy_device();
        
        ASSERT_EQ(count, 0);

        _scanner->start_scanning();

        ASSERT_EQ(count, 1);
        
        // Destroy scanner here
        _scanner = nullptr;
        
        ASSERT_EQ(count, 2);
    });
}

TEST_F(DummyDeviceTest, MultipleScansTest) {
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        size_t count1 = 0;
        size_t count2 = 0;
        size_t count3 = 0;
        _scanner->visible_devices()->next([&count1](std::future<std::shared_ptr<transport::Device>>& dev){
            if(count1 < 2)
            {
                ASSERT_NO_THROW(dev.get());
            }
            else
            {
                ASSERT_THROW(dev.get(), threading::ObservableCompletedException);
            }
            count1++;
        }, queue);
        register_dummy_device();
        
        ASSERT_EQ(count1, 0);
        
        _scanner->start_scanning();
        
        _scanner->visible_devices()->next([&count2](std::future<std::shared_ptr<transport::Device>>& dev){
            if(count2 < 2)
            {
                ASSERT_NO_THROW(dev.get());
            }
            else
            {
                ASSERT_THROW(dev.get(), threading::ObservableCompletedException);
            }
            count2++;
        }, queue);
        
        ASSERT_EQ(count1, 1);
        ASSERT_EQ(count2, 1);
        
        _scanner->stop_scanning();
        
        _scanner->visible_devices()->next([&count3](std::future<std::shared_ptr<transport::Device>>& dev){
            if(count3 == 0)
            {
                ASSERT_NO_THROW(dev.get());
            }
            else
            {
                ASSERT_THROW(dev.get(), threading::ObservableCompletedException);
            }
            count3++;
        }, queue);
        
        ASSERT_EQ(count1, 1);
        ASSERT_EQ(count2, 1);
        ASSERT_EQ(count3, 0);
        
        _scanner->start_scanning();

        ASSERT_EQ(count1, 2);
        ASSERT_EQ(count2, 2);
        ASSERT_EQ(count3, 1);
        
        // Destroy scanner here
        _scanner = nullptr;
        
        ASSERT_EQ(count1, 3);
        ASSERT_EQ(count2, 3);
        ASSERT_EQ(count3, 2);
        
    });
}

TEST_F(DummyDeviceTest, ArpcDeviceTest) {
    size_t count = 0;
    std::shared_ptr<transport::Device> dev = nullptr;
    std::unique_ptr<srtp::SrtpEndpoint> srtp = nullptr;
    std::unique_ptr<arpc::ArpcEndpoint> arpc = nullptr;

    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        // Recover device
        register_dummy_device();
        
        _scanner->visible_devices()->next([&dev](std::future<std::shared_ptr<transport::Device>>& dev_fut){
            dev = dev_fut.get();
        }, queue);
        _scanner->start_scanning();
        
        ASSERT_NE(dev, nullptr);
        
        // Recover blob file
        auto blob_file = platform::platform_factory()->get_resource_path(platform::PlatformBundle::APP, "blob.pb");
        
        // Instantiate proto store
        auto proto_store = std::make_shared<arpc::ProtoStore>();
        
        // Register blob
        proto_store->register_services_from_files({blob_file});
        
        // Instantiate arpc and srtp
        srtp = std::make_unique<srtp::SrtpEndpoint>(dev->open_endpoint_for_channel(0));
        arpc = std::make_unique<arpc::ArpcEndpoint>(srtp->open());
        arpc->set_proto_store(proto_store);
        
        // Now send a bunch of dummy commands
        arpc->call_method_json("arm.song2.test.Dummy", "SetInteger", json_t("{an_int: 34}"))->then([&count](std::future<json_t>& fut)
                                                                                                  {
                                                                                                      ASSERT_NO_THROW(fut.get());
                                                                                                      count++;
                                                                                                  }, queue);
    });
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 1);
        
        arpc->call_method_json("arm.song2.test.Dummy", "GetInteger", json_t("{}"))->then([&count](std::future<json_t>& fut)
                                                                                                  {
                                                                                                      ASSERT_NO_THROW(ASSERT_EQ(fut.get(), json_t("{\"anInt\":34}")));
                                                                                                      count++;
                                                                                                  }, queue);
    });
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 2);
        
        arpc->call_method_json("arm.song2.test.Dummy", "SetString", json_t("{a_string: \"Hello World\"}"))->then([&count](std::future<json_t>& fut)
                                                                                                  {
                                                                                                      ASSERT_NO_THROW(fut.get());
                                                                                                      count++;
                                                                                                  }, queue);
    });
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 3);
        
        arpc->call_method_json("arm.song2.test.Dummy", "GetString", json_t("{}"))->then([&count](std::future<json_t>& fut)
                                                                                        {
                                                                                            ASSERT_NO_THROW(ASSERT_EQ(fut.get(), json_t("{\"aString\":\"Hello World\"}")));
                                                                                            count++;
                                                                                        }, queue);
    });
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 4);
        
        // Send a unknown command (unknown service)
        arpc->call_method_json("arm.song2.test.Dummier", "GetInteger", json_t("{}"))->then([&count](std::future<json_t>& fut)
                                                                                       {
                                                                                           ASSERT_THROW(fut.get(), arpc::ArpcException);
                                                                                           count++;
                                                                                       }, queue);
    });
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 5);
        
        // Send a unknown command (unknown method)
        arpc->call_method_json("arm.song2.test.Dummy", "GetSomethingElse", json_t("{}"))->then([&count](std::future<json_t>& fut)
                                                                                          {
                                                                                              ASSERT_THROW(fut.get(), arpc::ArpcException);
                                                                                              count++;
                                                                                          }, queue);
    });
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 6);
        
        // Send an invalid message
        arpc->call_method_json("arm.song2.test.Dummy", "SetInteger", json_t("{\"aBlah\": 42}"))->then([&count](std::future<json_t>& fut)
                                                                                              {
                                                                                                  ASSERT_THROW(fut.get(), arpc::ArpcClientException);
                                                                                                  count++;
                                                                                              }, queue);
    });
    run_on_dispatcher([&](const threading::Queue::ptr& queue){
        ASSERT_EQ(count, 7);
        
    });
}
