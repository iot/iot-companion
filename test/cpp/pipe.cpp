//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  pipe.cpp
//  song2-tests
//
//  Created by Donatien Garnier on 15/01/2018.

//

#include <gtest/gtest.h>

#include "cpp/threading/Queue.hpp"
#include "cpp/threading/BlockingDispatcher.hpp"
#include "cpp/pipe/Endpoint.hpp"

using namespace arm::song2;

class PipeTest : public ::testing::Test {
public:
    // Create JS Environment
    virtual void SetUp()
    {
        _dptch = threading::make_blocking_dispatcher();
        _q = threading::make_queue(_dptch);
        _q->post([this](){
            std::tie(_ep1, _ep2) = pipe::make_endpoints();
        });
        
        _dptch->start();
        _dptch->process();
    }
    
    virtual void TearDown()
    {
    }
    
    void run_on_dispatcher(const std::function<void()>& fn)
    {
        auto q = threading::make_queue(_dptch);
        bool has_run = false;
        q->post([&fn, &has_run](){
            fn();
            has_run = true;
        });
        _dptch->process();
        ASSERT_TRUE(has_run);
    }
    
    typename threading::BlockingDispatcher::ptr _dptch;
    typename threading::Queue::ptr _q;
    typename pipe::Endpoint::ptr _ep1;
    typename pipe::Endpoint::ptr _ep2;
};

TEST_F(PipeTest, OneWayTest) {
    run_on_dispatcher([&](){
        size_t count = 0;
        
        _ep2->receive([&count](std::future<bytes_t>& fut){
            switch (count) {
                case 0:
                {
                    ASSERT_EQ(fut.get(), bytes_t({1,2,3}));
                }
                break;
                    
                case 1:
                {
                    ASSERT_EQ(fut.get(), bytes_t({4,5,6}));
                }
                break;
                    
                case 2:
                {
                    ASSERT_THROW(fut.get(), threading::ObservableCompletedException);
                }
                    
                default:
                    break;
            }
            count++;
        });
        
        ASSERT_EQ(count, 0);
        
        _ep1->send({1,2,3});
        
        ASSERT_EQ(count, 1);
        
        _ep1->send({4,5,6});
        
        ASSERT_EQ(count, 2);
        
        ASSERT_NO_THROW(_ep1 = nullptr);

        ASSERT_EQ(count, 3);
    });
}

TEST_F(PipeTest, PingPongTest) {
    run_on_dispatcher([&](){
        size_t count1 = 0;
        size_t count2 = 0;
        
        _ep2->receive([this, &count2](std::future<bytes_t>& fut){
            if(count2 < 1)
            {
                auto v = fut.get();
                v[0]++;
                
                count2++; // Needs to be re-entrant so do this here otherwise stack will explode
                _ep2->send(v);
            }
            else {
                count2++;
            }
        });
        
        _ep1->receive([this, &count1](std::future<bytes_t>& fut){
            auto v = fut.get();
            v[0]++;
            count1++;
            _ep1->send(v);
        });
        
        ASSERT_EQ(count1, 0);
        ASSERT_EQ(count2, 0);

        _ep1->send({1});

        ASSERT_EQ(count1, 1);
        ASSERT_EQ(count2, 2);
        
        ASSERT_NO_THROW(_ep1 = nullptr);
        ASSERT_NO_THROW(_ep2 = nullptr);

        ASSERT_EQ(count1, 1);
        ASSERT_EQ(count2, 3);
    });
}

TEST_F(PipeTest, AsyncPingPongTest) {
    run_on_dispatcher([&](){
        size_t count1 = 0;
        size_t count2 = 0;
        
        _ep2->receive_async([this, &count2](std::future<bytes_t>& fut){
            if(count2 < 1)
            {
                auto v = fut.get();
                v[0]++;
                
                count2++; // Needs to be re-entrant so do this here otherwise stack will explode
                _ep2->send(v);
            }
            else {
                ASSERT_NO_THROW(_ep1 = nullptr);
                count2++;
            }
        }, _q);
        
        _ep1->receive_async([this, &count1](std::future<bytes_t>& fut){
            auto v = fut.get();
            v[0]++;
            count1++;
            _ep1->send(v);
        }, _q);
        
        ASSERT_EQ(count1, 0);
        ASSERT_EQ(count2, 0);
        
        _ep1->send({1});
        
        ASSERT_EQ(count1, 0);
        ASSERT_EQ(count2, 0);
        
        _dptch->start();
        _dptch->process();

        ASSERT_NO_THROW(_ep2 = nullptr);
        
        ASSERT_EQ(count1, 1);
        ASSERT_EQ(count2, 3);
    });
}
