//
//  main.cpp
//  Song2 Tests
//
//  Created by Donatien Garnier on 19/12/2017.
//  Copyright © 2017 ARM Ltd. All rights reserved.
//

#include "test.hpp"

#include "gtest/gtest.h"

#include "cpp/platform/Platform.hpp"
#include "gen/djinni/cpp/daemon.hpp"

using namespace arm::song2::platform;

#if __ANDROID__
#include <android/log.h>
#include <unistd.h>
#include <thread>
#include <memory>
#include <atomic>
// For Android, we need to redirect stdout and stderr to logcat

class StdOutRedirection {
private:
    static const char *LOG_TAG;

    std::atomic<bool> _terminating;
    std::unique_ptr<std::thread> _logging_thread;
    int _pfd[2];

    void logging_loop() {
        ssize_t sz;
        char buf[256];

        while( (sz = read(_pfd[0], buf, sizeof buf - 1)) > 0 ) {
            if(_terminating) {
                return;
            }
            if(buf[sz - 1] == '\n') {
                --sz;
            }

            buf[sz] = 0;  // add null-terminator

            __android_log_write(ANDROID_LOG_DEBUG, LOG_TAG, buf);
        }
    }
public:
    StdOutRedirection() : _terminating(false) {
        // From https://stackoverflow.com/questions/10531050/redirect-stdout-to-logcat-in-android-ndk
        setvbuf(stdout, 0, _IOLBF, 0); // make stdout line-buffered
        setvbuf(stderr, 0, _IONBF, 0); // make stderr unbuffered
        
        // Pipe and redirect stdout and stderr
        pipe(_pfd);
        dup2(_pfd[1], 1);
        dup2(_pfd[1], 2);
        
        _logging_thread = std::make_unique<std::thread>(&StdOutRedirection::logging_loop, this);
    }
    
    ~StdOutRedirection() {
        _terminating = true;
        std::cout << std::endl; // Unblock
        _logging_thread->join();
    }
};

const char* StdOutRedirection::LOG_TAG = "Song2 Tests";

#endif

int32_t arm::song2::platform::Daemon::run(const std::vector<std::string> & args, const std::shared_ptr<PlatformFactory> & platform_factory)
{
#if __ANDROID__
    StdOutRedirection stdr;
#endif
    // Convert back to standard C args for Google Test
    int argc = static_cast<int>(args.size());
    char* argv[args.size()];
    
    for(size_t argi = 0; argi < args.size(); argi++)
    {
        argv[argi] = (char*)(args[argi].c_str());
    }
    
    platform::set_platform_factory(platform_factory);
    
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

