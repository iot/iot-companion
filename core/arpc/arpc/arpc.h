/*
 * Copyright (c) 2017, ARM Limited, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** @file arpc.h
 * A RPC framework
 */

#ifndef ARPC_ARPC_H
#define ARPC_ARPC_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup arpc
 * A RPC framework
 * Messages are sent on the wire using the following protocol:
 *
 *  @code{.unparsed}
 *  Byte 0:
 *  _ _ _ _ _ _ _ _
 *  x                0 : Request / 1 : Response
 *    x              0 : More segments / 1 : Last message (0 only valid if streaming)
 *
 *
 *  Bytes 1..2: Command Id (BE)
 *
 *  For requests:
 *  Bytes n..n+1: Method # -- Discovery::GetMethodShortId is always 0
 *  Bytes n+2..: Payload
 *
 *  For responses:
 *  Byte n: Response code (only if last message)
 *  Bytes n+1..: Payload
 *  @endcode
 * @{
 */

#include "stdbool.h"

#include "acore/ac_buffer.h"
#include "acore/ac_buffer_reader.h"
#include "acore/ac_buffer_builder.h"

#include "arpc_responses.h"

#if ARPC_DEBUG && !defined(NDEBUG)
#include "stdio.h"
#include "stdarg.h"
static inline void arpc_dbg_print(const char* type, const char* module, unsigned int line, const char* fmt, ...) {
#if !defined(NDEBUG)
    printf("ARPC [%s] %s:%u ", type, module, line);
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    printf("\r\n");
#endif
}

#if !defined(ARPC_DBG)
#define ARPC_DBG(...) arpc_dbg_print("DBG", __MODULE__, __LINE__, __VA_ARGS__)
#endif

#if !defined(ARPC_WARN)
#define ARPC_WARN(...) arpc_dbg_print("WARN", __MODULE__, __LINE__, __VA_ARGS__)
#endif

#if !defined(ARPC_ERR)
#define ARPC_ERR(...) arpc_dbg_print("ERR", __MODULE__, __LINE__, __VA_ARGS__)
#endif

#else

#if !defined(ARPC_DBG)
#define ARPC_DBG(...)
#endif

#if !defined(ARPC_WARN)
#define ARPC_WARN(...)
#endif

#if !defined(ARPC_ERR)
#define ARPC_ERR(...)
#endif
#endif

struct __arpc_connection;
struct __arpc_implementation;

typedef struct __arpc_connection arpc_connection_t;
typedef struct __arpc_implementation arpc_implementation_t;

typedef uint16_t arpc_command_t;
typedef uint16_t arpc_method_num_t;

struct __arpc_connection
{
    void* user;
    const arpc_implementation_t* impl;
};

/**
 * Connection opened callback
 *
 * This needs to be implemented by the user.
 * @param connection the new connection which was opened
 * @param user data set in arpc_transport_connection_open()
 */
typedef void (*arpc_impl_connection_opened_t)(const arpc_connection_t* connection, void* user);

/**
 * Connection closed callback
 *
 * This needs to be implemented by the user.
 * @param connection the connection which was closed
 * @param user data set in arpc_transport_connection_open()
 */
typedef void (*arpc_impl_connection_closed_t)(const arpc_connection_t* connection, void* user);

/**
 * Response callback (client)
 *
 * This needs to be implemented by the user.
 * Called when a response is received from the server.
 * @param connection the relevant connection
 * @param command_id the command id sent in the request
 * @param last_response set to true if no further responses are expected from this command id
 * @param response_code indicates success or an error (if an error is raised, last_response will always be true)
 * @param payload the actual payload from this response, or NULL if an error is raised
 * @param user data set in arpc_transport_connection_open()
 */
typedef void (*arpc_impl_responded_t)(const arpc_connection_t* connection, arpc_command_t command_id, bool last_response, arpc_response_code_t response_code, const ac_buffer_t* payload, void* user);

/**
 * Request callback (server)
 *
 * This needs to be implemented by the user.
 * Called when a request is received from the client.
 * @param connection the relevant connection
 * @param command_id the command id sent by the client and that needs to be sent back with any response
 * @param method_num the number of the method to call in the server's method table
 * @param payload the actual payload from this request
 * @param user data set in arpc_transport_connection_open()
 */
typedef void (*arpc_impl_requested_t)(const arpc_connection_t* connection, arpc_command_t command_id, arpc_method_num_t method_num, const ac_buffer_t* payload, void* user);

/**
 * Data to send
 *
 * This needs to be implemented by the user.
 * Called when a message must be sent on underlying transport layer.
 * @param connection the relevant connection
 * @param buffer the data to send
 * @param user data set in arpc_transport_connection_open()
 * @return true on success, false if the transport is unavailable (out-of-memory, etc.)
 */
typedef bool (*arpc_impl_transport_send_t)(const arpc_connection_t* connection, const ac_buffer_t* buffer, void* user);

struct __arpc_implementation
{
    arpc_impl_connection_opened_t connection_opened;
    arpc_impl_connection_closed_t connection_closed;
    arpc_impl_responded_t responded;
    arpc_impl_requested_t requested;
    arpc_impl_transport_send_t transport_send;
};

/**
 * Open a connection
 *
 * @param connection the connection to open
 * @param implementation the callbacks table to use
 * @param user the data to associate with the connection
 */
void arpc_transport_connection_open(arpc_connection_t* connection, const arpc_implementation_t* implementation, void* user);

/**
 * Signal a connection's termination
 *
 * @param connection the connection to set to closed
 */
void arpc_transport_connection_closed(const arpc_connection_t* connection);

/**
 * Signal data received from transport layer
 *
 * @param connection the connection to set to closed
 * @param buffer the data which was received from the transport
 */
void arpc_transport_received(const arpc_connection_t* connection, const ac_buffer_t* buffer);

/**
 * Send a request to server
 *
 * @param connection the relevant connection
 * @param command_id an id unique within the connection's lifetime
 * @param method_num the number of the method to call in the server's method table
 * @param payload the actual payload to send with this request
 * @return true if the request could be sent, false otherwise
 */
bool arpc_request(const arpc_connection_t* connection, arpc_command_t command_id, arpc_method_num_t method_num, const ac_buffer_t* payload);

/**
 * Send a response to client
 *
 * @param connection the relevant connection
 * @param command_id the command id sent by the client for this command
 * @param last_response set to true if this is the last response
 * @param response_code indicates success or an error (if an error is raised, last_response will always be set to true)
 * @param payload the actual payload to send with this request
 * @return true if the response could be sent, false otherwise
 */
bool arpc_respond(const arpc_connection_t* connection, arpc_command_t command_id, bool last_response, arpc_response_code_t response_code, const ac_buffer_t* payload);


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif //ARPC_ARPC_H
