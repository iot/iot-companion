/*
 * Copyright (c) 2017, ARM Limited, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ARPC_ARPC_RESPONSES_H
#define ARPC_ARPC_RESPONSES_H

enum arpc_response_code //numbering guaranteed by C99 standard
{
    // Remote responses
    arpc_response_ok = 0,
    arpc_response_err_unimplemented = 1,
    arpc_response_err_unavailable,
    arpc_response_err_invalid_method,
    arpc_response_err_timeout,
    arpc_response_err_permission_denied,
    arpc_response_err_parsing_error,
    arpc_response_err_invalid_argument, // Parsing ok but parameters are invalid
    arpc_response_err_memory_error,
    arpc_response_err_internal,
    arpc_response_err_unknown,
    arpc_response_code_max = arpc_response_err_unknown


};

typedef enum arpc_response_code arpc_response_code_t;

#endif //ARPC_ARPC_RESPONSES_H
