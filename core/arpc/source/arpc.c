/*
 * Copyright (c) 2017, ARM Limited, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define __MODULE__ "arpc.c"
#define __DEBUG__ 4

#include "stdint.h"

#include "arpc/arpc.h"

bool arpc_request(const arpc_connection_t* connection, arpc_command_t command_id, arpc_method_num_t method_num, const ac_buffer_t* payload)
{
    uint8_t header_bytes[5];
    ac_buffer_builder_t header_bldr;
    ac_buffer_builder_init(&header_bldr, header_bytes, sizeof(header_bytes));

    // Build frame
    ac_buffer_builder_write_nu8(&header_bldr, (0 << 7) /* request */);
    ac_buffer_builder_write_nu16(&header_bldr, command_id);
    ac_buffer_builder_write_nu16(&header_bldr, method_num);

    // Join ac_buffers
    ac_buffer_append( ac_buffer_builder_buffer(&header_bldr), (ac_buffer_t*)payload );

    return connection->impl->transport_send(connection, ac_buffer_builder_buffer(&header_bldr), connection->user);
}

bool arpc_respond(const arpc_connection_t* connection, arpc_command_t command_id,  
    bool last_response, arpc_response_code_t response_code, const ac_buffer_t* payload)
{
    uint8_t header_bytes[4];
    ac_buffer_builder_t header_bldr;
    ac_buffer_builder_init(&header_bldr, header_bytes, sizeof(header_bytes));

    // Build frame
    ac_buffer_builder_write_nu8(&header_bldr, (1 << 7) /* response */ | ( (last_response?1:0) << 6 ));
    ac_buffer_builder_write_nu16(&header_bldr, command_id);
    if(last_response)
    {
        ac_buffer_builder_write_nu8(&header_bldr, response_code);
    }

    // Join ac_buffers
    ac_buffer_append( ac_buffer_builder_buffer(&header_bldr), (ac_buffer_t*)payload );

    return connection->impl->transport_send(connection, ac_buffer_builder_buffer(&header_bldr), connection->user);
}

void arpc_transport_connection_open(arpc_connection_t* connection, const arpc_implementation_t* implementation, void* user)
{
    connection->user = user;
    connection->impl = implementation;
    connection->impl->connection_opened(connection, connection->user);
}

void arpc_transport_connection_closed(const arpc_connection_t* connection)
{
    connection->impl->connection_closed(connection, connection->user);
}

void arpc_transport_received(const arpc_connection_t* connection, const ac_buffer_t* ac_buffer)
{
    ac_buffer_t msg;
    ac_buffer_dup(&msg, ac_buffer);
    
    // Parse message
    if( ac_buffer_reader_readable(&msg) < 3 )
    {
        ARPC_WARN("Message too short");
        return;
    }

    // Read command id
    uint8_t b = ac_buffer_read_nu8(&msg);
    arpc_command_t command_id = ac_buffer_read_nu16(&msg);
    bool request_nresponse = ( b & 0x80 )?false:true; //MSb is response/nrequest bit

    if( request_nresponse )
    {
        if( ac_buffer_reader_readable(&msg) < 2 )
        {
            ARPC_ERR("Message too short");
            return;
        }

        // This imposes max 65536 methods per device which should be ok -- that's what Android thought but hey we're a bit smaller
        arpc_method_num_t method_num = ac_buffer_read_nu16(&msg);

        ARPC_DBG("Command request %u for method %u", command_id, method_num);
        connection->impl->requested(connection, command_id, method_num, &msg, connection->user);
    }
    else
    {
        bool last_response = ( b & 0x40 )?true:false;
        
        ARPC_DBG("Command response %u", command_id);
        arpc_response_code_t resp_code = arpc_response_ok;
        if( last_response )
        {
            if( ac_buffer_reader_readable(&msg) < 1 )
            {
                ARPC_ERR("Message too short");
                return;
            }

            uint8_t resp_code_raw = ac_buffer_read_nu8(&msg);
            if( resp_code_raw > arpc_response_code_max)
            {
                resp_code_raw = arpc_response_err_unknown;
            }

            resp_code  = (arpc_response_code_t)resp_code_raw;
            ARPC_DBG("Response code %u", resp_code);
        }
        connection->impl->responded(connection, command_id, last_response, resp_code, &msg, connection->user);
    }
}
