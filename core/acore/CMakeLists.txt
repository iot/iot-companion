add_library(acore source/ac_buffer.c source/ac_buffer_builder.c source/ac_buffer_reader.c source/ac_stream.c
            acore/ac_buffer.h acore/ac_buffer_builder.h acore/ac_buffer_reader.h acore/ac_debug.h acore/ac_macros.h
            acore/ac_stream.h)
target_include_directories(acore PUBLIC .)
