/*
 * Copyright (c) 2017, ARM Limited, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define __MODULE__ "srtp.c"
#define __DEBUG__ 4

#include "srtp/srtp.h"

#define SRTP_HEADER_SZ 1

enum srtp_pkt_type;
typedef enum srtp_pkt_type srtp_pkt_type_t;
enum srtp_pkt_type
{
    srtp_pkt_msg = 0,
    srtp_pkt_rst = 1,
    srtp_pkt_ack = 2,
    srtp_pkt_nak = 3
};

static void send_pkt(srtp_channel_t* channel, srtp_pkt_type_t pkt_type, uint8_t pkt_number, ac_buffer_t* pkt)
{
    uint8_t header_bytes[SRTP_HEADER_SZ];
    ac_buffer_builder_t header;
    ac_buffer_builder_init(&header, header_bytes, SRTP_HEADER_SZ);
    ac_buffer_builder_write_nu8(&header, (pkt_type << 6) | pkt_number);
    SRTP_DBG("send_pkt");

    ac_buffer_t pkt_dup;

    if( pkt != NULL )
    {
        // Work on copy
        ac_buffer_dup(&pkt_dup, pkt);

        ac_buffer_set_next(ac_buffer_builder_buffer(&header), &pkt_dup);
    }
    channel->impl->channel_transport_send(channel, ac_buffer_builder_buffer(&header), channel->user);
}

static void closing(srtp_channel_t* channel)
{
    channel->state = srtp_channel_state_closing;
    send_pkt(channel, srtp_pkt_rst, (channel->next_sent) & 0x3F, NULL);
    // Increment packet count
    channel->next_sent++;
    channel->next_sent &= 0x3F;
}

static void closed(srtp_channel_t* channel)
{
    if( channel->state == srtp_channel_state_closed )
    {
        return; //No-op
    }
    channel->state = srtp_channel_state_closed;
    for( size_t i = 0; i < channel->pending_count; i++ )
    {
        channel->impl->tx_pending_pop(channel, channel->user);
        channel->impl->channel_sent(channel, false, channel->user);
    }
    channel->pending_count = 0;
    channel->impl->channel_closed(channel, channel->user);
}

void srtp_channel_open(srtp_channel_t* channel, const srtp_implementation_t* implementation)
{
    channel->impl = implementation;
    channel->next_rcv = 0;
    channel->next_sent = 0;
    channel->pending_count = 0;
    channel->state = srtp_channel_state_open;
    channel->user = NULL;
}

bool srtp_channel_send(srtp_channel_t* channel, ac_buffer_t* msg)
{
    // Are we closed or closing?
    if( channel->state != srtp_channel_state_open )
    {
        return false;
    }

    // Save packet number
    uint8_t pkt_number = channel->next_sent;
    
    // Increment packet count
    channel->next_sent++;
    channel->next_sent &= 0x3F;
    
    // Save packet
    channel->impl->tx_pending_push(channel, msg, channel->user);
    channel->pending_count++;

    // Make sure we call this last as it could generate some callbacks on synchronous implementations
    send_pkt(channel, srtp_pkt_msg, pkt_number, msg);
    return true;
}

void srtp_channel_transport_received(srtp_channel_t* channel, ac_buffer_t* pkt)
{
    //Consume header byte
    uint8_t b = ac_buffer_read_nu8(pkt);
    srtp_pkt_type_t t = (srtp_pkt_type_t)(b >> 6);
    uint8_t pkt_number = b & 0x3F;

    if( channel->state == srtp_channel_state_closed )
    {
        // Always respond with reset
        send_pkt(channel, srtp_pkt_rst, (channel->next_sent - 1) & 0x3F, NULL);
        return;
    }
    else if( channel->state == srtp_channel_state_closing )
    {
        // If ack received, move to closed state
        if( (t == srtp_pkt_ack) && (pkt_number == ((channel->next_sent - 1) & 0x3F)) )
        {
            closed(channel);
            return;
        }
        else
        {
            // Retransmit reset
            send_pkt(channel, srtp_pkt_rst, (channel->next_sent - 1) & 0x3F, NULL);
            return;
        }
    }

    switch(t)
    {
        case srtp_pkt_msg:
        {
            SRTP_DBG("Received msg");
            //Is this the next expected packet number?
            if( pkt_number == channel->next_rcv )
            {
                // Yes, pass it to upper layer and ack
                send_pkt(channel, srtp_pkt_ack, channel->next_rcv & 0x3F, NULL);

                // Increment packet count
                channel->next_rcv++;
                channel->next_rcv &= 0x3F;

                if( ac_buffer_reader_readable(pkt) > 0 ) // Otherwise just a keepalive packet
                {
                    channel->impl->channel_received(channel, pkt, channel->user);
                }
            }
            else
            {
                //Send nak
                send_pkt(channel, srtp_pkt_nak, channel->next_rcv & 0x3F, NULL);
            }
        }
        break;

        case srtp_pkt_ack:
        {
            SRTP_DBG("Received ACK");

            // ACK up to this pkt number
            uint8_t pkt_num_diff = ((channel->next_sent - 1) - pkt_number /* last packet received by peer */) & 0x3F;
            SRTP_DBG("pkt_num_diff = %u, channel->pending_count = %u", pkt_num_diff, channel->pending_count);

            if( pkt_num_diff > 32 ) // Not good, this is a really old packet or ahead of packet count
            {
                // Send reset and move to closing state
                closing(channel);
                return;
            }

            if( pkt_num_diff < channel->pending_count )
            {
                size_t acked_count = channel->pending_count - pkt_num_diff;
                SRTP_DBG("acked_count = %u", acked_count);

                // Make sure we mutate all state before calling callback, as callback can call us again!
                // ac_buffer_t* acked_pkts[acked_count];
                // memmove(acked_pkts, &channel->pending[pkt_num_diff], sizeof(ac_buffer_t*) * acked_count);

                // memmove(&channel->pending[0], &channel->pending[pkt_num_diff], sizeof(ac_buffer_t*) * (channel->pending_count - pkt_num_diff));
                for( size_t i = 0; i < acked_count; i++ )
                {
                    channel->impl->tx_pending_pop(channel, channel->user);
                }
                channel->pending_count -= acked_count;

                // Now notify using callback - in the right order, oldest first
                for( size_t i = 0; i < acked_count; i++ )
                {
                    channel->impl->channel_sent(channel, /*acked_pkts[acked_count - 1 - i],*/ true, channel->user);
                }
            }
        }
        break;

        case srtp_pkt_nak:
        {
            //This means that the other end is missing one of more messages from this pkt number so try and retransmit them all
            uint8_t pkt_num_diff = ((channel->next_sent - 1) - pkt_number /* first packet NOT received by peer */) & 0x3F;

            size_t missing_count = pkt_num_diff + 1;

            if( missing_count > channel->pending_count ) // Not good, we don't have those in store!
            {
                // Send reset and move to closing state
                closing(channel);
                return;
            }

            //Re-send all missing messages, oldest first
            for( size_t i = 0; i < missing_count; i++ )
            {
                ac_buffer_t missing_pkt;
                channel->impl->tx_pending_get(channel, i, &missing_pkt, channel->user);
                send_pkt(channel, srtp_pkt_msg, channel->next_sent - missing_count + i, &missing_pkt);
            }

            //This also means that all previous message are acked!
            //TODO
        }
        break;

        case srtp_pkt_rst:
        default:
            // Still ack it
            send_pkt(channel, srtp_pkt_ack, pkt_number, NULL);
            closed(channel);
            break;
    }
}

void srtp_channel_close(srtp_channel_t* channel)
{
    if( channel->state != srtp_channel_state_open )
    {
        return; //No-op
    }

    closing(channel);
}

void srtp_channel_set_user(srtp_channel_t* channel, void* user)
{
    channel->user = user;
}

srtp_channel_state_t srtp_channel_get_state(srtp_channel_t* channel)
{
    return channel->state;
}

void srtp_channel_send_keepalive(srtp_channel_t* channel)
{
    ac_buffer_t dummy;
    ac_buffer_init(&dummy, NULL, 0);

    srtp_channel_send(channel, &dummy);
}
