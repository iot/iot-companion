/*
 * Copyright (c) 2017, ARM Limited, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** @file srtp.h
 * Simple Reliable Transport Protocol.
 */

#ifndef SRTP_SRTP_H
#define SRTP_SRTP_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @addtogroup srtp
 * Simple Reliable Transport Protocol.
 * @{
 */

#include "acore/ac_buffer.h"
#include "acore/ac_buffer_reader.h"
#include "acore/ac_buffer_builder.h"

#if SRTP_DEBUG && !defined(NDEBUG)

#include "stdio.h"
#include "stdarg.h"
static inline void srtp_dbg_print(const char* type, const char* module, unsigned int line, const char* fmt, ...) {
#if !defined(NDEBUG)
    printf("SRTP [%s] %s:%u ", type, module, line);
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    printf("\r\n");
#endif
}

#if !defined(SRTP_DBG)
#define SRTP_DBG(...) srtp_dbg_print("DBG", __MODULE__, __LINE__, __VA_ARGS__)
#endif

#if !defined(SRTP_WARN)
#define SRTP_WARN(...) srtp_dbg_print("WARN", __MODULE__, __LINE__, __VA_ARGS__)
#endif

#if !defined(SRTP_ERR)
#define SRTP_ERR(...) srtp_dbg_print("ERR", __MODULE__, __LINE__, __VA_ARGS__)
#endif

#else

#if !defined(SRTP_DBG)
#define SRTP_DBG(...)
#endif

#if !defined(SRTP_WARN)
#define SRTP_WARN(...)
#endif

#if !defined(SRTP_ERR)
#define SRTP_ERR(...)
#endif
#endif

struct srtp_channel;
typedef struct srtp_channel srtp_channel_t;
struct srtp_implementation;
typedef struct srtp_implementation srtp_implementation_t;

/**
 * SRTP Channel status
 */
enum srtp_channel_state
{
    srtp_channel_state_open, ///< Open
    srtp_channel_state_closing, ///< Closing
    srtp_channel_state_closed ///< Closed
};

typedef enum srtp_channel_state srtp_channel_state_t;

struct srtp_channel
{
    void* user;
    const srtp_implementation_t* impl;

    uint8_t next_rcv;
    uint8_t next_sent;

    size_t pending_count;

    srtp_channel_state_t state;
};

/**
 * Open a SRTP channel
 *
 * @param channel a srtp_channel_t structure that will be initialized.
 * @param implementation the callbacks table to use
 */
void srtp_channel_open(srtp_channel_t* channel, const srtp_implementation_t* implementation);

/**
 * Send a message over a channel
 *
 * @param channel the channel over which the message needs to be sent
 * @param message a ac_buffer referencing the message to send
 * @return whether the message could be processed
 */
bool srtp_channel_send(srtp_channel_t* channel, ac_buffer_t* message);

/**
 * Handle a packet received from underlying transport layer
 *
 * @param channel the matching channel for which the packets needs to be processed
 * @param pkt a ac_buffer refencing the packet which was received
 */
void srtp_channel_transport_received(srtp_channel_t* channel, ac_buffer_t* pkt);

/**
 * Puts channel in closing state
 *
 * The library will try to close gracefully. If closing is acknowledged by the other party, srtp_impl_channel_closed() be called.
 * @param channel the channel to close
 */
void srtp_channel_close(srtp_channel_t* channel);

/**
 * Set user data for a channel
 *
 * This should be used by implementation to associate contextual data with a channel
 * @param channel the channel to associate data with
 * @param user the data to associate with the channel
 */
void srtp_channel_set_user(srtp_channel_t* channel, void* user);

/**
 * Get channel's state
 *
 * @param channel the channel check the state of
 * @return A srtp_channel_state_t value (open, closing, closed)
 */
srtp_channel_state_t srtp_channel_get_state(srtp_channel_t* channel);

/**
 * Send keepalive message
 *
 * This should be sent regularly when some message haven't been ack'ed yet to help detect non-reception of previous messages on the other end
 * @param channel the channel to send the keepalive message on
 */
void srtp_channel_send_keepalive(srtp_channel_t* channel);

/**
 * Message sent callback
 *
 * This needs to be implemented by the user.
 * @param channel the relevant channel
 * @param success whether the message could be sent or had to be dropped (if so, the channel will go to closing state)
 * @param user data set using srtp_channel_set_user()
 */
typedef void (*srtp_impl_channel_sent_t)(srtp_channel_t* channel, bool success, void* user);

/**
 * Message received callback
 *
 * This needs to be implemented by the user.
 * @param channel the relevant channel
 * @param msg a ac_buffer referencing the message
 * @param user data set using srtp_channel_set_user()
 */
typedef void (*srtp_impl_channel_received_t)(srtp_channel_t* channel, ac_buffer_t* msg, void* user);

/**
 * Packet for transport layer callback
 *
 * This needs to be implemented by the user.
 * @param channel the relevant channel
 * @param pkt a ac_buffer referencing the packet that needs to be sent by the underlying layer
 * @param user data set using srtp_channel_set_user()
 */
typedef void (*srtp_impl_channel_transport_send_t)(srtp_channel_t* channel, ac_buffer_t* pkt, void* user);

/**
 * Channel closed callback
 * This will either be called if we receive a closing request from the peer or after the peer has acknowledged the closing request sent from this end.
 * This needs to be implemented by the user.
 * @param channel the relevant channel
 * @param user data set using srtp_channel_set_user()
 */
typedef void (*srtp_impl_channel_closed_t)(srtp_channel_t* channel, void* user);

/**
 * Push message at end of queue callback
 * This needs to be implemented by the user.
 * Push a message on pending message queue.
 * Before a message has been successfully transmitted it needs to be pushed onto a queue
 * of pending messages. Basically, the underlying message's reference count needs to be incremented
 * and it needs to be pushed at the back to the queue.
 * @param channel the relevant channel
 * @param msg the message that needs to be pushed onto the queue
 * @param user data set using srtp_channel_set_user()
 */
typedef void (*srtp_impl_tx_pending_push_t)(srtp_channel_t* channel, ac_buffer_t* msg, void* user);

/**
 * Retrieve message from queue callback
 *
 * This needs to be implemented by the user.
 * Retrieve a message from pending message queue.
 * @param channel the relevant channel
 * @param pos the position of the message in the queue
 * @param msg ac_buffer_t instance that needs to be populated with the relevant message
 * @param user data set using srtp_channel_set_user()
 */
typedef void (*srtp_impl_tx_pending_get_t)(srtp_channel_t* channel, size_t pos, ac_buffer_t* msg, void* user);

/**
 * Remove message from queue callback
 *
 * This needs to be implemented by the user.
 * Remove the first message from pending message queue.
 * @param channel the relevant channel
 * @param user data set using srtp_channel_set_user()
 */
typedef void (*srtp_impl_tx_pending_pop_t)(srtp_channel_t* channel, void* user);

/**
* SRTP Implementation
*/
struct srtp_implementation
{
    srtp_impl_channel_sent_t channel_sent; ///< Message sent
    srtp_impl_channel_received_t channel_received; ///< Message received
    srtp_impl_channel_transport_send_t channel_transport_send; ///< Send to transport
    srtp_impl_channel_closed_t channel_closed; ///< Channel closed
    srtp_impl_tx_pending_push_t tx_pending_push; ///< Push message on queue
    srtp_impl_tx_pending_get_t tx_pending_get; ///< Retrieve message from queue
    srtp_impl_tx_pending_pop_t tx_pending_pop; ///< Pop message from queue
};

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif //SRTP_SRTP_H
