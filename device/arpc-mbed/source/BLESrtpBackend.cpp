/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "arpc-mbed/BLESrtpBackend.h"

namespace srtp {
namespace mbed {

BLESrtpBackend::BLESrtpBackend() :
    _service(NULL),
    _channel_allocated(false),
    _connection_handle(),
    _channel_id(),
    _channel(),
    _msg_queue()
{
}

BLESrtpBackend::~BLESrtpBackend() { }

void BLESrtpBackend::register_service(Song2Service& service)
{
    _service = &service;
    _service->register_event_handler(*this);
}

SrtpChannel* BLESrtpBackend::bind(
    ::ble::connection_handle_t connection_handle, uint8_t channel_id
) {
    if (_channel_allocated) {
        return NULL;
    }

    if (!_service) {
        return NULL;
    }

    _connection_handle = connection_handle;
    _channel_id = channel_id;
    _channel_allocated = true;

    open_channel(&_channel, &_msg_queue);
    return &_channel;
}

bool BLESrtpBackend::send(SrtpChannel* channel, ac_buffer_t* pkt)
{
    if (is_channel_valid(channel) == false) {
        return false;
    }

    if(!_service) {
        return false;
    }

    return _service->send_frame(_connection_handle, _channel_id, pkt);
}

void BLESrtpBackend::release(SrtpChannel* channel)
{
    if (is_channel_valid(channel) == false) {
        return;
    }

    while(_msg_queue.pop());

    _channel_allocated = false;
}

void BLESrtpBackend::on_packet_received(
    connection_handle_t connection, uint8_t channel, ac_buffer_t* packet
) {
    if ((!_channel_allocated) ||
        (connection != _connection_handle) ||
        (_channel_id != channel))
    {
        return;
    }

    when_packet_received(&_channel, packet);
}

void BLESrtpBackend::on_reception_error(
    connection_handle_t connection, frame_reception_error_t error
) {
    if ((!_channel_allocated) || (connection != _connection_handle)) {
        return;
    }

    switch(error.value()) {
        case frame_reception_error_t::SERVER_RESET:
        case frame_reception_error_t::PEER_DISCONNECTED:
            when_unexpected_close(&_channel);
            break;

        case frame_reception_error_t::INVALID_FRAGMENT_SIZE:
        case frame_reception_error_t::INCOMPLETE_FRAME:
        case frame_reception_error_t::OUT_OF_MEMORY:
            // sends a keep alive to get a retransmission
            _channel.send_keepalive();
            break;

        default:
            break;
    }
}

bool BLESrtpBackend::is_channel_valid(SrtpChannel* channel)
{
    if (!_channel_allocated) {
        return false;
    }

    if (channel != &_channel) {
        return false;
    }

    if (!_service) {
        return false;
    }

    return true;
}

} // namespace mbed
} // namespace arpc
