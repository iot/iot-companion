/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "arpc-mbed/ArpcOverBleFacade.h"

namespace arpc {
namespace mbed {

ArpcOverBleFacade::ArpcOverBleFacade() :
    _ble(NULL),
    _chainable_gap_event_handler(NULL),
    _chainable_gatt_server_event_handler(NULL),
    _song2_service(),
    _ble_srtp_backend(),
    _connection_handle(0xFFFF),
    _channel(NULL),
    _arpc_srtp_transport(),
    _arpc_connection(NULL)
{
}

ArpcOverBleFacade::~ArpcOverBleFacade()
{
    cleanup();
}

void ArpcOverBleFacade::bind_connection(arpc::mbed::ArpcConnection& connection)
{
    if (_channel) {
        return;
    }

    _arpc_connection = &connection;
}

void ArpcOverBleFacade::register_facade(BLE& ble_instance, ChainableGapEventHandler &chainable_gap_event_handler,
        ChainableGattServerEventHandler &chainable_gatt_server_event_handler)
{
    if (_ble) {
        return;
    }

    _ble = &ble_instance;
    _chainable_gap_event_handler = &chainable_gap_event_handler;
    _chainable_gatt_server_event_handler = &chainable_gatt_server_event_handler;
    
    _song2_service.register_service(*_ble, *_chainable_gap_event_handler, *_chainable_gatt_server_event_handler);
    _ble_srtp_backend.register_service(_song2_service);

    // register callbacks
    // The ChainableGapEventHandler allows us to dispatch events from GAP to more than a single event handler */
    _chainable_gap_event_handler->addEventHandler(this);
    _chainable_gatt_server_event_handler->addEventHandler(this);

    gap().onShutdown().add(as_cb(&Self::on_gap_shutdown));
}

void ArpcOverBleFacade::onConnectionComplete(const ble::ConnectionCompleteEvent &event)
{
    if (_channel) {
        return;
    }

    _connection_handle = event.getConnectionHandle();
    _channel = _ble_srtp_backend.bind(_connection_handle);
    _arpc_srtp_transport.open(_channel);
    _arpc_connection->open(&_arpc_srtp_transport);
}

void ArpcOverBleFacade::onDisconnectionComplete(const ble::DisconnectionCompleteEvent &event)
{
    if ((event.getConnectionHandle() != _connection_handle) || (_channel == NULL)) {
        return;
    }

    release_srtp_channel();
}

void ArpcOverBleFacade::on_gap_shutdown(const Gap*)
{
    cleanup();
}

void ArpcOverBleFacade::onShutdown(const GattServer &server)
{
    cleanup();
}

void ArpcOverBleFacade::release_srtp_channel()
{
    if (_channel) {
        _channel->close();
        _channel = NULL;
        _connection_handle = 0xFFFF;
    }
}

void ArpcOverBleFacade::cleanup()
{
    release_srtp_channel();

    if (_ble) {
        gap().onShutdown(as_cb(&Self::on_gap_shutdown));
        _chainable_gap_event_handler->removeEventHandler(this);
        _chainable_gatt_server_event_handler->removeEventHandler(this);

        _ble = NULL;
    }
}

Gap& ArpcOverBleFacade::gap()
{
    return _ble->gap();
}

GattServer& ArpcOverBleFacade::gatt_server()
{
    return _ble->gattServer();
}

} // namespace mbed
} // namespace arpc
