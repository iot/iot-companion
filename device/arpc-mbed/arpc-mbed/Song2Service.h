/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MBED_SONG2_BLE_SERVICE_
#define MBED_SONG2_BLE_SERVICE_

#include "ble/BLE.h"
#include "ble/Gap.h"
#include "ble/gap/ChainableGapEventHandler.h"
#include "ble/gatt/ChainableGattServerEventHandler.h"
#include "ble/gap/Events.h"
#include "ble/GattServer.h"
#include "platform/NonCopyable.h"
#include "ble/common/SafeEnum.h"
#include "acore/ac_buffer.h"
#include "acore/ac_buffer_reader.h"

namespace srtp {
namespace mbed {

/**
 * Type of errors that can be encountered during frame reception.
 */
struct frame_reception_error_t : ::ble::SafeEnum<frame_reception_error_t, uint8_t> {
    enum type {
        /**
         * The local GATT server has been reset.
         */
        SERVER_RESET,

        /**
         * The peer has been disconnected
         */
        PEER_DISCONNECTED,

        /**
         * The size of one of the fragment received was invalid.
         */
        INVALID_FRAGMENT_SIZE,

        /**
         * One fragment had an unexpected fragment number.
         */
        INCOMPLETE_FRAME,

        /**
         * The frame has been released because the system run out of memory.
         */
        OUT_OF_MEMORY,

        /**
         * The frame has been dropped because the frame allocated has
         * insufficient capacity.
         */
        INSUFFICIENT_CAPACITY
    };

    frame_reception_error_t(type v) :
        ::ble::SafeEnum<frame_reception_error_t, uint8_t>(v) { }
};

/**
 * The song2 service sends and receives srtp packets used for provisioning.
 *
 * @par Usage
 *
 * Once BLE is initialized, application code should register the service into
 * the local GATT server with the help of the function @p register_service and
 * register an event handler which will forward packets received or reception
 * faillure from the connected clients. Application code can also sends SRTP
 * packets with the help of the function @p send_frame.
 *
 * @par Service Layout
 *
 * The service UUID is EC8C0100-EC9B-30AF-1D41-AB00BEB494DF and is composed of
 * three characteristics:
 *   - TX: This characteristic accept write commands from peers. The service
 *   handles the reassembly process and forward complete packets to application
 *   code. Its UUID is EC8C0101-EC9B-30AF-1D41-AB00BEB494DF.
 *   - RX: This characteristic is used to sends srtp packets to a given peer.
 *   The service handle the segmentation process. Its UUID is
 *   EC8C0102-EC9B-30AF-1D41-AB00BEB494DF.
 *   - Service Revision: This characteristic contains the revision of the service.
 *   Its UUID is EC8C0103-EC9B-30AF-1D41-AB00BEB494DF.
 *
 * @par Protocol internal
 *
 * SRTP packets sent or received by the service are split into fragments to
 * accommodate SRTP packets size with GATT MTU size.
 *
 * The first byte of every fragment is split into two fields:
 *   - Last fragment flag: Located at the Most significant bit, a value of 1
 *   indicates that this fragment is the last fragment of a packet.
 *   - Fragment number: The 7 Least Significant bits contains the fragment number.
 *
 * First fragments also contain the channel number in their second byte.
 */
class Song2Service : ::mbed::NonCopyable<Song2Service>, private ble::Gap::EventHandler, private ble::GattServer::EventHandler {

public:
    typedef ::ble::connection_handle_t connection_handle_t;

    /**
     * Construct a Song2 service.
     *
     * The service does nothing by itself, application code needs to register it
     * into a GATT server and register an event handler to capture the packets
     * received.
     */
    Song2Service();

    /**
     * Song2 destructor.
     *
     * Incomming incomplete packets are destroyed.
     */
    ~Song2Service();

    /**
     * Register the service into the GATT server.
     *
     * @param[in] ble_instance Ble instance that will host the GATT service.
     * @param[in] chainable_gap_event_handler A chainable GAP event handler registered against the ble instance.
     * @param[in] chainable_gatt_server_event_handler A chainable GATT Server event handler registered against the ble instance.
     *
     * @important This function shall only be called once while the GattServer
     * is enabled. A reset of the GattServer requires a new registration.
     */
    ble_error_t register_service(BLE& ble_instance, ChainableGapEventHandler &chainable_gap_event_handler,
        ChainableGattServerEventHandler &chainable_gatt_server_event_handler);

    /**
     * Interface that handles events from a song2 service.
     *
     * Events comming out the song2 service are triggered by the data received on
     * the RX characteristic:
     *   - on_packet_received: A complete packet has been received; it is forwarded
     *   to the event handler.
     *   - on_reception_error: Signal an error during the reception of a packets.
     */
    struct EventHandler {
        virtual ~EventHandler() { }

        /**
         * Signal the reception of a packet
         *
         * @param[in] connection The connection that received the packet.
         * @param[in] channel Channel on which the packet has been received.
         * @param[in] packet The packet received.
         *
         * @important the packet parameter is not const due to limitations ac_buffer
         * APIs. Do not modify it.
         */
        virtual void on_packet_received(
            connection_handle_t connection, uint8_t channel, ac_buffer_t* packet
        ) = 0;

        /**
         * Signal that a reception error happened on a given connection.
         *
         * @param[in] connection The connection which has encountered the error.
         */
        virtual void on_reception_error(
            connection_handle_t connection, frame_reception_error_t error
        ) = 0;
    };

    /**
     * Register application code event handler.
     *
     * @param[in] The event handler used by the service to notify packets
     * received and reception error to the application code.
     */
    void register_event_handler(EventHandler& event_handler);

    /**
     * Set the total capacity in bytes allowed for packet reception.
     *
     * When the first fragment of a packet is received, the service allocate a
     * buffer that will hold the entirety of the packet. The default capacity
     * is equal to DEFAULT_RX_CAPACITY but it can be overriden with the use of
     * this function.
     *
     * If allocation of the frame fail then an out of memory error is reported to
     * the event handler. This error
     *
     * @param[in] capacity The maximum number of bytes that a received frame can
     * occupy.
     *
     * @note ongoing transactions are not affected by the capacity change.
     */
    void set_rx_frame_capacity(size_t capacity);


    /**
     * Send a song2 packet.
     *
     * This function handles channel selection and segmentation of the packet
     * into fragments. All fragments are passed sequentially to the stack which
     * will send these fragments to the peer in notification packets.
     *
     * If the stack refuses one of the fragment, the process stop and the function
     * returns false.
     *
     * @param connection The handle to the peer that will receive the packet
     * @param channel The song2 channel used to send this packet.
     * @param buf The packet to send
     *
     * @return true if the stack accepted all the fragments that should be sent
     * or false otherwise.
     */
    bool send_frame(
        connection_handle_t connection,
        uint8_t channel,
        ac_buffer_t* buf
    );

    /**
     * Default capacity for received frames.
     */
    static const size_t DEFAULT_RX_CAPACITY = 800;

private:
    struct FrameBuilder;

    ///////////////////////////////////////////////////////////////////////////
    // BLE state management

    void cleanup_handlers();

    ///////////////////////////////////////////////////////////////////////////
    // ble::Gap::EventHandler implementation

    virtual void onDisconnectionComplete(const ::ble::DisconnectionCompleteEvent &event) override;

    ///////////////////////////////////////////////////////////////////////////
    // ble::GattServer::EventHandler implementation
    virtual void onDataWritten(const GattWriteCallbackParams &params) override;
    virtual void onShutdown(const GattServer &server) override;

    ///////////////////////////////////////////////////////////////////////////
    // TRANSMISSION

    bool send_fragment(
        connection_handle_t connection,
        ac_buffer_t* buf_reader,
        size_t payload_size,
        uint8_t fragment_id,
        bool last_fragment
    );

    ///////////////////////////////////////////////////////////////////////////
    // RECEPTION

    void when_first_fragment_received(
        connection_handle_t connection,
        const uint8_t* fragment_data,
        size_t fragment_len,
        bool is_last_fragment
    );

    void when_non_first_fragment_received(
        connection_handle_t connection,
        const uint8_t* fragment_data,
        size_t fragment_len,
        bool is_last_fragment,
        uint8_t fragment_number
    );

    void signal_error(
        connection_handle_t connection, frame_reception_error_t error
    );

    ///////////////////////////////////////////////////////////////////////////
    // FRAME BUILDERS MANAGEMENT

    void add_frame_builder(FrameBuilder* cb);

    FrameBuilder* get_frame_builder(connection_handle_t connection) const;

    void remove_frame_builder(FrameBuilder* frame_builder);

    ///////////////////////////////////////////////////////////////////////////
    // UTIL

    uint16_t mtu_size(connection_handle_t connection) const;

    ///////////////////////////////////////////////////////////////////////////
    // FIELDS

    static const size_t BASE_MTU_SIZE = 20;
    static const size_t MAX_MTU_SIZE = 244;
    static const uint16_t FRAME_HEADER_SIZE = 1;
    static const uint8_t LAST_FRAGMENT_FLAG_MASK = 0x80;
    static const uint8_t FRAGMENT_NUMBER_MASK = 0x7F;

    static const char SERVICE_UUID[];
    static const char TX_UUID[];
    static const char RX_UUID[];
    static const char VERSION_UUID[];

    // external dependencies
    BLE* _ble_instance;
    ChainableGapEventHandler* _chainable_gap_event_handler;
    ChainableGattServerEventHandler* _chainable_gatt_server_event_handler;
    EventHandler* _event_handler;

    // Characteristics of the Song2 service
    uint8_t _tx_buffer[MAX_MTU_SIZE];
    GattCharacteristic _tx_characteristic;
    uint8_t _rx_buffer[MAX_MTU_SIZE];
    GattCharacteristic _rx_characteristic;
    uint32_t _service_revision;
    GattCharacteristic _service_revision_characteristic;

    GattCharacteristic* _service_characteristics[3];
    GattService _service;

    // frame builders
    FrameBuilder* _frame_builders;

    size_t _rx_frame_capacity;
};

} // namespace mbed
} // namespace arpc

#endif /* MBED_SONG2_BLE_SERVICE_ */
