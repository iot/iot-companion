//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "srtp/srtp.h"
#include "srtp-device/SrtpChannel.h"
#include "srtp-device/SrtpBackend.h"

namespace srtp {
namespace mbed {

void SrtpBackend::open_channel(
    SrtpChannel* channel, SrtpChannelMsgQueue* msg_queue
) {
    channel->open(this, msg_queue);
}

void SrtpBackend::when_packet_received(SrtpChannel* channel, ac_buffer_t* pkt)
{
    srtp_channel_transport_received(channel, pkt);
}

void SrtpBackend::when_unexpected_close(SrtpChannel* channel)
{
    // FIXME: is it gracefull enough ???
    channel->notify_closed();
    release(channel);
}

} // namespace mbed
} // namespace srtp
