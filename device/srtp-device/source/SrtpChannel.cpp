/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "srtp/srtp.h"
#include "platform/NonCopyable.h"
#include "srtp-device/SrtpChannel.h"
#include "srtp-device/SrtpBackend.h"
#include "srtp-device/SrtpChannelMessageQueue.h"
#include "platform/mbed_assert.h"

namespace srtp {
namespace mbed {

SrtpChannel::SrtpChannel() :
    srtp_channel_t(),
    _backend(NULL),
    _event_handler(NULL),
    _msg_queue(NULL) {
}

SrtpChannel::~SrtpChannel() { }

void SrtpChannel::set_event_handler(EventHandler* event_handler)
{
    _event_handler = event_handler;
}

bool SrtpChannel::send(ac_buffer_t* message)
{
    return srtp_channel_send(this, message);
}

void SrtpChannel::close()
{
    srtp_channel_close(this);
}

srtp_channel_state_t SrtpChannel::get_state()
{
    return srtp_channel_get_state(this);
}

void SrtpChannel::send_keepalive()
{
    srtp_channel_send_keepalive(this);
}

const srtp_implementation_t* SrtpChannel::get_thunk_implementation() {
    static const srtp_implementation_t impl = {
        message_sent_thunk,
        message_received_thunk,
        channel_transport_send_thunk,
        channel_closed_thunk,
        tx_pending_push_thunk,
        tx_pending_get_thunk,
        tx_pending_pop_thunk
    };
    return &impl;
}

SrtpChannel* SrtpChannel::as_this(void* user)
{
    return static_cast<SrtpChannel*>(user);
}

void SrtpChannel::message_sent_thunk(
    srtp_channel_t* channel, bool success, void* user
) {
    MBED_ASSERT(as_this(user)->_event_handler);
    as_this(user)->_event_handler->on_message_sent(as_this(user), success);
}

void SrtpChannel::message_received_thunk(
    srtp_channel_t* channel, ac_buffer_t* msg, void* user
) {
    MBED_ASSERT(as_this(user)->_event_handler);
    as_this(user)->_event_handler->on_message_received(as_this(user), msg);
}

void SrtpChannel::channel_transport_send_thunk(
    srtp_channel_t* channel, ac_buffer_t* pkt, void* user
) {
    MBED_ASSERT(as_this(user)->_backend);
    as_this(user)->_backend->send(as_this(user), pkt);
}

void SrtpChannel::channel_closed_thunk(srtp_channel_t* channel, void* user)
{
    MBED_ASSERT(as_this(user)->_backend);
    // Notify the application event handler then release the channel
    as_this(user)->notify_closed();
    as_this(user)->_backend->release(as_this(user));
}

void SrtpChannel::tx_pending_push_thunk(
    srtp_channel_t* channel, ac_buffer_t* msg, void* user
) {
    MBED_ASSERT(as_this(user)->_msg_queue);
    as_this(user)->_msg_queue->push(msg);
}

void SrtpChannel::tx_pending_get_thunk(
    srtp_channel_t* channel, size_t index, ac_buffer_t* msg, void* user
) {
    MBED_ASSERT(as_this(user)->_msg_queue);
    as_this(user)->_msg_queue->get(index, msg);
}

void SrtpChannel::tx_pending_pop_thunk(srtp_channel_t* channel, void* user)
{
    MBED_ASSERT(as_this(user)->_msg_queue);
    as_this(user)->_msg_queue->pop();
}

void SrtpChannel::open(SrtpBackend* backend, SrtpChannelMsgQueue* msg_queue)
{
    _backend = backend;
    _msg_queue = msg_queue;
    srtp_channel_open(this, get_thunk_implementation());
    srtp_channel_set_user(this, this);
}

void SrtpChannel::notify_closed()
{
    MBED_ASSERT(as_this(user)->_event_handler);
    _event_handler->on_channel_closed(this);
}

} // namespace mbed
} // namespace srtp
