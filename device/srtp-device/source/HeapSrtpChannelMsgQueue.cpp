/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <memory>

#include "srtp-device/HeapSrtpChannelMsgQueue.h"

namespace srtp {
namespace mbed {

struct HeapSrtpChannelMsgQueue::packed_buffer_t : ac_buffer_t {
    packed_buffer_t* next;
    uint8_t data[1];

    static void release(packed_buffer_t* to_release)
    {
        to_release->~packed_buffer_t();
        free(to_release);
    }

    static packed_buffer_t* create(const ac_buffer_t* ref)
    {
        ac_buffer_t to_copy;
        ac_buffer_dup(&to_copy, ref);
        size_t size = ac_buffer_reader_readable(&to_copy);
        void* res = malloc(sizeof(packed_buffer_t) + size);
        return new(res) packed_buffer_t(&to_copy, size);
    }

private:
    packed_buffer_t(ac_buffer_t* ref, size_t size) :
        ac_buffer_t(),
        next(NULL),
        data()
    {
        ac_buffer_read_n_bytes(ref, data, size);
        ac_buffer_init(this, data, size);
    }

    packed_buffer_t(packed_buffer_t&);
    packed_buffer_t& operator=(const packed_buffer_t&);
};

HeapSrtpChannelMsgQueue::HeapSrtpChannelMsgQueue() :
    _buffers(NULL) { }

HeapSrtpChannelMsgQueue::~HeapSrtpChannelMsgQueue()
{
    while(_buffers) {
        pop();
    }
}

bool HeapSrtpChannelMsgQueue::push(ac_buffer_t* msg)
{
    packed_buffer_t* buf = packed_buffer_t::create(msg);
    if (!buf) {
        return false;
    }

    if (!_buffers) {
        _buffers = buf;
        return true;
    }

    packed_buffer_t* it = _buffers;
    while (it->next) {
        it = it->next;
    }

    it->next = buf;
    return true;
}

bool HeapSrtpChannelMsgQueue::get(size_t index, ac_buffer_t* msg) const
{
    const packed_buffer_t* it = _buffers;

    while(index && it) {
        it = it->next;
        --index;
    }

    if ((index != 0) || (it == NULL)) {
        return false;
    }

    ac_buffer_dup(msg, it);
    return true;
}

bool HeapSrtpChannelMsgQueue::pop()
{
    if (!_buffers) {
        return false;
    }

    packed_buffer_t* head = _buffers;
    _buffers = head->next;
    packed_buffer_t::release(head);
    return true;
}

} // namespace mbed
} // namespace srtp

