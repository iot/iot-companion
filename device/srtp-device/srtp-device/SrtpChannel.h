/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MBED_SONG2_SRTP_CHANNEL_
#define MBED_SONG2_SRTP_CHANNEL_

#include "srtp/srtp.h"
#include "platform/NonCopyable.h"

namespace srtp {
namespace mbed {

// forward declarations
class SrtpBackend;
class SrtpChannelMsgQueue;

/**
 * SRTP communication channel.
 *
 * An SRTP channel can send and receive messages packed into srtp frames.
 */
class SrtpChannel : public srtp_channel_t, ::mbed::NonCopyable<SrtpChannel> {
public:

    /**
     * Interface that handles events from a channel.
     *
     * @note: A single instance of an event handler can be used to handle
     * multiples event channels and reduce duplication of events handlers if
     * relevant.
     */
    struct EventHandler {
        virtual ~EventHandler() { }

        /**
         * Indicates that a message has been successfully sent on @p channel.
         */
        virtual void on_message_sent(SrtpChannel* channel, bool success) = 0;

        /**
         * Indicates that a message as been received by @p channel.
         */
        virtual void on_message_received(SrtpChannel* channel, ac_buffer_t* msg) = 0;

        /**
         * Indicates that @p channel has been close.
         *
         * @important: The channel should not be used after it has been closed.
         */
        virtual void on_channel_closed(SrtpChannel* channel) = 0;
    };

    /**
     * Construct an empty channel.
     *
     * @note The channel must be registered into a backend or created by a
     * backend to be operational.
     */
    SrtpChannel();

    /**
     * Destructor.
     */
    virtual ~SrtpChannel();

    /**
     * Install the channel event handler.
     *
     * @param[in] event_handler The event handler that will be used to notify
     * application code of events.
     *
     * @warning an event_handler must be set before any data transfer occurrence.
     */
    void set_event_handler(EventHandler* event_handler);

    /**
     * Send a message over the channel.
     *
     * @param[in] message The message to send through the channel
     * @return whether the message could be processed
     */
    bool send(ac_buffer_t* message);

    /**
     * Close the channel.
     *
     * The function on_channel_closed of the event handler registered is
     * called once the channel has been closed.
     */
    void close();

    /**
     * Return the channel's state
     */
    srtp_channel_state_t get_state();

    /**
     * Send a keepalive message to the peer at the other end of the channel.
     */
    void send_keepalive();

private:
    friend class SrtpBackend;

    static const srtp_implementation_t* get_thunk_implementation();

    static SrtpChannel* as_this(void* user);

    static void message_sent_thunk(
        srtp_channel_t* channel, bool success, void* user
    );

    static void message_received_thunk(
        srtp_channel_t* channel, ac_buffer_t* msg, void* user
    );

    static void channel_transport_send_thunk(
        srtp_channel_t* channel, ac_buffer_t* pkt, void* user
    );

    static void channel_closed_thunk(srtp_channel_t* channel, void* user);

    static void tx_pending_push_thunk(
        srtp_channel_t* channel, ac_buffer_t* msg, void* user
    );

    static void tx_pending_get_thunk(
        srtp_channel_t* channel, size_t index, ac_buffer_t* msg, void* user
    );

    static void tx_pending_pop_thunk(srtp_channel_t* channel, void* user);

    // note: used by SrtpBackend to finish channel registration/creation.
    void open(SrtpBackend* backend, SrtpChannelMsgQueue* msg_queue);

    // note: used by SrtpBackend to notify the application event handler that
    // the channel has been closed unexpectedly and will be released
    void notify_closed();

    SrtpBackend* _backend;
    EventHandler* _event_handler;
    SrtpChannelMsgQueue* _msg_queue;
};

} // namespace mbed
} // namespace srtp

#endif /* MBED_SONG2_SRTP_CHANNEL_ */
