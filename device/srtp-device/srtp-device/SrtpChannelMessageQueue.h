/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MBED_SONG2_SRTP_CHANNEL_MESSAGE_QUEUE_
#define MBED_SONG2_SRTP_CHANNEL_MESSAGE_QUEUE_

#include <stddef.h>
#include "platform/NonCopyable.h"
#include "acore/ac_buffer.h"

namespace srtp {
namespace mbed {

/**
 * Interface of a FIFO queue that stores pending tx messages of an SrtpChannel.
 */
struct SrtpChannelMsgQueue : ::mbed::NonCopyable<SrtpChannelMsgQueue> {
    /**
     * Destructor - should be overriden by implementation.
     */
    virtual ~SrtpChannelMsgQueue() { }

    /**
     * Push a message at the end of the queue.
     *
     * @param[in] msg Message to push at the end of the queue.
     *
     * @return true if the message was succesfully pushed at the end of the queue
     * and false otherwise.
     */
    virtual bool push(ac_buffer_t* msg) = 0;

    /**
     * Retrieve the message stored at @p index.
     *
     * @param[in] index The index of the message to retrieve.
     * @param[out] msg The message retrieved.
     *
     * @return true if the message was succesfully retrieved and false otherwise.
     */
    virtual bool get(size_t index, ac_buffer_t* msg) const = 0;

    /**
     * Remove the message at the top of the queue.
     *
     * @return true if the message was succesfully removed and false otherwise.
     */
    virtual bool pop() = 0;
};

} // namespace mbed
} // namespace srtp

#endif /* MBED_SONG2_SRTP_CHANNEL_MESSAGE_QUEUE_ */
