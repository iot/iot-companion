# Song2 RPC framework implementation for mbed OS
This is the 'embedded server' implementation of the Song2 RPC framework for mbed OS.

It includes different components:
* ACORE: Safe buffer handling
* SRTP: Reliable transport layer
* ARPC: RPC layer
* BLE: BLE transport implementation

To use this project, include the library within your Mbed OS project, install `arpc-codegen` which lives in [the main Song2 repo](https://github.com/ARMmbed/mbed-song2/tree/master/tools/generator).

Create a protobuf descriptor of your "Service" as described within the [Google Protocol Buffer documentation](https://developers.google.com/protocol-buffers/docs/proto#services).

Under the hood the serialization/deserialization is handled by [nanopb](https://jpa.kapsi.fi/nanopb/) and you can use an `.options` file as described in [the documentation](https://jpa.kapsi.fi/nanopb/docs/reference.html#proto-file-options).

You can then run the code generator within your source directory:
```bash
$ arpc-codegen mydescriptor.proto
```

## Example

Please check-out the [WiFi commissioning example](https://github.com/ARMmbed/mbed-scout-wifi-fw).

## Known issues

Within the BLE service, the write characteristic is not properly handled by the OS if we don't set the `BLE_GATT_CHAR_PROPERTIES_WRITE` property although only `BLE_GATT_CHAR_PROPERTIES_WRITE_WITHOUT_RESPONSE` should be required.

## Third-party software

NanoPb is included within this repo and is licensed under the [zlib license](third_party/pb/LICENSE.txt).


