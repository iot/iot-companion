//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "discovery.pb.h"
#include "ArpcDiscoveryService.h"

#include "pb/pb.h"

namespace arpc {
namespace mbed {
namespace generated {

ArpcDiscoveryService::ArpcDiscoveryService() : arpc::mbed::ArpcService()
{

}

ArpcDiscoveryService::~ArpcDiscoveryService()
{

}


size_t ArpcDiscoveryService::_s_methods_count = 3;

arpc::mbed::ArpcMethodDescriptor ArpcDiscoveryService::_s_methods_descriptors[] = {
    {"GetMethodNumber", arpc_GetMethodNumberRequest_fields, arpc_GetMethodNumberResponse_fields,
        arpc::mbed::ArpcMethodCall<arpc_GetMethodNumberRequest, arpc_GetMethodNumberResponse>::make_shared,
        (const arpc::mbed::ArpcMethodRequestCallback_t) &ArpcDiscoveryService::get_method_number
        },
    {"GetServiceNumber", arpc_GetServiceNumberRequest_fields, arpc_GetServiceNumberResponse_fields,
        arpc::mbed::ArpcMethodCall<arpc_GetServiceNumberRequest, arpc_GetServiceNumberResponse>::make_shared,
        (const arpc::mbed::ArpcMethodRequestCallback_t) &ArpcDiscoveryService::get_service_number
        },
    {"GetServicesCount", arpc_GetServicesCountRequest_fields, arpc_GetServicesCountResponse_fields,
        arpc::mbed::ArpcMethodCall<arpc_GetServicesCountRequest, arpc_GetServicesCountResponse>::make_shared,
        (const arpc::mbed::ArpcMethodRequestCallback_t) &ArpcDiscoveryService::get_services_count
        },
};

const char* ArpcDiscoveryService::_s_name = "arpc.Discovery";

void ArpcDiscoveryService::get_method_descriptors(const arpc::mbed::ArpcMethodDescriptor** methods, size_t* count) const {
    *methods = _s_methods_descriptors;
    *count = _s_methods_count;
}

const char* ArpcDiscoveryService::get_name() const {
    return _s_name;
}


}

namespace details {
template<> const arpc_GetServiceNumberRequest& Prototype<arpc_GetServiceNumberRequest>::value() {
    static arpc_GetServiceNumberRequest prototype = arpc_GetServiceNumberRequest_init_default;
    return prototype;
}
template<> const arpc_GetServiceNumberResponse& Prototype<arpc_GetServiceNumberResponse>::value() {
    static arpc_GetServiceNumberResponse prototype = arpc_GetServiceNumberResponse_init_default;
    return prototype;
}
template<> const arpc_GetMethodNumberRequest& Prototype<arpc_GetMethodNumberRequest>::value() {
    static arpc_GetMethodNumberRequest prototype = arpc_GetMethodNumberRequest_init_default;
    return prototype;
}
template<> const arpc_GetMethodNumberResponse& Prototype<arpc_GetMethodNumberResponse>::value() {
    static arpc_GetMethodNumberResponse prototype = arpc_GetMethodNumberResponse_init_default;
    return prototype;
}
template<> const arpc_GetServicesCountRequest& Prototype<arpc_GetServicesCountRequest>::value() {
    static arpc_GetServicesCountRequest prototype = arpc_GetServicesCountRequest_init_default;
    return prototype;
}
template<> const arpc_GetServicesCountResponse& Prototype<arpc_GetServicesCountResponse>::value() {
    static arpc_GetServicesCountResponse prototype = arpc_GetServicesCountResponse_init_default;
    return prototype;
}
}

}
}