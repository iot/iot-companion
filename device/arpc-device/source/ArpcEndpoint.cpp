/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "arpc-device/ArpcEndpoint.h"
#include "arpc-device/ArpcService.h"
#include "arpc/arpc.h"

namespace arpc {
namespace mbed {

using ::mbed::Span;

/*
Implementation note:
All methods are viewed as a single array, i.e. if a service n has m(n) methods
then the method number for method x of service y will be:
m(0) + m(1) + ... + m(y-1) + x

Only two methods of the discovery service have a known, fixed number:
- A method to retrieve a method number given a service number and the method's name (0)
- A method to retrieve a service number given the service's name (1)

The service number is only used to avoid having to transmit the service's name in every method resolution.
*/

ArpcEndpoint::ArpcEndpoint() :
	_arpc_connection(),
	_services_list(NULL)
{
    _arpc_connection.set_event_handler(this);
    
    // We implement the discovery service
    register_service(*static_cast<generated::ArpcDiscoveryService*>(this));
}

ArpcEndpoint::~ArpcEndpoint() {
    _arpc_connection.set_event_handler(NULL);
}

arpc::mbed::ArpcConnection& ArpcEndpoint::connection() {
    return _arpc_connection;
}

void ArpcEndpoint::register_service(ArpcService &service) {
    service.set_endpoint(this);

    if(_services_list == NULL) {
        _services_list = &service;
        return;
    }

    ArpcService* node = _services_list;
    while(node->get_next_service() != NULL) {
        node = node->get_next_service();
    }

    node->set_next_service(&service);
}

void ArpcEndpoint::on_connection_opened(ArpcConnection *connection) {
    // Propagate to all registered services
    ArpcService* service = _services_list;
    while(service != NULL) {
        service->on_connection_opened();
        service = service->get_next_service();
    }
}

void ArpcEndpoint::on_connection_closed(ArpcConnection *connection) {
    // Propagate to all registered services
    ArpcService* service = _services_list;
    while(service != NULL) {
        service->on_connection_closed();
        service = service->get_next_service();
    }
}

void ArpcEndpoint::on_response_received(
    ArpcConnection *connection,
    arpc_command_t command_id,
    bool last_response,
    arpc_response_code_t response_code,
    const ac_buffer_t* payload
) {
    // Out of scope, client is not implemented for now
}

void ArpcEndpoint::on_request_received(
    ArpcConnection *connection,
    arpc_command_t command_id,
    arpc_method_num_t method_num,
    const ac_buffer_t* payload
) {
    // Go through all services
    ArpcService* service = _services_list;
    while(service != NULL) {
        size_t service_methods_count = service->methods_count();
        if( method_num < service_methods_count ) {
            // This call is meant for this service, let service handle it
            service->handle_request(command_id, method_num, payload);
            return;
        }

        method_num -= service_methods_count;
        service = service->get_next_service();
    }

    // Invalid method, we've gone past all registered services
    connection->send_response(
		command_id,
		true,
		arpc_response_err_invalid_method,
		NULL
	);
}
    
void ArpcEndpoint::get_method_number(const ::mbed::SharedPtr< ArpcMethodCall<arpc_GetMethodNumberRequest, arpc_GetMethodNumberResponse> >& call) {
    ArpcService* service = _services_list;
    size_t service_num = 0;
    arpc_method_num_t method_offset = 0;
    while((service != NULL) && (service_num < call->request().service_number)) {
        method_offset += service->methods_count();
        service = service->get_next_service();
        service_num++;
    }
    
    if( service == NULL )
    {
        // Reject, the service can't be found
        call->fail(arpc_response_err_invalid_argument);
        return;
    }
    
    // Find number
    arpc_method_num_t method_num = 0;
    if(!service->method_number_for_name(call->request().method_name, &method_num)) {
        call->response().found = false;
        call->success();
        return;
    }
    
    // Add offset
    method_num += method_offset;
    
    call->response().found = true;
    call->response().method_number = method_num;
    call->success();
}
    
void ArpcEndpoint::get_service_number(const ::mbed::SharedPtr< ArpcMethodCall<arpc_GetServiceNumberRequest, arpc_GetServiceNumberResponse> >& call) {
    ArpcService* service = _services_list;
    size_t service_num = 0;
    while(service != NULL) {
        if(!strcmp(service->name(), call->request().service_name)) {
            break;
        }
        
        service = service->get_next_service();
        service_num++;
    }
    
    if( service == NULL )
    {
        call->response().found = false;
        call->success();
        return;
    }
    
    call->response().found = true;
    call->response().service_number = static_cast<uint32_t>(service_num);
    call->success();
}
    
void ArpcEndpoint::get_services_count(const ::mbed::SharedPtr< ArpcMethodCall<arpc_GetServicesCountRequest, arpc_GetServicesCountResponse> >& call) {
    ArpcService* service = _services_list;
    call->response().count = 0;

    while(service != NULL) {
        service = service->get_next_service();
        call->response().count++;
    }
    
    call->success();
}

} // namespace mbed
} // namespace arpc
