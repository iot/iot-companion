/* mbed Microcontroller Library
 * Copyright (c) 2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "arpc-device/SrtpChannelToArpcTransportAdapter.h"

namespace arpc {
namespace mbed {

SrtpChannelToArpcTransportAdapter::SrtpChannelToArpcTransportAdapter() :
    _channel(NULL) { }

SrtpChannelToArpcTransportAdapter::~SrtpChannelToArpcTransportAdapter() {
    // reset the channel;
    // note: notification to application handler done at base class level
    if (_channel) {
        _channel->set_event_handler(NULL);
    }
}

bool SrtpChannelToArpcTransportAdapter::open(SrtpChannel* channel)
{
    if (_channel || (channel == NULL)) {
        return false;
    }

    _channel = channel;
    _channel->set_event_handler(this);
    return true;
}

////////////////////////////////////////////////////////////////////////////
// SrtpChannel::EventHandler

void SrtpChannelToArpcTransportAdapter::on_message_sent(SrtpChannel* channel, bool success)
{
    // nothing to report.
    // FIXME: what if there is an error ???
}

void SrtpChannelToArpcTransportAdapter::on_message_received(SrtpChannel* channel, ac_buffer_t* msg)
{
    when_message_received(msg);
}

void SrtpChannelToArpcTransportAdapter::on_channel_closed(SrtpChannel* channel)
{
    // notify that the transport was closed
    when_transport_closed();

    // reset the channel
    _channel->set_event_handler(NULL);
    _channel = NULL;
}

////////////////////////////////////////////////////////////////////////////
// ArpcTransport

bool SrtpChannelToArpcTransportAdapter::send(const ac_buffer_t* buffer)
{
    if (!_channel) {
        return false;
    }

    // FIXME: remove once srtp API is const correct
    ac_buffer_t msg;
    ac_buffer_dup(&msg, buffer);
    return _channel->send(&msg);
}

} // namespace mbed
} // namespace arpc
