//
//  FileSystem.cpp
//  song2
//
//  Created by Donatien Garnier on 16/02/2019.
//  Copyright © 2019 ARM Ltd. All rights reserved.
//

#include <random>
#include <fstream>
#include <cstdio>
#include <stdexcept>

#include <sys/stat.h>
#include <sys/types.h>

// For recursive deletion
#include <ftw.h>
#include <unistd.h>

#include "FileSystem.hpp"


// Needed for cleanup
static int unlink_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
    return remove(fpath);
}
        
void arm::song2::util::filesystem::remove_all(const std::string& path)
{
    int ret = nftw(path.c_str(), unlink_cb, 64 /* max number of directories open simultaneously */, FTW_DEPTH | FTW_PHYS);
    if(ret) {
        throw std::runtime_error("Could not remove path");
    }
}

void arm::song2::util::filesystem::make_directory(const std::string& path, bool throw_if_exists)
{
    int ret = mkdir(path.c_str(), 0770);
    if(ret && (throw_if_exists || (errno != EEXIST))) {
        throw std::runtime_error("Could not create directory");
    }
}

void arm::song2::util::filesystem::move_file(const std::string& from, const std::string& to)
{
    int ret = rename(from.c_str(), to.c_str());
    if(ret) {
        throw std::runtime_error("Could not move file");
    }
}

void arm::song2::util::filesystem::remove_file(const std::string& path)
{
    if(remove(path.c_str()) != 0) {
        throw std::runtime_error("Could not remove file");
    }
}

// Potentially unsafe is touch is false and path accessed concurrently
std::string arm::song2::util::filesystem::get_random_filename(const std::string& path, bool touch)
{
    std::string alphanumeric("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
    static thread_local std::random_device random_device;
    static thread_local std::mt19937 generator(random_device());
    thread_local static std::uniform_int_distribution<std::string::size_type> pick(0, alphanumeric.size() - 1);
    static const size_t result_sz = 32;
    
    bool collision;
    std::string random_name;
    do {
        random_name.clear();
        random_name.reserve(result_sz);
        
        for(size_t i = 0; i < result_sz; i++)
        {
            random_name += alphanumeric[pick(generator)];
        }
        
        // Check if it exists (collision) - very unlikely
        std::ifstream test_ifstream(path + "/" + random_name);
        collision = test_ifstream.good();
    } while(collision);
    
    // 'Touch' file
    if(touch)
    {
        std::ofstream touch_ofstream(path + "/" + random_name, std::ios::binary);
        if(!touch_ofstream.good()) {
            throw std::runtime_error("Could create random file");
        }
    }
    
    return random_name;
}
