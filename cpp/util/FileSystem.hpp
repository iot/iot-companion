//
//  FileSystem.hpp
//  song2
//
//  Created by Donatien Garnier on 16/02/2019.
//  Copyright © 2019 ARM Ltd. All rights reserved.
//

#pragma once

#include <string>

namespace arm {
    namespace song2 {
        namespace util {
            namespace filesystem {
                void remove_all(const std::string& path);
                
                void make_directory(const std::string& path, bool throw_if_exists);
                
                void move_file(const std::string& from, const std::string& to);
                
                void remove_file(const std::string& path);
                
                // Potentially unsafe is touch is false and path accessed concurrently
                std::string get_random_filename(const std::string& path, bool touch);
            }
        }
    }
}
