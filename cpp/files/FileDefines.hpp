//
//  FileDefines.hpp
//  song2
//
//  Created by Donatien Garnier on 18/02/2019.
//  Copyright © 2019 ARM Ltd. All rights reserved.
//

#pragma once

#include <string>
#include <vector>

namespace arm {
    namespace song2 {
        namespace files {
            struct FileDescriptor {
                std::string name;
                std::string path;
                bool read_only;
            };
        }
    }
}
