//
//  FileManager.hpp
//  song2
//
//  Created by Donatien Garnier on 15/02/2019.
//  Copyright © 2019 ARM Ltd. All rights reserved.
//

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "cpp/threading/Queue.hpp"
#include "cpp/threading/Promise.hpp"
#include "cpp/threading/ObservableVector.hpp"

#include "FileDefines.hpp"

namespace arm {
    namespace song2 {
        namespace files {
            class FileManager
            {
            public:
                FileManager(const std::string& new_files_path, const std::vector<FileDescriptor>& file_descriptors);
                ~FileManager();
                
                threading::Future<FileDescriptor>::ptr create_file(const std::string& name);
                threading::Subscriber<FileDescriptor>::ptr new_files();
                const std::vector<FileDescriptor>& files() const;
                
                threading::Queue::ptr queue() const;
            private:
                threading::Queue::ptr _queue;
                
                std::string _new_files_path;
                std::vector<FileDescriptor> _files;
                threading::ObservableVector<FileDescriptor> _files_obs;
            };
        }
    }
}


