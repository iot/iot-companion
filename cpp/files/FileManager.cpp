//
//  FileManager.cpp
//  song2
//
//  Created by Donatien Garnier on 15/02/2019.
//  Copyright © 2019 ARM Ltd. All rights reserved.
//

#include "FileManager.hpp"

#include "cpp/util/FileSystem.hpp"

using namespace arm::song2::files;
using namespace arm::song2;

FileManager::FileManager(const std::string& new_files_path, const std::vector<FileDescriptor>& file_descriptors) : _queue(threading::make_queue()), _new_files_path(new_files_path),
_files(file_descriptors),
_files_obs([this](threading::Observable<FileDescriptor>& obs){
    for(auto& file : _files)
    {
        obs.next(file);
    }
})
{
    util::filesystem::make_directory(_new_files_path, false);
}

FileManager::~FileManager()
{
    _queue->disable();
}

threading::Subscriber<FileDescriptor>::ptr FileManager::new_files()
{
    return _files_obs.get_subscriber();
}

const std::vector<FileDescriptor>& FileManager::files() const
{
    return _files;
}

threading::Future<FileDescriptor>::ptr FileManager::create_file(const std::string& name)
{
    return threading::make_resolved_future()
    ->then_async([this, name](std::future<void>& f)
                 {
                     FileDescriptor fd;
                     fd.name = name;
                     fd.read_only = false;
                     fd.path = _new_files_path + "/" + util::filesystem::get_random_filename(_new_files_path, true);
                     _files.push_back(fd);
                     _files_obs.next(fd);
                     return fd;
                 }, _queue);
}

threading::Queue::ptr FileManager::queue() const
{
    return _queue;
}
