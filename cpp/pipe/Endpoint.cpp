//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Endpoint.cpp
//  song2
//
//  Created by Donatien Garnier on 05/01/2018.

//

#include "Endpoint.hpp"

using namespace arm::song2::pipe;

Endpoint::Endpoint() : _closed(false)
{
    
}

void Endpoint::send(bytes_t&& bytes)
{
    _sender.next(std::move(bytes));
}

void Endpoint::send(const bytes_t& bytes)
{
    _sender.next(bytes);
}

void Endpoint::close()
{
    if(_closed) { return; }
    _closed = true;
    if(!_sender.has_completed())
    {
        _sender.complete();
    }
}

std::pair<Endpoint::ptr, Endpoint::ptr> arm::song2::pipe::make_endpoints()
{
    auto pair = std::make_pair(std::make_shared<Endpoint>(), std::make_shared<Endpoint>());
    
    pair.first->_receiver = pair.second->_sender.get_subscriber();
    pair.second->_receiver = pair.first->_sender.get_subscriber();
    
    return pair;
}
