//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Channel.hpp
//  song2
//
//  Created by Donatien Garnier on 05/01/2018.

//

#pragma once

#include <memory>
#include <utility>
#include <string>

#include "cpp/threading/Queue.hpp"
#include "cpp/threading/Observable.hpp"
#include "cpp/types/Types.hpp"

// Basically like a socket

namespace arm {
    namespace song2 {
        namespace pipe {
            /** \addtogroup Pipe
             * A pipe connecting two endpoints
             *  @{
             */
            /** Pipe Endpoint
             *
             * This class provides an easy way to build pipes of data:
             * Endpoints are always paired with another endpoint
             * * Data can be sent to the other endpoint
             * * Data can be received from the other endpoint
             * * If an endpoint is destroyed or closed, the "connection" will be terminated once the other endpoint has processed all data sent prior to closure
             *
             * Endpoints should not be constructed manually but the factory function make_endpoints() should be used
             */
            class Endpoint : public std::enable_shared_from_this<Endpoint> {
            public:
                using ptr = std::shared_ptr<Endpoint>;
                
                Endpoint();

            public:
                /** Send data over the pipe
                 *
                 * \param bytes the bytes to be sent (moveable)
                 */
                void send(bytes_t&& bytes);
                
                /** Send data over the pipe
                 *
                 * \param bytes the bytes to be sent (copyable)
                 */
                void send(const bytes_t& bytes);

                /** Receive data from the pipe
                 *
                 * Data will be received on the caller's queue
                 * \param lambda a lambda function with the following signature: (std::future<bytes_t>&) -> T
                 * \return a continuation of type threading::Subscriber<T>::ptr
                 * The future will always be resolvable and will throw a threading::ObservableCompletedException on closure of the remote endpoint
                 */
                template <typename X>
                decltype(std::declval<typename threading::Subscriber<bytes_t>::ptr>()->next(std::declval<X>(), std::declval<typename threading::Queue::ptr>()))
                receive(X&& lambda, const threading::Queue::ptr& queue = threading::Queue::current())
                {
                    std::weak_ptr<Endpoint> weak_self = shared_from_this();
                    return _receiver
                    ->next([this, weak_self](auto& fut){
                        try
                        {
                            return fut.get();
                        }
                        catch(...)
                        {
                            auto self = weak_self.lock();
                            if(self)
                            {
                                close();
                            }
                            std::rethrow_exception(std::current_exception());
                        }
                    }, queue)->next(std::move(lambda), queue);
                }
                
                /** Receive data from the pipe
                 *
                 * Data will be received asynchronously on the specified queue
                 * \param lambda a lambda function with the following signature: (std::future<bytes_t>&) -> T
                 * \param queue the queue on which to process the received data (defaults to current queue)
                 * \return a continuation of type threading::Subscriber<T>::ptr
                 * The future will always be resolvable and will throw a threading::ObservableCompletedException on closure of the remote endpoint
                 */
                template <typename X>
                decltype(std::declval<typename threading::Subscriber<bytes_t>::ptr>()->next_async(std::declval<X>()))
                receive_async(X&& lambda, const threading::Queue::ptr& queue = threading::Queue::current())
                {
                    std::weak_ptr<Endpoint> weak_self = shared_from_this();
                    return _receiver
                    ->next([this, weak_self](auto& fut){
                        try
                        {
                            return fut.get();
                        }
                        catch(...)
                        {
                            auto self = weak_self.lock();
                            if(self)
                            {
                                close();
                            }
                            std::rethrow_exception(std::current_exception());
                        }
                    }, queue)->next_async(std::move(lambda), queue);
                }
                
                /** Manually close the pipe
                 */
                void close();
                
            private:
                friend std::pair<Endpoint::ptr, Endpoint::ptr> make_endpoints();
                
                bool _closed;
                threading::Observable<bytes_t> _sender;
                typename threading::Subscriber<bytes_t>::ptr _receiver;
            };
            
            /** Create a pipe with two endpoints
             *
             * \return a pair of two endpoints connected to each other
             */
            std::pair<Endpoint::ptr, Endpoint::ptr> make_endpoints();
            
            /** @}*/
        }
    }
}
