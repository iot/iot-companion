//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  ProtoStore.cpp
//  song2
//
//  Created by Donatien Garnier on 12/01/2018.

//

#include <fstream>
#include <sstream>
#include <google/protobuf/descriptor.pb.h>

#include "ProtoStore.hpp"
#include "ArpcException.hpp"

using namespace arm::song2::arpc;
using namespace arm::song2;

ProtoStore::ProtoStore() : _message_factory(&_desc_pool)
{
    
}

void ProtoStore::register_services(const std::string& binary)
{
    google::protobuf::FileDescriptorSet file_descriptors;
    
    bool r = file_descriptors.ParseFromString(binary);
    if(!r)
    {
        throw ArpcException("Could not parse protobuf blob");
    }
    
    for(auto& file : file_descriptors.file())
    {
        _desc_pool.BuildFile(file);
    }
}

void ProtoStore::register_services(const bytes_t& binary)
{
    google::protobuf::FileDescriptorSet file_descriptors;
    
    bool r = file_descriptors.ParseFromArray(binary.data(), static_cast<int>(binary.size()));
    if(!r)
    {
        throw ArpcException("Could not parse protobuf blob");
    }
    
    for(auto& file : file_descriptors.file())
    {
        _desc_pool.BuildFile(file);
    }
}

void ProtoStore::register_services_from_files(const std::vector<std::string>& files)
{
    for(auto& filename : files)
    {
        std::fstream fs(filename, std::fstream::in | std::fstream::binary);
        google::protobuf::FileDescriptorSet file_descriptors;
        
        bool r = file_descriptors.ParseFromIstream(&fs);
        fs.close();
        if(!r)
        {
            throw ArpcException("Could not parse protobuf blob");
        }
        
        for(auto& file : file_descriptors.file())
        {
            _desc_pool.BuildFile(file);
        }
    }
}

std::unique_ptr<google::protobuf::Message> ProtoStore::input_message(const std::string& service_name, const std::string& method_name) const
{
    auto fqn = service_name + "." + method_name; // fully qualified name
    
    // These are const ptrs owned by desc pool
    auto method_descriptor = _desc_pool.FindMethodByName(fqn);
    if( method_descriptor == nullptr )
    {
        throw ArpcException("Unknown method");
    }
    auto msg_descriptor = method_descriptor->input_type();
    if( msg_descriptor == nullptr )
    {
        throw ArpcException("Unknown input message type");
    }
    
    auto msg_prototype = _message_factory.GetPrototype(msg_descriptor);
    
    std::unique_ptr<google::protobuf::Message> msg(msg_prototype->New());
    
    return msg;
}

std::unique_ptr<google::protobuf::Message>  ProtoStore::output_message(const std::string& service_name, const std::string& method_name) const
{
    auto fqn = service_name + "." + method_name; // fully qualified name
    
    // These are const ptrs owned by desc pool
    auto method_descriptor = _desc_pool.FindMethodByName(fqn);
    if( method_descriptor == nullptr )
    {
        throw ArpcException("Unknown method");
    }
    auto msg_descriptor = method_descriptor->output_type();
    if( msg_descriptor == nullptr )
    {
        throw ArpcException("Unknown ouput message type");
    }
    
    auto msg_prototype = _message_factory.GetPrototype(msg_descriptor);
    
    std::unique_ptr<google::protobuf::Message> msg(msg_prototype->New());
    
    return msg;
}

