//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  ArpcEndpoint.hpp
//  song2
//
//  Created by Donatien Garnier on 12/01/2018.

//

#pragma once

#include <memory>
#include <map>
#include <utility>
#include <boost/optional.hpp>

#include <google/protobuf/message.h>
#include <google/protobuf/util/json_util.h>

#include "cpp/pipe/Endpoint.hpp"
#include "cpp/types/Types.hpp"

#include "cpp/threading/Promise.hpp"
#include "cpp/threading/Observable.hpp"
#include "cpp/threading/EnableExecuteIfValid.hpp"

extern "C" {
#include "arpc/arpc.h"
}

namespace arm {
    namespace song2 {
        namespace arpc {
            class ProtoStore;
            /** \addtogroup ARPC
             *
             * This module provides a C++ API on top of the ARPC library. This provides a RPC client layer (server needs to be implemented), serialization using protobuf and a JSON serialization layer.
             *  @{
             */
            
            /** ARPC C++ mapping
             *
             * This class provides a C++ API on top of the ARPC library and implements services and methods discovery and caching.
             */
            class ArpcEndpoint : public threading::EnableExecuteIfValid
            {
            public:
                /** Instanciate ArpcEndpoint
                 * \param endpoint a pipe endpoint from a reliable transport channel (e.g. SRTP)
                 */
                ArpcEndpoint(typename pipe::Endpoint::ptr&& endpoint);
                
                /** Set Proto Store
                 * \param proto_store a ProtoStore instance with known registered services
                 */
                void set_proto_store(const std::shared_ptr<const ProtoStore>& proto_store);
                
                /** Set JSON formatting options
                 * \param add_whitespace whether to output 'pretty' JSON with whitespaces or not
                 */
                void set_json_options(bool add_whitespace);
                
                /** Call a method on the device using a JSON payload and deserialize response to JSON
                 * \param service_name the relevant service
                 * \param method_name the relevant method
                 * \param json_payload the JSON-encoded payload
                 * \return a json_t future which will resolve to the device's response or throw an ArpcException exception
                 */
                threading::Future< json_t >::ptr call_method_json(const std::string& service_name, const std::string& method_name, const json_t& json_payload);
                
                /** Call a method on the device using a JSON payload and stream and deserialize responses to JSON
                 * \param service_name the relevant service
                 * \param method_name the relevant method
                 * \param json_payload the JSON-encoded payload
                 * \return a json_t subscriber which will get the device's responses or throw an ArpcException exception
                 */
                threading::Subscriber< json_t >::ptr call_method_json_stream(const std::string& service_name, const std::string& method_name, const json_t& json_payload);
                
                /** Call a method on the device using a protobuf payload and deserialize response to protobuf structure
                 * \param service_name the relevant service
                 * \param method_name the relevant method
                 * \param input_payload the protobuf message to send (needs to remain valid during the whole call)
                 * \param output_payload a pointer to the protobuf message to receive (needs to remain valid during the whole call)
                 * \return a void future which will resolve to the device's response or throw an ArpcException exception
                 */
                threading::Future< void >::ptr call_method(const std::string& service_name, const std::string& method_name, const google::protobuf::Message& input_payload, google::protobuf::Message* output_payload);
                
                /** Call a method on the device using a pre-serialized protobuf payload and get serialized response
                 * \param service_name the relevant service
                 * \param method_name the relevant method
                 * \param input_bytes the parameters serialized as a bytes buffer (needs to remain valid during the whole call)
                 * \return a future which will resolve to the device's serialized response or throw an ArpcException exception
                 */
                threading::Future< bytes_t >::ptr call_method_serialized(const std::string& service_name, const std::string& method_name, const bytes_t& input_bytes);
                
                /** Close ArpcEndpoint */
                void close();
                
                // TODO add server APIs too
                
                // ARPC API implementation
                void arpc_responded(arpc_command_t command_id, bool last_response, arpc_response_code_t response_code, const ac_buffer_t* payload);
                void arpc_requested(arpc_command_t command_id, arpc_method_num_t method_num, const ac_buffer_t* payload);
                bool arpc_transport_send(const ac_buffer_t* buffer);
                
            private:
                struct MethodCache;
                struct ServiceCache;
                threading::Future<ServiceCache*>::ptr resolve_service(const std::string& service_name);
                threading::Future<MethodCache*>::ptr resolve_method(const std::string& service_name, const std::string& method_name, bool need_serializers);

                threading::Subscriber<bytes_t>::ptr call_method_intl_stream(arpc_method_num_t method_num, const bytes_t& payload);
                threading::Future<bytes_t>::ptr call_method_intl(arpc_method_num_t method_num, const bytes_t& payload);
                
                bytes_t serialize_message(const google::protobuf::Message& msg) const;
                void deserialize_message(google::protobuf::Message& msg, const bytes_t& bytes) const;
                
                bytes_t serialize_message_json(const google::protobuf::Message& prototype, const json_t& json) const;
                json_t deserialize_message_json(const google::protobuf::Message& prototype, const bytes_t& bytes) const;
                
                void throw_if_closed();
                
                typename pipe::Endpoint::ptr _endpoint;
                std::shared_ptr<const ProtoStore> _proto_store;
                
                arpc_connection_t _arpc_connection;
                
                struct MethodCache {
                    arpc_method_num_t num;
                    std::unique_ptr<google::protobuf::Message> input_prototype;
                    std::unique_ptr<google::protobuf::Message> output_prototype;
                };
                
                struct ServiceCache {
                    size_t service_number;
                    std::map<std::string, boost::optional<MethodCache>> methods;
                };
                
                std::map< std::string /* service name */, boost::optional<ServiceCache> > _cache;
                bool _closed;
                
                arpc_command_t _last_command_id;
                std::map< arpc_command_t, threading::Observable<bytes_t> > _commands;
                
                google::protobuf::util::JsonPrintOptions _json_options;
            };
            /** @}*/
        }
    }
}
