//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  ArpcEndpoint.hpp
//  song2
//
//  Created by Donatien Garnier on 12/01/2018.

//

#include "ArpcEndpoint.hpp"
#include "ArpcException.hpp"
#include "ProtoStore.hpp"

#include "gen/proto/cpp/discovery.pb.h"

using namespace arm::song2;
using namespace arm::song2::arpc;

#define DISCOVERY_SERVICE_PACKAGE "arpc"

#define GET_METHOD_NUMBER_NAME "GetMethodNumber"
#define GET_SERVICE_NUMBER_NAME "GetServiceNumber"

#define GET_METHOD_NUMBER_NUM 0
#define GET_SERVICE_NUMBER_NUM 1

// ARPC Implementation
static void arpc_impl_connection_opened(const arpc_connection_t* connection, void* user);
static void arpc_impl_connection_closed(const arpc_connection_t* connection, void* user);
static void arpc_impl_responded(const arpc_connection_t* connection, arpc_command_t command_id, bool last_response, arpc_response_code_t response_code, const ac_buffer_t* payload, void* user);
static void arpc_impl_requested(const arpc_connection_t* connection, arpc_command_t command_id, arpc_method_num_t method_num, const ac_buffer_t* payload, void* user);
static bool arpc_impl_transport_send(const arpc_connection_t* connection, const ac_buffer_t* buffer, void* user);

const static arpc_implementation_t arpc_endpoint_vtable
{
    .connection_opened = arpc_impl_connection_opened,
    .connection_closed = arpc_impl_connection_closed,
    .responded = arpc_impl_responded,
    .requested = arpc_impl_requested,
    .transport_send = arpc_impl_transport_send
};

ArpcEndpoint::ArpcEndpoint(typename pipe::Endpoint::ptr&& endpoint) : _endpoint(std::move(endpoint)), _proto_store(nullptr),
    _closed(false), _last_command_id(0)
{
    // Set default JSON options
    _json_options.add_whitespace = false;
    _json_options.always_print_primitive_fields = true;
    
    _endpoint->receive(execute_if_valid([this](std::future<bytes_t>& fut){
        try {
            ac_buffer_t buf;
            auto bytes = fut.get();
            ac_buffer_map_bytes(&buf, bytes);
            arpc_transport_received(&_arpc_connection, &buf);
        }
        catch(threading::ObservableCompletedException& e)
        {
            _closed = true;
        }
    }));
    
    // Open ARPC connection
    arpc_transport_connection_open(&_arpc_connection, &arpc_endpoint_vtable, this);
}
    
void ArpcEndpoint::set_proto_store(const std::shared_ptr<const ProtoStore>& proto_store)
{
    _cache.clear();
    _proto_store = proto_store;
}

void ArpcEndpoint::set_json_options(bool add_whitespace)
{
    _json_options.add_whitespace = add_whitespace;
}

threading::Future< json_t >::ptr ArpcEndpoint::call_method_json(const std::string& service_name, const std::string& method_name, const json_t& json_payload)
{
    throw_if_closed();
    return resolve_method(service_name, method_name, true)->then([this, json_payload](std::future<MethodCache*>& fut_method)
                                                    {
                                                        auto method = fut_method.get();
                                                        auto input_bytes = serialize_message_json(*method->input_prototype, json_payload);
                                                        return call_method_intl(method->num, input_bytes)
                                                        ->then([this, method](std::future<bytes_t>& fut_output_bytes)
                                                               {
                                                                   auto output_bytes = fut_output_bytes.get();
                                                                   return deserialize_message_json(*method->output_prototype, output_bytes);
                                                               }, threading::Queue::current());
                                                    }, threading::Queue::current());
}
    
threading::Future< void >::ptr ArpcEndpoint::call_method(const std::string& service_name, const std::string& method_name, const google::protobuf::Message& input_payload, google::protobuf::Message* output_payload)
    {
        throw_if_closed();
        return resolve_method(service_name, method_name, false)->then([this, &input_payload, output_payload](std::future<MethodCache*>& fut_method)
                                                               {
                                                                   auto method = fut_method.get();
                                                                   auto input_bytes = serialize_message(input_payload);
                                                                   return call_method_intl(method->num, input_bytes)
                                                                   ->then([this, method, output_payload](std::future<bytes_t>& fut_output_bytes)
                                                                          {
                                                                              auto output_bytes = fut_output_bytes.get();
                                                                              return deserialize_message(*output_payload, output_bytes);
                                                                          }, threading::Queue::current());
                                                               }, threading::Queue::current());
    }

threading::Future< bytes_t >::ptr ArpcEndpoint::call_method_serialized(const std::string& service_name, const std::string& method_name, const bytes_t& input_bytes)
{
    throw_if_closed();
    return resolve_method(service_name, method_name, false)->then([this, &input_bytes](std::future<MethodCache*>& fut_method)
                                                           {
                                                               auto method = fut_method.get();
                                                               return call_method_intl(method->num, input_bytes)
                                                               ->then([this, method](std::future<bytes_t>& fut_output_bytes)
                                                                      {
                                                                          auto output_bytes = fut_output_bytes.get();
                                                                          return output_bytes;
                                                                      }, threading::Queue::current());
                                                           }, threading::Queue::current());
}


threading::Subscriber< json_t >::ptr ArpcEndpoint::call_method_json_stream(const std::string& service_name, const std::string& method_name, const json_t& json_payload)
{
    throw_if_closed();
    
    auto obs = std::make_shared<threading::Observable< json_t >>();
    
    // A bit convoluted but any exception will be propagated through destructors
    resolve_method(service_name, method_name, true)->then([this, json_payload, obs](std::future<MethodCache*>& fut_method)
                                                           {
                                                               auto method = fut_method.get();
                                                               auto input_bytes = serialize_message_json(*method->input_prototype, json_payload);
                                                               return call_method_intl_stream(method->num, input_bytes)
                                                               ->next([this, method, obs](std::future<bytes_t>& fut_output_bytes)
                                                                      {
                                                                          auto output_bytes = fut_output_bytes.get();
                                                                          obs->next(deserialize_message_json(*method->output_prototype, output_bytes));
                                                                      }, threading::Queue::current());
                                                           }, threading::Queue::current());
    
    return obs->get_subscriber();
}

threading::Future<ArpcEndpoint::ServiceCache*>::ptr ArpcEndpoint::resolve_service(const std::string& service_name)
{
    threading::Promise<ArpcEndpoint::ServiceCache*> prom;
    auto fut = prom.get_future();
    
    auto sit = _cache.find(service_name);
    if(sit != _cache.cend())
    {
        auto& service = sit->second;
        if(service)
        {
            // Resolved and valid
            prom.resolve(&(*service));
            return fut;
        }
        else
        {
            // Resolved but device does not support it
            prom.reject(std::make_exception_ptr(ArpcClientException(arpc_response_code_t::arpc_response_err_invalid_method, "Unknown service")));
            return fut;
        }
    }
    
    // Otherwise we need to resolve it - use discovery service
    ::arpc::GetServiceNumberRequest req;
    req.set_service_name(service_name);
    auto req_bytes = serialize_message(req);
    return call_method_intl( GET_SERVICE_NUMBER_NUM, req_bytes )->then
    ([this, service_name]( std::future<bytes_t>& resp_bytes )
     {
         ::arpc::GetServiceNumberResponse resp;
         deserialize_message(resp, resp_bytes.get()); // Can throw
         
         // Save in cache
         auto sit = _cache.emplace(std::make_pair(service_name, boost::none)).first;
         auto& service = sit->second;
         if( resp.found() )
         {
             service = ServiceCache();
             service->service_number = resp.service_number();
             return &(*service);
         }
         else
         {
             // Resolved but device does not support it
             service = boost::none;
             throw ArpcClientException(arpc_response_code_t::arpc_response_err_invalid_method, "Unknown service");
         }
     }, threading::Queue::current());
    
    
    return prom.get_future();
}

threading::Future<ArpcEndpoint::MethodCache*>::ptr ArpcEndpoint::resolve_method(const std::string& service_name, const std::string& method_name, bool need_serializers)
{
    return resolve_service(service_name)->then([this, service_name, method_name, need_serializers](std::future<ArpcEndpoint::ServiceCache*>& service_fut)
                                              {
                                                  // Do we already know about this?
                                                  auto service = service_fut.get();
                                                  auto mit = service->methods.find(method_name);
                                                  if(mit != service->methods.cend())
                                                  {
                                                      auto& method = mit->second;
                                                      if(method)
                                                      {
                                                          // Resolved and valid
                                                          return threading::make_resolved_future(&*method);
                                                      }
                                                      else
                                                      {
                                                          // Resolved but device does not support it
                                                          throw ArpcClientException(arpc_response_code_t::arpc_response_err_invalid_method, "Unknown method");
                                                      }
                                                  }
                                                  
                                                  // Otherwise we need to resolve it
                                                  // Save in cache
                                                  // By default, it's set to boost::none
                                                  mit = service->methods.emplace(method_name, boost::none).first;
                                                  auto method_opt_ptr = &mit->second;
                                                  
                                                  // 1- First check with proto store if we know about this (might throw)
                                                  auto method_cache = std::make_shared<MethodCache>();
                                                  if(need_serializers)
                                                  {
                                                      if(_proto_store == nullptr)
                                                      {
                                                          throw ArpcClientException(arpc_response_code_t::arpc_response_err_internal, "No proto store provided");
                                                      }
                                                      method_cache->input_prototype = _proto_store->input_message(service_name, method_name);
                                                      method_cache->output_prototype = _proto_store->output_message(service_name, method_name);
                                                  }
                                                  
                                                  
                                                  // 2- Use discovery service
                                                  ::arpc::GetMethodNumberRequest req;
                                                  req.set_service_number(static_cast<uint32_t>(service->service_number));
                                                  req.set_method_name(method_name);
                                                  
                                                  auto req_bytes = serialize_message(req);
                                                  return call_method_intl( GET_METHOD_NUMBER_NUM, req_bytes )->then
                                                  ([this, method_opt_ptr, method_cache](std::future<bytes_t>& resp_bytes_fut) mutable
                                                   {
                                                       auto resp_bytes = resp_bytes_fut.get();
                                                       
//                                                       if( code != arpc_response_ok )
//                                                       {
//                                                           throw ArpcClientException(code, "Server error in service discovery");
//                                                       }
                                                       
                                                       ::arpc::GetMethodNumberResponse resp;
                                                       deserialize_message(resp, resp_bytes);
                                                       
                                                       // Save in cache
                                                       if( resp.found() )
                                                       {
                                                           auto& method_opt = *method_opt_ptr;
                                                           method_opt = std::move(*method_cache);
                                                           
                                                           auto& method = *method_opt;
                                                           
                                                           method.num = resp.method_number();
                                                           return &method;
                                                       }
                                                       else
                                                       {
                                                           // Resolved but device does not support it
                                                           throw ArpcClientException(arpc_response_code_t::arpc_response_err_invalid_method, "Unknown method");
                                                       }
                                                   }, threading::Queue::current());
                                              }, threading::Queue::current()
                                              );
}

threading::Subscriber<bytes_t>::ptr ArpcEndpoint::call_method_intl_stream(arpc_method_num_t method_num, const bytes_t& payload)
{
    auto command_id = _last_command_id++;
    auto& obs = _commands[command_id];
    
    // Make sure we get the subscriber here, as observer might be destroyed by a callback indirectly called by arpc_request() if the implementation is synchronous
    auto subscriber = obs.get_subscriber();
    
    ac_buffer_t buffer;
    ac_buffer_map_bytes(&buffer, payload);
    if(!arpc_request(&_arpc_connection, command_id, method_num, &buffer))
    {
        throw ArpcClientException(arpc_response_code_t::arpc_response_err_unknown, "Transport error");
    }
    
    return subscriber;
}

threading::Future<bytes_t>::ptr ArpcEndpoint::call_method_intl(arpc_method_num_t method_num, const bytes_t& payload)
{
    auto prom = std::make_shared<threading::Promise<bytes_t>>();
    auto fut = prom->get_future();
    call_method_intl_stream(method_num, payload)->next([prom](std::future<bytes_t>& fut) mutable {
        if(!prom->resolved())
        {
            prom->set(fut);
        }
    }, threading::Queue::current());
    return fut;
}

bytes_t ArpcEndpoint::serialize_message(const google::protobuf::Message& msg) const
{
    size_t sz = msg.ByteSize();
    bytes_t bytes(sz);
    if(!msg.SerializeToArray(bytes.data(), static_cast<int>(sz)))
    {
        throw ArpcClientException(arpc_response_err_internal, "Serialization error");
    }
    return bytes;
}

void ArpcEndpoint::deserialize_message(google::protobuf::Message& msg, const bytes_t& bytes) const
{
    if(!msg.ParseFromArray(bytes.data(), static_cast<int>(bytes.size())))
    {
        throw ArpcClientException(arpc_response_err_parsing_error, "Deserialization error");
    }
}

bytes_t ArpcEndpoint::serialize_message_json(const google::protobuf::Message& prototype, const json_t& json) const
{
    auto msg = std::unique_ptr<google::protobuf::Message>(prototype.New());
    
    google::protobuf::util::JsonParseOptions options;
    options.ignore_unknown_fields = true; // Be flexible
    auto result = google::protobuf::util::JsonStringToMessage(json, msg.get(), options);
    if( result.error_code() != google::protobuf::util::error::OK )
    {
        // Throw with details
        // protobuf deserialization error
        throw ArpcClientException(arpc_response_err_parsing_error, result.error_message());
    }
    
    return serialize_message(*msg);
}

json_t ArpcEndpoint::deserialize_message_json(const google::protobuf::Message& prototype, const bytes_t& bytes) const
{
    auto msg = std::unique_ptr<google::protobuf::Message>(prototype.New());
    deserialize_message(*msg, bytes);

    json_t json;
    
    auto result = google::protobuf::util::MessageToJsonString(*msg, &json, _json_options);
    if( result.error_code() != google::protobuf::util::error::OK )
    {
        // Throw with details
        // protobuf deserialization error
        throw ArpcClientException(arpc_response_err_parsing_error, result.error_message());
    }
    
    return json;
}

void ArpcEndpoint::throw_if_closed()
{
    if(_closed)
    {
        throw ArpcException("Endpoint is closed");
    }
}

void ArpcEndpoint::close()
{
    _closed = true;
    _endpoint->close();
}

void ArpcEndpoint::arpc_responded(arpc_command_t command_id, bool last_response, arpc_response_code_t response_code, const ac_buffer_t* payload)
{
    ac_buffer_t payload_dup;
    ac_buffer_dup(&payload_dup, payload);
    auto bytes = ac_buffer_to_bytes(&payload_dup);
    
    auto cit = _commands.find(command_id);
    if(cit != _commands.cend())
    {
        if(response_code == arpc_response_ok)
        {
            // Send bytes
            cit->second.next(std::move(bytes));
            
            // Close and dealloc resources if last response
            if(last_response)
            {
                _commands.erase(cit);
            }
        }
        else
        {
            // Send exception and dealloc
            ArpcClientException ex(response_code);
            cit->second.error(std::make_exception_ptr(ex));
            _commands.erase(cit);
        }
    }
}

void ArpcEndpoint::arpc_requested(arpc_command_t command_id, arpc_method_num_t method_num, const ac_buffer_t* payload)
{
    
}

bool ArpcEndpoint::arpc_transport_send(const ac_buffer_t* buffer)
{
    ac_buffer_t payload_dup;
    ac_buffer_dup(&payload_dup, buffer);
    auto bytes = ac_buffer_to_bytes(&payload_dup);
    _endpoint->send(std::move(bytes));
    return true;
}

// Implementations of ARPC API


void arpc_impl_connection_opened(const arpc_connection_t* connection, void* user)
{
    // Ignore
}

void arpc_impl_connection_closed(const arpc_connection_t* connection, void* user)
{
    // Ignore
}

void arpc_impl_responded(const arpc_connection_t* connection, arpc_command_t command_id, bool last_response, arpc_response_code_t response_code, const ac_buffer_t* payload, void* user)
{
    ArpcEndpoint* inst = static_cast<ArpcEndpoint*>(user);
    inst->arpc_responded(command_id, last_response, response_code, payload);
}

void arpc_impl_requested(const arpc_connection_t* connection, arpc_command_t command_id, arpc_method_num_t method_num, const ac_buffer_t* payload, void* user)
{
    ArpcEndpoint* inst = static_cast<ArpcEndpoint*>(user);
    inst->arpc_requested(command_id, method_num, payload);
}

bool arpc_impl_transport_send(const arpc_connection_t* connection, const ac_buffer_t* buffer, void* user)
{
    ArpcEndpoint* inst = static_cast<ArpcEndpoint*>(user);
    return inst->arpc_transport_send(buffer);
}


