//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  ArpcException.hpp
//  song2
//
//  Created by Donatien Garnier on 12/01/2018.

//

#pragma once

#include <stdexcept>
#include <string>

extern "C" {
#include "arpc/arpc.h"
}

namespace arm {
    namespace song2 {
        namespace arpc {
            /** \addtogroup ARPC
             *  @{
             */
            
            /** A base exception for Arpc errors
             */
            class ArpcException : public std::runtime_error {
            public:
                explicit ArpcException(const char* msg);
                explicit ArpcException(const std::string& msg);
                virtual ~ArpcException();
            };
            
            /** An exception for Arpc client errors
             */
            class ArpcClientException : public ArpcException
            {
            public:
                explicit ArpcClientException(arpc_response_code_t code);
                explicit ArpcClientException(arpc_response_code_t code, const std::string& msg);
                explicit ArpcClientException(arpc_response_code_t code, const char* msg);
                
                virtual ~ArpcClientException();
                
                arpc_response_code_t code; ///< ARPC error code
            private:
                static const std::string& default_message_for_code(arpc_response_code_t code);
            };
            /** @}*/
        }
    }
}
