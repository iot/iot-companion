//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  ProtoStore.hpp
//  song2
//
//  Created by Donatien Garnier on 12/01/2018.

//

#pragma once

#include <google/protobuf/message.h>
#include <google/protobuf/descriptor.pb.h>
#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/descriptor.h>

#include <string>
#include <map>
#include <vector>
#include <utility>
#include <memory>
#include <boost/optional.hpp>

#include "cpp/types/Types.hpp"

// From https://developers.google.com/protocol-buffers/docs/reference/cpp/
// Thread-safety in the Protocol Buffer library follows a simple rule:
// unless explicitly noted otherwise, it is always safe to use an object from multiple threads
// simultaneously as long as the object is declared const in all threads (or, it is only used in ways that would be allowed if it were declared const).
// However, if an object is accessed in one thread in a way that would not be allowed if it were const, then it is not safe to access that object in any other
// thread simultaneously.
//
// Put simply, read-only access to an object can happen in multiple threads simultaneously, but write access can only happen in a single thread at a time.
//
// The implementation does contain some "const" methods which actually modify the object behind the scenes – e.g., to cache results – but in these cases mutex
// locking is used to make the access thread-safe.

namespace arm {
    namespace song2 {
        namespace arpc {
            /** \addtogroup ARPC
             *
             *  @{
             */
            
            /** A ProtoStore parses protocol buffer descriptors and registers services and message types
             */
            class ProtoStore
            {
            public:
                /** Instanciate a ProtoStore
                 */
                ProtoStore();
                
                /** Register services from binary-string
                 * \param binary the binary blob
                 */
                void register_services(const std::string& binary);
                
                /** Register services from binary-string
                 * \param binary the binary blob
                 */
                void register_services(const bytes_t& binary);
                
                /** Register services from list of files
                 * \param files the list of files to use
                 */
                void register_services_from_files(const std::vector<std::string>& files);
                
                /** Get an instance of the input message for a specific service and method
                 * \param service_name the relevant service
                 * \param method_name the relevant method
                 * \return a new message instance based on the relevant prototype
                 */
                std::unique_ptr<google::protobuf::Message> input_message(const std::string& service_name, const std::string& method_name) const;
                
                /** Get an instance of the output message for a specific service and method
                 * \param service_name the relevant service
                 * \param method_name the relevant method
                 * \return a new message instance based on the relevant prototype
                 */
                std::unique_ptr<google::protobuf::Message> output_message(const std::string& service_name, const std::string& method_name) const;
                
            private:
                google::protobuf::DescriptorPool _desc_pool;
                mutable google::protobuf::DynamicMessageFactory _message_factory;
                
                std::vector<google::protobuf::ServiceDescriptor*> _service_descriptors;
            };
            /** @}*/
        }
    }
}
