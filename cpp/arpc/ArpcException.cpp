//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  ArpcException.cpp
//  song2
//
//  Created by Donatien Garnier on 12/01/2018.

//

#include "ArpcException.hpp"

#include <string>

using namespace arm::song2::arpc;

ArpcException::ArpcException(const char* msg) : std::runtime_error::runtime_error(msg) {}
ArpcException::ArpcException(const std::string& msg) : std::runtime_error::runtime_error(msg) {}
ArpcException::~ArpcException() {}

static const std::string messages[] = {
    "OK", // <<< arpc_response_ok
    "Method is unimplemented", // <<< arpc_response_err_unimplemented
    "Peer is unavailable", // <<< arpc_response_err_unavailable,
    "Method is invalid", // <<< arpc_response_err_invalid_method,
    "Timeout", // <<< arpc_response_err_timeout,
    "Permission denied", // <<< arpc_response_err_permission_denied,
    "Parsing error", // <<< arpc_response_err_parsing_error,
    "Invalid argument", // <<< arpc_response_err_invalid_argument, // Parsing ok but parameters are invalid
    "Out of memory", // <<< arpc_response_err_memory_error,
    "Internal error", // <<< arpc_response_err_internal,
    "Unknown error", // <<< arpc_response_err_unknown
};

ArpcClientException::ArpcClientException(arpc_response_code_t code) : ArpcException(default_message_for_code(code)), code(code) {}
ArpcClientException::ArpcClientException(arpc_response_code_t code, const std::string& msg) : ArpcException(msg), code(code) {}
ArpcClientException::ArpcClientException(arpc_response_code_t code, const char* msg) : ArpcException(msg), code(code) {}

ArpcClientException::~ArpcClientException() {}

const std::string& ArpcClientException::default_message_for_code(arpc_response_code_t code)
{
    if( code > arpc_response_code_max )
    {
        code = arpc_response_err_unknown;
    }
    
    return messages[code];
}
