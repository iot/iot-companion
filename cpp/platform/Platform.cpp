//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Platform.cpp
//  song2
//
//  Created by Donatien Garnier on 10/01/2018.

//

#include <cassert>

#include "Platform.hpp"

using namespace arm::song2;

static std::shared_ptr<platform::PlatformFactory> _platform_factory = nullptr;

const std::shared_ptr<platform::PlatformFactory>& arm::song2::platform::platform_factory()
{
    assert(_platform_factory != nullptr);
    return _platform_factory;
}

void arm::song2::platform::set_platform_factory(const std::shared_ptr<platform::PlatformFactory>& platform_factory)
{
    _platform_factory = platform_factory;
}
