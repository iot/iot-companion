//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  JsDevice.hpp
//  song2-tests
//
//  Created by Donatien Garnier on 16/01/2018.

//

#pragma once

#include <string>
#include <memory>
#include <boost/optional.hpp>

#include "cpp/types/Types.hpp"
#include "cpp/threading/Promise.hpp"
#include "cpp/pipe/Endpoint.hpp"
#include "cpp/transport/Device.hpp"

#include "DeviceInfo.hpp"

namespace arm {
    namespace song2 {
        namespace arpc {
            class ArpcEndpoint;
            class ProtoStore;
        }
        
        namespace srtp {
            class SrtpEndpoint;
        }
        
        namespace device {
            class Device : public DeviceInfo
            {
            public:
                Device(const std::shared_ptr<transport::Device>& device, uint32_t channel_num);
                ~Device();
                
                threading::Future<void>::ptr connect(const std::shared_ptr<const arpc::ProtoStore>& proto_store);
                threading::Future<void>::ptr disconnect();
                threading::Future<json_t>::ptr rpc(const std::string& service_name, const std::string& method_name, const json_t& parameters);

            private:
                // Protocol layers
                std::unique_ptr<arpc::ArpcEndpoint> _arpc;
                std::unique_ptr<srtp::SrtpEndpoint> _srtp;
                std::shared_ptr<transport::Device> _device;
                uint32_t _channel_num;
            };
        }
    }
}

