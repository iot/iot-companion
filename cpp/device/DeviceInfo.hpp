//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  DeviceInfo.hpp
//  song2
//
//  Created by Donatien Garnier on 26/03/2018.

//

#pragma once

#include <memory>
#include <string>
#include <boost/optional.hpp>

#include "cpp/types/Types.hpp"
#include "cpp/transport/Device.hpp"
#include "cpp/threading/ObservableVector.hpp"
#include "cpp/threading/Queue.hpp"

namespace arm {
    namespace song2 {
        namespace device {
            // A class used as the API for jobs
            class DeviceInfo
            {
            public:
                DeviceInfo(const std::shared_ptr<transport::Device>& device);
                ~DeviceInfo();

                boost::optional<std::string> url() const;
                boost::optional<bytes_t> id() const;
                
                std::string transport_name() const;
                double rssi() const;
                transport::DeviceStatus status() const;
                threading::Subscriber<transport::DeviceStatus>::ptr status_updates();
                
                threading::Queue::ptr queue() const;
                
            protected:
                threading::Queue::ptr _queue;
                
            private:
                std::string _transport_name;
                double _rssi;
                boost::optional<std::string> _url;
                boost::optional<bytes_t> _id;
                transport::DeviceStatus _status;
                threading::ObservableVector<transport::DeviceStatus> _status_obs;
            };
        }
    }
}
