//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  DeviceInfo.cpp
//  song2
//
//  Created by Donatien Garnier on 26/03/2018.

//

#include "DeviceInfo.hpp"

using namespace arm::song2::device;
using namespace arm::song2;

DeviceInfo::DeviceInfo(const std::shared_ptr<transport::Device>& device) :
_queue(threading::make_queue()),
_transport_name(device->transport_name()),
_rssi(0),
_url(device->url),
_id(device->id),
_status(device->status())
{
    // todo watch connection status
}

DeviceInfo::~DeviceInfo()
{
    
}

boost::optional<std::string> DeviceInfo::url() const
{
    return _url;
}

boost::optional<bytes_t> DeviceInfo::id() const
{
    return _id;
}

std::string DeviceInfo::transport_name() const
{
    return _transport_name;
}

double DeviceInfo::rssi() const
{
    return _rssi;
}

transport::DeviceStatus DeviceInfo::status() const
{
    return _status;
}

threading::Subscriber<transport::DeviceStatus>::ptr DeviceInfo::status_updates()
{
    return _status_obs.get_subscriber();
}

threading::Queue::ptr DeviceInfo::queue() const
{
    return _queue;
}
