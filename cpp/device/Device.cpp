//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Device.cpp
//  song2-tests
//
//  Created by Donatien Garnier on 16/01/2018.

//

#include "Device.hpp"

#include "cpp/arpc/ArpcEndpoint.hpp"
#include "cpp/srtp/SrtpEndpoint.hpp"
#include "cpp/arpc/ProtoStore.hpp"
#include "cpp/platform/Platform.hpp"
#include "cpp/util/base64.hpp"
#include "cpp/logging/Logging.hpp"

using namespace arm::song2;
using namespace arm::song2::device;

using util::encode_b64;
using util::decode_b64;

Device::Device(const std::shared_ptr<transport::Device>& device, uint32_t channel_num) :
DeviceInfo(device),
_srtp(nullptr),
_arpc(nullptr),
_device(device),
_channel_num(channel_num)
{

}

Device::~Device()
{
    _queue->disable();
}

threading::Future<void>::ptr Device::connect(const std::shared_ptr<const arpc::ProtoStore>& proto_store)
{
    return _device->connect()
    ->then_async([this, proto_store](std::future<void>& f){
        f.get();
        if(_arpc == nullptr)
        {
            auto ep = _device->open_endpoint_for_channel(_channel_num);
            _srtp = std::make_unique<srtp::SrtpEndpoint>(std::move(ep));
            _arpc = std::make_unique<arpc::ArpcEndpoint>(_srtp->open());
        }
        _arpc->set_proto_store(proto_store);
    }, _queue);
}

threading::Future<void>::ptr Device::disconnect()
{
   return _device->disconnect()
    ->then([this](std::future<void>& f){
        f.get();
        _srtp = nullptr;
        _arpc = nullptr;
    }, _queue);
}

threading::Future<json_t>::ptr Device::rpc(const std::string& service_name, const std::string& method_name, const json_t& parameters)
{
    if((_device->status() != transport::DeviceStatus::Connected) || (_arpc == nullptr))
    {
        return threading::make_rejected_future<json_t>(std::make_exception_ptr(std::runtime_error("Device not connected")));
    }
    
    return _arpc->call_method_json(service_name, method_name, parameters);
}
