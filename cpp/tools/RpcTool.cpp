//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  rpc_tool.cpp
//  song2
//
//  Created by Donatien Garnier on 20/03/2018.

//

#include <iostream>
#include <memory>
#include <fstream>
#include <streambuf>

#include "cpp/js/JsContext.hpp"

#include "cpp/platform/Platform.hpp"

#include "cpp/threading/Queue.hpp"
#include "cpp/threading/BlockingDispatcher.hpp"

#include "cpp/srtp/SrtpEndpoint.hpp"
#include "cpp/arpc/ArpcEndpoint.hpp"
#include "cpp/arpc/ArpcException.hpp"
#include "cpp/arpc/ProtoStore.hpp"

#include "cpp/device/Device.hpp"

#include "cpp/transport/in_memory/InMemoryScanner.hpp"

#include "test/cpp/dummy_device/DummyDevice.hpp"

#include "gen/djinni/cpp/daemon.hpp"

// This is a private API - if using a custom-built framework, can use the following
//#include <JavaScriptCore.framework/PrivateHeaders/JSRemoteInspector.h>

// If using macOS's shipped framework, used supplied header
#include "cpp/3rdparty/JSRemoteInspector.h"

using namespace arm::song2::platform;
using namespace arm::song2;

//typename threading::BlockingDispatcher::ptr _dptch;
std::shared_ptr<test::DummyDevice> _device;
std::unique_ptr<transport::InMemoryScanner> _scanner;
std::unique_ptr<js::JsContext> _ctx;
std::shared_ptr<device::Device> _js_dev;

static std::string get_script(const std::string& filepath)
{
    // Open filename (in the future, this could be a full URL?)
    auto path = platform::platform_factory()->get_resource_path(filepath);
    
    std::ifstream filestream(path);
    if(filestream.bad())
    {
        // Could not open file
        throw std::runtime_error("Could not open file");
    }
    
    std::string script;
    
    filestream.seekg(0, std::ios::end);
    script.reserve(filestream.tellg());
    filestream.seekg(0, std::ios::beg);
    
    script.assign((std::istreambuf_iterator<char>(filestream)),
                  std::istreambuf_iterator<char>());
    
    return script;
}

static void register_dummy_device()
{
    transport::DeviceIdentifier id
    {
        std::string("https://mbed.com/d/my-device")
    };
    _scanner->register_device(id, [](uint32_t channel_num, pipe::Endpoint::ptr&& ep)
                              {
                                  _device->new_channel(channel_num, std::move(ep));
                              },
                              [](){
                                  
                              });
}


int32_t arm::song2::platform::Daemon::run(const std::vector<std::string>& args, const std::shared_ptr<PlatformFactory> & platform_factory)
{
    // Arg 0: Protobuf file
    // Arg 1: JS File to execute initially
    if(args.size() < 1)
    {
        std::cerr << "Not enough arguments" << std::endl;
        return 1;
    }
    
    // com.apple.security.get-task-allow needs to be set to YES in entitlements file for this to work
    JSRemoteInspectorSetLogToSystemConsole(true);
    
    // This will enable the Remote WebInspector (can be used with Safari)
    JSRemoteInspectorSetInspectionEnabledByDefault(true);
    
    platform::set_platform_factory(platform_factory);
    
    // No 'main' dispatcher as NSApplicationMain has not been called
    //_dptch = threading::make_blocking_dispatcher();
    
//    auto platform_dptch = platform_factory->create_dispatcher("JS");
    auto platform_dptch = platform_factory->get_main_dispatcher();
    
    auto queue = threading::make_queue(platform_dptch);
    queue->post([](){
        std::shared_ptr<arpc::ProtoStore> proto_store = nullptr;
        
        _device = std::make_shared<test::DummyDevice>();
        _scanner = std::make_unique<transport::InMemoryScanner>();
        
//        _ctx->global_object()->set_property("exit", [&](){
//            _dptch->terminate(false);
//        });
        
        // Recover blob file
        auto blob_file = platform::platform_factory()->get_resource_path("blob.pb");
        
        // Instantiate proto store
        proto_store = std::make_shared<arpc::ProtoStore>();
        
        // Register blob
        proto_store->register_services_from_files({blob_file});
        
        // Recover device
        register_dummy_device();
        
        _scanner->visible_devices()->next([&proto_store](std::future<std::shared_ptr<transport::Device>>& dev_fut){
        });
        

    });
    
    platform_dptch->start();
    
    platform_factory->run_main_dispatcher();
    
    
    return 0;
}

