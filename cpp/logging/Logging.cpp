//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

/*
 * logging.cpp
 *
 *  Created on: 11 Jan 2017
 *      Author: donatiengarnier
 */

#if __ANDROID__
#include <android/log.h>
#else
#include <iostream>
#endif
#include "cpp/logging/Logging.hpp"

using namespace arm::song2::logging;
using namespace arm::song2::logging::details;

Logger::Logger()
{

}

std::shared_ptr<Logger> Logger::inst()
{
    static std::shared_ptr<Logger> pLogger = std::make_shared<Logger>();
    return pLogger;
}

void Logger::log(size_t level, const std::string& str)
{
    std::unique_lock<std::mutex> lock(_mtx);
#if __ANDROID__
    int prio;
    switch(level)
    {
        case 0:
            prio = ANDROID_LOG_DEBUG;
            break;
        case 1:
            prio = ANDROID_LOG_WARN;
            break;
        case 2:
        default:
            prio = ANDROID_LOG_ERROR;
            break;
    }
    __android_log_write(prio, "SONG2", str.c_str());
#else
    switch(level)
    {
        case 0:
            std::cout << "[DEBUG] ";
            break;
        case 1:
            std::cout << "[WARN] ";
            break;
        case 2:
            std::cout << "[ERR] ";
            break;
    }

    std::cout << str << std::endl;
#endif
}
