//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

/*
 * logging.hpp
 *
 *  Created on: 11 Jan 2017
 *      Author: donatiengarnier
 */

#pragma once

#include <sstream>
#include <string>
#include <memory>
#include <mutex>
#include <sstream>

namespace arm
{
    namespace song2
    {
        namespace logging
        {
            namespace details
            {
                template<size_t level>
                class SafeStream;
                
                class Logger
                {
                public:
                    Logger();
                    
                protected:
                    friend SafeStream<0> ;
                    friend SafeStream<1> ;
                    friend SafeStream<2> ;
                    
                    static std::shared_ptr<Logger> inst();
                    
                private:
                    void log(size_t level, const std::string& str);
                    
                    std::mutex _mtx;
                };
                
                template<size_t level>
                class SafeStream: public std::ostream
                {
                public:
                    SafeStream() :
                    std::ostream(&_str_buf), _logger(Logger::inst())
                    {
                        if (_logger == nullptr)
                        {
                            throw std::runtime_error("nullptr logger");
                        }
                    }
                    virtual ~SafeStream()
                    {
                        _logger->log(level, _str_buf.str());
                    }
                private:
                    std::stringbuf _str_buf;
                    std::shared_ptr<Logger> _logger;
                };
            }
            
            using debug = details::SafeStream<0>;
            using warn = details::SafeStream<1>;
            using err = details::SafeStream<2>;
            
        }
    }
}

