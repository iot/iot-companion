//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Promise.cpp
//  song2
//
//  Created by Donatien Garnier on 02/01/2018.

//

#include "Promise.hpp"

using namespace arm::song2::threading;

typename Future<void>::ptr arm::song2::threading::make_resolved_future()
{
    Promise<void> prom;
    prom.resolve();
    return prom.get_future();
}

