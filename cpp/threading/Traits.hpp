//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Traits.hpp
//  song2
//
//  Created by Donatien Garnier on 05/01/2018.

//

#pragma once

#include <future>
#include <memory>
#include <tuple>

namespace arm
{
    namespace song2
    {
        namespace threading
        {
            template <typename T>
            class Future;
            
            namespace traits {
                template<typename X, typename Ret, typename... Args>
                std::tuple<Args...>
                functor_traits_args_helper (Ret (X::*)(Args...) const);
                
                template<typename X, typename Ret, typename... Args>
                std::tuple<Args...>
                functor_traits_args_helper (Ret (X::*)(Args...));
                
                template<typename X>
                struct functor_traits
                {
                    using args_tuple = decltype( functor_traits_args_helper(&X::operator()) );
                    using args_count = typename std::tuple_size<args_tuple>;
                    using args_sequence = std::make_index_sequence<args_count{}>;
                    
                    using return_type = decltype( &X::operator() );

                    template <size_t Idx>
                    using args_element = typename std::tuple_element<Idx, args_tuple>::type;
                };
                
                // For lambdas, etc
                template<typename X, typename T>
                struct callback_traits
                {
                    using is_valid = std::true_type; // todo, should only be true if it's actually a callable with the required signature
                    using return_type = decltype(std::declval<X&>()(std::declval<std::future<T>&>()));
                };
                

                template <typename T>
                struct future_traits
                {
                    using is_future = std::false_type;
                    using underlying_type = T;
                };
                
                template <typename T>
                struct future_traits<std::shared_ptr<Future<T>>>
                {
                    using is_future = std::true_type;
                    using underlying_type = T;
                };
                
                template<typename T, typename Clazz, typename Return, typename... Args>
                struct callback_traits<Return(Clazz::*)(Args...), T>
                {
                    using is_valid = std::true_type;
                    using return_type = Return;
                };
            }

        }
    }
}
