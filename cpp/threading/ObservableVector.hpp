//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  ObservableVector.hpp
//  song2
//
//  Created by Donatien Garnier on 12/04/2018.

//

#pragma once

#include <list>
#include <functional>
#include <vector>

#include "Observable.hpp"
#include "Queue.hpp"

namespace arm
{
    namespace song2
    {
        namespace threading
        {
            /** \addtogroup Threading
             *  @{
             */
            /** \defgroup ObservablesSubscribers Observables & Subscribers
             * Some utilities for basic functional programming.
             *  @{
             */
            
            /** A Base ObservableVector class which implements methods shared by the generic and void specializations.
             */
            template <typename T>
            class BaseObservableVector {
            public:
                /** Create a new ObservableVector
                 * \param initializer a function that can be used to populate any new observables with initial values
                 */
                BaseObservableVector(const std::function<void(Observable<T>&)>& initializer = nullptr) : _queue(threading::make_queue()), _completed(false), _initializer(initializer)
                {
                    
                }
                            
                virtual ~BaseObservableVector()
                {
                    _queue->disable();
                }
                
                /** Get a new subscriber associated with a new observable which will be registered with this vector
                 * \return the associated subscriber object
                 */
                std::shared_ptr<Subscriber<T>> get_subscriber() const
                {
                    Queue::assert_threadsafe(_queue);
                    _observables.emplace_back();
                    auto& obs = _observables.back();
                    obs.set_cancelation([this, obs_ptr = &obs](){
                        _queue->post([this, obs_ptr](){
                            auto cit = std::find_if(_observables.cbegin(), _observables.cend(), [obs_ptr](auto& obs){
                                return &obs == obs_ptr;
                            });
                            if(cit != _observables.cend())
                            {
                                _observables.erase(cit);
                            }
                        });
                    });
                    
                    // Get initial content (if applicable)
                    if(_initializer != nullptr)
                    {
                        _initializer(obs);
                    }
                    
                    return obs.get_subscriber();
                }
                
                /** Manually mark all observable streams as complete
                 */
                void complete()
                {
                    Queue::assert_threadsafe(_queue);
                    _completed = true;
                    for(auto& obs: _observables)
                    {
                        obs.complete();
                    }
                }
                
                /** Close all observables using a specific exception
                 * \param e an exception pointer to forward to the subscriber
                 */
                void error(std::exception_ptr e)
                {
                    Queue::assert_threadsafe(_queue);
                    _completed = true;
                    for(auto& obs: _observables)
                    {
                        obs.error(e);
                    }
                }
                
                /** Check whether the observable has already completed
                 * \return the completion status
                 */
                bool has_completed() const
                {
                    return _completed;
                }
                
            protected:
                threading::Queue::ptr _queue;
                mutable std::list<Observable<T>> _observables;
                bool _completed;
                std::function<void(Observable<T>&)> _initializer;
            };
            
            /** An Observable 'vector' which allows to push content to multiple subscribers.
             * \note might be more efficient to implement using std::shared_future
             */
            template <typename T>
            class ObservableVector : public BaseObservableVector<T>
            {
            public:
                using BaseObservableVector<T>::BaseObservableVector;
                virtual ~ObservableVector(){}
                
                /** Send next value
                 * \param t the (copyable) value to pass to the subscribers
                 */
                void next(const T& t)
                {
                    Queue::assert_threadsafe(BaseObservableVector<T>::_queue);
                    for(auto& obs: this->_observables)
                    {
                        obs.next(t);
                    }
                }
            };
            
            /** An Observable 'vector' which allows to push content to multiple subscribers - void specialization.
             * \note might be more efficient to implement using std::shared_future
             */
            template <>
            class ObservableVector<void> : public BaseObservableVector<void>
            {
            public:
                using BaseObservableVector<void>::BaseObservableVector;
                virtual ~ObservableVector(){}
                
                /** Send next value
                 */
                void next()
                {
                    Queue::assert_threadsafe(BaseObservableVector<void>::_queue);
                    for(auto& obs: _observables)
                    {
                        obs.next();
                    }
                }
            };
            
            /** A Subject records all values emitted so that any subscriber will get the same sequence of data, no matter when they do subscribe
             */
            template <typename T>
            class Subject : protected ObservableVector<T>
            {
            public:
                /** Construct a subject from an Observable's subscriber
                 * \param subs the subscriber to use
                 */
                Subject(const std::shared_ptr<Subscriber<T>>& subs) : ObservableVector<T>([this](Observable<T>& obs){
                    for(auto& item : _previous_values)
                    {
                        obs.next(item);
                    }
                    if(_exc != nullptr)
                    {
                        obs.error(_exc);
                    }
                }), _exc(nullptr)
                {
                    subs->next([this](std::future<T>& fut_value){
                        try {
                            auto value = fut_value.get();
                            _previous_values.push_back(value);
                            ObservableVector<T>::next(value);
                        }
                        catch(ObservableCompletedException&)
                        {
                            ObservableVector<T>::complete();
                            _exc = std::current_exception();
                        }
                        catch(...)
                        {
                            ObservableVector<T>::error(std::current_exception());
                            _exc = std::current_exception();
                        }
                    }, threading::Queue::current());
                }
                
                virtual ~Subject(){}
                
                using ObservableVector<T>::get_subscriber;
                
            private:
                std::vector<T> _previous_values;
                std::exception_ptr _exc;
            };
            
            
            /** @}*/
            /** @}*/
        }
    }
}
