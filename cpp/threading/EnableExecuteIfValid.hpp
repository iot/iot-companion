//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  RefCount.hpp
//  song2
//
//  Created by Donatien Garnier on 11/01/2018.

//

#pragma once

#include <memory>
#include "Traits.hpp"

namespace arm {
    namespace song2 {
        namespace threading {
            /** \addtogroup Threading
             *  @{
             */
            /** \defgroup ConditionalExecution Conditional Execution
             * A safety barrier to only execute code if an object is still valid
             *  @{
             */
            
            /** An utility class that can be derived from to enable conditional execution
             * \note This is not meant to be thread-safe (useful if they're used on the same queue)
             */
            class EnableExecuteIfValid
            {
            public:
                EnableExecuteIfValid() : _ref_flag(std::make_shared<bool>())
                {
                    *_ref_flag = true;
                }
                
                ~EnableExecuteIfValid()
                {
                    *_ref_flag = false;
                }
                
                /** Wrap a lambda
                 *
                 * The lambda will only be executed if this class is still instantiated
                 * \param x the lambda to wrap
                 * \return a lambda that can be safely called even if this class' instance has gone away (calling this returned lambda will call x is this instance is valid, otherwise it won't do anything
                 */
                template <typename X>
                auto execute_if_valid(X&& x)
                {
                    return [x = std::move(x), flag = _ref_flag](auto&... args){
                        if(*flag)
                        {
                            x(args...);
                        }
                    };
                }
                
            private:
                std::shared_ptr<bool> _ref_flag;
            };
            /** @}*/
            /** @}*/
        }
    }
}
