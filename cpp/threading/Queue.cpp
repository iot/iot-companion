//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Queue.cpp
//  song2
//
//  Created by Donatien Garnier on 02/01/2018.

//

#include "Queue.hpp"
#include "cpp/logging/Logging.hpp"
#include <cassert>

using namespace arm::song2::threading;
using namespace arm::song2;

Queue::Queue(const std::shared_ptr<platform::Dispatcher>& dispatcher) : _enabled(true), _dispatcher(dispatcher), _first_delayed_item(_queue.cend())
{
    // Make sure current queue is initialized in any case
    Queue::current();
}

Queue::Queue() : _enabled(true), _first_delayed_item(_queue.cend())
{
    auto current_queue = Queue::current();
    if(current_queue == nullptr)
    {
        throw std::runtime_error("No current queue");
    }
    
    auto dispatcher = current_queue->dispatcher();
    if(dispatcher == nullptr)
    {
        throw std::runtime_error("No current dispatcher");
    }
    
    _dispatcher = dispatcher;
}

Queue::~Queue()
{
    
}

void Queue::disable()
{
    std::lock_guard<decltype(_mtx)> lock(_mtx);
    _enabled = false;
    _queue.clear();
}

void Queue::enable()
{
    std::lock_guard<decltype(_mtx)> lock(_mtx);
    _enabled = true;
}

bool Queue::empty() const
{
    std::lock_guard<decltype(_mtx)> lock(_mtx);
    return _queue.empty();
}

std::chrono::milliseconds Queue::delay() const
{
    std::lock_guard<decltype(_mtx)> lock(_mtx);
    if(_queue.empty())
    {
        return std::chrono::milliseconds::max();
    }
    else
    {
        auto now = std::chrono::steady_clock::now();
        if(_queue.front().tp > now)
        {
            auto delay = _queue.front().tp - now;
            return std::chrono::duration_cast<std::chrono::milliseconds>(delay);
        }
        else
        {
            return std::chrono::milliseconds::zero();
        }
    }
}

void Queue::post( std::function<void()>&& fn )
{
    do_post( std::move(fn), std::chrono::milliseconds::zero() );
}

void Queue::post( const std::function<void()>& fn )
{
    auto fn_cpy = fn;
    do_post( std::move(fn_cpy), std::chrono::milliseconds::zero() );
}

void Queue::post_delayed( std::function<void()>&& fn, std::chrono::milliseconds duration )
{
    do_post( std::move(fn), duration );
}

void Queue::post_delayed( const std::function<void()>& fn, std::chrono::milliseconds duration )
{
    auto fn_cpy = fn;
    do_post( std::move(fn_cpy), duration );
}

void Queue::do_post(std::function<void()>&& fn, std::chrono::milliseconds duration)
{
    std::lock_guard<decltype(_mtx)> lock(_mtx);
    
    if(!_enabled)
    {
        return; // Drop work
    }
    
    if(duration != std::chrono::milliseconds::zero())
    {
        std::chrono::steady_clock::time_point time_point = std::chrono::steady_clock::now() + duration;
        // Find correct position in queue
        // This will return an iterator to the first queued item that needs to be executed after this work item (or past the end iterator)
        auto cit = std::find_if(_first_delayed_item, _queue.cend(), [&](const work_unit_t& dfn){
            return time_point < dfn.tp;
        });
        
        // Will insert element before cit
        auto new_cit = _queue.insert(cit, work_unit_t {std::move(fn), time_point});
        
        if( cit == _first_delayed_item )
        {
            // Our item is the earliest delayed item
            _first_delayed_item = new_cit;
            
            // Notify is expensive, so only do it if useful
            // (if there's an earlier event queued, or if queue is being processed now, no need to re-notify)
            if((new_cit == _queue.cbegin()) && (this != current().get()) && (_dispatcher != nullptr))
            {
                bool result = _dispatcher->notify( shared_from_this(), duration.count() );
                if(!result)
                {
                    // Dispatcher is terminating or has terminated
                    _dispatcher = nullptr;
                }
            }
        }
    }
    else
    {
        auto new_cit = _queue.insert(_first_delayed_item, work_unit_t {std::move(fn), std::chrono::steady_clock::time_point::min()});
        
        // Notify is expensive, so only do it if useful
        // (if there's already an immediate event queued, or if queue is being processed now, no need to re-notify)
        if((new_cit == _queue.cbegin()) && (this != current().get()) && (_dispatcher != nullptr))
        {
            bool result = _dispatcher->notify( shared_from_this(), 0 );
            if(!result)
            {
                // Dispatcher is terminating or has terminated
                _dispatcher = nullptr;
            }
        }
    }
}


/** Work jobs, run on native queue */
int64_t Queue::process()
{
    Queue::ptr previous_queue;
    work_unit_t work;
    {
        std::lock_guard<decltype(_mtx)> lock(_mtx);
        // Abort if disabled
        if(!_enabled)
        {
            return -1;
        }
        
        // Set ourselves as the currently running queue
        previous_queue = _tls_current_queue;
        _tls_current_queue = shared_from_this();
    }
    
    while(true)
    {
        {
            std::lock_guard<decltype(_mtx)> lock(_mtx);
            if(_queue.empty() || (_dispatcher == nullptr) || !_enabled)
            {
                _tls_current_queue = previous_queue;
                return -1; // Wait forever
            }
            
            auto now = std::chrono::steady_clock::now();
            if(now < _queue.front().tp)
            {
                auto delay = _queue.front().tp - now;

                _tls_current_queue = previous_queue;
                return std::chrono::duration_cast<std::chrono::milliseconds>(delay).count(); // Wait for a bit
            }
            
            // Adjust first delayed item if necessary
            if(_queue.cbegin() == _first_delayed_item)
            {
                _first_delayed_item++;
            }
            
            work = std::move(_queue.front());
            _queue.erase(_queue.cbegin());
        }
        
        try {
            // Do not keep lock while executing
            work.func();
        }
        catch(std::exception& e)
        {
            logging::err() << "Exception thrown but not caught in queue " << this << ": " << e.what();
            std::terminate();
        }
        catch(...)
        {
            logging::err() << "Unknown exception thrown but not caught in queue" << this;
            std::terminate();
        }
    }
}

void Queue::set_dispatcher(const std::shared_ptr<platform::Dispatcher>& dispatcher)
{
    std::lock_guard<decltype(_mtx)> lock(_mtx);
    assert(_dispatcher == nullptr);
    _dispatcher = dispatcher;
}

std::shared_ptr<platform::Dispatcher> Queue::dispatcher() const
{
    std::lock_guard<decltype(_mtx)> lock(_mtx);
    return _dispatcher;
}

thread_local Queue::ptr Queue::_tls_current_queue = nullptr;

// Warning: Only != nullptr if called from function run on a queue!
const Queue::ptr& Queue::current()
{
    // Make sure _tls_current_queue gets initialized on first run
    static int init = [](){ Queue::_tls_current_queue = nullptr; return 0; }();
    (void)init;
    return Queue::_tls_current_queue;
}

// Use at own risks!
void Queue::set_current(const Queue::ptr& queue)
{
    Queue::_tls_current_queue = queue;
}

void Queue::assert_threadsafe(const Queue::ptr& queue)
{
    assert(_tls_current_queue != nullptr);
    assert(queue->dispatcher()->is_equal(_tls_current_queue->dispatcher()));
}

typename Queue::ptr arm::song2::threading::make_queue()
{
    return std::make_shared<Queue>();
}

typename Queue::ptr arm::song2::threading::make_queue(const std::shared_ptr<platform::Dispatcher>& dispatcher)
{
    return std::make_shared<Queue>(dispatcher);
}
