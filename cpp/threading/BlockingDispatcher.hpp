//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  BlockingDispatcher.hpp
//  song2
//
//  Created by Donatien Garnier on 03/01/2018.

//

#pragma once

#include <mutex>
#include <vector>
#include <chrono>
#include <condition_variable>

#include "gen/djinni/cpp/dispatcher.hpp"
#include "gen/djinni/cpp/dispatcher_status.hpp"

namespace arm
{
    namespace song2
    {
        namespace threading
        {
            /** \addtogroup Threading
             *  @{
             */
            /** \addtogroup Dispatchers
             * Dispatchers execute units of work present in a queue
             *  @{
             */
            /** \defgroup BlockingDispatcher Blocking Dispatcher
             *  @{
             */
            
            /** A generic, platform-independent dispatcher which will synchronously execute work and block until all work queued is executed
             */
            class BlockingDispatcher : public platform::Dispatcher
            {
            public:
                using ptr = std::shared_ptr<BlockingDispatcher>;
                
                BlockingDispatcher();
                virtual ~BlockingDispatcher();
                
                virtual bool is_equal(const std::shared_ptr<Dispatcher> & other) override;
                
                virtual platform::DispatcherStatus status() override;
                
                /** Returns true if it was actually started */
                virtual bool start() override;
                
                /** Must be called to run actual work
                 */
                void process();
                
                /** Block till work is available
                 */
                void wait();
                
                /**
                 * Returns true if it was actually terminated
                 * If kill is false, pending work will be completed (and the method will block)
                 */
                virtual bool terminate(bool kill) override;
                
                /**
                 * Called by library to indicate that there is work to do in the queue (if so, when)
                 * A dispatcher can handle multiple queues
                 * Returns true if work was queued successfully
                 */
                virtual bool notify(const std::shared_ptr<platform::Queue> & queue, int64_t delay_ms) override;
                
            private:
                std::vector<std::shared_ptr<platform::Queue>> _pending_queues;
                platform::DispatcherStatus _status;
                std::chrono::steady_clock::time_point _next_exec;
                std::mutex _mtx;
                std::condition_variable _cond_var;
            };
            
            /** A factory function to make a blocking dispatcher
             \return A BlockingDispatcher::ptr instance
             */
            typename BlockingDispatcher::ptr make_blocking_dispatcher();
            
            /** @}*/
            /** @}*/
            /** @}*/
        }
    }
}
