//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Promise.hpp
//  song2
//
//  Created by Donatien Garnier on 02/01/2018.

//

#pragma once

#include "Queue.hpp"
#include "Traits.hpp"

#include "cpp/logging/Logging.hpp"

#include <memory>
#include <future>
#include <mutex>
#include <cassert>
#include <vector>
#include <functional>

namespace arm
{
    namespace song2
    {
        namespace threading
        {
            /** \addtogroup Threading
             *  @{
             */
            /** \defgroup PromisesFutures Promises & Futures
             * A resolvable contract for asynchronous operations.
             *  @{
             */
            template <typename T>
            class Subscriber;
            
            template <typename T>
            class Future;
            
            /** An exception that is raised in a future when a promise is destroyed before being resolved
             */
            class PromiseNotResolvedException : public std::runtime_error
            {
            public:
                using std::runtime_error::runtime_error;
            };
            
            namespace details {
                class AbstractPromise { public: ~AbstractPromise(){}};
                
                /** A Base Promise class which implements methods shared by the generic and void specializations.
                 */
                template <typename T>
                class BasePromise : public AbstractPromise
                {
                public:
                    BasePromise() : _future( std::make_shared<Future<T>>(_std_promise.get_future()) ), _future_retrieved(false), _resolved(false)
                    {
                        
                    }
                    
                    BasePromise(BasePromise&&) = default;
                    
                    virtual ~BasePromise()
                    {
                        // Catch exception there?
                        auto e = std::current_exception();
                        if( !_resolved && _future_retrieved )
                        {
                            if( e == nullptr )
                            {
                                e = std::make_exception_ptr(PromiseNotResolvedException("Promise was destroyed without having been resolved"));
                            }
                            reject(e);
                        }
                    }
                    
                    /** Get future associated with this promise
                     * \return the associated future object
                     */
                    std::shared_ptr<Future<T>> get_future()
                    {
                        _future_retrieved = true;
                        return _future;
                    }
                    
                    /** Reject the promise using a specific exception
                     * \param e an exception pointer to forward to the future
                     */
                    void reject(std::exception_ptr e)
                    {
                        assert(!_resolved);
                        _resolved = true;
                        _std_promise.set_exception(e);
                        notify_future();
                    }
                    
                    /** Check whether the promise has been resolved
                     * \return a boolean set to true if this promise has already been resolved
                     */
                    bool resolved() const
                    {
                        return _resolved;
                    }
                    
                protected:
                    void notify_future();
                    
                    std::promise<T> _std_promise;
                    std::shared_ptr<Future<T>> _future;
                    bool _future_retrieved;
                    bool _resolved;
                };
            }
            
            // Push-end
            /** A Promise is on the "push-end" of a promise/future contract.
             *
             * A promise can be used to asynchronously resolve a contract with a future object.
             * We extend standard C++ promises/futures by providing a callback mecanism and continuations.
             * If the promise is destroyed before being resolved, then it will be rejected using the current exception, or if no exception has been thrown, using a PromiseNotResolvedException exception
             */
            template <typename T>
            class Promise : public details::BasePromise<T>
            {
            public:
                Promise() = default;
                Promise(Promise&&) = default;
                virtual ~Promise() {};

                /** Resolve or reject promise using provided standard C++ future
                 * \param fut the future to use to resolve the promise
                 * The promise will be rejected if the future throws
                 */
                void set(std::future<T>& fut)
                {
                    try {
                        this->resolve(fut.get());
                    } catch(...) {
                        this->reject(std::current_exception());
                    }
                }
                
                /** Resolve promise
                 * \param t the (copyable) value to resolve the promise with
                 */
                void resolve(const T& t)
                {
                    assert(!this->_resolved);
                    this->_resolved = true;
                    this->_std_promise.set_value(t);
                    this->notify_future();
                }
                
                /** Resolve promise
                 * \param t the (movable) value to resolve the promise with
                 */
                void resolve(T&& t)
                {
                    assert(!this->_resolved);
                    this->_resolved = true;
                    this->_std_promise.set_value(std::move(t));
                    this->notify_future();
                }
            };
            
            /** A Promise specialization for void
             */
            template <>
            class Promise<void> : public details::BasePromise<void>
            {
            public:
                Promise() = default;
                Promise(Promise&&) = default;
                virtual ~Promise() {};
                
                void set(std::future<void>& fut)
                {
                    try {
                        fut.get();
                        this->resolve();
                    } catch(...) {
                        this->reject(std::current_exception());
                    }
                }
                
                void resolve()
                {
                    assert(!this->_resolved);
                    this->_resolved = true;
                    this->_std_promise.set_value();
                    this->notify_future();
                }
            };
            
            /** A Future is on the "pull-end" of a promise/future contract.
             *
             * A future can be used to asynchronously retrieve the result of a contract with a promise object.
             * We extend standard C++ promises/futures by providing a callback mecanism and continuations.
             */
            template <typename T>
            class Future : public std::enable_shared_from_this<Future<T>>
            {
            public:
                Future(std::future<T>&& future) : _resolver(nullptr), _queue(nullptr), _future(std::move(future)), _ready(false), _async(false)
                {}
                
                virtual ~Future() {
                    if(_ready && _future.valid())
                    {
                        try {
                            _future.get();
                        } catch(PromiseNotResolvedException&) {
                            
                        }
                        catch(std::exception& e)
                        {
                            logging::err() << "Exception thrown but not re-thrown in Future " << this << ": " << e.what();
                            std::terminate();
                        }
                        catch(...)
                        {
                            logging::err() << "Unknown exception thrown but not re-thrown in Future" << this;
                            std::terminate();
                        }
                    }
                }

                using ptr = std::shared_ptr<Future<T>>;
                
                /** Process the promise's result
                 *
                 * The lambda will be called on the promise caller's thread or on the current thread if it was already resolved
                 * \param resolver a lambda function with the following signature: (std::future<T>&) -> U
                 * \param queue the queue to call the callback on
                 * \return a continuation of type threading::Future<U>::ptr
                 */
                template <typename X>
                typename std::enable_if<
                    traits::callback_traits<X, T>::is_valid::value,
                    typename Future<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::ptr
                >::type
                then(X&& resolver, const Queue::ptr& queue /*= Queue::current()*/) // On caller's thread
                {
                    std::lock_guard<decltype(_mtx)> lock(_mtx);
                    
                    _async = false;
                    _queue = queue;

                    auto continuation_future = make_continuation(std::move(resolver));
                    
                    if(_ready)
                    {
                        // Resolve now
                        if(queue != Queue::current())
                        {
                            queue->post(std::move(_resolver));
                        }
                        else
                        {
                            _resolver();
                        }
                    }
                    
                    return continuation_future;
                }
                
                /** Process the promise's result
                 *
                 * The lambda will be called on the provided queue which defaults to the queue that method is being run on
                 * \param resolver a lambda function with the following signature: (std::future<T>&) -> U
                 * \param queue the queue to call the callback on
                 * \return a continuation of type threading::Future<U>::ptr
                 */
                template <typename X>
                typename std::enable_if<
                    traits::callback_traits<X, T>::is_valid::value,
                    typename Future<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::ptr
                >::type
                then_async(X&& resolver, const Queue::ptr& queue = Queue::current())
                // On current queue by default
                {
                    assert(queue != nullptr);
                    std::lock_guard<decltype(_mtx)> lock(_mtx);

                    _async = true;
                    _queue = queue;
                    
                    auto continuation_future = make_continuation(std::move(resolver));

                    if(_ready)
                    {
                        // Resolve now
                        queue->post(std::move(_resolver));
                    }
                    
                    return continuation_future;
                }

            protected:
                friend class details::BasePromise<T>;
                friend class Promise<T>;
                friend class Subscriber<T>;

                void notify()
                {
                    std::lock_guard<decltype(_mtx)> lock(_mtx);
                    _ready = true;
                    if( _resolver == nullptr )
                    {
                        return;
                    }
                    if( (_queue != nullptr) && (_async || (_queue != Queue::current()) ) )
                    {
                        _queue->post(std::move(_resolver));
                    }
                    else
                    {
                        _resolver();
                    }
                }

            private:
                template <typename X>
                typename std::enable_if<
                !std::is_void<typename traits::callback_traits<X, T>::return_type>::value
                and !traits::future_traits<typename traits::callback_traits<X, T>::return_type>::is_future::value,
                typename Future<typename traits::callback_traits<X, T>::return_type>::ptr
                >::type
                make_continuation(X&& resolver)
                {
                    auto continuation = std::make_shared<Promise<typename traits::callback_traits<X, T>::return_type>>();
                    
                    auto future = continuation->get_future();
                    
                    _continuation = continuation;
                    
                    _resolver = [this, resolver = std::move(resolver), self = this->shared_from_this()]() mutable {
                        auto r_continuation = reinterpret_cast<decltype(continuation.get())>(_continuation.get());
                        try
                        {
                            r_continuation->resolve( resolver(_future) );
                        }
                        catch(...)
                        {
                            r_continuation->reject( std::current_exception() );
                        };
                        _resolver = nullptr; // Loose self reference
                    };
                    
                    return future;
                }
                
                template <typename X>
                typename std::enable_if<
                !std::is_void<typename traits::callback_traits<X, T>::return_type>::value
                 and traits::future_traits<typename traits::callback_traits<X, T>::return_type>::is_future::value
                 and !std::is_void<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::value,
                typename Future<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::ptr
                >::type
                make_continuation(X&& resolver)
                {
                    auto continuation = std::make_shared<Promise<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>>();
                    
                    auto future = continuation->get_future();
                    
                    _continuation = continuation;
                    
                    _resolver = [this, resolver = std::move(resolver), self = this->shared_from_this()]() mutable {
                        auto r_continuation = reinterpret_cast<decltype(continuation.get())>(_continuation.get());
                        try
                        {
                            resolver(_future)->then([r_continuation, self = this->shared_from_this(), this](std::future<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>& fut){
                                try
                                {
                                    r_continuation->resolve( fut.get() );
                                }
                                catch(...)
                                {
                                    r_continuation->reject( std::current_exception() );
                                };
                                _resolver = nullptr; // Loose self reference
                            }, _queue);
                        }
                        catch(...)
                        {
                            r_continuation->reject( std::current_exception() );
                            _resolver = nullptr; // Loose self reference
                        };
                    };
                    
                    return future;
                }
                
                template <typename X>
                typename std::enable_if<
                !std::is_void<typename traits::callback_traits<X, T>::return_type>::value
                and traits::future_traits<typename traits::callback_traits<X, T>::return_type>::is_future::value
                and std::is_void<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::value,
                typename Future<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::ptr
                >::type
                make_continuation(X&& resolver)
                {
                    auto continuation = std::make_shared<Promise<void>>();
                    
                    auto future = continuation->get_future();
                    
                    _continuation = continuation;
                    
                    _resolver = [this, resolver = std::move(resolver), self = this->shared_from_this()]() mutable {
                        auto r_continuation = reinterpret_cast<decltype(continuation.get())>(_continuation.get());
                        try
                        {
                            resolver(_future)->then([r_continuation, self = this->shared_from_this(), this](std::future<void>& fut){
                                try
                                {
                                    fut.get();
                                    r_continuation->resolve();
                                }
                                catch(...)
                                {
                                    r_continuation->reject( std::current_exception() );
                                };
                                _resolver = nullptr; // Loose self reference
                            }, _queue);
                        }
                        catch(...)
                        {
                            r_continuation->reject( std::current_exception() );
                            _resolver = nullptr; // Loose self reference
                        };
                    };
                    
                    return future;
                }
                
                template <typename X>
                typename std::enable_if<
                    std::is_void<typename traits::callback_traits<X, T>::return_type>::value,
                    typename Future<typename traits::callback_traits<X, T>::return_type>::ptr
                >::type
                make_continuation(X&& resolver)
                {
                    auto continuation = std::make_shared<Promise<void>>();

                    auto future = continuation->get_future();
                    
                    _continuation = continuation;
                    
                    _resolver = [this, resolver = std::move(resolver), self = this->shared_from_this()]() mutable {
                        auto r_continuation = reinterpret_cast<decltype(continuation.get())>(_continuation.get());
                        try
                        {
                            resolver(_future);
                            r_continuation->resolve();
                        }
                        catch(...)
                        {
                            r_continuation->reject( std::current_exception() );
                        };
                        _resolver = nullptr; // Loose self reference
                    };
                    
                    return future;
                }

                std::function<void()> _resolver;
                Queue::ptr _queue;
                std::future<T> _future;
                bool _ready;
                bool _async;
                std::shared_ptr<details::AbstractPromise> _continuation;
                std::mutex _mtx;
            };
           
            // Promise methods impl
            namespace details
            {
                template <typename T>
                void BasePromise<T>::notify_future()
                {
                    _future->notify();
                }
            }
            
            /** A factory to create a resolved future
             *
             * \param t the (copyable) value to use to resolve the promise
             * \return a resolved future holding t as a value
             */
            template <typename T>
            typename Future<std::decay_t<T>>::ptr make_resolved_future(const T& t)
            {
                Promise<std::decay_t<T>> prom;
                prom.resolve(t);
                return prom.get_future();
            }
            
            /** A factory to create a resolved future
             *
             * \param t the (movable) value to use to resolve the promise
             * \return a resolved future holding t as a value
             */
            template <typename T>
            typename Future<std::decay_t<T>>::ptr make_resolved_future(T&& t)
            {
                Promise<std::decay_t<T>> prom;
                prom.resolve(std::move(t));
                return prom.get_future();
            }
            
            /** A factory to create a rejected future
             *
             * \return a rejected future
             */
            template <typename T>
            typename Future<T>::ptr make_rejected_future(std::exception_ptr e)
            {
                Promise<std::decay_t<T>> prom;
                prom.reject(e);
                return prom.get_future();
            }
            
            /** A factory to create a resolved void future
             *
             * \return a resolved void future
             */
            typename Future<void>::ptr make_resolved_future();
            
            /** A utility function to wait on many futures to get resolved
             * \param futures a vector of futures to wait on
             * \param queue the queue on which to return the future
             * \return a future of vector of std::futures (no guarantee on which queue this will get resolved on)
             */
            template <typename T>
            typename Future<std::vector<std::future<T>>>::ptr wait_all(std::vector<typename Future<T>::ptr>& futures, const Queue::ptr& queue = Queue::current())
            {
                struct context_t {
                    std::vector<typename Future<T>::ptr> futures;
                    typename std::vector<typename Future<T>::ptr>::iterator futures_iterator;
                    std::vector<std::future<T>> results;
                    std::function<typename Future<std::vector<std::future<T>>>::ptr(std::future<T>&)> wait_for_next;
                };
                
                auto context = std::make_shared<context_t>();
                if(futures.size() == 0)
                {
                    return make_resolved_future(std::move(context->results));
                }
                
                context->futures = futures;
                context->futures_iterator = context->futures.begin();

                context->wait_for_next = [context, queue](std::future<T>& result){
                    context->results.push_back(std::move(result));
                    context->futures_iterator++;
                    if(context->futures_iterator == context->futures.end())
                    {
                        return make_resolved_future(std::move(context->results));
                    }
                    else
                    {
                        return (*(context->futures_iterator))->then(context->wait_for_next, queue);
                    }
                };
                
                return (*(context->futures_iterator))->then(context->wait_for_next, queue);
            }
            
            /** @}*/
            /** @}*/
        }
    }
}
