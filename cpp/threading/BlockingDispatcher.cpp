//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  BlockingDispatcher.cpp
//  song2
//
//  Created by Donatien Garnier on 03/01/2018.

//

#include <thread>
#include <boost/optional.hpp>
#include <algorithm>

#include "BlockingDispatcher.hpp"
#include "gen/djinni/cpp/queue.hpp"

using namespace arm::song2::threading;
using namespace arm::song2;

BlockingDispatcher::BlockingDispatcher() : _status(platform::DispatcherStatus::PENDING), _next_exec(std::chrono::steady_clock::time_point::max())
{
    
}

BlockingDispatcher::~BlockingDispatcher()
{
    
}

bool BlockingDispatcher::is_equal(const std::shared_ptr<Dispatcher> & other)
{
    return other.get() == this;
}

platform::DispatcherStatus BlockingDispatcher::status()
{
    std::lock_guard<decltype(_mtx)> lock(_mtx);
    return _status;
}

bool BlockingDispatcher::start()
{
    std::lock_guard<decltype(_mtx)> lock(_mtx);
    if(_status != platform::DispatcherStatus::PENDING)
    {
        return false;
    }
    
    _status = platform::DispatcherStatus::RUNNING;
    
    return true;
}

void BlockingDispatcher::process()
{
    std::vector<std::shared_ptr<platform::Queue>> pending_queues;
    while (true)
    {
        {
            // Retrieve pending queues
            std::lock_guard<decltype(_mtx)> lock(_mtx);
            if(_status == platform::DispatcherStatus::TERMINATED)
            {
                return;
            }
            
            // Check whether there's work to do now
            auto now = std::chrono::steady_clock::now();
            if(now < _next_exec)
            {
                // Give a chance to wait
                return;
            }
            
            pending_queues = std::move(_pending_queues);
            _pending_queues.clear();
            
            // Reset _next_exec
            _next_exec = std::chrono::steady_clock::time_point::max();
        }
        
        if(pending_queues.size() == 0)
        {
            // No more work
            break;
        }
        
        // Go through the list of queues
        for(auto& queue : pending_queues)
        {
            int64_t delay = queue->process();
            if(delay >= 0)
            {
                // Re-queue queue
                notify(queue, delay);
            }
        }
    }
}

void BlockingDispatcher::wait()
{
    std::unique_lock<decltype(_mtx)> lock(_mtx); // Have to use unique_lock for condition variable
    while(true)
    {
        if(_status >= platform::DispatcherStatus::TERMINATING)
        {
            return;
        }
        
        if(_pending_queues.size() == 0)
        {
            _next_exec = std::chrono::steady_clock::time_point::max();
        }
        
        // Wait - this will unlock the mutex
        _cond_var.wait_until(lock, _next_exec);
        
        // Mutex is acquired again
        if(_next_exec <= std::chrono::steady_clock::now())
        {
            // Work to do now
            return;
        }
        
        // This was either a spurious wake-up, or a notification _next_exec being updated, so continue
    }
}

bool BlockingDispatcher::terminate(bool kill)
{
    std::lock_guard<decltype(_mtx)> lock(_mtx);
    if(_status == platform::DispatcherStatus::TERMINATED)
    {
        return false;
    }
    
    // Shortcut to terminated
    _status = platform::DispatcherStatus::TERMINATED;
    return true;
}

bool BlockingDispatcher::notify(const std::shared_ptr<platform::Queue> & queue, int64_t delay_ms)
{
    std::lock_guard<decltype(_mtx)> lock(_mtx);
    if(_status <= platform::DispatcherStatus::RUNNING)
    {
        _pending_queues.push_back(queue);
        std::chrono::steady_clock::time_point tp;
        if(delay_ms >= 0)
        {
            tp = std::chrono::steady_clock::now() + std::chrono::milliseconds(delay_ms);
        }
        else
        {
            tp = std::chrono::steady_clock::time_point::max();
        }
        if(tp <= _next_exec)
        {
            _next_exec = tp;
            
            // Notify condition variable as we have work to do earlier than expected
            _cond_var.notify_one();
        }
        return true;
    }
    else
    {
        return false;
    }
}

typename BlockingDispatcher::ptr arm::song2::threading::make_blocking_dispatcher()
{
    return std::make_shared<BlockingDispatcher>();
}
