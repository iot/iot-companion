//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Observable.hpp
//  song2
//
//  Created by Donatien Garnier on 03/01/2018.

//

#pragma once

#include "Traits.hpp"
#include "Queue.hpp"
#include "Promise.hpp"

#include "cpp/logging/Logging.hpp"

#include <memory>
#include <future>
#include <mutex>
#include <queue>
#include <cassert>
#include <algorithm>

namespace arm
{
    namespace song2
    {
        namespace threading
        {
            /** \addtogroup Threading
             *  @{
             */
            /** \defgroup ObservablesSubscribers Observables & Subscribers
             * Some utilities for basic functional programming.
             *  @{
             */
            template <typename T>
            class Subscriber;
            
            /** An exception that is raised when an observable has been closed
             */
            struct ObservableCompletedException : public std::exception
            {
                explicit ObservableCompletedException();
                
                virtual const char* what() const noexcept override;
                
                // Has to be there otherwise exception rethrowing from exception_ptr fails
                virtual ~ObservableCompletedException();
            };
            
            namespace details {
                // Push-end
                class AbstractObservable {};

                /** A Base Observable class which implements methods shared by the generic and void specializations.
                 */
                template <typename T>
                class BaseObservable : public AbstractObservable
                {
                public:
                    BaseObservable() : _subscriber( std::make_shared<Subscriber<T>>(this, _std_promise.get_future()) ), _canceled(false), _completed(false),
                    _cancelation(nullptr)
                    {
                        
                    }
                    
                    void set_cancelation(std::function<void()>&& cancelation)
                    {
                        _cancelation = std::move(cancelation);
                    }
                    
                    void set_cancelation(const std::function<void()>& cancelation)
                    {
                        _cancelation = cancelation;
                    }
                    
                    virtual ~BaseObservable()
                    {
                        // The observable must be destroyed on the same dispatcher it's been created on
                        _subscriber->observable_destroyed();
                        
                        // Catch exception there?
                        auto e = std::current_exception();
                        if( !_completed )
                        {
                            if( e == nullptr )
                            {
                                e = std::make_exception_ptr(ObservableCompletedException());
                            }
                            error(e);
                        }
                    }
                    
                    /** Get subscriber associated with this observable
                     * \return the associated subscriber object
                     */
                    std::shared_ptr<Subscriber<T>> get_subscriber() const
                    {
                        return _subscriber;
                    }
                    
                    /** Manually mark the observable stream as complete
                     */
                    void complete()
                    {
                        assert(!_completed);
                        _completed = true;
                        if(_canceled) { return; }
                        auto e = std::make_exception_ptr(ObservableCompletedException());
                        _std_promise.set_exception(e);
                        notify_subscriber(std::future<T>());
                    }
                    
                    /** Close the observable using a specific exception
                     * \param e an exception pointer to forward to the subscriber
                     */
                    void error(std::exception_ptr e)
                    {
                        assert(!_completed);
                        _completed = true;
                        if(_canceled) { return; }
                        _std_promise.set_exception(e);
                        notify_subscriber(std::future<T>());
                    }
                    
                    /** Check whether the observable has already completed
                     * \return the completion status
                     */
                    bool has_completed() const
                    {
                        return _completed;
                    }
                    
                private:
                    friend class Subscriber<T>;
                    
                    /** Cancelation callback from subscriber
                     */
                    void cancel()
                    {
                        if(_cancelation != nullptr) {
                            _cancelation();
                        }
                    }
                    
                protected:
                    void notify_subscriber(std::future<T>&& fut);

                    std::promise<T> _std_promise;
                    std::shared_ptr<Subscriber<T>> _subscriber;
                    bool _canceled;
                    bool _completed;
                    std::function<void()> _cancelation;
                };
            }
            
            /** An Observable is on the "push-end" of an observable/subscriber relationship.
             *
             * An observable can be used to asynchronously send data to a Subscriber.
             * This is basically the "streaming" equivalent of the promise/future pair, and an implementation of the observer pattern.
             *
             */
            template <typename T>
            class Observable : public details::BaseObservable<T>
            {
            public:
                using details::BaseObservable<T>::BaseObservable;
                virtual ~Observable() {}
                
                /** Send next value
                 * \param t the (movable) value to pass to the subscriber
                 */
                void next(T&& t)
                {
                    assert(!this->_completed);
                    if(this->_canceled) { return; }
                    this->_std_promise.set_value(t);
                    this->_std_promise = std::promise<T>();
                    this->notify_subscriber(this->_std_promise.get_future());
                }
                
                /** Send next value
                 * \param t the (copyable) value to pass to the subscriber
                 */
                void next(const T& t)
                {
                    assert(!this->_completed);
                    if(this->_canceled) { return; }
                    this->_std_promise.set_value(t);
                    this->_std_promise = std::promise<T>();
                    this->notify_subscriber(this->_std_promise.get_future());
                }
            };
            
            /** An Observable specialization for void
             */
            template <>
            class Observable<void> : public details::BaseObservable<void>
            {
            public:
                using details::BaseObservable<void>::BaseObservable;
                virtual ~Observable() {};
                
                void next()
                {
                    assert(!_completed);
                    if(_canceled) { return; }
                    this->_std_promise.set_value();
                    this->_std_promise = std::promise<void>();
                    notify_subscriber(this->_std_promise.get_future());
                }
            };
            
            /** An Subscriber is on the "pull-end" of an observable/subscriber relationship.
             *
             * An subscriber can be used to asynchronously receive data from an observable.
             * This is basically the "streaming" equivalent of the promise/future pair, and an implementation of the observer pattern.
             *
             */
            template <typename T>
            class Subscriber : public std::enable_shared_from_this<Subscriber<T>>
            {
            public:
                Subscriber(details::BaseObservable<T>* observable, std::future<T>&& future) : _observable(observable), _resolver(nullptr), _queue(nullptr), _futures(), _canceled(false), _async(false)
                {
                    _futures.emplace(std::move(future));
                }
                
                ~Subscriber() {
                    while(!_futures.empty()) {
                        auto& future = _futures.front();
                        if(future.valid())
                        {
                            try {
                                future.get();
                            }
                            catch( ObservableCompletedException& )
                            {
                                // Swallow it, that's ok
                            }
                            catch(std::exception& e)
                            {
                                logging::err() << "Exception thrown but not re-thrown in Subscriber " << this << ": " << e.what();
                                std::terminate();
                            }
                            catch(...)
                            {
                                logging::err() << "Unknown exception thrown but not re-thrown in Subscriber" << this;
                                std::terminate();
                            }
                        }
                        _futures.pop();
                    }
                }
                
                using ptr = std::shared_ptr<Subscriber<T>>;
                
                /** Process the observable's values
                 *
                 * The lamba will be called on the observable caller's thread or on the current thread if there are pending items
                 * \param resolver a lambda function with the following signature: (std::future<T>&) -> U
                 * \param queue the queue to call the callback on
                 * \return a continuation subscriber of type threading::Future<U>::ptr
                 */
                template <typename X>
                typename std::enable_if<
                    traits::callback_traits<X, T>::is_valid::value,
                    typename Subscriber<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::ptr
                >::type
                next(X&& resolver, const Queue::ptr& queue /*= Queue::current()*/) // On caller's thread
                {
                    std::lock_guard<decltype(_mtx)> lock(_mtx);
                    
                    _async = false;
                    _queue = queue;

                    auto continuation_subscriber = make_continuation(std::move(resolver));

                    if(&_futures.front() != &_futures.back())// equivalent to (_futures.size() > 1) in constant time - basically is there already another future pending
                    {
                        // Resolve now
                        if(queue != Queue::current())
                        {
                            queue->post(_resolver);
                        }
                        else
                        {
                            _resolver();
                        }
                    }
                    
                    return continuation_subscriber;
                }
                
                /** Process the observable's values
                 *
                 * The lambda will be called on the provided queue which defaults to the queue that method is being run on
                 * \param resolver a lambda function with the following signature: (std::future<T>&) -> U
                 * \param queue the queue the call the callback on
                 * \return a continuation subscriber of type threading::Future<U>::ptr
                 */
                template <typename X>
                typename std::enable_if<
                    traits::callback_traits<X, T>::is_valid::value,
                    typename Subscriber<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::ptr
                >::type
                next_async(X&& resolver, const Queue::ptr& queue = Queue::current())
                // On current queue by default
                {
                    assert(queue != nullptr);
                    std::lock_guard<decltype(_mtx)> lock(_mtx);
                    
                    _async = true;
                    _queue = queue;
                    
                    auto continuation_subscriber = make_continuation(std::move(resolver));
                    
                    if(&_futures.front() != &_futures.back())
                    {
                        // Resolve now
                        queue->post(_resolver);
                    }
                    
                    return continuation_subscriber;
                }
                
                /** Cancel subscription
                 */
                void cancel() {
                    std::lock_guard<decltype(_mtx)> lock(_mtx);
                    assert(!_canceled);
                    _canceled = true;
                    if(_observable != nullptr)
                    {
                        _observable->cancel();
                    }
                }
                
            protected:
                friend class details::BaseObservable<T>;
                friend class Observable<T>;
                void notify(std::future<T>&& future)
                {
                    std::lock_guard<decltype(_mtx)> lock(_mtx);
                    
                    // Push future on queue
                    _futures.push(std::move(future));

                    if( _resolver == nullptr )
                    {
                        return;
                    }
                    
                    if( (_queue != nullptr) && (_async || (_queue != Queue::current()) ) )
                    {
                        // Pass by ref
                        // As we passed resolver by ref, make sure it remains valid until completion
                        _queue->post(_resolver);
                    }
                    else
                    {
                        _resolver();
                    }
                }
                
                void observable_destroyed()
                {
                    std::lock_guard<decltype(_mtx)> lock(_mtx);
                    _observable = nullptr;
                }
                
            private:
                template <typename X>
                typename std::enable_if<
                    !std::is_void<typename traits::callback_traits<X, T>::return_type>::value
                    and !traits::future_traits<typename traits::callback_traits<X, T>::return_type>::is_future::value,
                    typename Subscriber<typename traits::callback_traits<X, T>::return_type>::ptr
                >::type
                make_continuation(X&& resolver)
                {
                    auto continuation = std::make_shared<Observable<typename traits::callback_traits<X, T>::return_type>>();
                    continuation->set_cancelation([this](){
                        // Cancelation function: forward to parent
                        this->cancel();
                    });
                    
                    auto subscriber = continuation->get_subscriber();

                    _continuation = continuation;
                    
                    _resolver = [this, resolver = std::move(resolver), self = this->shared_from_this()]() mutable {
                        auto r_continuation = reinterpret_cast<decltype(continuation.get())>(_continuation.get());

                        std::future<T> std_future;
                        while(true) {
                            {
                                std::lock_guard<decltype(_mtx)> lock(_mtx);
                                assert(!_futures.empty());
                                if(&_futures.front() == &_futures.back())
                                {
                                    return;
                                }
                                
                                if(_canceled)
                                {
                                    return;
                                }
                                
                                // Remove it before callback to ensure reentrancy (otherwise it this gets called _futures.front() gets resolved twice (invalid))
                                std_future = std::move(_futures.front());
                                _futures.pop();
                            }

                            // We can only forward a value/completion event/error if the continuation has not completed yet (for instance because of an exception raised within the continuation)
                            if(!r_continuation->has_completed())
                            {
                                try
                                {
                                    r_continuation->next( resolver(std_future) );
                                } catch (ObservableCompletedException&) {
                                    // OK not to handle that
                                    r_continuation->complete();
                                }
                                catch(...)
                                {
                                   r_continuation->error( std::current_exception() );
                                };
                            }
                        }
                    };
                    
                    return subscriber;
                }
                
                template <typename X>
                typename std::enable_if<
                !std::is_void<typename traits::callback_traits<X, T>::return_type>::value
                and traits::future_traits<typename traits::callback_traits<X, T>::return_type>::is_future::value
                and !std::is_void<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::value,
                typename Subscriber<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::ptr
                >::type
                make_continuation(X&& resolver)
                {
                    auto continuation = std::make_shared<Observable<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>>();
                    continuation->set_cancelation([this](){
                        // Cancelation function: forward to parent
                        this->cancel();
                    });
                    
                    auto subscriber = continuation->get_subscriber();
                    
                    _continuation = continuation;
                    
                    _resolver = [this, resolver = std::move(resolver), self = this->shared_from_this()]() mutable {
                        auto r_continuation = reinterpret_cast<decltype(continuation.get())>(_continuation.get());
                        
                        std::future<T> std_future;
                        while(true) {
                            {
                                std::lock_guard<decltype(_mtx)> lock(_mtx);
                                assert(!_futures.empty());
                                if(&_futures.front() == &_futures.back())
                                {
                                    return;
                                }
                                
                                if(_canceled)
                                {
                                    return;
                                }
                                
                                // Remove it before callback to ensure reentrancy (otherwise it this gets called _futures.front() gets resolved twice (invalid))
                                std_future = std::move(_futures.front());
                                _futures.pop();
                            }
                            
                            // We can only forward a value/completion event/error if the continuation has not completed yet (for instance because of an exception raised within the continuation)
                            if(!r_continuation->has_completed())
                            {
                                try
                                {
                                    resolver(std_future)->then([r_continuation, self = this->shared_from_this(), this](std::future<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>& fut){
                                        // We can only forward a value/completion event/error if the continuation has not completed yet (for instance because of an exception raised within the continuation)
                                        if(!r_continuation->has_completed())
                                        {
                                            try
                                            {
                                                r_continuation->next( fut.get() );
                                            } catch (ObservableCompletedException&) {
                                                // OK not to handle that
                                                r_continuation->complete();
                                            }
                                            catch(...)
                                            {
                                                r_continuation->error( std::current_exception() );
                                            };
                                        }
                                    }, _queue);
                                }
                                catch(...)
                                {
                                    r_continuation->error( std::current_exception() );
                                };
                            }
                        }
                    };
                    
                    return subscriber;
                }
                
                template <typename X>
                typename std::enable_if<
                !std::is_void<typename traits::callback_traits<X, T>::return_type>::value
                and traits::future_traits<typename traits::callback_traits<X, T>::return_type>::is_future::value
                and std::is_void<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::value,
                typename Subscriber<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>::ptr
                >::type
                make_continuation(X&& resolver)
                {
                    auto continuation = std::make_shared<Observable<typename traits::future_traits<typename traits::callback_traits<X, T>::return_type>::underlying_type>>();
                    continuation->set_cancelation([this](){
                        // Cancelation function: forward to parent
                        this->cancel();
                    });
                    
                    auto subscriber = continuation->get_subscriber();
                    
                    _continuation = continuation;
                    
                    _resolver = [this, resolver = std::move(resolver), self = this->shared_from_this()]() mutable {
                        auto r_continuation = reinterpret_cast<decltype(continuation.get())>(_continuation.get());
                        
                        std::future<T> std_future;
                        while(true) {
                            {
                                std::lock_guard<decltype(_mtx)> lock(_mtx);
                                assert(!_futures.empty());
                                if(&_futures.front() == &_futures.back())
                                {
                                    return;
                                }
                                
                                if(_canceled)
                                {
                                    return;
                                }
                                
                                // Remove it before callback to ensure reentrancy (otherwise it this gets called _futures.front() gets resolved twice (invalid))
                                std_future = std::move(_futures.front());
                                _futures.pop();
                            }
                            
                            // We can only forward a value/completion event/error if the continuation has not completed yet (for instance because of an exception raised within the continuation)
                            if(!r_continuation->has_completed())
                            {
                                try
                                {
                                    resolver(std_future)->then([r_continuation, self = this->shared_from_this(), this](std::future<void>& fut){
                                        // We can only forward a value/completion event/error if the continuation has not completed yet (for instance because of an exception raised within the continuation)
                                        if(!r_continuation->has_completed())
                                        {
                                            try
                                            {
                                                fut.get();
                                                r_continuation->next();
                                            } catch (ObservableCompletedException&) {
                                                // OK not to handle that
                                                r_continuation->complete();
                                            }
                                            catch(...)
                                            {
                                                r_continuation->error( std::current_exception() );
                                            };
                                        }
                                    }, _queue);
                                }
                                catch(...)
                                {
                                    r_continuation->error( std::current_exception() );
                                };
                            }
                        }
                    };
                    
                    return subscriber;
                }
                
                template <typename X>
                typename std::enable_if<
                std::is_void<typename traits::callback_traits<X, T>::return_type>::value,
                typename Subscriber<typename traits::callback_traits<X, T>::return_type>::ptr
                >::type
                make_continuation(X&& resolver)
                {
                    auto continuation = std::make_shared<Observable<typename traits::callback_traits<X, T>::return_type>>();
                    continuation->set_cancelation([this](){
                        // Cancelation function: forward to parent
                        this->cancel();
                    });
                    
                    auto subscriber = continuation->get_subscriber();
                    
                    _continuation = continuation;
                    
                    _resolver = [this, resolver = std::move(resolver), self = this->shared_from_this()]() mutable {
                        auto r_continuation = reinterpret_cast<decltype(continuation.get())>(_continuation.get());
                        
                        std::future<T> std_future;
                        while(true) {
                            {
                                std::lock_guard<decltype(_mtx)> lock(_mtx);
                                assert(!_futures.empty());
                                if(&_futures.front() == &_futures.back())
                                {
                                    return;
                                }
                                
                                if(_canceled)
                                {
                                    return;
                                }
                                
                                // Remove it before callback to ensure reentrancy (otherwise it this gets called _futures.front() gets resolved twice (invalid))
                                std_future = std::move(_futures.front());
                                _futures.pop();
                            }
                            
                            // We can only forward a value/completion event/error if the continuation has not completed yet (for instance because of an exception raised within the continuation)
                            if(!r_continuation->has_completed())
                            {
                                try
                                {
                                    resolver(std_future);
                                    r_continuation->next();
                                } catch (ObservableCompletedException&) {
                                    // OK not to handle that
                                    r_continuation->complete();
                                }
                                catch(...)
                                {
                                    r_continuation->error( std::current_exception() );
                                };
                            }
                        }
                    };
                    
                    return subscriber;
                }
                
                std::function<void()> _resolver;
                Queue::ptr _queue;
                std::queue<std::future<T>> _futures;
                std::shared_ptr<details::AbstractObservable> _continuation;
                details::BaseObservable<T>* _observable;
                bool _canceled;
                bool _async;
                std::recursive_mutex _mtx;
            };
            
            // Observable methods impl
            namespace details
            {
                template <typename T>
                void BaseObservable<T>::notify_subscriber(std::future<T>&& fut)
                {
                    _subscriber->notify(std::move(fut));
                }
            }
            
            /** @}*/
            /** @}*/
        }
    }
}
