//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Queue.hpp
//  song2
//
//  Created by Donatien Garnier on 02/01/2018.

//

#pragma once

#include <queue>
#include <list>

#include <functional>
#include <memory>
#include <mutex>
#include <chrono>
#include <algorithm>

#include "gen/djinni/cpp/queue.hpp"
#include "gen/djinni/cpp/dispatcher.hpp"

namespace arm
{
    namespace song2
    {
        namespace threading
        {
            /** \addtogroup Threading
             * Some abstractions for asynchronous execution
             *  @{
             */
            /** \defgroup Queue Queue
             * A queue that executes units of work.
             *  @{
             */
            /** A queue provides an abstraction on top of a thread, it is basically a polling loop similar to a dispatch queue on Darwin or Looper/Handler on Android.
             *
             * Jobs can be posted on a queue - it's associated with a native dispatcher (platform - specific) which will actually run the work.
             *
             * A job is basically a std::function<void()> that will be executed as soon as possible or after a specific time.
             */
            class Queue : public platform::Queue, public std::enable_shared_from_this<Queue>
            {
            public:
                using ptr = std::shared_ptr<Queue>;
                
                /** Create a new queue
                 * \param dispatcher the dispatcher to use, or nullptr to defer setting it
                 */
                Queue(const std::shared_ptr<platform::Dispatcher>& dispatcher);
                
                /** Create a new queue on the same dispatcher used by the current queue
                 */
                Queue();
                
                virtual ~Queue();
                
                /** Prevent any work from being queued or executed
                 * The pending queue is cleared
                 */
                void disable();
                
                /** Re-enable a disabled queue
                 */
                void enable();
                
                /** Check if the queue is empty
                 * \return Whether the queue is empty
                 */
                bool empty() const;
                
                /** Return the time left before this queue needs to be processed again
                 * \return the time in milliseconds before the queue should be processed, or std::chrono::milliseconds::max() if the queue is empty
                 */
                std::chrono::milliseconds delay() const;
                
                /** Post a job to the queue
                 * \param fn the (movable) job to execute
                 */
                void post( std::function<void()>&& fn );
                
                /** Post a job to the queue
                 * \param fn the (copyable) job to execute
                 */
                void post( const std::function<void()>& fn );
                
                /** Post a job to the queue to be executed after a specific duration
                 * \param fn the (movable) job to execute
                 * \param duration the delay after which the job should be executed
                 */
                void post_delayed( std::function<void()>&& fn, std::chrono::milliseconds duration );
                
                /** Post a job to the queue to be executed after a specific duration
                 * \param fn the (copyable) job to execute
                 * \param duration the delay after which the job should be executed
                 */
                void post_delayed( const std::function<void()>& fn, std::chrono::milliseconds duration );
                
                /** Called by dispatcher: Work jobs, run on native queue
                 * \return return the delay in milliseconds after which this should be called again
                 */
                virtual int64_t process() override;
                
                /** Set the dispatcher to use for the queue
                 * \param dispatcher the dispatcher to set
                 */
                void set_dispatcher(const std::shared_ptr<platform::Dispatcher>& dispatcher);
                
                /** Retrieve dispatcher used by this queue
                 * \return the relevant dispatcher instance
                 */
                std::shared_ptr<platform::Dispatcher> dispatcher() const;
                
                /** Return the current queue this function is being run on
                 * \return the current queue or nullptr otherwise
                 */
                static const Queue::ptr& current();
                
                /** Set (override) the current queue
                 * \param queue the current queue or nullptr otherwise
                 */
                static void set_current(const Queue::ptr& queue);
                
                /** Sanity check that the current execution is happening on the expected dispatcher shared with the specified queue
                 * \param queue the queue to check against
                 */
                static void assert_threadsafe(const Queue::ptr& queue);
            private:
                void do_post(std::function<void()>&& fn, std::chrono::milliseconds duration);
                
                mutable std::mutex _mtx;
                bool _enabled;
                
                struct work_unit_t {
                    std::function<void()> func;
                    std::chrono::steady_clock::time_point tp;
                };
                std::list<work_unit_t> _queue;
                std::list<work_unit_t>::const_iterator _first_delayed_item;
                
                std::shared_ptr<platform::Dispatcher> _dispatcher;
                
                static thread_local Queue::ptr _tls_current_queue;
            };
            
            /** A factory function to create a new queue
             * \param dispatcher the platform-specific dispatcher to run the queue on
             */
            typename Queue::ptr make_queue(const std::shared_ptr<platform::Dispatcher>& dispatcher);
            
            /** A factory function to create a new queue on the current dispatcher
             */
            typename Queue::ptr make_queue();
            /** @}*/
            /** @}*/
        }
    }
}
