//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  DeviceStatus.hpp
//  song2
//
//  Created by Donatien Garnier on 19/01/2018.

//

#pragma once

namespace arm {
    namespace song2 {
        namespace transport
        {
            /** An enum containing potential device visibility status
             *
             */
            enum class DeviceStatus
            {
                FailedToConnectError, ///< Connection to device failed
                TransportSetupProcessError, ///< Initial setup specific to transport failed
                TransportRequirementError, ///< Device does not match an implementation requirement
                
                Lost, ///< Device was visible but is now lost
                Visible, ///< Device is visible to scanner
                Connecting, ///< Device is being connected to
                Connected ///< Device is connected
            };
        }
    }
}
