//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Scanner.hpp
//  song2-tests
//
//  Created by Donatien Garnier on 15/01/2018.

//

#pragma once

#include <memory>
#include <vector>
#include <set>
#include <list>
#include <string>

#include "cpp/threading/Promise.hpp"
#include "cpp/threading/Observable.hpp"
#include "cpp/threading/ObservableVector.hpp"
#include "cpp/pipe/Endpoint.hpp"

#include "DeviceStatus.hpp"

namespace arm
{
    namespace song2
    {
        namespace transport
        {
            struct DeviceIdentifier;
            class Device;
            
            /** \addtogroup Transport
             * Transport layer (BLE, NFC, etc)
             *  @{
             */
            /** Scanner
             *
             * This is a base class for scanners. A scanner is built on top of a specific transport and has the following functionality:
             * * Start/Stop scanning for devices
             * * Keep track of devices which are visible, connected or lost
             *
             * A final class will have to implement some methods defined below.
             */
            class Scanner {
            public:
                /** Create a scanner
                 */
                Scanner();
                virtual ~Scanner();
                
                /** Start scanning for devices
                 */
                void start_scanning();
                
                /** Stop scanning for devices
                 */
                void stop_scanning();
                
                /** Observe devices which are visible
                 *
                 * The full list of currently visible devices will be sent and any new visible devices after that
                 * \return a subscriber to new visible devices' instances
                 */
                threading::Subscriber<std::shared_ptr<Device>>::ptr visible_devices();
                
            protected:
                friend class Device;
                
                // All of these to be implemented
                /** Start scanning for devices (implementation)
                 *
                 * Must be implemented
                 */
                virtual void start_specific_scanning() = 0;
                
                /** Stop scanning for devices (implementation)
                 *
                 * Must be implemented
                 */
                virtual void stop_specific_scanning() = 0;
                
                /** Connect device (implementation)
                 *
                 * \param device the device (as returned by visible_devices()) to connect to
                 *
                 * Must be implemented
                 */
                virtual void connect_specific_device(const std::shared_ptr<Device>& device) = 0;
                
                /** Disconnect device (implementation)
                 *
                 * \param device the device (as returned by visible_devices()) to disconnect from
                 *
                 * Must be implemented
                 */
                virtual void disconnect_specific_device(const std::shared_ptr<Device>& device) = 0;
                
                /** Open endpoint (implementation)
                 *
                 * This establish a pipe of communication to the device on a specific channel (channel numbers can be used to disambiguate simultaneous connections)
                 *
                 * \param device the device (as returned by visible_devices()) to disconnect from
                 * \param channel the channel number to use
                 * \return an endpoint over which data can be sent and received
                 *
                 * Must be implemented
                 */
                virtual typename pipe::Endpoint::ptr open_specific_endpoint_for_device_and_channel(const std::shared_ptr<Device>& device, uint32_t channel) = 0;
                
                /** Retrieve transport name (implementation)
                 *
                 * \return the transport's name (e.g. "BLE", "NFC", etc.)
                 *
                 * Must be implemented
                 */
                virtual const std::string& transport_name() const = 0;
                
                /** Update a device's status (called by implementation)
                 *
                 * \param device the device (can be new) to update the status of
                 * \param status the updated status
                 */
                void update_device_status(const std::shared_ptr<Device>& device, DeviceStatus status);
                
                // Called by device
                void register_device(Device* device);
                void unregister_device(Device* device);
                void connect_device(Device* device);
                void disconnect_device(Device* device);
                typename pipe::Endpoint::ptr open_endpoint_for_device_and_channel(Device* device, uint32_t channel);
                                
                // Lifetime should be:
                // If found or connected, leave in cache
                // If lost, leave in cache as long as it's referenced by any user of the class
                
            private:                
                std::set<Device*> _connected_devices;
                std::set<std::shared_ptr<Device>> _visible_devices;
                std::set<Device*> _all_devices;
                
                threading::ObservableVector<std::shared_ptr<Device>> _visibility_observables;
            };
            /** @}*/
        }
    }
}

