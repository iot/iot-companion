//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Scanner.cpp
//  song2-tests
//
//  Created by Donatien Garnier on 15/01/2018.

//

#include "Scanner.hpp"
#include "Device.hpp"

using namespace arm::song2;
using namespace arm::song2::transport;

Scanner::Scanner() : _visibility_observables([this](threading::Observable<std::shared_ptr<Device>>& obs){
    // Push all visible devices
    for(auto& device : _visible_devices) {
        obs.next(device);
    }
})
{
    
}

Scanner::~Scanner()
{
    // Make sure we clear all ref to devices before we get destroyed
    _visible_devices.clear();
}

void Scanner::start_scanning()
{
    start_specific_scanning();
}

void Scanner::stop_scanning()
{
    stop_specific_scanning();
}

threading::Subscriber<std::shared_ptr<Device>>::ptr Scanner::visible_devices()
{
    return _visibility_observables.get_subscriber();
}

void Scanner::update_device_status(const std::shared_ptr<Device>& dev, DeviceStatus status)
{
    // Will only be set to true if the call to _visible_devices.insert() succeeds (it will fail if it's alerady present within the set)
    bool newly_visible = false;
    
    switch (status) {
        case arm::song2::transport::DeviceStatus::Visible:
            newly_visible = _visible_devices.insert(dev).second;
            _connected_devices.erase(dev.get());
            break;
            
        case arm::song2::transport::DeviceStatus::Connected:
            newly_visible = _visible_devices.insert(dev).second;
            _connected_devices.insert(dev.get());
            break;
            
        case arm::song2::transport::DeviceStatus::Lost:
        default:
            _connected_devices.erase(dev.get());
            _visible_devices.erase(dev);
            break;
    }
    
    // Always update device's status
    dev->update_status(status);

    // If it's newly visible, advertise:
    if( newly_visible )
    {
        _visibility_observables.next(dev);
    }
}

void Scanner::register_device(Device* device)
{
    // Called by device's contructor
    _all_devices.insert(device);
}

void Scanner::unregister_device(Device* device)
{
    // Called by device's destructor
    _all_devices.erase(device);
}

void Scanner::connect_device(Device* device)
{
    connect_specific_device(device->shared_from_this());
}

void Scanner::disconnect_device(Device* device)
{
    disconnect_specific_device(device->shared_from_this());
}

typename pipe::Endpoint::ptr Scanner::open_endpoint_for_device_and_channel(Device* device, uint32_t channel)
{
    return open_specific_endpoint_for_device_and_channel(device->shared_from_this(), channel);
}
