//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Device.cpp
//  song2-tests
//
//  Created by Donatien Garnier on 15/01/2018.

//

#include "Device.hpp"
#include "Scanner.hpp"

using namespace arm::song2;
using namespace arm::song2::transport;

Device::Device(const DeviceIdentifier& id, Scanner* scanner) : _scanner(scanner), _status(DeviceStatus::Lost)
{
    this->url = id.url;
    this->id = id.id;
    _scanner->register_device(this);
}

Device::~Device()
{
    _scanner->unregister_device(this);
}

threading::Future<void>::ptr Device::connect()
{
    auto obs = std::make_shared<threading::Promise<void>>();
    if( _status == DeviceStatus::Connected )
    {
        obs->resolve();
    }
    else
    {
        _connect_promises.push_back(obs);
        _scanner->connect_device(this); // will probably throw if not visible
    }
    
    return obs->get_future();
}

threading::Future<void>::ptr Device::disconnect()
{
    auto obs = std::make_shared<threading::Promise<void>>();
    if( _status != DeviceStatus::Connected )
    {
        obs->resolve();
    }
    else
    {
        _disconnect_promises.push_back(obs);
        _scanner->disconnect_device(this);
    }
    
    return obs->get_future();
}

DeviceStatus Device::status() const
{
    return _status;
}

std::string Device::transport_name() const
{
    return _scanner->transport_name();
}

typename pipe::Endpoint::ptr Device::open_endpoint_for_channel(uint32_t channel)
{
    return _scanner->open_endpoint_for_device_and_channel(this, channel);
}

void Device::update_status(DeviceStatus status)
{
    _status = status;
    if(status == DeviceStatus::Connected)
    {
        auto connect_promises = std::move(_connect_promises);
        _connect_promises.clear();
        for(auto& p : connect_promises)
        {
            p->resolve();
        }
    }
    else if((status == DeviceStatus::Visible) || (status == DeviceStatus::Lost))
    {
        auto disconnect_promises = std::move(_disconnect_promises);
        _disconnect_promises.clear();
        for(auto& p : disconnect_promises)
        {
            p->resolve();
        }
    }
}
