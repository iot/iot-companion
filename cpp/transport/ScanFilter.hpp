//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  ScanFilter.hpp
//  song2-tests
//
//  Created by Donatien Garnier on 16/01/2018.

//

#pragma once

#include <vector>
#include <boost/optional.hpp>
#include "cpp/types/Types.hpp"

namespace arm
{
    namespace song2
    {
        namespace transport
        {
            struct ScanFilter {
                ScanFilter();
                
                boost::optional<std::string> url_prefix;
                boost::optional<bytes_t> id_prefix;
                std::vector<std::string> acceptable_transports;
            };
        }
    }
}
