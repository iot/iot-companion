//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  InMemoryScanner.hpp
//  song2-tests
//
//  Created by Donatien Garnier on 15/01/2018.

//

#pragma once

#include <map>
#include <functional>

#include "cpp/transport/Scanner.hpp"
#include "cpp/transport/Device.hpp"

namespace arm
{
    namespace song2
    {
        namespace transport
        {
            /** \addtogroup Dispatchers
             *  @{
             */
            /** \defgroup BlockingDispatcher Blocking Dispatcher
             *  @{
             */
            
            /** The InMemoryScanner will be used for testing
             */
            class InMemoryScanner : public Scanner {
            public:
                InMemoryScanner();
                virtual ~InMemoryScanner();
                
                std::shared_ptr<Device> register_device(const DeviceIdentifier& id,
                                     std::function<void(uint32_t, pipe::Endpoint::ptr&&)>&& endpoint_handler,
                                     std::function<void()>&& disconnection_handler);
                void unregister_device(const std::shared_ptr<Device>& dev);
 
            protected:
                // All of these to be implemented
                virtual void start_specific_scanning() override;
                virtual void stop_specific_scanning() override;
                virtual void connect_specific_device(const std::shared_ptr<Device>& device) override;
                virtual void disconnect_specific_device(const std::shared_ptr<Device>& device) override;
                virtual typename pipe::Endpoint::ptr open_specific_endpoint_for_device_and_channel(const std::shared_ptr<Device>& device, uint32_t channel) override;
                virtual const std::string& transport_name() const override;
                
            private:
                struct Handlers {
                    std::shared_ptr<Device> device;
                    std::function<void(uint32_t, pipe::Endpoint::ptr&&)> endpoint_handler;
                    std::function<void()> disconnection_handler;
                };
                
                std::map<Device*, Handlers> _handlers;
            };
            
            /** @}*/
            /** @}*/
        }
    }
}
