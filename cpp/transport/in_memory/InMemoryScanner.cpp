//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  InMemoryScanner.cpp
//  song2-tests
//
//  Created by Donatien Garnier on 15/01/2018.

//

#include "InMemoryScanner.hpp"
// Only for tests

using namespace arm::song2;
using namespace arm::song2::transport;

InMemoryScanner::InMemoryScanner() : Scanner()
{
    
}

InMemoryScanner::~InMemoryScanner()
{
    
}

std::shared_ptr<Device> InMemoryScanner::register_device(const DeviceIdentifier& id,
                                        std::function<void(uint32_t, pipe::Endpoint::ptr&&)>&& endpoint_handler,
                                        std::function<void()>&& disconnection_handler)
{
    auto dev = std::make_shared<Device>(id, this);
    InMemoryScanner::Handlers h {
        .device = dev,
        .endpoint_handler = std::move(endpoint_handler),
        .disconnection_handler = std::move(disconnection_handler)} ;
    _handlers.insert(std::make_pair(dev.get(), std::move(h)));
    return dev;
}

void InMemoryScanner::unregister_device(const std::shared_ptr<Device>& dev)
{
    _handlers.erase(dev.get());
    this->update_device_status(dev, DeviceStatus::Lost);
}

// Scanner Implementation
void InMemoryScanner::start_specific_scanning()
{
    for(auto& h : _handlers)
    {
        this->update_device_status(h.second.device, DeviceStatus::Visible);
    }
}

void InMemoryScanner::stop_specific_scanning()
{
    for(auto& h : _handlers)
    {
        this->update_device_status(h.second.device, DeviceStatus::Lost);
    }
}

void InMemoryScanner::connect_specific_device(const std::shared_ptr<Device>& device)
{
    auto cit = _handlers.find(device.get());
    if( cit == _handlers.cend() )
    {
        return;
    }
    
    this->update_device_status(device, DeviceStatus::Connected);
}

void InMemoryScanner::disconnect_specific_device(const std::shared_ptr<Device>& device)
{
    auto cit = _handlers.find(device.get());
    if( cit == _handlers.cend() )
    {
        return;
    }
    
    cit->second.disconnection_handler();
    this->update_device_status(device, DeviceStatus::Visible);
}

typename pipe::Endpoint::ptr InMemoryScanner::open_specific_endpoint_for_device_and_channel(const std::shared_ptr<Device>& device, uint32_t channel)
{
    auto cit = _handlers.find(device.get());
    if( cit == _handlers.cend() )
    {
        throw std::runtime_error("Unknown device");
    }
    
    auto endpoints = pipe::make_endpoints();
    cit->second.endpoint_handler(channel, std::move(endpoints.first));
    
    return std::move(endpoints.second);
}

const std::string& InMemoryScanner::transport_name() const
{
    static std::string str = "In Memory";
    return str;
}
