//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Device.hpp
//  song2-tests
//
//  Created by Donatien Garnier on 15/01/2018.

//

#pragma once

#include <string>
#include <boost/optional.hpp>
#include <memory>
#include <list>

#include "cpp/threading/Promise.hpp"
#include "cpp/threading/Observable.hpp"

#include "cpp/pipe/Endpoint.hpp"
#include "cpp/types/Types.hpp"

#include "DeviceStatus.hpp"

namespace arm {
    namespace song2 {
        namespace transport
        {
            /** \addtogroup Transport
             *  @{
             */
            /** Device Identifier
             *
             * This is a class containing various identifiers for a device
             */
            struct DeviceIdentifier
            {
                boost::optional<std::string> url; ///< An optional URL identifying a device (from an Eddystone-URI beacon or URL NDEF record)
                boost::optional<bytes_t> id; ///< An optional ID identifying a device (from an Eddystone-UID beacon)
            };

            class Scanner;
            
            /** Device
             *
             * This is a class implementing low-level, transport agnostic access to a device.
             * Devices are instantiated by the final Scanner class.
             */
            class Device : public DeviceIdentifier, public std::enable_shared_from_this<Device> {
            public:
                /** Create a new device
                 *
                 * \param id the relevant identifiers for this device
                 * \param scanner a pointer to the relevant scanner
                 */
                Device(const DeviceIdentifier& id, Scanner* scanner);
                virtual ~Device();
                
                /** Connect to device
                 *
                 * \return a future which will resolve succesfully on device connection (or will fail)
                 */
                threading::Future<void>::ptr connect();
                
                /** Disconnect from device
                 *
                 * \return a future which will resolve succesfully on device disconnection (or will fail)
                 */
                threading::Future<void>::ptr disconnect();
                
                // todo?
                //threading::Subscriber<DeviceStatus>::ptr watch_status();
                
                /** Return the current device status
                 *
                 * \return a DeviceStatus enum item (Lost, Visible or Connected)
                 */
                DeviceStatus status() const;
                
                /** Return the underlying transport name
                 *
                 * \return a string containing the transport name
                 */
                std::string transport_name() const;
                
                /** Open endpoint for channel number
                 *
                 * Open an endpoint for the relevant channel number (used for disambiguation for concurrent device access)
                 * \return an endpoint that can be written to and read from
                 */
                typename pipe::Endpoint::ptr open_endpoint_for_channel(uint32_t channel);
                 
            private:
                // Called by transport
                void update_status(DeviceStatus status);
                
                friend class Scanner;
                Scanner* _scanner;
                
                DeviceStatus _status;
                
                std::list<std::shared_ptr<threading::Promise<void>>> _connect_promises;
                std::list<std::shared_ptr<threading::Promise<void>>> _disconnect_promises;
            };
            /** @}*/
        }
    }
}

