//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  BleScanner.cpp
//  song2-tests
//
//  Created by Donatien Garnier on 15/01/2018.

//

#include "BleScanner.hpp"
#include "BleScannerAdapterProxy.hpp"

#include "cpp/logging/Logging.hpp"

using namespace arm::song2;
using namespace arm::song2::transport;

BleScanner::BleScanner(const std::shared_ptr<platform::BleAdapter>& adapter) :
_adapter_status_obs(),
_adapter_proxy(std::make_shared<BleScannerAdapterProxy>(this, adapter))
{
    _adapter_proxy->connect();
}

BleScanner::~BleScanner()
{
    // Disconnect proxy - this creates a barrier against any callbacks we might receive
    _adapter_proxy->disconnect();
}

threading::Subscriber<platform::BleAdapterStatus>::ptr BleScanner::adapter_status()
{
    return _adapter_status_obs.get_subscriber();
}

void BleScanner::start_specific_scanning()
{
    _adapter_proxy->start_scanning();
}

void BleScanner::stop_specific_scanning()
{
    _adapter_proxy->stop_scanning();
}

void BleScanner::connect_specific_device(const std::shared_ptr<Device>& device)
{
    _adapter_proxy->connect_device(device);
}

void BleScanner::disconnect_specific_device(const std::shared_ptr<Device>& device)
{
    // Clear all device channels
    _endpoints.erase(device);
    
    _adapter_proxy->disconnect_device(device);
}

typename pipe::Endpoint::ptr BleScanner::open_specific_endpoint_for_device_and_channel(const std::shared_ptr<Device>& device, uint32_t channel)
{
    if(device->status() != DeviceStatus::Connected)
    {
        throw std::runtime_error("Trying to open endpoint on a device which is disconnected");
    }
    
    auto& channels_map = _endpoints[device];

    // Make endpoints pair
    auto endpoints_pair = pipe::make_endpoints();
    
    // Make sure this channel is not in use already
    auto new_channel_entry = channels_map.emplace(std::make_pair(channel, std::move(endpoints_pair.first)));
    
    if(!new_channel_entry.second)
    {
        // The channel is already being used, throw
        throw std::runtime_error("Trying to open endpoint on a channel which is already in use");
    }
    
    std::weak_ptr<Device> device_weak = device;
    new_channel_entry.first->second->receive([this, device_weak, channel](std::future<bytes_t>& message){
        if(auto device = device_weak.lock()) {
            _adapter_proxy->send_message(device, channel, message.get());
        }
    });
    
    return std::move(endpoints_pair.second);
}

const std::string& BleScanner::transport_name() const
{
    static std::string str = "BLE";
    return str;
}

void BleScanner::adapter_proxy_update_device_status(const std::shared_ptr<Device>& device, DeviceStatus status)
{
    this->update_device_status(device, status);
}

void BleScanner::adapter_proxy_update_adapter_status(platform::BleAdapterStatus status)
{
    _adapter_status_obs.next(status);
}

void BleScanner::adapter_proxy_received_message(const std::shared_ptr<Device>& device, uint32_t channel, const bytes_t& message)
{
    // Make sure device is connected & this channel is actually open - if not ignore
    if(device->status() != DeviceStatus::Connected)
    {
        logging::err() << "Received message from a disconnected device";
        return;
    }
    
    auto& channels_map = _endpoints[device];
    
    auto channel_entry = channels_map.find(channel);
    if(channel_entry == channels_map.cend())
    {
        // Not an active channel
        logging::warn() << "Received message on a closed channel";
        return;
    }
    
    auto& ep = channel_entry->second;
    ep->send(message);
}

