//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  BleScannerAdapterProxy.hpp
//  song2
//
//  Created by Donatien Garnier on 23/02/2018.

//

#pragma once

#include <memory>
#include <vector>
#include <map>
#include <queue>

#include "cpp/threading/Queue.hpp"
#include "cpp/threading/Observable.hpp"
#include "gen/djinni/cpp/ble_adapter.hpp"
#include "gen/djinni/cpp/ble_adapter_delegate.hpp"
#include "cpp/transport/Device.hpp"
#include "cpp/pipe/Endpoint.hpp"

namespace arm
{
    namespace song2
    {
        namespace transport
        {
            class BleScanner;
            class Device;
            
            class BleScannerAdapterProxy : public platform::BleAdapterDelegate, public std::enable_shared_from_this<BleScannerAdapterProxy>
            {
            public:
                BleScannerAdapterProxy(transport::BleScanner* scanner, const std::shared_ptr<platform::BleAdapter>& adapter);
                virtual ~BleScannerAdapterProxy();
                
                void connect();
                void disconnect();
                
                void start_scanning();
                void stop_scanning();
                
                void connect_device(const std::shared_ptr<Device> & device);
                void disconnect_device(const std::shared_ptr<Device> & device);

                void send_message(const std::shared_ptr<Device>& device, uint32_t channel, const bytes_t& message);
                
            protected:
                // platform::BleAdapterDelegate
                virtual void on_status_updated() override;
                
                virtual void on_device_discovered(const std::shared_ptr<platform::BleDevice> & device, int32_t rssi, const std::vector<std::vector<uint8_t>> & services, const std::vector<platform::BleServicesDataType> & services_data, const boost::optional<platform::BleManufacturerSpecificDataType> & manufacturer_specific_data) override;
                
                virtual void on_device_connected(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error) override;
                
                virtual void on_device_disconnected(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error) override;
                
                virtual void on_device_rssi_updated(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error) override;
                
                virtual void on_gattc_services_discovered(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error) override;
                
                virtual void on_gattc_characteristics_discovered(const std::shared_ptr<platform::GattcService> & service, platform::BleError error) override;
                
                virtual void on_gattc_descriptors_discovered(const std::shared_ptr<platform::GattcCharacteristic> & characteristic, platform::BleError error) override;
                
                virtual void on_gattc_characteristic_written(const std::shared_ptr<platform::GattcCharacteristic> & characteristic, platform::BleError error) override;
                
                virtual void on_gattc_characteristic_read(const std::shared_ptr<platform::GattcCharacteristic> & characteristic, platform::BleError error) override;
                
                virtual void on_gattc_characteristic_updated(const std::shared_ptr<platform::GattcCharacteristic> & characteristic, platform::BleError error) override;
                
                virtual void on_gattc_characteristic_notifications_set(const std::shared_ptr<platform::GattcCharacteristic> & characteristic, platform::BleError error) override;
                
                virtual void on_gattc_descriptor_written(const std::shared_ptr<platform::GattcDescriptor> & descriptor, platform::BleError error) override;
                
                virtual void on_gattc_descriptor_read(const std::shared_ptr<platform::GattcDescriptor> & descriptor, platform::BleError error) override;
                
                virtual void on_gattc_mtu_updated(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error) override;
                
                virtual void on_gattc_phy_updated(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error) override;
                
            private:
                struct DeviceProxy;
                std::shared_ptr<DeviceProxy> device_proxy_from_ble_device(const std::shared_ptr<platform::BleDevice>& ble_device);
                std::shared_ptr<DeviceProxy> device_proxy_from_transport_device(const std::shared_ptr<transport::Device>& transport_device);
                
                void scanner_context_for_device(const std::shared_ptr<DeviceProxy>& device_proxy, std::function<void(std::shared_ptr<transport::Device>)>&& fn);
                
                void device_connection_failed(const std::shared_ptr<platform::BleDevice> & device, DeviceStatus error);
                
                void send_next_fragment(const std::shared_ptr<DeviceProxy>& device_proxy);
                
                std::shared_ptr<platform::BleAdapter> _adapter;
                std::shared_ptr<threading::Queue> _adapter_queue;
                BleScanner* _scanner;
                std::shared_ptr<threading::Queue> _scanner_queue;
                
                struct DeviceProxy {
                    DeviceProxy();
                    
                    // This should be instanciated on scanner's queue, however the transport device weak pointer should be set on the adapter's queue
                    struct TransportDeviceWrapper : transport::Device
                    {
                        TransportDeviceWrapper(const std::shared_ptr<DeviceProxy>& device_proxy, const DeviceIdentifier& id, Scanner* scanner);
                        virtual ~TransportDeviceWrapper();
                        
                        // This will remain valid as long as the underlying Bluetooth device is visible or connected
                        std::weak_ptr<DeviceProxy> device_proxy;
                    };
                    
                    // All of these should only be accessed on adapter's queue
                    std::weak_ptr<TransportDeviceWrapper> transport_device;
                    
                    BleScannerAdapterProxy* adapter_proxy;
                    DeviceIdentifier id;
                    bool id_checked;
                    std::shared_ptr<platform::GattcCharacteristic> rx_characteristic;
                    std::shared_ptr<platform::GattcCharacteristic> tx_characteristic;
                    std::shared_ptr<platform::BleDevice> ble_device;
                    
                    
                    uint32_t current_channel_num;
                    
                    bytes_t rx_partial_message;
                    size_t rx_next_fragment_num;
                    
                    std::queue< std::pair<uint32_t /* channel*/, bytes_t> > tx_partial_messages;
                    size_t tx_next_fragment_num;
                };
                
                friend class DeviceProxy::TransportDeviceWrapper;

                std::map<std::vector<uint8_t>, std::shared_ptr<DeviceProxy>> _ble_devices_map;
            };
        }
    }
}
