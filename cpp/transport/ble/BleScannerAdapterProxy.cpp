//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  BleScannerAdapterProxy.cpp
//  song2
//
//  Created by Donatien Garnier on 23/02/2018.

//

#include "cpp/types/Types.hpp"
#include "cpp/logging/Logging.hpp"

#include "BleScannerAdapterProxy.hpp"
#include "BleScanner.hpp"

#include "gen/djinni/cpp/ble_adapter_status.hpp"
#include "gen/djinni/cpp/ble_scan_flags.hpp"
#include "gen/djinni/cpp/ble_device_connection_flags.hpp"
#include "gen/djinni/cpp/ble_device.hpp"
#include "gen/djinni/cpp/ble_device_status.hpp"
#include "gen/djinni/cpp/ble_error.hpp"
#include "gen/djinni/cpp/ble_services_data_type.hpp"
#include "gen/djinni/cpp/gattc_service.hpp"
#include "gen/djinni/cpp/gattc_characteristic.hpp"
#include "gen/djinni/cpp/gattc_descriptor.hpp"

// Scan for Eddystone only
#define EDDYSTONE_SERVICE_UUID {0xfe, 0xaa}

#define EDDYSTONE_URL_FRAME_TYPE 0x10
#define EDDYSTONE_UID_FRAME_TYPE 0x00

const static std::vector<std::string> eddystone_url_scheme_prefixes = {
        "http://www.",
        "https://www.",
        "http://",
        "https://"
};

const static std::vector<std::string> eddystone_url_encoding_shortcuts = {
        ".com",
        ".org",
        ".edu",
        ".net",
        ".info",
        ".biz",
        ".gov"
};

// ARM has been assigned the following 16bit UUID: 0xFE8E
// However using it for this service doesn't add much value
// We should use the 16bit UUID if we want to beacon it, which is not the case as we're using Eddystone
// This could be used as a mean to broadcast an mbed cloud device id which is 128bit long

// Enumerate for Song2 service
#define SONG2_SERVICE_UUID {0xec, 0x8c, 0x01, 0x00, 0xec, 0x9b, 0x30, 0xaf, 0x1d, 0x41, 0xab, 0x00, 0xbe, 0xb4, 0x94, 0xdf}
#define SONG2_TX_CHAR_UUID {0xec, 0x8c, 0x01, 0x01, 0xec, 0x9b, 0x30, 0xaf, 0x1d, 0x41, 0xab, 0x00, 0xbe, 0xb4, 0x94, 0xdf}
#define SONG2_RX_CHAR_UUID {0xec, 0x8c, 0x01, 0x02, 0xec, 0x9b, 0x30, 0xaf, 0x1d, 0x41, 0xab, 0x00, 0xbe, 0xb4, 0x94, 0xdf}
#define CCCD_UUID {0x29, 0x02}
#define CCCD_ENABLE_NOTIF {0x01, 0x00}

const static std::vector< std::vector<uint8_t> > song2_services_uuids { SONG2_SERVICE_UUID };
const static std::vector< std::vector<uint8_t> > eddystone_services_uuids {{ EDDYSTONE_SERVICE_UUID }};
const static std::vector<uint8_t> eddystone_service_uuid { EDDYSTONE_SERVICE_UUID };
const static std::vector<uint8_t> song2_service_uuid { SONG2_SERVICE_UUID };
const static std::vector< std::vector<uint8_t> > song2_characteristics_uuids {{ SONG2_TX_CHAR_UUID , SONG2_RX_CHAR_UUID }};
const static std::vector<uint8_t> song2_tx_char_uuid { SONG2_TX_CHAR_UUID };
const static std::vector<uint8_t> song2_rx_char_uuid { SONG2_RX_CHAR_UUID };
const static std::vector<uint8_t> cccd_descriptor_uuid { CCCD_UUID };
const static std::vector<uint8_t> cccd_enable_notif { CCCD_ENABLE_NOTIF };


using namespace arm::song2;
using namespace arm::song2::transport;

BleScannerAdapterProxy::BleScannerAdapterProxy(transport::BleScanner* scanner, const std::shared_ptr<platform::BleAdapter>& adapter) :
        _adapter(adapter),
        _adapter_queue(threading::make_queue(adapter->get_dispatcher())),
        _scanner(scanner),
        _scanner_queue(threading::make_queue()) // Use current dispatcher
{

}

BleScannerAdapterProxy::~BleScannerAdapterProxy()
{
    // Make sure we have called disconnect() before
    assert(_scanner == nullptr);
    assert(_adapter == nullptr);
}

void BleScannerAdapterProxy::connect()
{
    // We need to execute this on adapter's queue
    _adapter_queue->post([self = shared_from_this(), this](){
        _adapter->set_delegate(self);
    });
}

void BleScannerAdapterProxy::disconnect()
{
    // Called on scanner's queue so this is safe:
    _scanner_queue->disable();
    _scanner = nullptr;

    // We need to execute this on adapter's queue
    _adapter_queue->post([self = shared_from_this(), this](){
        _adapter_queue->disable();

        // Make sure adapter does not hold a reference anymore
        _adapter->set_delegate(nullptr);
        _adapter = nullptr;
    });
}

void BleScannerAdapterProxy::start_scanning()
{
    _adapter_queue->post([self = shared_from_this(), this](){
        _adapter->start_scanning(eddystone_services_uuids, platform::BleScanFlags::BACKGROUND);
    });
}

void BleScannerAdapterProxy::stop_scanning()
{
    _adapter_queue->post([self = shared_from_this(), this](){
        // Stop scanning
        _adapter->stop_scanning();

        // Look at all devices: all devices that are only visible should now be lost and removed from the cache
        std::vector<std::shared_ptr<DeviceProxy>> lost_devices;
        for(auto cit = _ble_devices_map.cbegin(); cit != _ble_devices_map.cend(); )
        {
            auto& device_proxy = cit->second;
            if(device_proxy->ble_device->status() == platform::BleDeviceStatus::DISCONNECTED)
            {
                device_proxy->ble_device = nullptr;
                lost_devices.push_back(device_proxy);
                cit = _ble_devices_map.erase(cit);
            }
            else
            {
                cit++;
            }
        }

        _scanner_queue->post([this, lost_devices = std::move(lost_devices)](){
            for(auto& device : lost_devices)
            {
                _scanner->adapter_proxy_update_device_status(device->transport_device.lock(), transport::DeviceStatus::Lost);
            }
        });
    });
}

void BleScannerAdapterProxy::connect_device(const std::shared_ptr<Device> & device)
{
    _adapter_queue->post([self = shared_from_this(), this, device](){
        auto device_proxy = device_proxy_from_transport_device(device);
        _adapter->connect_device(device_proxy->ble_device,
                                 platform::BleDeviceConnectionFlags::PHY_1M
                                 | platform::BleDeviceConnectionFlags::PHY_2M);
    });
}

void BleScannerAdapterProxy::disconnect_device(const std::shared_ptr<Device> & device)
{
    _adapter_queue->post([self = shared_from_this(), this, device](){
        auto device_proxy = device_proxy_from_transport_device(device);
        _adapter->disconnect_device(device_proxy->ble_device);
    });
}

void BleScannerAdapterProxy::send_message(const std::shared_ptr<Device>& device, uint32_t channel, const bytes_t& message)
{
    _adapter_queue->post([self = shared_from_this(), this, device, channel, message](){
        auto device_proxy = device_proxy_from_transport_device(device);
        auto transport_device = device_proxy->transport_device.lock();

        if(!device_proxy->id_checked || (transport_device == nullptr))
        {
            logging::err() << "Trying to send messsage to unknown device";
            return;
        }

        if(transport_device->status() != DeviceStatus::Connected)
        {
            logging::warn() << "Trying to send message to a device which is not connected";
            return;
        }
        
        // Push message at back of queue
        device_proxy->tx_partial_messages.push(std::make_pair(channel, message));
        
        // Are we currently not transmitting? If so start doing so
        if( device_proxy->tx_partial_messages.size() == 1 ) {
            this->send_next_fragment(device_proxy);
        }
    });
}

void BleScannerAdapterProxy::send_next_fragment(const std::shared_ptr<DeviceProxy>& device_proxy)
{
    if( !device_proxy->tx_partial_messages.empty() )
    {
        if(device_proxy->tx_partial_messages.front().second.empty())
        {
            device_proxy->tx_next_fragment_num = 0;
            device_proxy->tx_partial_messages.pop();
        }
    }
    
    if( device_proxy->tx_partial_messages.empty() )
    {
        // Return - all done
        return;
    }
    
    auto channel = device_proxy->tx_partial_messages.front().first;
    auto& message = device_proxy->tx_partial_messages.front().second;
    
    uint32_t mtu = static_cast<uint32_t>(device_proxy->ble_device->gattc_mtu());
    assert(mtu >= 20);
    
    // Split in smaller messages
    auto& fragment_num = device_proxy->tx_next_fragment_num;
    assert(fragment_num <= 0x7f);
    std::vector<uint8_t> fragment;
    fragment.push_back(static_cast<uint8_t>(fragment_num));
        
    if(fragment_num == 0)
    {
        // Include channel number in first fragment
        assert(channel <= 0xff);
        fragment.push_back(static_cast<uint8_t>(channel));
    }
    
    // How many bytes can we copy?
    size_t copy_size = std::min(mtu - fragment.size(), message.size());
    
    fragment.insert(fragment.cend(), message.cbegin(), message.cbegin() + copy_size);
    message.erase(message.cbegin(), message.cbegin() + copy_size);
    
    // If this is the last fragment, set is as so
    if(message.empty())
    {
        fragment[0] |= 0x80;
    }
    
    fragment_num++;
    _adapter->gattc_characteristic_write(device_proxy->tx_characteristic, fragment, false);
}

void BleScannerAdapterProxy::on_status_updated()
{
    auto status = _adapter->get_status();
    _scanner_queue->post([self = shared_from_this(), this, status](){
        _scanner->adapter_proxy_update_adapter_status(status);
    });
}

void BleScannerAdapterProxy::on_device_discovered(const std::shared_ptr<platform::BleDevice> & device, int32_t rssi, const std::vector<std::vector<uint8_t>> & services, const std::vector<platform::BleServicesDataType> & services_data, const boost::optional<platform::BleManufacturerSpecificDataType> & manufacturer_specific_data)
{
    // Check if we know this device
    auto device_proxy = device_proxy_from_ble_device(device);
    if(!device_proxy->id_checked)
    {
        device_proxy->id_checked = true;

        // Find eddystone service
        auto service_cit = std::find_if(services.cbegin(), services.cend(),
                                        [](const std::vector<uint8_t>& service_uuid){
                                            if(service_uuid == eddystone_service_uuid)
                                            {
                                                return true;
                                            }
                                            return false;
                                        }
        );

        if(service_cit == services.cend())
        {
            // Do not go further
            return;
        }

        // Find service data
        auto service_data_cit = std::find_if(services_data.cbegin(), services_data.cend(),
                                             [](const platform::BleServicesDataType& service_data){
                                                 if(service_data.service_uuid == eddystone_service_uuid)
                                                 {
                                                     return true;
                                                 }
                                                 return false;
                                             }
        );

        if(service_data_cit == services_data.cend())
        {
            // Do not go further
            return;
        }

        logging::debug() << "Found new Eddystone BLE device " << device.get();

        // Check that we have at least one byte in the frame
        if( service_data_cit->data.size() < 1 )
        {
            logging::warn() << "Eddystone frame too short";
            return;
        }

        // Check frame type
        switch (service_data_cit->data[0]) {
            case EDDYSTONE_UID_FRAME_TYPE:
            {
                // Check that we have at least one byte in the frame
                if( service_data_cit->data.size() != 20 )
                {
                    logging::warn() << "Invalid UID Eddystone frame size";
                    return;
                }

                // Skip frame type
                auto data_cit = service_data_cit->data.cbegin();
                data_cit++;

                // Ignore Tx Power for now
                *data_cit++;

                // Copy namespace & instance
                device_proxy->id.id.emplace(data_cit, data_cit + 16);

                break;
            }

            case EDDYSTONE_URL_FRAME_TYPE:
            {
                // Check that we have at least one byte in the frame
                if( (service_data_cit->data.size() < 4) || (service_data_cit->data.size() > 20) )
                {
                    logging::warn() << "Invalid URL Eddystone frame size";
                    return;
                }

                // Skip frame type
                auto data_cit = service_data_cit->data.cbegin();
                data_cit++;

                // Ignore Tx Power for now
                *data_cit++;

                if(*data_cit > 3)
                {
                    logging::warn() << "Invalid URL Eddystone Scheme prefix";
                    return;
                }

                std::string decoded_url = eddystone_url_scheme_prefixes[*data_cit];

                while(++data_cit != service_data_cit->data.cend())
                {
                    if(*data_cit <= 6)
                    {
                        // Use expansion (including trailing slash)
                        decoded_url += eddystone_url_encoding_shortcuts[*data_cit];
                        decoded_url += "/";
                    }
                    else if(*data_cit <= 13)
                    {
                        // Use expansion (excluding trailing slash)
                        decoded_url += eddystone_url_encoding_shortcuts[*data_cit - 7];
                    }
                    else if(*data_cit <= 32)
                    {
                        // Invalid/RFU char, abort
                        logging::warn() << "Invalid char in URL Eddystone encoding";
                        return;
                    }
                    else if(*data_cit <= 126)
                    {
                        // Use ASCII encoding
                        decoded_url += ('\0' + *data_cit);
                    }
                    else
                    {
                        // Invalid/RFU char, abort
                        logging::warn() << "Invalid char in URL Eddystone encoding";
                        return;
                    }
                }

                // Eddystone URL is valid :)
                logging::debug() << "Eddystone URL: " << decoded_url;

                device_proxy->id.url = std::move(decoded_url);
                break;
            }

            default:
                break;
        }
    }

    // Only advertise to scanner if the id is non null
    if(device_proxy->id.id || device_proxy->id.url) {
        scanner_context_for_device(device_proxy, [this](const std::shared_ptr<transport::Device>& transport_device){
            _scanner->adapter_proxy_update_device_status(transport_device, transport::DeviceStatus::Visible);
        });
    }
}

void BleScannerAdapterProxy::on_device_connected(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error)
{
    if(error != platform::BleError::OK)
    {
        // How to exploit that error code better? Details?
        device_connection_failed(device, DeviceStatus::FailedToConnectError);
        return;
    }
    
    // Reset everything for the device
    auto device_proxy = device_proxy_from_ble_device(device);
    device_proxy->tx_next_fragment_num = 0;
    while(!device_proxy->tx_partial_messages.empty())
    {
        device_proxy->tx_partial_messages.pop();
    }
    
    device_proxy->rx_next_fragment_num = 0;
    device_proxy->rx_partial_message.clear();

    // If services haven't been discovered, do it now (can have been cached by implementation, assume services won't change - TODO)
    if(device->gattc_services().empty())
    {
        // Start services discovery
        _adapter->gattc_discover_services(device, song2_services_uuids);
    }
    else
    {
        // Move on to the next stage
        on_gattc_services_discovered(device, platform::BleError::OK);
    }
}

void BleScannerAdapterProxy::on_device_disconnected(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error)
{
    auto device_proxy = device_proxy_from_ble_device(device);

    transport::DeviceStatus device_status;
    if(_adapter->get_status() < platform::BleAdapterStatus::SCANNING_BACKGROUND)
    {
        device_status = transport::DeviceStatus::Lost;
    }
    else
    {
        device_status = transport::DeviceStatus::Visible;
    }
    scanner_context_for_device(device_proxy, [this, device_status](const std::shared_ptr<transport::Device>& transport_device){
        _scanner->adapter_proxy_update_device_status(transport_device, device_status);
    });
}

void BleScannerAdapterProxy::on_device_rssi_updated(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error)
{
    // TODO
}

void BleScannerAdapterProxy::on_gattc_services_discovered(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error)
{
    if(error != platform::BleError::OK)
    {
        device_connection_failed(device, DeviceStatus::TransportSetupProcessError);
        return;
    }

    auto services = device->gattc_services();
    auto cit = std::find_if(services.cbegin(), services.cend(),
                            [](const std::shared_ptr<platform::GattcService>& service){
                                if(service->uuid() == song2_service_uuid)
                                {
                                    return true;
                                }
                                return false;
                            }
    );

    if( cit == services.cend() )
    {
        // RequirementError
        device_connection_failed(device, DeviceStatus::TransportRequirementError);
        return;
    }

    // If characteristics haven't been discovered, do it now (can have been cached by implementation, assume characteristics won't change - TODO)
    if((*cit)->characteristics().empty())
    {
        // Discover characteristics
        _adapter->gattc_discover_characteristics(*cit, song2_characteristics_uuids);
    }
    else
    {
        // Move on to the next stage
        on_gattc_characteristics_discovered(*cit, platform::BleError::OK);
    }
}

void BleScannerAdapterProxy::on_gattc_characteristics_discovered(const std::shared_ptr<platform::GattcService> & service, platform::BleError error)
{
    if(error != platform::BleError::OK)
    {
        // Disconnect device
        auto device = _adapter->get_device_for_gattc_service(service);
        device_connection_failed(device, DeviceStatus::TransportSetupProcessError);
        return;
    }

    // Have the correct characteristics been found?
    auto characteristics = service->characteristics();

    auto find_characteristic = [&characteristics](const std::vector<uint8_t>& uuid) -> std::shared_ptr<platform::GattcCharacteristic>{
        auto cit = std::find_if(characteristics.cbegin(), characteristics.cend(),
                                [uuid](const std::shared_ptr<platform::GattcCharacteristic>& characteristic){
                                    if(characteristic->uuid() == uuid)
                                    {
                                        return true;
                                    }
                                    return false;
                                }
        );
        if( cit != characteristics.cend() )
        {
            return *cit;
        }
        else
        {
            return nullptr;
        }
    };

    auto tx_char = find_characteristic(song2_tx_char_uuid);
    auto rx_char = find_characteristic(song2_rx_char_uuid);

    if( (tx_char == nullptr) || (rx_char == nullptr) )
    {
        // Implementation issue
        auto device = _adapter->get_device_for_gattc_service(service);
        device_connection_failed(device, DeviceStatus::TransportRequirementError);
        return;
    }

    // Save tx & rx chars
    auto ble_device = _adapter->get_device_for_gattc_service(service);
    auto device_proxy = device_proxy_from_ble_device(ble_device);
    device_proxy->tx_characteristic = tx_char;
    device_proxy->rx_characteristic = rx_char;

    // Discover descriptors for RX characteristic if required
    if(rx_char->descriptors().empty())
    {
        // Discover descriptors
        _adapter->gattc_discover_descriptors(rx_char);
    }
    else
    {
        // Move on to the next stage
        on_gattc_descriptors_discovered(rx_char, platform::BleError::OK);
    }
}

void BleScannerAdapterProxy::on_gattc_descriptors_discovered(const std::shared_ptr<platform::GattcCharacteristic> & characteristic, platform::BleError error)
{
    if(error != platform::BleError::OK)
    {
        // Disconnect device
        auto service = _adapter->get_gattc_service_for_characteristic(characteristic);
        auto device = _adapter->get_device_for_gattc_service(service);
        device_connection_failed(device, DeviceStatus::TransportSetupProcessError);
        return;
    }

    // OK, start notifications on RX characteristic
    if(characteristic->notifications())
    {
        on_gattc_characteristic_notifications_set(characteristic, platform::BleError::OK);
    }
    else
    {
        _adapter->gattc_characteristic_set_notifications(characteristic, true);
    }
}

void BleScannerAdapterProxy::on_gattc_characteristic_written(const std::shared_ptr<platform::GattcCharacteristic> & characteristic, platform::BleError error)
{
    // Get device from characteristic
    auto service = _adapter->get_gattc_service_for_characteristic(characteristic);
    auto device = _adapter->get_device_for_gattc_service(service);
    
    if(error != platform::BleError::OK)
    {
        logging::warn() << "Write to characteristic failed " << static_cast<std::underlying_type_t<platform::BleError>>(error);
        
        // Disconnect device
        _adapter->disconnect_device(device);
        return;
    }
    
    // Find device
    auto device_proxy = device_proxy_from_ble_device(device);

    // Send next fragment
    send_next_fragment(device_proxy);
}

void BleScannerAdapterProxy::on_gattc_characteristic_read(const std::shared_ptr<platform::GattcCharacteristic> & characteristic, platform::BleError error)
{

}

void BleScannerAdapterProxy::on_gattc_characteristic_updated(const std::shared_ptr<platform::GattcCharacteristic> & characteristic, platform::BleError error)
{
    // Get device from characteristic
    auto service = _adapter->get_gattc_service_for_characteristic(characteristic);
    auto device = _adapter->get_device_for_gattc_service(service);

    // Find device
    auto device_proxy = device_proxy_from_ble_device(device);

    if(device_proxy->rx_characteristic->identifier() != characteristic->identifier() )
    {
        // Not the right characteristic, ignore
        return;
    }

    const auto& value = characteristic->value();
    if(!value)
    {
        throw std::runtime_error("Characteristic value updated but not set");
    }

    // Retrieve iterator
    auto cit = value->cbegin();

    // Make sure characteristic value is big enough
    if( cit == value->cend() )
    {
        logging::warn() << "RX Characteristic value is too small";
        return;
    }

    // Decode fragment
    uint8_t fragment_num = *cit & 0x7F;
    bool last_fragment = !!(*cit & 0x80);
    cit++;

    // Check fragment number
    if( (fragment_num != 0) && (fragment_num != device_proxy->rx_next_fragment_num) )
    {
        // Invalid fragment number, reset fragments context and ignore
        logging::warn() << "Received fragment number " << fragment_num << " although was expecting " << device_proxy->rx_next_fragment_num ;
        device_proxy->rx_next_fragment_num = 0;
        device_proxy->rx_partial_message.clear();
        return;
    }

    // If first fragment, retrieve channel number
    if( fragment_num == 0 )
    {
        // Make sure characteristic value is big enough
        if( cit == value->cend() )
        {
            logging::warn() << "RX Characteristic value is too small";
            return;
        }

        device_proxy->current_channel_num = *cit++;

        // Clear partial fragment
        device_proxy->rx_partial_message.clear();
    }

    // Copy data to buffer
    device_proxy->rx_partial_message.insert(device_proxy->rx_partial_message.end(), cit, value->cend());

    // Check if it is last fragment
    if( last_fragment )
    {
        // Pass message to BleScanner
        scanner_context_for_device(device_proxy, [this, channel_num = device_proxy->current_channel_num, message = std::move(device_proxy->rx_partial_message)](const std::shared_ptr<transport::Device>& transport_device) {
            _scanner->adapter_proxy_received_message(transport_device, channel_num, message);
        });

        device_proxy->rx_next_fragment_num = 0;
        device_proxy->rx_partial_message.clear();
        return;
    }

    // Update next fragment number
    device_proxy->rx_next_fragment_num = fragment_num + 1;
}

void BleScannerAdapterProxy::on_gattc_characteristic_notifications_set(const std::shared_ptr<platform::GattcCharacteristic> & characteristic, platform::BleError error)
{
    auto service = _adapter->get_gattc_service_for_characteristic(characteristic);
    auto device = _adapter->get_device_for_gattc_service(service);
    
    if(error != platform::BleError::OK)
    {
        // Disconnect device
        device_connection_failed(device, DeviceStatus::TransportSetupProcessError);
        return;
    }

    // Update CCCD
    for(auto& descriptor : characteristic->descriptors())
    {
        if(descriptor->uuid() == cccd_descriptor_uuid)
        {
            _adapter->gattc_descriptor_write(descriptor, cccd_enable_notif);
            return;
        }
    }
    
    // Could not find descritpor
    device_connection_failed(device, DeviceStatus::TransportSetupProcessError);
    return;
}

void BleScannerAdapterProxy::on_gattc_descriptor_written(const std::shared_ptr<platform::GattcDescriptor> & descriptor, platform::BleError error)
{
    auto characteristic = _adapter->get_gattc_characteristic_for_descriptor(descriptor);
    auto service = _adapter->get_gattc_service_for_characteristic(characteristic);
    auto ble_device = _adapter->get_device_for_gattc_service(service);
    
    if(error != platform::BleError::OK)
    {
        // Disconnect device
        device_connection_failed(ble_device, DeviceStatus::TransportSetupProcessError);
        return;
    }

    auto device_proxy = device_proxy_from_ble_device(ble_device);
    
    // OK, connected
    scanner_context_for_device(device_proxy, [this](const std::shared_ptr<transport::Device>& transport_device) {
        _scanner->adapter_proxy_update_device_status(transport_device, transport::DeviceStatus::Connected);
    });
}

void BleScannerAdapterProxy::on_gattc_descriptor_read(const std::shared_ptr<platform::GattcDescriptor> & descriptor, platform::BleError error)
{

}

void BleScannerAdapterProxy::on_gattc_mtu_updated(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error)
{

}

void BleScannerAdapterProxy::on_gattc_phy_updated(const std::shared_ptr<platform::BleDevice> & device, platform::BleError error)
{

}

// These methods MUST be called on adapter's queue
BleScannerAdapterProxy::DeviceProxy::DeviceProxy() :
        id_checked(false), rx_characteristic(nullptr), tx_characteristic(nullptr), ble_device(nullptr),
        transport_device(), current_channel_num(0), rx_next_fragment_num(0), tx_next_fragment_num(0)
{

}

std::shared_ptr<BleScannerAdapterProxy::DeviceProxy> BleScannerAdapterProxy::device_proxy_from_ble_device(const std::shared_ptr<platform::BleDevice>& ble_device)
{
    auto cit = _ble_devices_map.find(ble_device->identifier());
    if(cit == _ble_devices_map.cend())
    {
        // Create new one
        auto device_proxy = std::make_shared<DeviceProxy>();
        device_proxy->ble_device = ble_device;
        _ble_devices_map[ble_device->identifier()] = device_proxy;
        return device_proxy;
    }

    return cit->second;
}

std::shared_ptr<BleScannerAdapterProxy::DeviceProxy> BleScannerAdapterProxy::device_proxy_from_transport_device(const std::shared_ptr<transport::Device>& transport_device)
{
    auto device_proxy = static_cast<DeviceProxy::TransportDeviceWrapper*>(transport_device.get())->device_proxy.lock();
    if(device_proxy == nullptr)
    {
        // Error, the device is gone
        throw std::runtime_error("Unknown device");
    }

    return device_proxy;
}

BleScannerAdapterProxy::DeviceProxy::TransportDeviceWrapper::TransportDeviceWrapper(const std::shared_ptr<DeviceProxy>& device_proxy, const DeviceIdentifier& id, Scanner* scanner) : Device::Device(id, scanner), device_proxy(device_proxy)
{
}

BleScannerAdapterProxy::DeviceProxy::TransportDeviceWrapper::~TransportDeviceWrapper()
{
}

void BleScannerAdapterProxy::scanner_context_for_device(const std::shared_ptr<DeviceProxy>& device_proxy, std::function<void(std::shared_ptr<transport::Device>)>&& fn)
{
    if(!device_proxy->id_checked)
    {
        return; // don't bother
    }

    _scanner_queue->post([this, device_proxy, fn = std::move(fn)](){
        // Transport devices need to be created and destructed on scanner's queue
        auto transport_device = device_proxy->transport_device.lock();
        if(transport_device == nullptr)
        {
            transport_device = std::make_shared<DeviceProxy::TransportDeviceWrapper>(device_proxy, device_proxy->id, _scanner);
            device_proxy->transport_device = transport_device;
        }

        fn(transport_device);
    });
}

void BleScannerAdapterProxy::device_connection_failed(const std::shared_ptr<platform::BleDevice> & device, DeviceStatus error)
{
    // Advertise to scanner
    auto device_proxy = device_proxy_from_ble_device(device);
    scanner_context_for_device(device_proxy, [this, error](const std::shared_ptr<transport::Device>& transport_device){
        _scanner->adapter_proxy_update_device_status(transport_device, error);
    });

    // Disconnect device
    _adapter->disconnect_device(device);
}
