//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  BleScanner.hpp
//  song2-tests
//
//  Created by Donatien Garnier on 15/01/2018.

//

#pragma once

#include <memory>
#include <vector>
#include <map>

#include "cpp/transport/Scanner.hpp"
#include "gen/djinni/cpp/ble_adapter_status.hpp"
#include "gen/djinni/cpp/ble_adapter.hpp"

namespace arm
{
    namespace song2
    {
        namespace transport
        {
            class BleScannerAdapterProxy;
            class BleScanner : public Scanner
            {
            public:
                BleScanner(const std::shared_ptr<platform::BleAdapter>& adapter);
                virtual ~BleScanner();
                
                threading::Subscriber<platform::BleAdapterStatus>::ptr adapter_status();
            protected:
                // Scanner
                virtual void start_specific_scanning() override;
                virtual void stop_specific_scanning() override;
                virtual void connect_specific_device(const std::shared_ptr<Device>& device) override;
                virtual void disconnect_specific_device(const std::shared_ptr<Device>& device) override;
                virtual typename pipe::Endpoint::ptr open_specific_endpoint_for_device_and_channel(const std::shared_ptr<Device>& device, uint32_t channel) override;
                virtual const std::string& transport_name() const override;
                
            private:
                // Adapter proxy
                friend class BleScannerAdapterProxy;
                void adapter_proxy_update_device_status(const std::shared_ptr<Device>& device, DeviceStatus status);
                void adapter_proxy_update_adapter_status(platform::BleAdapterStatus status);
                void adapter_proxy_received_message(const std::shared_ptr<Device>& device, uint32_t channel, const bytes_t& message);
            private:
                threading::Observable<platform::BleAdapterStatus> _adapter_status_obs;
                std::shared_ptr<BleScannerAdapterProxy> _adapter_proxy;
                
                std::map< std::shared_ptr<Device>, std::map<uint32_t /* channel num */, pipe::Endpoint::ptr> > _endpoints;
            };
        }
    }
}
