//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#pragma once

#include <string>
#include <vector>

#include "acore/ac_buffer.h"

namespace arm {
    namespace song2 {
        /** \addtogroup Types
         *
         * This module provides some generic type containers and various converters.
         *  @{
         */
        
        /** A generic id type used be the database
        */
        using id_t = int64_t;
        
        /** A generic type to store a byte-array
         *
         * This derives from std::vector<uint8_t>
         */
        struct bytes_t : std::vector<uint8_t> {
            using std::vector<uint8_t>::vector;
        };
        
        /** A generic type to store a JSON-formatted string
         *
         * This derives from std::string. No JSON-validity checks are performed.
         */
        struct json_t : std::string {
            using std::string::string;
            json_t(const std::string s) : std::string(s) { };
        };
        
        /** Stores the content of a acore buffer into a bytes_t instance
         *
         * \param buffer the ac_buffer_t instance to convert
         * \return a bytes_t instance containing a copy of the buffer's content
         */
        bytes_t ac_buffer_to_bytes(ac_buffer_t* buffer);
        
        /** Map a bytes_t instance's content onto an ac_buffer_t instance
         *
         * No copy is performed, the bytes_t instance must therefore remain in scope while the ac_buffer_t instance is used
         * \param buffer the ac_buffer_t instance to initialize
         * \param bytes the bytes_t instance to create a mapping from
         */
        void ac_buffer_map_bytes(ac_buffer_t* buffer,
                               const bytes_t& bytes);
        
        /** @}*/
    }
}
