//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  Types.cpp
//  song2
//
//  Created by Donatien Garnier on 12/01/2018.

//

#include "Types.hpp"

#include "acore/ac_buffer_reader.h"

using namespace arm::song2;

bytes_t arm::song2::ac_buffer_to_bytes(ac_buffer_t* buffer)
{
    bytes_t bytes;
    
    // Work on a copy
    ac_buffer_t buffer_cpy;
    ac_buffer_dup(&buffer_cpy, buffer);
    
    size_t l = ac_buffer_reader_readable(&buffer_cpy);
    bytes.resize(l);
    
    ac_buffer_read_n_bytes(&buffer_cpy, bytes.data(), l);
    
    return bytes;
}

void arm::song2::ac_buffer_map_bytes(ac_buffer_t* buffer,
                      const bytes_t& bytes)
{
    ac_buffer_init(buffer, bytes.data(), bytes.size());
}
