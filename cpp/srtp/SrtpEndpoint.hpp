//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  SrtpEndpoint.hpp
//  song2-tests
//
//  Created by Donatien Garnier on 15/01/2018.

//

#pragma once

#include <list>

#include "cpp/threading/Queue.hpp"
#include "cpp/threading/EnableExecuteIfValid.hpp"
#include "cpp/pipe/Endpoint.hpp"
#include "cpp/types/Types.hpp"


extern "C" {
#include "srtp/srtp.h"
}

namespace arm {
    namespace song2 {
        namespace srtp {
            /** \addtogroup SRTP
             *
             * This module provides a C++ API on top of the SRTP library. This provides a reliability layer on top of the underlying transport (a bit like TCP) and handles retransmissions.
             *  @{
             */
            
            /** SRTP C++ mapping
             *
             * This class provides a C++ API on top of the SRTP library and implements the required retransmissions and buffer storage.
             */
            class SrtpEndpoint : public threading::EnableExecuteIfValid
            {
            public:
                /** Instantiate a SrtpEndpoint
                 * The instantiation (and any other calls to this class) must be run on the dispatcher that should be used for retransmissions
                 * \param transport_endpoint The underlying transport (BLE, NFC, etc.) socket
                 */
                SrtpEndpoint(typename pipe::Endpoint::ptr&& transport_endpoint);
                
                /** Destroy the SrtpEndpoint
                 */
                ~SrtpEndpoint();
                
                /** Open the connection
                 * Should only be called ONCE
                 * \return pipe::Endpoint instance that ensures transport reliability
                 */
                pipe::Endpoint::ptr open();
                
                // SRTP API implementation
                void channel_sent(srtp_channel_t* channel, bool success);
                void channel_received(srtp_channel_t* channel, ac_buffer_t* msg);
                void channel_transport_send(srtp_channel_t* channel, ac_buffer_t* pkt);
                void channel_closed(srtp_channel_t* channel);
                void tx_pending_push(srtp_channel_t* channel, ac_buffer_t* msg);
                void tx_pending_get(srtp_channel_t* channel, size_t pos, ac_buffer_t* msg);
                void tx_pending_pop(srtp_channel_t* channel);
                
            private:
                // Queue retransmission or throw exception if maximum number of retries reached
                void queue_retransmission_or_fail();
                
                // Cancel retransmissions
                void cancel_retransmissions();
                
                // This needs to be destructed *after* endpoints as they will use it...
                struct RetransmissionToken : threading::EnableExecuteIfValid {};
                RetransmissionToken _retransmission_token;
                
                // Queue used for retransmissions
                threading::Queue::ptr _queue;
                
                std::list<bytes_t> _tx_buffers;
                
                srtp_channel _srtp_channel;
                size_t _retransmissions_count;
                
                // Endpoints should be last constructed and first to be destroyed
                pipe::Endpoint::ptr _transport_ep;
                pipe::Endpoint::ptr _reliable_ep;
            };
            /** @}*/
        }
    }
}
