//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

//
//  SrtpEndpoint.cpp
//  song2-tests
//
//  Created by Donatien Garnier on 15/01/2018.

//

#include "SrtpEndpoint.hpp"

#include <stdexcept>

using namespace arm::song2;
using namespace arm::song2::srtp;

// SRTP implementation
static void srtp_impl_channel_sent(srtp_channel_t* channel, bool success, void* user);
static void srtp_impl_channel_received(srtp_channel_t* channel, ac_buffer_t* msg, void* user);
static void srtp_impl_channel_transport_send(srtp_channel_t* channel, ac_buffer_t* pkt, void* user);
static void srtp_impl_channel_closed(srtp_channel_t* channel, void* user);
static void srtp_impl_tx_pending_push(srtp_channel_t* channel, ac_buffer_t* msg, void* user);
static void srtp_impl_tx_pending_get(srtp_channel_t* channel, size_t pos, ac_buffer_t* msg, void* user);
static void srtp_impl_tx_pending_pop(srtp_channel_t* channel, void* user);

const static srtp_implementation_t srtp_endpoint_vtable
{
    .channel_sent = srtp_impl_channel_sent,
    .channel_received = srtp_impl_channel_received,
    .channel_transport_send = srtp_impl_channel_transport_send,
    .channel_closed = srtp_impl_channel_closed,
    .tx_pending_push = srtp_impl_tx_pending_push,
    .tx_pending_get = srtp_impl_tx_pending_get,
    .tx_pending_pop = srtp_impl_tx_pending_pop,
};

SrtpEndpoint::SrtpEndpoint(typename pipe::Endpoint::ptr&& transport_endpoint) : _queue(threading::make_queue()), _retransmissions_count(0), _transport_ep(std::move(transport_endpoint)), _reliable_ep(nullptr)
{
    // This won't start any I/O
    srtp_channel_open(&_srtp_channel, &srtp_endpoint_vtable);
    srtp_channel_set_user(&_srtp_channel, this);
}
    
SrtpEndpoint::~SrtpEndpoint()
{
    // Disable queue
    _queue->disable();
}

// Should only be called ONCE on initialization
pipe::Endpoint::ptr SrtpEndpoint::open()
{
    if( _reliable_ep != nullptr )
    {
        throw std::runtime_error("SrtpEndpoint::open() called more than once");
    }
    pipe::Endpoint::ptr remote_ep;
    std::tie(remote_ep, _reliable_ep) = pipe::make_endpoints();
    
    // Start processing transport layer
    _transport_ep->receive_async(execute_if_valid([this](std::future<bytes_t>& fut){
        try {
            ac_buffer_t pkt;
            auto bytes = fut.get();
            ac_buffer_map_bytes(&pkt, bytes);
            srtp_channel_transport_received(&_srtp_channel, &pkt);
        }
        catch(...)
        {
            cancel_retransmissions();
            if( _reliable_ep != nullptr )
            {
                _reliable_ep->close();
            }
        }
    }), _queue);
    
    // Start processing reliable layer
    _reliable_ep->receive_async(execute_if_valid([this](std::future<bytes_t>& fut){
        try {
            ac_buffer_t message;
            auto bytes = fut.get();
            ac_buffer_map_bytes(&message, bytes);
            srtp_channel_send(&_srtp_channel, &message);
        }
        catch(...)
        {
            // Do not close SRTP channel as the underlying transport has failed
            cancel_retransmissions();
            _transport_ep->close();
        }
    }), _queue);
    
    return remote_ep;
}

// SRTP API implementation

void SrtpEndpoint::channel_sent(srtp_channel_t* channel, bool success)
{
    if(success && _tx_buffers.empty())
    {
        cancel_retransmissions();
    }
}

void SrtpEndpoint::channel_received(srtp_channel_t* channel, ac_buffer_t* msg)
{
    _reliable_ep->send(ac_buffer_to_bytes(msg));
}

void SrtpEndpoint::channel_transport_send(srtp_channel_t* channel, ac_buffer_t* pkt)
{
    // Reset retransmission timer
    cancel_retransmissions();
    queue_retransmission_or_fail();
    _transport_ep->send(ac_buffer_to_bytes(pkt));
}

void SrtpEndpoint::channel_closed(srtp_channel_t* channel)
{
    // Safe to call multiple times
    _transport_ep->close();
    _reliable_ep->close();
}

void SrtpEndpoint::tx_pending_push(srtp_channel_t* channel, ac_buffer_t* msg)
{
    _tx_buffers.push_back( ac_buffer_to_bytes(msg) );
}

void SrtpEndpoint::tx_pending_get(srtp_channel_t* channel, size_t pos, ac_buffer_t* msg)
{
    auto cit = _tx_buffers.cbegin();
    while( pos > 0 )
    {
        if( cit == _tx_buffers.cend() )
        {
            // Error, return empty buffer
            ac_buffer_init(msg, NULL, 0);
            return;
        }
        cit++;
        pos--;
    }
    
    if( cit == _tx_buffers.cend() )
    {
        // Error, return empty buffer
        ac_buffer_init(msg, NULL, 0);
        return;
    }
    
    ac_buffer_map_bytes(msg, *cit);
}

void SrtpEndpoint::tx_pending_pop(srtp_channel_t* channel)
{
    _tx_buffers.pop_front();
}

void SrtpEndpoint::queue_retransmission_or_fail()
{
    if(_retransmissions_count < 5)
    {
        _queue->post_delayed(
                                 execute_if_valid( // guard on class instance
                                 _retransmission_token.execute_if_valid( // guard on token
                                 [this]()
                                 {
                                     // Send keepalive
                                     srtp_channel_send_keepalive(&_srtp_channel);
                                     _retransmissions_count++;
                                     queue_retransmission_or_fail();
                                 })),
                                 std::chrono::milliseconds(300 * (1 << _retransmissions_count))
                             );
    }
    else
    {
        // Close all
        _transport_ep->close();
        _reliable_ep->close();
    }
}

void SrtpEndpoint::cancel_retransmissions()
{
    // Invalidate retransmission token
    _retransmission_token = RetransmissionToken();
    _retransmissions_count = 0;
}

// C-bindings
void srtp_impl_channel_sent(srtp_channel_t* channel, bool success, void* user)
{
    srtp::SrtpEndpoint* inst = static_cast<srtp::SrtpEndpoint*>(user);
    inst->channel_sent(channel, success);
}

void srtp_impl_channel_received(srtp_channel_t* channel, ac_buffer_t* msg, void* user)
{
    srtp::SrtpEndpoint* inst = static_cast<srtp::SrtpEndpoint*>(user);
    inst->channel_received(channel, msg);
}

void srtp_impl_channel_transport_send(srtp_channel_t* channel, ac_buffer_t* pkt, void* user)
{
    srtp::SrtpEndpoint* inst = static_cast<srtp::SrtpEndpoint*>(user);
    inst->channel_transport_send(channel, pkt);
}

void srtp_impl_channel_closed(srtp_channel_t* channel, void* user)
{
    srtp::SrtpEndpoint* inst = static_cast<srtp::SrtpEndpoint*>(user);
    inst->channel_closed(channel);
}

void srtp_impl_tx_pending_push(srtp_channel_t* channel, ac_buffer_t* msg, void* user)
{
    srtp::SrtpEndpoint* inst = static_cast<srtp::SrtpEndpoint*>(user);
    inst->tx_pending_push(channel, msg);
}

void srtp_impl_tx_pending_get(srtp_channel_t* channel, size_t pos, ac_buffer_t* msg, void* user)
{
    srtp::SrtpEndpoint* inst = static_cast<srtp::SrtpEndpoint*>(user);
    inst->tx_pending_get(channel, pos, msg);
}

void srtp_impl_tx_pending_pop(srtp_channel_t* channel, void* user)
{
    srtp::SrtpEndpoint* inst = static_cast<srtp::SrtpEndpoint*>(user);
    inst->tx_pending_pop(channel);
}
