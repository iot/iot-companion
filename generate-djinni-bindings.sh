#!/bin/bash

#
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

TARGET_DIR=$(pwd)/gen/djinni
IDL_DIR=$(pwd)/cpp
IDL=$(ls -1 "${IDL_DIR}" | grep ".djinni")
if [ ! -f "${IDL}" ]; then
   IDL=${IDL_DIR}/${IDL}
fi
if [ ! -d "${DJINNI_PATH}" ]; then
   DJINNI_POTENTIAL_DIR=$(pwd)/dependencies/djinni/
   if [ ! -d "${DJINNI_POTENTIAL_DIR}" ]; then
      echo "The DJINNI_PATH environment variable does not point to a correct location"
      exit 1
   fi
   DJINNI_PATH=${DJINNI_POTENTIAL_DIR}
fi
echo Generating Djinni bindings...
pushd "${DJINNI_PATH}"
rm -r "${TARGET_DIR}"
mkdir -p "${TARGET_DIR}" || exit 1
mkdir "${TARGET_DIR}/lib"
cp -R support-lib/* "${TARGET_DIR}/lib"
java -jar ./src/target/scala-2.11/src-assembly-0.1-SNAPSHOT.jar \
   --java-out "${TARGET_DIR}/java/com/arm/song2/api" \
   --java-package com.arm.song2.api \
   --java-generate-interfaces true \
   --java-implement-android-os-parcelable true \
   --ident-java-field mFooBar \
   --ident-cpp-method foo_bar \
   --ident-cpp-type FooBar \
   --cpp-namespace arm::song2::platform \
   --cpp-optional-template "boost::optional" \
   --cpp-optional-header "<boost/optional.hpp>" \
   --cpp-out "${TARGET_DIR}/cpp" \
   --jni-out "${TARGET_DIR}/jni" \
   --ident-jni-class NativeFooBar \
   --ident-jni-file native_foo_bar \
   --objc-out "${TARGET_DIR}/objc" \
   --objc-type-prefix S2 \
   --objcpp-out "${TARGET_DIR}/objc" \
   --objcpp-include-cpp-prefix "../cpp/" \
   --objc-base-lib-include-prefix "../lib/objc/" \
   --idl "${IDL}" \
|| exit 1
popd
echo ...done
