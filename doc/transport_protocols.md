# Song2 Transport protocols
Song2 SW supports multiple protocols (non-IP & IP). Each of these protocols should provide an associated discovery mechanism.
Apart from the discovery which is transport-specific, all of these transports are subsequentially treated as a pipe.
Transport reliability, security and applicative layers are kept separate and are the same no matter what the underlying transport is.

## BLE
### Discovery
GAP Advertising is used for discovery. We do not mandate what it should be but a Physical Web-compatible approach is encouraged (Eddystone-URL beacon).

### GATT Service Transport
The first underlying transport that can be used is through a GATT service with the following characteristics.
The BLUR GATT Service's UUID is EC8C0100-EC9B-30AF-1D41-AB00BEB494DF.

| Name              	| Property 	| Length     	| UUID                                 	|
|-------------------	|----------	|------------	|--------------------------------------	|
| TX Characteristic 	| Write    	| Minimum 20*	| EC8C0101-EC9B-30AF-1D41-AB00BEB494DF 	|
| RX Characteristic 	| Notify   	| Minimum 20*	| EC8C0102-EC9B-30AF-1D41-AB00BEB494DF 	|
| Service revision      | Read   	| 4         	| EC8C0103-EC9B-30AF-1D41-AB00BEB494DF 	|

_* Implementations should use the negotiated MTU_

The service revision is encoded as a big endian, unsigned, 32-bit integer. The current (and only) supported service revision is 1.

#### Frames format and fragmentation

All frames are sent on the TX characteristic by the client which responds by sending notifications on the RX characteristic.

Frames can be fragmented if they do not fit the maximum length of a characteristic.

Each fragment must have the following format:

| Name                  | Value                                                 | Length
|---------------------- |------------------------------------------------------ | ----------------------------------------
| Last fragment flag    | 1: Final fragment; 0: More fragments to come          | 1 bit (first byte's MSb)
| Fragment number       | Starts with 0 and increments with each fragment       | 7 bit
| Channel number        | Always set to 0 for now                               | 1 byte (only present in first fragment)
| Payload               |                                                       | Variable

If an incorrect fragment number is received, the whole frame should be ignored till a first fragment (numbered 0) is received. 

### L2CAP Channel Transport
_TODO, not implemented yet_

An L2CAP-channel implementation would be more efficient in terms of performance and therefore energy consumption. Should improve DFU performance.
[It's supported on iOS 11 for now](https://developer.apple.com/documentation/corebluetooth/cbperipheral/2880151-openl2capchannel), Android support to be monitored. Nordic's SDK supports it.

## NFC

The NFC implementation uses the following protocol stack:
* ISO 14443-3A/NFC A (NFC Forum terminology)
* ISO 14443-4/ISO-DEP
* ISO 7816-4

### Discovery
A NDEF message is used for discovery (NFC Forum Type 4 tag). Once again, it should be application-dependent but using a URL is widely encouraged.

### Transport
An ISO 7816-4 application with the following UID needs to be selected: _TODO_.
Once selected, APDUs are exchanged on the link.
Ideally the UID should be registered according to ISO 7816-5.
The transport is half-duplex and always initiated by the reader. Both endpoints are allowed to send empty messages if they have nothing to send.

TODO:
* Select UID
* Handle fragmentation in a similar way to BLE GATT
* Select INS
* Provide indications on back-off mechanism from both sides when there's nothing to send

## Other transports to be considered
* WiFi (mDNS for discovery, then TCP or UDP socket)
    * If UDP, assign some kind of connection ID
    * Could be useful for testing
* UART
* USB
* SWD

