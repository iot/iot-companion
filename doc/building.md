# Build System
For macOS and iOS, building uses XCode

For Android, Android Studio + CMake (C++) + Gradle (Java)

For Linux, CMake

For mbed-OS, mbed-cli

