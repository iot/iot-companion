
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name = "arpc-codegen",
    version = "2.0.13",
    author = "Donatien Garnier",
    author_email = "donatien.garnier@arm.com",
    description = ("Embedded code generator for ARPC Framework"),
    license = "Apache 2 (c) ARM Ltd 2016-2020",
    keywords = "arpc arm mbed rpc code generator",
    url = "https://mbed.com",
    packages=find_packages(),
    package_data={
        'arpc_codegen': ['requirements.txt', 'templates/**/**.h', 'templates/**/**.c', 'templates/**/*.hpp', 'templates/**/*.cpp', 'services/*.proto', 'services/*.options']
    },
    scripts=['bin/arpc-codegen', 'bin/protoc-gen-arpc'],
    install_requires=requirements
)