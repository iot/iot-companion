//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#define __MODULE__ "server.c"

#include "acore/buffer.h"
#include "acore/buffer_reader.h"
#include "acore/buffer_builder.h"

#include <string.h>

#include "server.h"

#include "arpc/arpc.h"
#include "arpc/arpc_responses.h"

#include "discovery.pb.h"

\ for file in files
#include "{{ file.c_name }}.arpc.h"
#include "{{ file.c_name }}.pb.h"
\ endfor


typedef void (*handler_t)({{ connection_type }}, arpc_command_t command_id, const ac_buffer_t* payload);

static handler_t server_methods[] = {
\ for file in files
\ for service in file.services
\ for method in service.methods
{{ file.c_package | lower }}_{{ service.c_name | lower }}_{{ method.c_name | lower }}_impl_request,
\ endfor
\ endfor
\ endfor
};

enum server_services
{
\ for file in files
\ for service in file.services
{{ file.c_package | lower }}_{{ service.c_name | lower }}_service_number,
\ endfor
\ endfor
};

typedef enum server_services server_services_t;

#define SERVER_SERVICES_COUNT ({{ total_services_count }})
#define SERVER_METHODS_COUNT ({{ total_methods_count }})

void {{prefix}}_impl_requested({{ connection_type }},
    arpc_command_t command_id,
    arpc_method_num_t method_num, const ac_buffer_t* payload, void* user)
{
    if( method_num >= SERVER_METHODS_COUNT )
    {
        // Reject for now - could provide a way to forward
        ARPC_WARN("Method number out of range");
        arpc_respond({{ connection_param }}, command_id, true, arpc_response_err_invalid_method, NULL);
        return;
    }

    server_methods[method_num]({{ connection_param }}, command_id, payload);
}

static bool find_name(const char* name, const char* list[], uint32_t* number)
{
    *number = 0;

    while(*list != NULL)
    {
        if(!strcmp(name, *list))
        {
            return true;
        }
        list++;
        (*number)++;
    }

    *number = 0;
    return false;
}

// Discovery service
void {{prefix}}_discovery_get_method_number_request({{ connection_type }}, arpc_command_t command_id, const arpc_GetMethodNumberRequest* data)
{
    arpc_GetMethodNumberResponse response;
    switch(data->service_number)
    {
\ for file in files
\ for service in file.services
    case {{ file.c_package | lower }}_{{ service.c_name | lower }}_service_number:
        // Compare names
        response.found = find_name(data->method_name, {{prefix}}_service_{{ service.c_name | lower }}_method_names, &response.method_number);
        break;
\ endfor
\ endfor
    default:
        // Bail
        {{prefix}}_discovery_get_method_number_respond({{ connection_param }}, command_id, arpc_response_err_invalid_argument, NULL);
        return;
    }

    // Adjust offset
    if(response.found)
    {
\ for file in files
\ for service in file.services
        if( data->service_number > {{ file.c_package | lower }}_{{ service.c_name | lower }}_service_number )
        {
            response.method_number += {{ service.methods | count }};
        }
\ endfor
\ endfor
    }

    // Respond
    {{prefix}}_discovery_get_method_number_respond({{ connection_param }}, command_id, arpc_response_ok, &response);
}

static const char* services_names[] = {
\ for file in files
\ for service in file.services
"{{ file.package }}.{{ service.name }}",
\ endfor
\ endfor
NULL
};

void {{prefix}}_discovery_get_service_number_request({{ connection_type }}, arpc_command_t command_id, const arpc_GetServiceNumberRequest* data)
{
    arpc_GetServiceNumberResponse response;

    // Compare names
    response.found = find_name(data->service_name, services_names, &response.service_number);
    {{prefix}}_discovery_get_service_number_respond({{ connection_param }}, command_id, arpc_response_ok, &response);
}


void {{prefix}}_discovery_get_services_count_request({{ connection_type }}, arpc_command_t command_id, const arpc_GetServicesCountRequest* data)
{
    (void) data;
    arpc_GetServicesCountResponse response;

    response.count = SERVER_SERVICES_COUNT;
    {{prefix}}_discovery_get_services_count_respond({{ connection_param }}, command_id, arpc_response_ok, &response);
}
