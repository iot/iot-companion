//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef {{prefix | upper}}_ARPC_SERVER_H
#define {{prefix | upper}}_ARPC_SERVER_H

#include "acore/buffer.h"

#include "arpc/arpc.h"
#include "arpc/arpc_responses.h"

void {{prefix}}_impl_requested({{ connection_type }},
    arpc_command_t command_id,
    arpc_method_num_t method_num, const ac_buffer_t* payload, void* user);

#endif