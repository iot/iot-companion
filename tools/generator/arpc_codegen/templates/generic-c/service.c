//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#define __MODULE__ "{{ file.c_name }}.arpc.c"

#include "arpc/arpc.h"

#include "pb/pb.h"
#include "pb/pb_decode.h"
#include "pb/pb_encode.h"

#include "acore/buffer.h"
#include "acore/buffer_reader.h"
#include "acore/buffer_builder.h"

#include "{{ file.c_name }}.arpc.h"
#include "{{ file.c_name }}.pb.h"

static bool proto_read_callback (pb_istream_t* stream, uint8_t* buf, size_t count)
{
  ac_buffer_t* rdr = (ac_buffer_t*)(stream->state);

  if(ac_buffer_reader_readable(rdr) < count)
  {
    return false;
  }

  ac_buffer_read_n_bytes(rdr, buf, count);

  return true;
}

static void proto_read_stream(pb_istream_t* stream, ac_buffer_t* payload)
{
  stream->callback = proto_read_callback;
  stream->state = (void*) payload;
  stream->bytes_left = ac_buffer_reader_readable(payload);
  stream->errmsg = NULL;
}

static bool proto_write_callback (pb_ostream_t* stream, const uint8_t* buf, size_t count)
{
  ac_buffer_builder_t* bldr = (ac_buffer_builder_t*)(stream->state);

  if(ac_buffer_builder_writable(bldr) < count)
  {
    return false;
  }

  ac_buffer_builder_write_n_bytes(bldr, buf, count);

  return true;
}

static void proto_write_stream(pb_ostream_t* stream, ac_buffer_builder_t* output)
{
  stream->callback = proto_write_callback;
  stream->state = (void*) output;
  stream->bytes_written = 0;
  stream->max_size = ac_buffer_builder_space(output);
  stream->errmsg = NULL;
}

\ for service in file.services

\ for method in service.methods
\if method.stream_response

bool {{ file.c_package | lower }}_{{ service.c_name | lower }}_{{ method.c_name | lower }}_respond_one({{ connection_type }}, arpc_command_t command_id, const {{ method.response.c_name }}* data)
{
    ac_buffer_builder_t builder;
    uint8_t bytes[{{prefix | upper}}_SERVICE_{{ service.c_name | upper }}_BUFFER_SIZE];
    ac_buffer_builder_init(&builder, bytes, {{prefix | upper}}_SERVICE_{{ service.c_name | upper }}_BUFFER_SIZE);

    pb_ostream_t stream;
    proto_write_stream(&stream, &builder);

    bool r = pb_encode(&stream, {{ method.response.c_name }}_fields, data);
    if(!r)
    {
        ARPC_WARN("Encoding of {{ method.response.name  | lower }} failed: %s", PB_GET_ERROR(&stream));
        return {{prefix}}_respond({{ connection_param }}, command_id, true, arpc_response_err_memory_error, NULL);
    }

    return {{prefix}}_respond({{ connection_param }}, command_id, false, arpc_response_ok, ac_buffer_builder_buffer(&builder));
}

bool {{ file.c_package | lower }}_{{ service.c_name | lower }}_{{ method.c_name | lower }}_respond_finish({{ connection_type }}, arpc_command_t command_id, arpc_response_code_t response_code, const {{ method.response.c_name }}* data)
{
    ac_buffer_builder_t builder;
    uint8_t bytes[{{prefix | upper}}_SERVICE_{{ service.c_name | upper }}_BUFFER_SIZE];
    ac_buffer_builder_init(&builder, bytes, {{prefix | upper}}_SERVICE_{{ service.c_name | upper }}_BUFFER_SIZE);

    if( response_code == arpc_response_ok )
    {
        pb_ostream_t stream;
        proto_write_stream(&stream, &builder);

        bool r = pb_encode(&stream, {{ method.response.c_name }}_fields, data);
        if(!r)
        {
            ARPC_WARN("Encoding of {{ method.response.name  | lower }} failed: %s", PB_GET_ERROR(&stream));
            return {{prefix}}_respond({{ connection_param }}, command_id, true, arpc_response_err_memory_error, NULL);
        }
    }

    return {{prefix}}_respond({{ connection_param }}, command_id, true, response_code, ac_buffer_builder_buffer(&builder));
}

\else

bool {{ file.c_package | lower }}_{{ service.c_name | lower }}_{{ method.c_name | lower }}_respond({{ connection_type }}, arpc_command_t command_id, arpc_response_code_t response_code, const {{ method.response.c_name }}* data)
{
    ac_buffer_builder_t builder;
    uint8_t bytes[{{prefix | upper}}_SERVICE_{{ service.c_name | upper }}_BUFFER_SIZE];
    ac_buffer_builder_init(&builder, bytes, {{prefix | upper}}_SERVICE_{{ service.c_name | upper }}_BUFFER_SIZE);

    if( response_code == arpc_response_ok )
    {
        pb_ostream_t stream;
        proto_write_stream(&stream, &builder);

        bool r = pb_encode(&stream, {{ method.response.c_name }}_fields, data);
        if(!r)
        {
            ARPC_WARN("Encoding of {{ method.response.name  | lower }} failed: %s", PB_GET_ERROR(&stream));
            return {{prefix}}_respond({{ connection_param }}, command_id, true, arpc_response_err_memory_error, NULL);
        }
    }

    return {{prefix}}_respond({{ connection_param }}, command_id, true, response_code, ac_buffer_builder_buffer(&builder));
}

\endif
\ endfor


// Internal use

\ for method in service.methods

void {{ file.c_package | lower }}_{{ service.c_name | lower }}_{{ method.c_name | lower }}_impl_request({{ connection_type }}, arpc_command_t command_id, const ac_buffer_t* payload)
{
    ac_buffer_t rdr;
    ac_buffer_dup(&rdr, payload);

    // Deserialize
    pb_istream_t stream;
    proto_read_stream(&stream, &rdr);

    {{ method.request.c_name }} data;
    bool r = pb_decode(&stream, {{ method.request.c_name }}_fields, &data);
    if(!r)
    {
        ARPC_WARN("Decoding of {{ method.request.name | lower }} failed: %s", PB_GET_ERROR(&stream));
        {{prefix}}_respond({{ connection_param }}, command_id, true, arpc_response_err_parsing_error, NULL);
        return;
    }

    {{ file.c_package | lower }}_{{ service.c_name | lower }}_{{ method.c_name | lower }}_request({{ connection_param }}, command_id, &data);
}


\ endfor

//const char* {{prefix}}_service_{{ service.c_name | lower }}_service_name = "{{ file.c_package }}.{{ service.name }}";

const char* {{prefix}}_service_{{ service.c_name | lower }}_method_names[] = {
\ for method in service.methods
"{{ method.name }}",
\ endfor
NULL
};

\ endfor