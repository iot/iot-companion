//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef {{prefix | upper}}_ARPC_SERVICE_{{ file.c_name | upper }}_H
#define {{prefix | upper}}_ARPC_SERVICE_{{ file.c_name | upper }}_H

#include "arpc/arpc.h"
#include "arpc/arpc_responses.h"

#include "acore/buffer.h"

#include "{{ file.c_name }}.pb.h"

\ for service in file.services

//
#define {{prefix | upper}}_SERVICE_{{ service.c_name | upper }}_BUFFER_SIZE 256
//

extern const char* {{prefix}}_service_{{ service.c_name | lower }}_method_names[];

// Responses
// Public API
\ for method in service.methods
\if method.stream_response
bool {{ file.c_package | lower }}_{{ service.c_name | lower }}_{{ method.c_name | lower }}_respond_one({{ connection_type }}, arpc_command_t command_id, const {{ method.response.c_name }}* data);
bool {{ file.c_package | lower }}_{{ service.c_name | lower }}_{{ method.c_name | lower }}_respond_finish({{ connection_type }}, arpc_command_t command_id, arpc_response_code_t response_code, const {{ method.response.c_name }}* data);
\else
bool {{ file.c_package | lower }}_{{ service.c_name | lower }}_{{ method.c_name | lower }}_respond({{ connection_type }}, arpc_command_t command_id, arpc_response_code_t response_code, const {{ method.response.c_name }}* data);
\endif
\ endfor
//

// Requests
// Must be implemented by user
\ for method in service.methods
void {{ file.c_package | lower }}_{{ service.c_name | lower }}_{{ method.c_name | lower }}_request({{ connection_type }}, arpc_command_t command_id, const {{ method.request.c_name }}* data);
\ endfor
//

// Internal use
\ for method in service.methods
void {{ file.c_package | lower }}_{{ service.c_name | lower }}_{{ method.c_name | lower }}_impl_request({{ connection_type }}, arpc_command_t command_id, const ac_buffer_t* payload);
\ endfor
//

\ endfor

#endif