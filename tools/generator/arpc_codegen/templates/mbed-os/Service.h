//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef {{prefix | upper}}_ARPC_SERVICE_{{ file.c_name | upper }}_
#define {{prefix | upper}}_ARPC_SERVICE_{{ file.c_name | upper }}_

#include "arpc-device/ArpcService.h"

#include "{{ file.c_name }}.pb.h"

namespace arpc {
    namespace mbed {
        namespace generated {
        \ for service in file.services

            class Arpc{{ service.cpp_name }}Service : public arpc::mbed::ArpcService {
            public:
                Arpc{{ service.cpp_name }}Service();
                virtual ~Arpc{{ service.cpp_name }}Service();

            protected:
                \ for method in service.methods
                virtual void {{ method.c_name | lower }}(const ::mbed::SharedPtr<arpc::mbed::ArpcMethodCall<{{ method.request.c_name }}, {{ method.response.c_name }}> >& call) = 0;
                \ endfor

            private:
                virtual void get_method_descriptors(const arpc::mbed::ArpcMethodDescriptor** methods, size_t* count) const;
                virtual const char* get_name() const;

                static const char* _s_name;
                static size_t _s_methods_count;
                static arpc::mbed::ArpcMethodDescriptor _s_methods_descriptors[];
            };

            \endfor
        }
    }
}

#endif /* {{prefix | upper}}_ARPC_SERVICE_{{ file.c_name | upper }}_ */