//
// Mbed - IoT Companion
//
// Copyright 2021 Arm Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "{{ file.c_name }}.pb.h"
#include "Arpc{{ file.cpp_name }}Service.h"

#include "pb/pb.h"

namespace arpc {
namespace mbed {
namespace generated {

\ for service in file.services

Arpc{{ service.cpp_name }}Service::Arpc{{ service.cpp_name }}Service() : arpc::mbed::ArpcService()
{

}

Arpc{{ service.cpp_name }}Service::~Arpc{{ service.cpp_name }}Service()
{

}


size_t Arpc{{ service.cpp_name }}Service::_s_methods_count = {{ service.methods | count }};

arpc::mbed::ArpcMethodDescriptor Arpc{{ service.cpp_name }}Service::_s_methods_descriptors[] = {
\ for method in service.methods
    {"{{ method.name }}", {{ method.request.c_name }}_fields, {{ method.response.c_name }}_fields,
        arpc::mbed::ArpcMethodCall<{{ method.request.c_name }}, {{ method.response.c_name }}>::make_shared,
        (const arpc::mbed::ArpcMethodRequestCallback_t) &Arpc{{ service.cpp_name }}Service::{{method.c_name | lower}}
        },
\ endfor
};

const char* Arpc{{ service.cpp_name }}Service::_s_name = "{{ file.package }}.{{ service.name }}";

void Arpc{{ service.cpp_name }}Service::get_method_descriptors(const arpc::mbed::ArpcMethodDescriptor** methods, size_t* count) const {
    *methods = _s_methods_descriptors;
    *count = _s_methods_count;
}

const char* Arpc{{ service.cpp_name }}Service::get_name() const {
    return _s_name;
}


\endfor

}

namespace details {
\ for message in file.messages
template<> const {{ message.c_name }}& Prototype<{{ message.c_name }}>::value() {
    static {{ message.c_name }} prototype = {{ message.c_name }}_init_default;
    return prototype;
}
\ endfor
}

}
}