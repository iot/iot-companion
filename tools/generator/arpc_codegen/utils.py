
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import string

def to_c_name(str):
    out = ""
    first = True
    for c in str:
        if c in string.ascii_letters + string.digits:
            out += c
            first = False
        elif first:
           pass
        else:
            out += '_'
            first = False
    return out

def to_cpp_name(str):
    out = ""
    cap = True
    for c in str:
        if c in string.ascii_letters:
            if cap:
                out += c.upper()
            else:
                out += c.lower()
            cap = False
        elif c in string.digits:
            out += c
            cap = True
        else:
            cap = True
    return out