
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from .file import File
from .service import Service
from .method import Method
from .message import Message


def parse_file(file_desc):
    file_name = os.path.splitext(file_desc.name)[0] # nanopb does the same
    file = File(name=file_name, package=file_desc.package)
    for service_desc in file_desc.service:
        service_id = 0
        service = Service(name = service_desc.name, id=service_id)
        file.add_service(service)
        for method_desc in service_desc.method:
            input = Message(name=method_desc.input_type)
            output = Message(name=method_desc.output_type)
            method = Method(name=method_desc.name, request=input, response=output,
                            stream_response=method_desc.client_streaming)
            service.add_method(method)

    for message_type_desc in file_desc.message_type:
        message = Message(name=file_desc.package + '_' + message_type_desc.name)
        file.add_message(message)

    return file
