
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .utils import to_c_name, to_cpp_name
from .error import IDLError

class Service(object):
    def __init__(self, name, id):
        self._name = name
        self._id = id
        self._methods = []
        self._file = None

    def add_method(self, method):
        method.service = self
        self._methods.append(method)

    @property
    def methods(self):
        return self._methods

    @property
    def file(self):
        return self._file

    @file.setter
    def file(self, file):
        self._file = file

    @property
    def name(self):
        return self._name

    @property
    def id(self):
        return self._id

    @property
    def c_name(self):
        return to_c_name(self._name)

    @property
    def cpp_name(self):
        return to_cpp_name(self._name)

    @property
    def dict(self):
        return {'name': self.name, 'c_name': self.c_name,
                'cpp_name': self.cpp_name,
                'id': self.id,
                'methods': self.methods}