
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import os
from google.protobuf.compiler import plugin_pb2 as plugin
from .parser import parse_file
from .generator import generate, generate_server, set_parameters
# from service_package_pb2 import ArpcServicePackage

def main():
    if sys.platform == "win32":
        import msvcrt

        # Set stdin and stdout to binary mode
        msvcrt.setmode(sys.stdin.fileno(), os.O_BINARY)
        msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)

    # Get parameters from environment variables
    prefix = os.getenv("ARPC_PREFIX", "arpc")
    connection_type = os.getenv("ARPC_CONNECTION_TYPE", "const arpc_connection_t* connection")
    connection_param = os.getenv("ARPC_CONNECTION_PARAM", "connection")
    generator = os.getenv("ARPC_GENERATOR", "mbed-os")
    set_parameters(prefix, connection_type, connection_param, generator)

    # Read request message from stdin
    data = sys.stdin.buffer.read()

    # Parse request
    request = plugin.CodeGeneratorRequest()
    request.ParseFromString(data)

    # Create response
    response = plugin.CodeGeneratorResponse()

    # Process request & generate code

    # Create a map of files
    files_descs = {}
    for file_desc in request.proto_file:
        files_descs[file_desc.name] = file_desc

    # Process these
    parsed_files = []
    for filename in request.file_to_generate:
        parsed_files.append(parse_file(files_descs[filename]))

    generated_code = []
    for file in parsed_files:
        generated_code += generate(file)

    generated_code += generate_server(parsed_files)

    # Serialise response message
    for code in generated_code:
        name, content = code
        f = response.file.add()
        f.name = name
        f.content = content

    output = response.SerializeToString()

    # Write to stdout
    sys.stdout.buffer.write(output)
    sys.exit(0)


if __name__ == "__main__":
    main()