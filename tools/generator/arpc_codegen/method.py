
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re

from .utils import to_c_name


class Method(object):
    def __init__(self, name, request, response, stream_response):
        self._name = name
        self._request = request
        self._response = response
        self._stream_response = stream_response
        self._service = None

    @property
    def service(self):
        return self._service

    @service.setter
    def service(self, service):
        self._service = service

    @property
    def name(self):
        return self._name

    @property
    def stream_response(self):
        return self._stream_response

    @property
    def request(self):
        return self._request

    @property
    def response(self):
        return self._response

    @property
    def full_name(self):
        return self._service.name + " " + self._name

    @property
    def c_name(self):
        camel_name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', self._name)
        camel_name = re.sub('([a-z0-9])([A-Z])', r'\1_\2', camel_name)
        return to_c_name(camel_name)

    @property
    def c_full_name(self):
        return to_c_name(self.full_name)

    @property
    def dict(self):
        return {'name': self.name, 'c_name': self.c_name, 'stream_response': self.stream_response,
                'request': self.request.dict, 'response': self.response.dict}