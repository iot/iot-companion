
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .utils import to_c_name, to_cpp_name
from .error import IDLError

from collections import OrderedDict

class File(object):
    def __init__(self, name, package):
        self._name = name
        self._package = package
        self._services = []
        self._messages = []

    def add_service(self, service):
        service.file = self
        self._services.append(service)

    def add_message(self, message):
        message.file = self
        self._messages.append(message)

    @property
    def services(self):
        return self._services

    @property
    def messages(self):
        return self._messages

    @property
    def package(self):
        return self._package

    @property
    def c_package(self):
        return to_c_name(self._package)

    @property
    def name(self):
        return to_c_name(self._name)

    @property
    def c_name(self):
        return to_c_name(self._name)

    @property
    def cpp_name(self):
        return to_cpp_name(self._name)

    @property
    def dict(self):
        return {'name': self.name, 'c_name': self.c_name, 'cpp_name': self.cpp_name, 'package': self.package,
                'c_package': self.c_package, 'services': self.services,
                'messages': [message.dict for message in self.messages]}