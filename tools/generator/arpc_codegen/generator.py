
# Mbed - IoT Companion
#
# Copyright 2021 Arm Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from jinja2 import Environment, PackageLoader

ENV = Environment(loader=PackageLoader('arpc_codegen', 'templates'))
ENV.line_statement_prefix = '\\'
ENV.keep_trailing_newline = True
ENV.trim_blocks = False
ENV.lstrip_blocks = False

PREFIX = None
CONNECTION_TYPE = None
CONNECTION_PARAM = None
GENERATOR = None


def set_parameters(prefix, connection_type, connection_param, generator):
    global PREFIX, CONNECTION_TYPE, CONNECTION_PARAM, GENERATOR
    PREFIX = prefix
    CONNECTION_TYPE = connection_type
    CONNECTION_PARAM = connection_param
    GENERATOR = generator


def generate_file(file, template_name):
    template = ENV.get_template(template_name)

    # UTF-8 files mess up C99
    return template.render(prefix=PREFIX, connection_type=CONNECTION_TYPE,
                           connection_param=CONNECTION_PARAM,
                           file=file.dict, encoding='ascii')


def generate_server_file(files, template_name):
    # compute total services/methods count
    total_methods_count = 0
    total_services_count = 0
    for file in files:
        for service in file.services:
            total_methods_count += len(service.methods)
            total_services_count += 1
    template = ENV.get_template(template_name)

    # UTF-8 files mess up C99
    return template.render(prefix=PREFIX, connection_type=CONNECTION_TYPE,
                           connection_param=CONNECTION_PARAM,
                           files=[file.dict for file in files],
                           total_services_count=total_services_count,
                           total_methods_count=total_methods_count, encoding='ascii')


def generate(file):
    if GENERATOR == "mbed-os":
        return [("Arpc" + file.cpp_name + "Service.h", generate_file(file, "mbed-os/Service.h")),
                ("Arpc" + file.cpp_name + "Service.cpp", generate_file(file, "mbed-os/Service.cpp"))]
    else:
        return [(file.c_name + ".arpc.h", generate_file(file, "generic-c/service.h")),
                (file.c_name + ".arpc.c", generate_file(file, "generic-c/service.c"))]


def generate_server(files):
    if GENERATOR == "mbed-os":
        return []
    else:
        return [("server.h", generate_server_file(files, "generic-c/server.h")),
            ("server.c", generate_server_file(files, "generic-c/server.c"))]
